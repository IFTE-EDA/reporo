// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:29 2019

module source_pla  ( 
    d, c, b, a, e,
    xor5  );
  input  d, c, b, a, e;
  output xor5;
  wire n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20,
    n21, n22, n23, n24, n25, n26, n27, n28, n29;
  inv1  g00(.a(a), .O(n7));
  inv1  g01(.a(e), .O(n8));
  nor2  g02(.a(n8), .b(n7), .O(n9));
  nor2  g03(.a(e), .b(a), .O(n10));
  nor2  g04(.a(n10), .b(n9), .O(n11));
  inv1  g05(.a(n11), .O(n12));
  nand2 g06(.a(b), .b(c), .O(n13));
  inv1  g07(.a(c), .O(n14));
  inv1  g08(.a(b), .O(n15));
  nand2 g09(.a(n15), .b(n14), .O(n16));
  nand2 g10(.a(n16), .b(n13), .O(n17));
  nand2 g11(.a(n17), .b(d), .O(n18));
  inv1  g12(.a(d), .O(n19));
  nor2  g13(.a(n15), .b(n14), .O(n20));
  nor2  g14(.a(b), .b(c), .O(n21));
  nor2  g15(.a(n21), .b(n20), .O(n22));
  nand2 g16(.a(n22), .b(n19), .O(n23));
  nand2 g17(.a(n23), .b(n18), .O(n24));
  nand2 g18(.a(n24), .b(n12), .O(n25));
  nor2  g19(.a(n22), .b(n19), .O(n26));
  nor2  g20(.a(n17), .b(d), .O(n27));
  nor2  g21(.a(n27), .b(n26), .O(n28));
  nand2 g22(.a(n28), .b(n11), .O(n29));
  nand2 g23(.a(n29), .b(n25), .O(xor5));
endmodule


