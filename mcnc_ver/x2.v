// Benchmark "x2" written by ABC on Tue Nov  5 15:01:29 2019

module x2 ( 
    a, b, c, d, e, f, g, h, i, j,
    k, l, m, n, o, p, q  );
  input  a, b, c, d, e, f, g, h, i, j;
  output k, l, m, n, o, p, q;
  wire n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n31, n32,
    n33, n35, n36, n38, n39, n40, n41, n42, n43, n44, n45, n46, n48, n49,
    n50, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64,
    n65, n66, n67, n68, n70, n71, n72, n73, n74, n75, n76, n77, n78, n79;
  inv1  g00(.a(h), .O(n18));
  nor2  g01(.a(i), .b(n18), .O(n19));
  nand2 g02(.a(n19), .b(j), .O(n20));
  nand2 g03(.a(j), .b(i), .O(n21));
  nand2 g04(.a(j), .b(n18), .O(n22));
  nand2 g05(.a(n22), .b(n21), .O(n23));
  nand2 g06(.a(i), .b(n18), .O(n24));
  inv1  g07(.a(i), .O(n25));
  inv1  g08(.a(j), .O(n26));
  nand2 g09(.a(n26), .b(n25), .O(n27));
  nand2 g10(.a(n27), .b(n24), .O(n28));
  nor2  g11(.a(n28), .b(n23), .O(n29));
  nand2 g12(.a(n29), .b(n20), .O(k));
  nand2 g13(.a(n26), .b(h), .O(n31));
  nand2 g14(.a(n31), .b(n24), .O(n32));
  nor2  g15(.a(n32), .b(n23), .O(n33));
  inv1  g16(.a(n33), .O(l));
  nor2  g17(.a(i), .b(h), .O(n35));
  nand2 g18(.a(n35), .b(n26), .O(n36));
  inv1  g19(.a(n36), .O(m));
  inv1  g20(.a(a), .O(n38));
  inv1  g21(.a(b), .O(n39));
  nand2 g22(.a(n39), .b(n38), .O(n40));
  nand2 g23(.a(n31), .b(n22), .O(n41));
  nor2  g24(.a(n41), .b(n40), .O(n42));
  inv1  g25(.a(n21), .O(n43));
  nor2  g26(.a(n43), .b(c), .O(n44));
  nand2 g27(.a(n44), .b(n20), .O(n45));
  nor2  g28(.a(n45), .b(m), .O(n46));
  nand2 g29(.a(n46), .b(n42), .O(n));
  nand2 g30(.a(n21), .b(g), .O(n48));
  nand2 g31(.a(n24), .b(n22), .O(n49));
  nor2  g32(.a(n49), .b(n48), .O(n50));
  nand2 g33(.a(n50), .b(n36), .O(o));
  inv1  g34(.a(c), .O(n52));
  nor2  g35(.a(n40), .b(n52), .O(n53));
  nand2 g36(.a(n53), .b(n43), .O(n54));
  inv1  g37(.a(d), .O(n55));
  nor2  g38(.a(e), .b(n55), .O(n56));
  nand2 g39(.a(n56), .b(n26), .O(n57));
  nand2 g40(.a(n57), .b(n54), .O(n58));
  nand2 g41(.a(n58), .b(h), .O(n59));
  nand2 g42(.a(n18), .b(c), .O(n60));
  nor2  g43(.a(n60), .b(n40), .O(n61));
  nor2  g44(.a(n61), .b(n26), .O(n62));
  nor2  g45(.a(n62), .b(i), .O(n63));
  inv1  g46(.a(f), .O(n64));
  nor2  g47(.a(h), .b(n64), .O(n65));
  nand2 g48(.a(n65), .b(n43), .O(n66));
  nand2 g49(.a(n66), .b(g), .O(n67));
  nor2  g50(.a(n67), .b(n63), .O(n68));
  nand2 g51(.a(n68), .b(n59), .O(p));
  nor2  g52(.a(b), .b(a), .O(n70));
  nor2  g53(.a(n26), .b(c), .O(n71));
  nand2 g54(.a(n71), .b(n70), .O(n72));
  inv1  g55(.a(e), .O(n73));
  nor2  g56(.a(n73), .b(n55), .O(n74));
  nor2  g57(.a(j), .b(n25), .O(n75));
  nand2 g58(.a(n75), .b(n74), .O(n76));
  nand2 g59(.a(n76), .b(n72), .O(n77));
  nand2 g60(.a(n77), .b(h), .O(n78));
  nor2  g61(.a(n67), .b(n33), .O(n79));
  nand2 g62(.a(n79), .b(n78), .O(q));
endmodule


