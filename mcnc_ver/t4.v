// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:28 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11,
    v12.0 , v12.1 , v12.2 , v12.3 , v12.4 , v12.5 , v12.6 , v12.7   );
  input  v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11;
  output v12.0 , v12.1 , v12.2 , v12.3 , v12.4 , v12.5 , v12.6 ,
    v12.7 ;
  wire n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34,
    n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48,
    n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62,
    n63, n64, n65, n66, n67, n68, n70, n71, n72, n73, n74, n75, n76, n77,
    n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n91, n92,
    n93, n94, n95, n96, n97, n98, n99, n100, n101, n103, n104, n105, n106,
    n107, n108, n109, n110, n111, n112, n114, n115, n116, n117, n118, n119,
    n120, n121, n122, n123, n124, n125, n126, n127, n128, n129, n130, n131,
    n132, n133, n134, n135, n136, n137, n138, n139, n140, n141, n143, n144,
    n145, n146, n147, n148, n149, n150, n151, n152, n153, n154, n156, n157,
    n158, n159, n160, n161, n162, n163, n164, n165, n166, n167, n168, n169,
    n170, n171, n172, n173, n174, n175, n177, n178, n179, n180, n181;
  inv1  g000(.a(v11), .O(n21));
  inv1  g001(.a(v6), .O(n22));
  inv1  g002(.a(v4), .O(n23));
  nand2 g003(.a(v8), .b(v7), .O(n24));
  inv1  g004(.a(v8), .O(n25));
  inv1  g005(.a(v1), .O(n26));
  nor2  g006(.a(v7), .b(n26), .O(n27));
  nand2 g007(.a(n27), .b(n25), .O(n28));
  nand2 g008(.a(n28), .b(n24), .O(n29));
  nand2 g009(.a(n29), .b(n23), .O(n30));
  nand2 g010(.a(v7), .b(v4), .O(n31));
  nor2  g011(.a(n31), .b(v8), .O(n32));
  nor2  g012(.a(v8), .b(n26), .O(n33));
  inv1  g013(.a(v7), .O(n34));
  nand2 g014(.a(n34), .b(n23), .O(n35));
  nor2  g015(.a(n35), .b(n33), .O(n36));
  nor2  g016(.a(n36), .b(n32), .O(n37));
  nand2 g017(.a(n37), .b(n30), .O(n38));
  nand2 g018(.a(n38), .b(v10), .O(n39));
  nor2  g019(.a(n39), .b(n22), .O(n40));
  nand2 g020(.a(n22), .b(v4), .O(n41));
  nor2  g021(.a(n41), .b(v10), .O(n42));
  nor2  g022(.a(n42), .b(n40), .O(n43));
  nor2  g023(.a(n43), .b(v9), .O(n44));
  inv1  g024(.a(v9), .O(n45));
  nor2  g025(.a(n41), .b(n45), .O(n46));
  nor2  g026(.a(n46), .b(n44), .O(n47));
  nor2  g027(.a(n47), .b(n21), .O(n48));
  inv1  g028(.a(v10), .O(n49));
  nor2  g029(.a(n49), .b(v9), .O(n50));
  inv1  g030(.a(n41), .O(n51));
  nor2  g031(.a(v11), .b(v7), .O(n52));
  nand2 g032(.a(n52), .b(n51), .O(n53));
  nor2  g033(.a(n53), .b(n50), .O(n54));
  nor2  g034(.a(n54), .b(n48), .O(n55));
  nor2  g035(.a(n55), .b(v5), .O(n56));
  nor2  g036(.a(v10), .b(v9), .O(n57));
  inv1  g037(.a(n57), .O(n58));
  nor2  g038(.a(n58), .b(n25), .O(n59));
  nand2 g039(.a(n45), .b(v8), .O(n60));
  nor2  g040(.a(n60), .b(n59), .O(n61));
  inv1  g041(.a(v5), .O(n62));
  nor2  g042(.a(n34), .b(n62), .O(n63));
  inv1  g043(.a(n63), .O(n64));
  nor2  g044(.a(n64), .b(v11), .O(n65));
  nand2 g045(.a(n65), .b(n51), .O(n66));
  nor2  g046(.a(n66), .b(n61), .O(n67));
  nor2  g047(.a(n67), .b(n56), .O(n68));
  nor2  g048(.a(n68), .b(v0), .O(v12.0 ));
  nor2  g049(.a(n25), .b(v4), .O(n70));
  nor2  g050(.a(v8), .b(n23), .O(n71));
  nor2  g051(.a(n71), .b(n70), .O(n72));
  nor2  g052(.a(n22), .b(v2), .O(n73));
  nand2 g053(.a(n73), .b(v10), .O(n74));
  nor2  g054(.a(n74), .b(n72), .O(n75));
  nor2  g055(.a(n75), .b(n42), .O(n76));
  nor2  g056(.a(n76), .b(v9), .O(n77));
  nor2  g057(.a(n77), .b(n46), .O(n78));
  nor2  g058(.a(n78), .b(n34), .O(n79));
  nor2  g059(.a(v9), .b(n22), .O(n80));
  nand2 g060(.a(n80), .b(v10), .O(n81));
  nor2  g061(.a(n25), .b(v7), .O(n82));
  nand2 g062(.a(n23), .b(v3), .O(n83));
  inv1  g063(.a(n83), .O(n84));
  nand2 g064(.a(n84), .b(n82), .O(n85));
  nor2  g065(.a(n85), .b(n81), .O(n86));
  nor2  g066(.a(n86), .b(n79), .O(n87));
  nor2  g067(.a(v5), .b(v0), .O(n88));
  nand2 g068(.a(n88), .b(v11), .O(n89));
  nor2  g069(.a(n89), .b(n87), .O(v12.1 ));
  nor2  g070(.a(v7), .b(v5), .O(n91));
  nor2  g071(.a(n91), .b(n63), .O(n92));
  nand2 g072(.a(v10), .b(v4), .O(n93));
  nor2  g073(.a(n93), .b(n92), .O(n94));
  nor2  g074(.a(v8), .b(v5), .O(n95));
  nand2 g075(.a(n95), .b(n49), .O(n96));
  nor2  g076(.a(n96), .b(n35), .O(n97));
  nor2  g077(.a(n97), .b(n94), .O(n98));
  nor2  g078(.a(v6), .b(v0), .O(n99));
  nor2  g079(.a(v11), .b(v9), .O(n100));
  nand2 g080(.a(n100), .b(n99), .O(n101));
  nor2  g081(.a(n101), .b(n98), .O(v12.2 ));
  nand2 g082(.a(v11), .b(n62), .O(n103));
  nor2  g083(.a(n103), .b(n78), .O(n104));
  nand2 g084(.a(n21), .b(v5), .O(n105));
  nor2  g085(.a(n105), .b(n41), .O(n106));
  nor2  g086(.a(n106), .b(n104), .O(n107));
  nor2  g087(.a(n107), .b(n34), .O(n108));
  nor2  g088(.a(n103), .b(n83), .O(n109));
  nand2 g089(.a(n109), .b(n82), .O(n110));
  nor2  g090(.a(n110), .b(n81), .O(n111));
  nor2  g091(.a(n111), .b(n108), .O(n112));
  nor2  g092(.a(n112), .b(v0), .O(v12.3 ));
  nand2 g093(.a(v10), .b(v6), .O(n114));
  nor2  g094(.a(n114), .b(n35), .O(n115));
  nor2  g095(.a(n115), .b(n42), .O(n116));
  nor2  g096(.a(n116), .b(n25), .O(n117));
  inv1  g097(.a(v2), .O(n118));
  nand2 g098(.a(v4), .b(n118), .O(n119));
  nor2  g099(.a(n119), .b(n34), .O(n120));
  inv1  g100(.a(n27), .O(n121));
  nand2 g101(.a(n23), .b(v2), .O(n122));
  nor2  g102(.a(n122), .b(n121), .O(n123));
  nor2  g103(.a(n123), .b(n120), .O(n124));
  inv1  g104(.a(n114), .O(n125));
  nand2 g105(.a(n125), .b(n25), .O(n126));
  nor2  g106(.a(n126), .b(n124), .O(n127));
  nor2  g107(.a(n127), .b(n117), .O(n128));
  nor2  g108(.a(n128), .b(v9), .O(n129));
  nand2 g109(.a(v9), .b(v8), .O(n130));
  nor2  g110(.a(n130), .b(n41), .O(n131));
  nor2  g111(.a(n131), .b(n129), .O(n132));
  nor2  g112(.a(n132), .b(n21), .O(n133));
  nand2 g113(.a(n21), .b(v8), .O(n134));
  nand2 g114(.a(n51), .b(n34), .O(n135));
  nor2  g115(.a(n135), .b(n134), .O(n136));
  nor2  g116(.a(n136), .b(n133), .O(n137));
  nor2  g117(.a(n137), .b(v5), .O(n138));
  nand2 g118(.a(n63), .b(n51), .O(n139));
  nor2  g119(.a(n139), .b(n134), .O(n140));
  nor2  g120(.a(n140), .b(n138), .O(n141));
  nor2  g121(.a(n141), .b(v0), .O(v12.4 ));
  inv1  g122(.a(n80), .O(n143));
  nor2  g123(.a(n143), .b(n39), .O(n144));
  nand2 g124(.a(n49), .b(v9), .O(n145));
  nor2  g125(.a(n145), .b(n41), .O(n146));
  nor2  g126(.a(n146), .b(n144), .O(n147));
  nor2  g127(.a(n147), .b(n21), .O(n148));
  nand2 g128(.a(n21), .b(v9), .O(n149));
  nor2  g129(.a(n149), .b(n135), .O(n150));
  nor2  g130(.a(n150), .b(n148), .O(n151));
  nor2  g131(.a(n151), .b(v5), .O(n152));
  nor2  g132(.a(n149), .b(n139), .O(n153));
  nor2  g133(.a(n153), .b(n152), .O(n154));
  nor2  g134(.a(n154), .b(v0), .O(v12.5 ));
  inv1  g135(.a(n99), .O(n156));
  nor2  g136(.a(n21), .b(v10), .O(n157));
  inv1  g137(.a(n157), .O(n158));
  nor2  g138(.a(n158), .b(v5), .O(n159));
  nor2  g139(.a(v11), .b(n49), .O(n160));
  inv1  g140(.a(n160), .O(n161));
  nor2  g141(.a(n161), .b(n62), .O(n162));
  nor2  g142(.a(n162), .b(n159), .O(n163));
  nor2  g143(.a(n163), .b(n34), .O(n164));
  inv1  g144(.a(n91), .O(n165));
  nor2  g145(.a(n160), .b(n157), .O(n166));
  nor2  g146(.a(n166), .b(n165), .O(n167));
  nor2  g147(.a(n167), .b(n164), .O(n168));
  nor2  g148(.a(n168), .b(n23), .O(n169));
  nand2 g149(.a(n91), .b(n23), .O(n170));
  nor2  g150(.a(v9), .b(v8), .O(n171));
  nor2  g151(.a(v11), .b(v10), .O(n172));
  nand2 g152(.a(n172), .b(n171), .O(n173));
  nor2  g153(.a(n173), .b(n170), .O(n174));
  nor2  g154(.a(n174), .b(n169), .O(n175));
  nor2  g155(.a(n175), .b(n156), .O(v12.6 ));
  nor2  g156(.a(n92), .b(n23), .O(n177));
  nand2 g157(.a(n57), .b(n25), .O(n178));
  nor2  g158(.a(n178), .b(n170), .O(n179));
  nor2  g159(.a(n179), .b(n177), .O(n180));
  nand2 g160(.a(n99), .b(n21), .O(n181));
  nor2  g161(.a(n181), .b(n180), .O(v12.7 ));
endmodule


