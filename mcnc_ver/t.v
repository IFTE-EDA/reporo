// Benchmark "C17.iscas" written by ABC on Tue Nov  5 15:01:28 2019

module C17.iscas  ( 
    1GAT(0) , 2GAT(1) , 3GAT(2) , 6GAT(3) , 7GAT(4) ,
    22GAT(10) , 23GAT(9)   );
  input  1GAT(0) , 2GAT(1) , 3GAT(2) , 6GAT(3) , 7GAT(4) ;
  output 22GAT(10) , 23GAT(9) ;
  wire n8, n9, n10, n12, n13, n14, n15;
  nand2 g0(.a(6GAT(3) ), .b(3GAT(2) ), .O(n8));
  nand2 g1(.a(n8), .b(2GAT(1) ), .O(n9));
  nand2 g2(.a(3GAT(2) ), .b(1GAT(0) ), .O(n10));
  nand2 g3(.a(n10), .b(n9), .O(22GAT(10) ));
  inv1  g4(.a(3GAT(2) ), .O(n12));
  inv1  g5(.a(6GAT(3) ), .O(n13));
  nor2  g6(.a(n13), .b(n12), .O(n14));
  nor2  g7(.a(7GAT(4) ), .b(2GAT(1) ), .O(n15));
  nor2  g8(.a(n15), .b(n14), .O(23GAT(9) ));
endmodule


