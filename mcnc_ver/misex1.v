// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:24 2019

module source_pla  ( 
    dmpst3, dmpst2, dmpst1, dmpst0, xskip, yskip, page, rmwB,
    dmnst3B, dmnst2B, dmnst1B, dmnst0B, adctlp2B, adctlp1B, adctlp0B  );
  input  dmpst3, dmpst2, dmpst1, dmpst0, xskip, yskip, page, rmwB;
  output dmnst3B, dmnst2B, dmnst1B, dmnst0B, adctlp2B, adctlp1B, adctlp0B;
  wire n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n28, n29, n30,
    n31, n32, n33, n34, n35, n36, n37, n38, n40, n41, n42, n43, n44, n45,
    n46, n47, n48, n49, n50, n51, n53, n54, n55, n56, n57, n58, n59, n60,
    n61, n62, n64, n65, n66, n67, n68, n69, n70, n71, n72, n74, n75, n76,
    n77, n78, n79, n80, n81, n83, n84, n85, n86, n87, n88;
  inv1  g00(.a(dmpst1), .O(n16));
  inv1  g01(.a(dmpst0), .O(n17));
  inv1  g02(.a(dmpst2), .O(n18));
  nor2  g03(.a(n18), .b(dmpst3), .O(n19));
  inv1  g04(.a(n19), .O(n20));
  nor2  g05(.a(n20), .b(n17), .O(n21));
  inv1  g06(.a(dmpst3), .O(n22));
  nor2  g07(.a(dmpst2), .b(n22), .O(n23));
  inv1  g08(.a(n23), .O(n24));
  nor2  g09(.a(n24), .b(dmpst0), .O(n25));
  nor2  g10(.a(n25), .b(n21), .O(n26));
  nor2  g11(.a(n26), .b(n16), .O(dmnst3B));
  nor2  g12(.a(yskip), .b(dmpst0), .O(n28));
  inv1  g13(.a(n28), .O(n29));
  nor2  g14(.a(n16), .b(dmpst2), .O(n30));
  nand2 g15(.a(n30), .b(n29), .O(n31));
  nand2 g16(.a(page), .b(n17), .O(n32));
  nand2 g17(.a(n32), .b(n18), .O(n33));
  nand2 g18(.a(n33), .b(n16), .O(n34));
  nand2 g19(.a(n34), .b(n31), .O(n35));
  nand2 g20(.a(n35), .b(n22), .O(n36));
  nor2  g21(.a(n17), .b(dmpst1), .O(n37));
  nand2 g22(.a(n37), .b(n23), .O(n38));
  nand2 g23(.a(n38), .b(n36), .O(dmnst2B));
  nand2 g24(.a(xskip), .b(n16), .O(n40));
  nand2 g25(.a(n40), .b(n17), .O(n41));
  nand2 g26(.a(n41), .b(dmpst2), .O(n42));
  inv1  g27(.a(yskip), .O(n43));
  nand2 g28(.a(n43), .b(dmpst1), .O(n44));
  inv1  g29(.a(page), .O(n45));
  nand2 g30(.a(n45), .b(n16), .O(n46));
  nand2 g31(.a(n46), .b(n44), .O(n47));
  nor2  g32(.a(dmpst0), .b(dmpst2), .O(n48));
  nand2 g33(.a(n48), .b(n47), .O(n49));
  nand2 g34(.a(n49), .b(n42), .O(n50));
  nand2 g35(.a(n50), .b(n22), .O(n51));
  nand2 g36(.a(n51), .b(n38), .O(dmnst1B));
  inv1  g37(.a(xskip), .O(n53));
  nand2 g38(.a(n53), .b(n17), .O(n54));
  nand2 g39(.a(n54), .b(rmwB), .O(n55));
  nor2  g40(.a(n20), .b(dmpst1), .O(n56));
  nand2 g41(.a(n56), .b(n55), .O(n57));
  nor2  g42(.a(n43), .b(dmpst3), .O(n58));
  inv1  g43(.a(n58), .O(n59));
  inv1  g44(.a(n30), .O(n60));
  nor2  g45(.a(n60), .b(dmpst0), .O(n61));
  nand2 g46(.a(n61), .b(n59), .O(n62));
  nand2 g47(.a(n62), .b(n57), .O(dmnst0B));
  nand2 g48(.a(n37), .b(dmpst3), .O(n64));
  nand2 g49(.a(n17), .b(dmpst1), .O(n65));
  nand2 g50(.a(n65), .b(n64), .O(n66));
  nand2 g51(.a(n66), .b(n18), .O(n67));
  nand2 g52(.a(dmpst0), .b(n18), .O(n68));
  nand2 g53(.a(n68), .b(n16), .O(n69));
  nand2 g54(.a(dmpst0), .b(dmpst1), .O(n70));
  nand2 g55(.a(n70), .b(n69), .O(n71));
  nand2 g56(.a(n71), .b(n22), .O(n72));
  nand2 g57(.a(n72), .b(n67), .O(adctlp2B));
  nand2 g58(.a(dmpst0), .b(n16), .O(n74));
  nand2 g59(.a(n65), .b(n74), .O(n75));
  nand2 g60(.a(n75), .b(dmpst3), .O(n76));
  nand2 g61(.a(n58), .b(dmpst1), .O(n77));
  nand2 g62(.a(n77), .b(n76), .O(n78));
  nand2 g63(.a(n78), .b(n18), .O(n79));
  nand2 g64(.a(n70), .b(n34), .O(n80));
  nand2 g65(.a(n80), .b(n22), .O(n81));
  nand2 g66(.a(n81), .b(n79), .O(adctlp1B));
  nand2 g67(.a(n28), .b(dmpst1), .O(n83));
  nand2 g68(.a(n83), .b(n76), .O(n84));
  nand2 g69(.a(n84), .b(n18), .O(n85));
  nand2 g70(.a(n53), .b(n16), .O(n86));
  nand2 g71(.a(n86), .b(n17), .O(n87));
  nand2 g72(.a(n87), .b(n19), .O(n88));
  nand2 g73(.a(n88), .b(n85), .O(adctlp0B));
endmodule


