// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:26 2019

module source_pla  ( 
    i_0_, i_1_, i_2_, i_3_, i_4_,
    o_0_, o_1_, o_2_  );
  input  i_0_, i_1_, i_2_, i_3_, i_4_;
  output o_0_, o_1_, o_2_;
  wire n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22,
    n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37,
    n38, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52,
    n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n65, n66,
    n67, n68, n69;
  inv1  g00(.a(i_3_), .O(n9));
  inv1  g01(.a(i_4_), .O(n10));
  nand2 g02(.a(n10), .b(n9), .O(n11));
  nand2 g03(.a(n11), .b(i_2_), .O(n12));
  nand2 g04(.a(i_4_), .b(i_3_), .O(n13));
  nand2 g05(.a(n13), .b(n12), .O(n14));
  nand2 g06(.a(n14), .b(i_1_), .O(n15));
  inv1  g07(.a(n13), .O(n16));
  nand2 g08(.a(n16), .b(i_2_), .O(n17));
  nand2 g09(.a(n17), .b(n15), .O(n18));
  nand2 g10(.a(n18), .b(i_0_), .O(n19));
  nand2 g11(.a(i_2_), .b(i_1_), .O(n20));
  inv1  g12(.a(n20), .O(n21));
  nand2 g13(.a(n21), .b(n16), .O(n22));
  nand2 g14(.a(n22), .b(n19), .O(o_0_));
  nand2 g15(.a(n13), .b(n11), .O(n24));
  nor2  g16(.a(i_2_), .b(i_1_), .O(n25));
  inv1  g17(.a(n25), .O(n26));
  nand2 g18(.a(n26), .b(n20), .O(n27));
  nand2 g19(.a(n27), .b(i_0_), .O(n28));
  inv1  g20(.a(i_0_), .O(n29));
  nor2  g21(.a(n25), .b(n21), .O(n30));
  nand2 g22(.a(n30), .b(n29), .O(n31));
  nand2 g23(.a(n31), .b(n28), .O(n32));
  nand2 g24(.a(n32), .b(n24), .O(n33));
  inv1  g25(.a(n24), .O(n34));
  nor2  g26(.a(n30), .b(n29), .O(n35));
  nor2  g27(.a(n27), .b(i_0_), .O(n36));
  nor2  g28(.a(n36), .b(n35), .O(n37));
  nand2 g29(.a(n37), .b(n34), .O(n38));
  nand2 g30(.a(n38), .b(n33), .O(o_1_));
  nor2  g31(.a(n10), .b(i_2_), .O(n40));
  inv1  g32(.a(i_2_), .O(n41));
  nor2  g33(.a(i_4_), .b(n41), .O(n42));
  nor2  g34(.a(n42), .b(n40), .O(n43));
  nor2  g35(.a(n43), .b(i_3_), .O(n44));
  nor2  g36(.a(n9), .b(i_1_), .O(n45));
  inv1  g37(.a(i_1_), .O(n46));
  nor2  g38(.a(i_4_), .b(n46), .O(n47));
  nor2  g39(.a(n47), .b(n45), .O(n48));
  nor2  g40(.a(n48), .b(i_2_), .O(n49));
  nor2  g41(.a(n49), .b(n44), .O(n50));
  nor2  g42(.a(n50), .b(n29), .O(n51));
  nand2 g43(.a(i_2_), .b(n46), .O(n52));
  nor2  g44(.a(n10), .b(i_3_), .O(n53));
  inv1  g45(.a(n53), .O(n54));
  nor2  g46(.a(n54), .b(n52), .O(n55));
  nor2  g47(.a(n55), .b(n51), .O(n56));
  nor2  g48(.a(n10), .b(i_0_), .O(n57));
  nor2  g49(.a(n57), .b(n47), .O(n58));
  nor2  g50(.a(n58), .b(i_2_), .O(n59));
  nand2 g51(.a(n10), .b(i_2_), .O(n60));
  nor2  g52(.a(n60), .b(i_1_), .O(n61));
  nor2  g53(.a(n61), .b(n59), .O(n62));
  nor2  g54(.a(n62), .b(n9), .O(n63));
  nor2  g55(.a(n53), .b(n42), .O(n64));
  nor2  g56(.a(n64), .b(n46), .O(n65));
  nor2  g57(.a(n52), .b(n10), .O(n66));
  nor2  g58(.a(n66), .b(n65), .O(n67));
  nor2  g59(.a(n67), .b(i_0_), .O(n68));
  nor2  g60(.a(n68), .b(n63), .O(n69));
  nand2 g61(.a(n69), .b(n56), .O(o_2_));
endmodule


