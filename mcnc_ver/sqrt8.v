// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:27 2019

module source_pla  ( 
    v[7] , v[6] , v[5] , v[4] , v[3] , v[2] , v[1] , v[0] ,
    sqrt[0] , sqrt[1] , sqrt[2] , sqrt[3]   );
  input  v[7] , v[6] , v[5] , v[4] , v[3] , v[2] , v[1] , v[0] ;
  output sqrt[0] , sqrt[1] , sqrt[2] , sqrt[3] ;
  wire n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26,
    n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40,
    n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53, n54,
    n55, n56, n57, n58, n59, n60, n61, n62, n63, n65, n66, n67, n68, n69,
    n70, n71, n72, n73, n74, n75, n76, n77, n79, n80;
  inv1  g00(.a(v[4] ), .O(n13));
  inv1  g01(.a(v[5] ), .O(n14));
  nor2  g02(.a(v[2] ), .b(v[3] ), .O(n15));
  inv1  g03(.a(n15), .O(n16));
  nand2 g04(.a(n16), .b(v[6] ), .O(n17));
  nand2 g05(.a(n17), .b(v[7] ), .O(n18));
  inv1  g06(.a(v[6] ), .O(n19));
  nand2 g07(.a(v[2] ), .b(v[3] ), .O(n20));
  inv1  g08(.a(n20), .O(n21));
  nand2 g09(.a(n21), .b(n19), .O(n22));
  nand2 g10(.a(n22), .b(n18), .O(n23));
  nand2 g11(.a(n23), .b(n14), .O(n24));
  nor2  g12(.a(n14), .b(v[7] ), .O(n25));
  nand2 g13(.a(n25), .b(n15), .O(n26));
  nand2 g14(.a(n26), .b(n24), .O(n27));
  nand2 g15(.a(n27), .b(n13), .O(n28));
  nor2  g16(.a(v[0] ), .b(v[1] ), .O(n29));
  nor2  g17(.a(v[5] ), .b(n19), .O(n30));
  nor2  g18(.a(n30), .b(v[3] ), .O(n31));
  nor2  g19(.a(n31), .b(v[7] ), .O(n32));
  nor2  g20(.a(n14), .b(v[6] ), .O(n33));
  nor2  g21(.a(n33), .b(n32), .O(n34));
  nor2  g22(.a(n34), .b(n13), .O(n35));
  nand2 g23(.a(v[3] ), .b(v[7] ), .O(n36));
  nor2  g24(.a(v[4] ), .b(n19), .O(n37));
  nand2 g25(.a(n37), .b(n15), .O(n38));
  nand2 g26(.a(n38), .b(n36), .O(n39));
  nand2 g27(.a(n39), .b(v[5] ), .O(n40));
  nor2  g28(.a(v[5] ), .b(v[6] ), .O(n41));
  nor2  g29(.a(v[2] ), .b(v[4] ), .O(n42));
  nand2 g30(.a(n42), .b(n41), .O(n43));
  nand2 g31(.a(n43), .b(n40), .O(n44));
  nor2  g32(.a(n44), .b(n35), .O(n45));
  nor2  g33(.a(n45), .b(n29), .O(n46));
  nand2 g34(.a(v[5] ), .b(v[7] ), .O(n47));
  nor2  g35(.a(v[5] ), .b(v[7] ), .O(n48));
  nand2 g36(.a(n48), .b(v[4] ), .O(n49));
  nand2 g37(.a(n49), .b(n47), .O(n50));
  nand2 g38(.a(n50), .b(v[6] ), .O(n51));
  nor2  g39(.a(n13), .b(n14), .O(n52));
  nand2 g40(.a(n52), .b(n19), .O(n53));
  nand2 g41(.a(n53), .b(n51), .O(n54));
  nand2 g42(.a(n54), .b(n16), .O(n55));
  nor2  g43(.a(n21), .b(v[4] ), .O(n56));
  nor2  g44(.a(n56), .b(n47), .O(n57));
  inv1  g45(.a(v[7] ), .O(n58));
  nand2 g46(.a(v[4] ), .b(n58), .O(n59));
  nor2  g47(.a(n59), .b(n20), .O(n60));
  nor2  g48(.a(n60), .b(n57), .O(n61));
  nand2 g49(.a(n61), .b(n55), .O(n62));
  nor2  g50(.a(n62), .b(n46), .O(n63));
  nand2 g51(.a(n63), .b(n28), .O(sqrt[0] ));
  nor2  g52(.a(v[4] ), .b(v[5] ), .O(n65));
  inv1  g53(.a(n65), .O(n66));
  nand2 g54(.a(n66), .b(v[6] ), .O(n67));
  nand2 g55(.a(n15), .b(v[6] ), .O(n68));
  nand2 g56(.a(n68), .b(n65), .O(n69));
  nand2 g57(.a(n69), .b(n67), .O(n70));
  nand2 g58(.a(n70), .b(v[7] ), .O(n71));
  nor2  g59(.a(n66), .b(v[6] ), .O(n72));
  nor2  g60(.a(n72), .b(n25), .O(n73));
  nor2  g61(.a(n73), .b(n15), .O(n74));
  inv1  g62(.a(n52), .O(n75));
  nor2  g63(.a(n75), .b(v[7] ), .O(n76));
  nor2  g64(.a(n76), .b(n74), .O(n77));
  nand2 g65(.a(n77), .b(n71), .O(sqrt[1] ));
  nand2 g66(.a(n66), .b(n19), .O(n79));
  nand2 g67(.a(v[6] ), .b(v[7] ), .O(n80));
  nand2 g68(.a(n80), .b(n79), .O(sqrt[2] ));
  nand2 g69(.a(n19), .b(n58), .O(sqrt[3] ));
endmodule


