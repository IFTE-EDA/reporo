// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:25 2019

module source_pla  ( 
    CPIPE1s<0> , CPIPE1s<1> , CPIPE1s<2> , CPIPE1s<3> , CPIPE1s<4> ,
    CPIPE1s<5> , CPIPE1s<7> , CPIPE1s<8> , RESET,
    selaluSUM, aluCINbar1, aluselSR, selaluAND, selaluOR, selaluXOR,
    selBIbar, storeSXT, pbusLtoINB, RD_WR, predecodeEA, pSTOREwrite,
    pLOADLtobusL, pSXTtobusL, byteEX, .p , 43, --------1 ,
    00000000000000000010000, ---0--11- , 00000000100001000000000,
    ---1111-- , 10000000000010010000000  );
  input  CPIPE1s<0> , CPIPE1s<1> , CPIPE1s<2> , CPIPE1s<3> ,
    CPIPE1s<4> , CPIPE1s<5> , CPIPE1s<7> , CPIPE1s<8> , RESET;
  output selaluSUM, aluCINbar1, aluselSR, selaluAND, selaluOR, selaluXOR,
    selBIbar, storeSXT, pbusLtoINB, RD_WR, predecodeEA, pSTOREwrite,
    pLOADLtobusL, pSXTtobusL, byteEX, .p , 43, --------1 ,
    00000000000000000010000, ---0--11- , 00000000100001000000000,
    ---1111-- , 10000000000010010000000;
  wire n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46,
    n47, n48, n49, n50, n51, n52, n53, n54, n56, n57, n58, n59, n60, n61,
    n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75,
    n76, n77, n78, n79, n80, n81, n83, n84, n85, n86, n88, n89, n90, n91,
    n93, n94, n96, n97, n99, n100, n101, n102, n103, n104, n105, n106,
    n107, n108, n109, n111, n112, n113, n115, n117, n119, n120, n121, n122,
    n123, n124, n125, n126, n127, n128, n130, n131, n132, n134, n135, n136,
    n139, n140, n142, n143, n144, n145, n146, n147, n148, n149, n150, n151,
    n152, n155, n156, n157, n158, n159, n160, n161, n162, n163, n164, n165,
    n166, n167, n168, n169, n170, n171, n172, n173, n174, n176, n177, n178,
    n179, n180, n181, n183, n184, n186, n187, n188, n189, n190, n191, n192,
    n193, n194, n195, n196, n198;
  inv1  g000(.a(CPIPE1s<4> ), .O(n33));
  inv1  g001(.a(CPIPE1s<2> ), .O(n34));
  nor2  g002(.a(CPIPE1s<3> ), .b(n34), .O(n35));
  inv1  g003(.a(n35), .O(n36));
  inv1  g004(.a(CPIPE1s<0> ), .O(n37));
  inv1  g005(.a(CPIPE1s<7> ), .O(n38));
  nor2  g006(.a(n38), .b(n37), .O(n39));
  nand2 g007(.a(n39), .b(n36), .O(n40));
  inv1  g008(.a(CPIPE1s<3> ), .O(n41));
  inv1  g009(.a(CPIPE1s<5> ), .O(n42));
  nor2  g010(.a(n42), .b(n41), .O(n43));
  nand2 g011(.a(n43), .b(n34), .O(n44));
  nand2 g012(.a(n44), .b(n40), .O(n45));
  nand2 g013(.a(n45), .b(n33), .O(n46));
  nor2  g014(.a(n38), .b(n42), .O(n47));
  inv1  g015(.a(n47), .O(n48));
  nor2  g016(.a(CPIPE1s<1> ), .b(n37), .O(n49));
  inv1  g017(.a(n49), .O(n50));
  nor2  g018(.a(n33), .b(CPIPE1s<3> ), .O(n51));
  inv1  g019(.a(n51), .O(n52));
  nand2 g020(.a(n52), .b(n50), .O(n53));
  nor2  g021(.a(n53), .b(n48), .O(n54));
  nand2 g022(.a(n54), .b(n46), .O(selaluSUM));
  nand2 g023(.a(n42), .b(CPIPE1s<4> ), .O(n56));
  nor2  g024(.a(n56), .b(n41), .O(n57));
  nor2  g025(.a(n57), .b(CPIPE1s<5> ), .O(n58));
  nor2  g026(.a(n58), .b(CPIPE1s<1> ), .O(n59));
  nor2  g027(.a(n42), .b(CPIPE1s<4> ), .O(n60));
  inv1  g028(.a(n60), .O(n61));
  nor2  g029(.a(n61), .b(CPIPE1s<3> ), .O(n62));
  nor2  g030(.a(n62), .b(n59), .O(n63));
  nor2  g031(.a(n63), .b(CPIPE1s<0> ), .O(n64));
  nand2 g032(.a(n33), .b(n41), .O(n65));
  nor2  g033(.a(n65), .b(n37), .O(n66));
  nor2  g034(.a(n66), .b(n64), .O(n67));
  nor2  g035(.a(n67), .b(CPIPE1s<2> ), .O(n68));
  nor2  g036(.a(CPIPE1s<5> ), .b(n33), .O(n69));
  nor2  g037(.a(n41), .b(CPIPE1s<1> ), .O(n70));
  nand2 g038(.a(n70), .b(n69), .O(n71));
  nand2 g039(.a(n71), .b(n61), .O(n72));
  nand2 g040(.a(n72), .b(CPIPE1s<2> ), .O(n73));
  nor2  g041(.a(n42), .b(n33), .O(n74));
  nor2  g042(.a(n41), .b(n37), .O(n75));
  inv1  g043(.a(n75), .O(n76));
  nor2  g044(.a(n76), .b(n74), .O(n77));
  nor2  g045(.a(CPIPE1s<5> ), .b(CPIPE1s<4> ), .O(n78));
  nor2  g046(.a(n78), .b(n77), .O(n79));
  nand2 g047(.a(n79), .b(n73), .O(n80));
  nor2  g048(.a(n80), .b(n68), .O(n81));
  nor2  g049(.a(n81), .b(n38), .O(aluCINbar1));
  nor2  g050(.a(CPIPE1s<2> ), .b(CPIPE1s<0> ), .O(n83));
  inv1  g051(.a(n83), .O(n84));
  inv1  g052(.a(n65), .O(n85));
  nand2 g053(.a(n85), .b(n47), .O(n86));
  nor2  g054(.a(n86), .b(n84), .O(aluselSR));
  nand2 g055(.a(n60), .b(CPIPE1s<7> ), .O(n88));
  inv1  g056(.a(CPIPE1s<1> ), .O(n89));
  nor2  g057(.a(n89), .b(CPIPE1s<0> ), .O(n90));
  nand2 g058(.a(n90), .b(n35), .O(n91));
  nor2  g059(.a(n91), .b(n88), .O(selaluAND));
  nand2 g060(.a(n60), .b(CPIPE1s<1> ), .O(n93));
  nand2 g061(.a(n39), .b(n35), .O(n94));
  nor2  g062(.a(n94), .b(n93), .O(selaluOR));
  nor2  g063(.a(CPIPE1s<1> ), .b(CPIPE1s<0> ), .O(n96));
  nand2 g064(.a(n96), .b(CPIPE1s<2> ), .O(n97));
  nor2  g065(.a(n97), .b(n86), .O(selaluXOR));
  nand2 g066(.a(n69), .b(CPIPE1s<3> ), .O(n99));
  nand2 g067(.a(n99), .b(n42), .O(n100));
  nand2 g068(.a(n100), .b(n89), .O(n101));
  inv1  g069(.a(n62), .O(n102));
  nand2 g070(.a(n102), .b(n101), .O(n103));
  nand2 g071(.a(n103), .b(n37), .O(n104));
  inv1  g072(.a(n66), .O(n105));
  nand2 g073(.a(n105), .b(n104), .O(n106));
  nand2 g074(.a(n106), .b(n34), .O(n107));
  inv1  g075(.a(n80), .O(n108));
  nand2 g076(.a(n108), .b(n107), .O(n109));
  nor2  g077(.a(n109), .b(n38), .O(selBIbar));
  nor2  g078(.a(n33), .b(n41), .O(n111));
  nor2  g079(.a(n38), .b(CPIPE1s<5> ), .O(n112));
  nand2 g080(.a(n112), .b(n111), .O(n113));
  nor2  g081(.a(n113), .b(n84), .O(storeSXT));
  nand2 g082(.a(CPIPE1s<8> ), .b(CPIPE1s<7> ), .O(n115));
  nor2  g083(.a(n115), .b(n74), .O(pSXTtobusL));
  inv1  g084(.a(pSXTtobusL), .O(n117));
  nand2 g085(.a(n117), .b(CPIPE1s<7> ), .O(pbusLtoINB));
  inv1  g086(.a(n43), .O(n119));
  nand2 g087(.a(n42), .b(n37), .O(n120));
  nor2  g088(.a(n38), .b(CPIPE1s<3> ), .O(n121));
  nand2 g089(.a(n121), .b(n120), .O(n122));
  nand2 g090(.a(n122), .b(n119), .O(n123));
  nand2 g091(.a(n123), .b(n34), .O(n124));
  nand2 g092(.a(n47), .b(CPIPE1s<2> ), .O(n125));
  nand2 g093(.a(n125), .b(n124), .O(n126));
  nand2 g094(.a(n126), .b(n33), .O(n127));
  nor2  g095(.a(n51), .b(n48), .O(n128));
  nand2 g096(.a(n128), .b(n127), .O(RD_WR));
  nor2  g097(.a(CPIPE1s<2> ), .b(n37), .O(n130));
  nor2  g098(.a(n56), .b(n38), .O(n131));
  nand2 g099(.a(n131), .b(n70), .O(n132));
  nor2  g100(.a(n132), .b(n130), .O(predecodeEA));
  nand2 g101(.a(CPIPE1s<3> ), .b(n34), .O(n134));
  nor2  g102(.a(n134), .b(n33), .O(n135));
  nand2 g103(.a(n135), .b(n96), .O(n136));
  nor2  g104(.a(n136), .b(n48), .O(pSTOREwrite));
  nor2  g105(.a(n38), .b(CPIPE1s<4> ), .O(n139));
  nand2 g106(.a(n139), .b(n43), .O(n140));
  nor2  g107(.a(n140), .b(n97), .O(byteEX));
  nand2 g108(.a(CPIPE1s<5> ), .b(n37), .O(n142));
  nor2  g109(.a(n34), .b(n89), .O(n143));
  nand2 g110(.a(n143), .b(CPIPE1s<5> ), .O(n144));
  nand2 g111(.a(n144), .b(CPIPE1s<2> ), .O(n145));
  nand2 g112(.a(n145), .b(CPIPE1s<0> ), .O(n146));
  nand2 g113(.a(n146), .b(n142), .O(n147));
  nand2 g114(.a(n147), .b(n41), .O(n148));
  nand2 g115(.a(n148), .b(n76), .O(n149));
  nand2 g116(.a(n149), .b(CPIPE1s<7> ), .O(n150));
  nand2 g117(.a(n150), .b(n44), .O(n151));
  nand2 g118(.a(n151), .b(n33), .O(n152));
  nand2 g119(.a(n152), .b(n54), .O(.p ));
  nand2 g120(.a(n127), .b(n47), .O(43));
  nand2 g121(.a(n65), .b(CPIPE1s<1> ), .O(n155));
  nor2  g122(.a(n38), .b(CPIPE1s<0> ), .O(n156));
  nand2 g123(.a(n156), .b(n155), .O(n157));
  nand2 g124(.a(n33), .b(CPIPE1s<3> ), .O(n158));
  nand2 g125(.a(n158), .b(n157), .O(n159));
  nand2 g126(.a(n159), .b(CPIPE1s<5> ), .O(n160));
  nand2 g127(.a(n85), .b(n39), .O(n161));
  nand2 g128(.a(n161), .b(n160), .O(n162));
  nand2 g129(.a(n162), .b(n34), .O(n163));
  nor2  g130(.a(n34), .b(CPIPE1s<1> ), .O(n164));
  nor2  g131(.a(n164), .b(n37), .O(n165));
  nor2  g132(.a(n165), .b(n121), .O(n166));
  nor2  g133(.a(n166), .b(n33), .O(n167));
  nor2  g134(.a(n49), .b(CPIPE1s<3> ), .O(n168));
  nor2  g135(.a(n168), .b(n167), .O(n169));
  nor2  g136(.a(n169), .b(CPIPE1s<5> ), .O(n170));
  nor2  g137(.a(CPIPE1s<4> ), .b(n34), .O(n171));
  nand2 g138(.a(n171), .b(n47), .O(n172));
  nand2 g139(.a(n172), .b(CPIPE1s<7> ), .O(n173));
  nor2  g140(.a(n173), .b(n170), .O(n174));
  nand2 g141(.a(n174), .b(n163), .O(00000000000000000010000));
  nor2  g142(.a(n83), .b(n42), .O(n176));
  inv1  g143(.a(n90), .O(n177));
  nor2  g144(.a(n177), .b(CPIPE1s<2> ), .O(n178));
  nor2  g145(.a(n178), .b(n176), .O(n179));
  nor2  g146(.a(RESET), .b(n38), .O(n180));
  nand2 g147(.a(n180), .b(n111), .O(n181));
  nor2  g148(.a(n181), .b(n179), .O(---0--11- ));
  inv1  g149(.a(RESET), .O(n183));
  nand2 g150(.a(n112), .b(n183), .O(n184));
  nor2  g151(.a(n184), .b(n136), .O(00000000100001000000000));
  nor2  g152(.a(n42), .b(CPIPE1s<3> ), .O(n186));
  nand2 g153(.a(CPIPE1s<2> ), .b(n37), .O(n187));
  nand2 g154(.a(n42), .b(CPIPE1s<3> ), .O(n188));
  nor2  g155(.a(n188), .b(n187), .O(n189));
  nor2  g156(.a(n189), .b(n186), .O(n190));
  nor2  g157(.a(n190), .b(n89), .O(n191));
  inv1  g158(.a(n176), .O(n192));
  nor2  g159(.a(n192), .b(CPIPE1s<3> ), .O(n193));
  nor2  g160(.a(n193), .b(n191), .O(n194));
  nor2  g161(.a(n38), .b(n33), .O(n195));
  nand2 g162(.a(n195), .b(n183), .O(n196));
  nor2  g163(.a(n196), .b(n194), .O(---1111-- ));
  nand2 g164(.a(n164), .b(n111), .O(n198));
  nor2  g165(.a(n198), .b(n184), .O(10000000000010010000000));
  zero  g166(.O(pLOADLtobusL));
  nand2 g167(.a(n127), .b(n47), .O(--------1 ));
endmodule


