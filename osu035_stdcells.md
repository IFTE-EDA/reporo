# Umbennenung der Standardzellen

Die Standardzellen wurden gegenüber der originalen Technologiedatei umbenannt:

FILL
BUFX1 = buf1
INVX1 = inv1
MIN3AX1 = min3A
MIN3BX1 = min3B
MUX2X1 = mux2
NAND2X1 = nand2
NOR2X1 = nor2
XOR2X1 = xor2

# Erzeugte Low-Power-Zellen

driv1A		(Treiberzelle - prim. PG-Pins auf Rail metal1, sek. PG-Pins auf Rail metal3)
driv1B		(Treiberzelle - prim. PG-Pins auf Rail, sek. PG-Pins in Mitte)

Nachfolgende Zellen sind fuer den Versuch mit NanoRoute erstellt worden:

rec_driv1A	(Treiberzelle - prim. PG-Pins auf Rail metal1, sek. PG-Pins auf Rail metal3)
rec_driv1B	(Treiberzelle - prim. PG-Pins auf Rail, sek. PG-Pins in Mitte)
rec_nand2A	(nand2 mit zusaetzlichen P/Pbar-Rails auf metal3)
rec_nand2B	(nand2 mit zusaetzlichen P/Pbar-Pins auf metal1
rec_nor2A	(nor2 mit zusaetzlichen P/Pbar-Rails auf metal3)
rec_nor2B	(nor2 mit zusaetzlichen P/Pbar-Pins auf metal1)

(Treiberzellen weichen von dieser Namenskonvention ab)

# Umbenennung von Pins:

## Input Pins

A = a
B = b
S = s
A0 = a0
A1 = a1

## Output Pins

Z = O

