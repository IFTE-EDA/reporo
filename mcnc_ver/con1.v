// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:19 2019

module source_pla  ( 
    f, b, c, d, a, h, g,
    f0, f1  );
  input  f, b, c, d, a, h, g;
  output f0, f1;
  wire n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n22, n23, n24,
    n25, n26, n27, n28, n29;
  inv1  g00(.a(a), .O(n10));
  inv1  g01(.a(f), .O(n11));
  nand2 g02(.a(h), .b(n11), .O(n12));
  nand2 g03(.a(n12), .b(n10), .O(n13));
  nand2 g04(.a(n13), .b(b), .O(n14));
  nand2 g05(.a(c), .b(f), .O(n15));
  inv1  g06(.a(b), .O(n16));
  inv1  g07(.a(c), .O(n17));
  nand2 g08(.a(n17), .b(n16), .O(n18));
  nand2 g09(.a(n18), .b(n15), .O(n19));
  nand2 g10(.a(n19), .b(d), .O(n20));
  nand2 g11(.a(n20), .b(n14), .O(f0));
  nand2 g12(.a(a), .b(b), .O(n22));
  nand2 g13(.a(n22), .b(g), .O(n23));
  nand2 g14(.a(n23), .b(n11), .O(n24));
  nor2  g15(.a(d), .b(b), .O(n25));
  nor2  g16(.a(n25), .b(n10), .O(n26));
  nor2  g17(.a(n26), .b(n11), .O(n27));
  nor2  g18(.a(a), .b(b), .O(n28));
  nor2  g19(.a(n28), .b(n27), .O(n29));
  nand2 g20(.a(n29), .b(n24), .O(f1));
endmodule


