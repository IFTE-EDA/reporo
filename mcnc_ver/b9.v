// Benchmark "b9" written by ABC on Tue Nov  5 15:01:17 2019

module b9 ( 
    a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x,
    y, z, a0, b0, c0, d0, e0, f0, g0, h0, i0, j0, k0, l0, m0, n0, o0,
    p0, q0, r0, s0, t0, u0, v0, w0, x0, y0, z0, a1, b1, c1, d1, e1, f1, g1,
    h1, i1, j1  );
  input  a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u,
    v, w, x, y, z, a0, b0, c0, d0, e0, f0, g0, h0, i0, j0, k0, l0, m0, n0,
    o0;
  output p0, q0, r0, s0, t0, u0, v0, w0, x0, y0, z0, a1, b1, c1, d1, e1, f1,
    g1, h1, i1, j1;
  wire n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76,
    n77, n78, n79, n80, n82, n83, n84, n86, n87, n88, n89, n90, n91, n92,
    n93, n94, n95, n97, n98, n99, n100, n101, n102, n103, n104, n105, n106,
    n108, n109, n110, n112, n114, n117, n118, n119, n120, n121, n122, n123,
    n124, n125, n126, n127, n129, n130, n132, n133, n134, n135, n136, n137,
    n138, n139, n140, n141, n142, n143, n144, n145, n146, n147, n148, n150,
    n151, n152, n153, n154, n155, n156, n157, n160, n161, n162, n163, n164,
    n165, n166, n167, n168, n169, n172, n174, n176, n177, n180, n181, n182,
    n183, n184, n185;
  inv1  g000(.a(p), .O(n63));
  inv1  g001(.a(b0), .O(n64));
  inv1  g002(.a(c0), .O(n65));
  inv1  g003(.a(j0), .O(n66));
  nor2  g004(.a(n66), .b(n65), .O(n67));
  nor2  g005(.a(n67), .b(l0), .O(n68));
  nor2  g006(.a(n68), .b(n64), .O(n69));
  inv1  g007(.a(n69), .O(n70));
  inv1  g008(.a(e), .O(n71));
  nand2 g009(.a(k), .b(n71), .O(n72));
  inv1  g010(.a(k0), .O(n73));
  nand2 g011(.a(j0), .b(n65), .O(n74));
  nand2 g012(.a(n74), .b(n73), .O(n75));
  nor2  g013(.a(n64), .b(e), .O(n76));
  nand2 g014(.a(n76), .b(n75), .O(n77));
  nand2 g015(.a(n77), .b(n72), .O(n78));
  inv1  g016(.a(n0), .O(n79));
  inv1  g017(.a(o0), .O(n80));
  nor2  g018(.a(n80), .b(n79), .O(x0));
  nand2 g019(.a(x0), .b(n78), .O(n82));
  nand2 g020(.a(n82), .b(n70), .O(n83));
  nand2 g021(.a(n83), .b(n63), .O(n84));
  nand2 g022(.a(n84), .b(q), .O(p0));
  nor2  g023(.a(n66), .b(n64), .O(n86));
  inv1  g024(.a(n86), .O(n87));
  nand2 g025(.a(n65), .b(e), .O(n88));
  nor2  g026(.a(n88), .b(n87), .O(n89));
  nor2  g027(.a(g0), .b(e0), .O(n90));
  inv1  g028(.a(n90), .O(n91));
  nand2 g029(.a(n91), .b(e), .O(n92));
  nor2  g030(.a(n64), .b(n71), .O(n93));
  nand2 g031(.a(n93), .b(k0), .O(n94));
  nand2 g032(.a(n94), .b(n92), .O(n95));
  nor2  g033(.a(n95), .b(n89), .O(q0));
  inv1  g034(.a(n74), .O(n97));
  inv1  g035(.a(i), .O(n98));
  nand2 g036(.a(n64), .b(n98), .O(n99));
  nand2 g037(.a(n99), .b(n97), .O(n100));
  nand2 g038(.a(n100), .b(e), .O(n101));
  inv1  g039(.a(x0), .O(n102));
  nand2 g040(.a(d0), .b(i), .O(n103));
  inv1  g041(.a(n103), .O(n104));
  nor2  g042(.a(n104), .b(c), .O(n105));
  nor2  g043(.a(n105), .b(n102), .O(n106));
  nand2 g044(.a(n106), .b(n101), .O(r0));
  nand2 g045(.a(l0), .b(n64), .O(n108));
  nand2 g046(.a(n67), .b(b0), .O(n109));
  nand2 g047(.a(n109), .b(n108), .O(n110));
  nand2 g048(.a(n110), .b(v), .O(s0));
  inv1  g049(.a(v), .O(n112));
  nand2 g050(.a(n110), .b(n112), .O(t0));
  nor2  g051(.a(n65), .b(n64), .O(n114));
  nor2  g052(.a(n114), .b(l0), .O(v0));
  inv1  g053(.a(v0), .O(u0));
  inv1  g054(.a(a), .O(n117));
  nor2  g055(.a(z), .b(n117), .O(n118));
  inv1  g056(.a(o), .O(n119));
  nand2 g057(.a(z), .b(n119), .O(n120));
  nand2 g058(.a(n120), .b(m0), .O(n121));
  nor2  g059(.a(n121), .b(n118), .O(n122));
  nand2 g060(.a(r), .b(n63), .O(n123));
  nor2  g061(.a(h0), .b(f0), .O(n124));
  nand2 g062(.a(n124), .b(n123), .O(n125));
  nand2 g063(.a(n125), .b(o), .O(n126));
  nand2 g064(.a(n126), .b(d), .O(n127));
  nor2  g065(.a(n127), .b(n122), .O(w0));
  nand2 g066(.a(i0), .b(a0), .O(n129));
  nand2 g067(.a(b0), .b(l), .O(n130));
  nor2  g068(.a(n130), .b(n129), .O(y0));
  nor2  g069(.a(n80), .b(n64), .O(n132));
  nand2 g070(.a(n132), .b(n75), .O(n133));
  nand2 g071(.a(n91), .b(o0), .O(n134));
  nand2 g072(.a(n134), .b(n133), .O(n135));
  nand2 g073(.a(f), .b(n71), .O(n136));
  nor2  g074(.a(n136), .b(n79), .O(n137));
  nand2 g075(.a(n137), .b(n135), .O(n138));
  inv1  g076(.a(n75), .O(n139));
  inv1  g077(.a(h), .O(n140));
  inv1  g078(.a(n76), .O(n141));
  nor2  g079(.a(n141), .b(n140), .O(n142));
  nand2 g080(.a(n142), .b(n102), .O(n143));
  nor2  g081(.a(n143), .b(n139), .O(n144));
  inv1  g082(.a(l0), .O(n145));
  nand2 g083(.a(b0), .b(g), .O(n146));
  nor2  g084(.a(n146), .b(n145), .O(n147));
  nor2  g085(.a(n147), .b(n144), .O(n148));
  nand2 g086(.a(n148), .b(n138), .O(z0));
  nor2  g087(.a(b0), .b(n98), .O(n150));
  nand2 g088(.a(n150), .b(n97), .O(n151));
  nor2  g089(.a(e0), .b(j), .O(n152));
  nand2 g090(.a(n152), .b(n151), .O(n153));
  nor2  g091(.a(n141), .b(n74), .O(n154));
  nor2  g092(.a(n103), .b(n102), .O(n155));
  nor2  g093(.a(n155), .b(n154), .O(n156));
  inv1  g094(.a(n156), .O(n157));
  nor2  g095(.a(n157), .b(n153), .O(a1));
  inv1  g096(.a(n83), .O(b1));
  nor2  g097(.a(t), .b(s), .O(n160));
  nand2 g098(.a(n160), .b(u), .O(n161));
  nor2  g099(.a(n161), .b(n70), .O(n162));
  nor2  g100(.a(n73), .b(n66), .O(n163));
  nor2  g101(.a(n163), .b(n65), .O(n164));
  nor2  g102(.a(n164), .b(n64), .O(n165));
  nor2  g103(.a(n165), .b(n91), .O(n166));
  nor2  g104(.a(n), .b(e), .O(n167));
  nand2 g105(.a(n167), .b(x0), .O(n168));
  nor2  g106(.a(n168), .b(n166), .O(n169));
  nor2  g107(.a(n169), .b(n162), .O(d1));
  inv1  g108(.a(d1), .O(c1));
  nand2 g109(.a(b0), .b(m), .O(n172));
  nor2  g110(.a(n172), .b(n129), .O(e1));
  nand2 g111(.a(w), .b(b), .O(n174));
  nor2  g112(.a(n174), .b(x), .O(f1));
  inv1  g113(.a(y), .O(n176));
  nand2 g114(.a(n176), .b(x), .O(n177));
  nor2  g115(.a(n177), .b(n174), .O(g1));
  inv1  g116(.a(a1), .O(h1));
  nand2 g117(.a(n65), .b(i), .O(n180));
  nor2  g118(.a(n180), .b(n66), .O(n181));
  inv1  g119(.a(n155), .O(n182));
  inv1  g120(.a(n152), .O(n183));
  nor2  g121(.a(n183), .b(n86), .O(n184));
  nand2 g122(.a(n184), .b(n182), .O(n185));
  nor2  g123(.a(n185), .b(n181), .O(j1));
  nor2  g124(.a(n157), .b(n153), .O(i1));
endmodule


