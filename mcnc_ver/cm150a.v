// Benchmark "CM150" written by ABC on Tue Nov  5 15:01:19 2019

module CM150 ( 
    a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u,
    v  );
  input  a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u;
  output v;
  wire n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36,
    n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50,
    n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64,
    n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78,
    n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92,
    n93, n94, n95, n96, n97, n98, n99, n100, n101, n102, n103, n104, n105,
    n106, n107, n108, n109, n110, n111, n112, n113, n114, n115, n116, n117,
    n118, n119, n120, n121, n122, n123, n124, n125, n126, n127, n128, n129,
    n130, n131, n132, n133, n134, n135, n136;
  inv1  g000(.a(u), .O(n23));
  inv1  g001(.a(o), .O(n24));
  nand2 g002(.a(q), .b(p), .O(n25));
  nand2 g003(.a(n25), .b(n24), .O(n26));
  inv1  g004(.a(p), .O(n27));
  nand2 g005(.a(q), .b(n27), .O(n28));
  nand2 g006(.a(n28), .b(n26), .O(n29));
  nand2 g007(.a(n29), .b(r), .O(n30));
  inv1  g008(.a(n), .O(n31));
  inv1  g009(.a(q), .O(n32));
  nor2  g010(.a(n32), .b(n31), .O(n33));
  nor2  g011(.a(n33), .b(m), .O(n34));
  nor2  g012(.a(n32), .b(n), .O(n35));
  nor2  g013(.a(n35), .b(n34), .O(n36));
  nand2 g014(.a(n36), .b(n30), .O(n37));
  nor2  g015(.a(n32), .b(n27), .O(n38));
  nor2  g016(.a(n38), .b(o), .O(n39));
  nor2  g017(.a(n32), .b(p), .O(n40));
  nor2  g018(.a(n40), .b(n39), .O(n41));
  nand2 g019(.a(n41), .b(r), .O(n42));
  nand2 g020(.a(n42), .b(n37), .O(n43));
  nand2 g021(.a(n43), .b(s), .O(n44));
  inv1  g022(.a(r), .O(n45));
  inv1  g023(.a(l), .O(n46));
  nor2  g024(.a(n32), .b(n46), .O(n47));
  nor2  g025(.a(n47), .b(k), .O(n48));
  nor2  g026(.a(n32), .b(l), .O(n49));
  nor2  g027(.a(n49), .b(n48), .O(n50));
  nor2  g028(.a(n50), .b(n45), .O(n51));
  inv1  g029(.a(i), .O(n52));
  nand2 g030(.a(q), .b(j), .O(n53));
  nand2 g031(.a(n53), .b(n52), .O(n54));
  nor2  g032(.a(n32), .b(j), .O(n55));
  inv1  g033(.a(n55), .O(n56));
  nand2 g034(.a(n56), .b(n54), .O(n57));
  nor2  g035(.a(n57), .b(n51), .O(n58));
  inv1  g036(.a(k), .O(n59));
  nand2 g037(.a(q), .b(l), .O(n60));
  nand2 g038(.a(n60), .b(n59), .O(n61));
  inv1  g039(.a(n49), .O(n62));
  nand2 g040(.a(n62), .b(n61), .O(n63));
  nor2  g041(.a(n63), .b(n45), .O(n64));
  nor2  g042(.a(n64), .b(n58), .O(n65));
  nand2 g043(.a(n65), .b(n44), .O(n66));
  nor2  g044(.a(n41), .b(n45), .O(n67));
  inv1  g045(.a(m), .O(n68));
  nand2 g046(.a(q), .b(n), .O(n69));
  nand2 g047(.a(n69), .b(n68), .O(n70));
  inv1  g048(.a(n35), .O(n71));
  nand2 g049(.a(n71), .b(n70), .O(n72));
  nor2  g050(.a(n72), .b(n67), .O(n73));
  nor2  g051(.a(n29), .b(n45), .O(n74));
  nor2  g052(.a(n74), .b(n73), .O(n75));
  nand2 g053(.a(n75), .b(s), .O(n76));
  nand2 g054(.a(n76), .b(n66), .O(n77));
  nand2 g055(.a(n77), .b(t), .O(n78));
  inv1  g056(.a(s), .O(n79));
  inv1  g057(.a(h), .O(n80));
  nor2  g058(.a(n32), .b(n80), .O(n81));
  nor2  g059(.a(n81), .b(g), .O(n82));
  nor2  g060(.a(n32), .b(h), .O(n83));
  nor2  g061(.a(n83), .b(n82), .O(n84));
  nor2  g062(.a(n84), .b(n45), .O(n85));
  inv1  g063(.a(e), .O(n86));
  nand2 g064(.a(q), .b(f), .O(n87));
  nand2 g065(.a(n87), .b(n86), .O(n88));
  nor2  g066(.a(n32), .b(f), .O(n89));
  inv1  g067(.a(n89), .O(n90));
  nand2 g068(.a(n90), .b(n88), .O(n91));
  nor2  g069(.a(n91), .b(n85), .O(n92));
  inv1  g070(.a(g), .O(n93));
  nand2 g071(.a(q), .b(h), .O(n94));
  nand2 g072(.a(n94), .b(n93), .O(n95));
  inv1  g073(.a(n83), .O(n96));
  nand2 g074(.a(n96), .b(n95), .O(n97));
  nor2  g075(.a(n97), .b(n45), .O(n98));
  nor2  g076(.a(n98), .b(n92), .O(n99));
  nor2  g077(.a(n99), .b(n79), .O(n100));
  inv1  g078(.a(c), .O(n101));
  nand2 g079(.a(q), .b(d), .O(n102));
  nand2 g080(.a(n102), .b(n101), .O(n103));
  nor2  g081(.a(n32), .b(d), .O(n104));
  inv1  g082(.a(n104), .O(n105));
  nand2 g083(.a(n105), .b(n103), .O(n106));
  nand2 g084(.a(n106), .b(r), .O(n107));
  inv1  g085(.a(b), .O(n108));
  nor2  g086(.a(n32), .b(n108), .O(n109));
  nor2  g087(.a(n109), .b(a), .O(n110));
  nor2  g088(.a(n32), .b(b), .O(n111));
  nor2  g089(.a(n111), .b(n110), .O(n112));
  nand2 g090(.a(n112), .b(n107), .O(n113));
  inv1  g091(.a(n106), .O(n114));
  nand2 g092(.a(n114), .b(r), .O(n115));
  nand2 g093(.a(n115), .b(n113), .O(n116));
  nor2  g094(.a(n116), .b(n100), .O(n117));
  nand2 g095(.a(n97), .b(r), .O(n118));
  inv1  g096(.a(n91), .O(n119));
  nand2 g097(.a(n119), .b(n118), .O(n120));
  inv1  g098(.a(n98), .O(n121));
  nand2 g099(.a(n121), .b(n120), .O(n122));
  nor2  g100(.a(n122), .b(n79), .O(n123));
  nor2  g101(.a(n123), .b(n117), .O(n124));
  nand2 g102(.a(n124), .b(n78), .O(n125));
  nor2  g103(.a(n75), .b(n79), .O(n126));
  nand2 g104(.a(n63), .b(r), .O(n127));
  inv1  g105(.a(n57), .O(n128));
  nand2 g106(.a(n128), .b(n127), .O(n129));
  inv1  g107(.a(n64), .O(n130));
  nand2 g108(.a(n130), .b(n129), .O(n131));
  nor2  g109(.a(n131), .b(n126), .O(n132));
  nor2  g110(.a(n43), .b(n79), .O(n133));
  nor2  g111(.a(n133), .b(n132), .O(n134));
  nand2 g112(.a(n134), .b(t), .O(n135));
  nand2 g113(.a(n135), .b(n125), .O(n136));
  nand2 g114(.a(n136), .b(n23), .O(v));
endmodule


