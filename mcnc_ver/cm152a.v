// Benchmark "mux_cl" written by ABC on Tue Nov  5 15:01:19 2019

module mux_cl ( 
    a, b, c, d, e, f, g, h, i, j, k,
    l  );
  input  a, b, c, d, e, f, g, h, i, j, k;
  output l;
  wire n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26,
    n27, n28, n29, n30, n31, n32, n33, n34, n35;
  inv1  g00(.a(i), .O(n13));
  nand2 g01(.a(n13), .b(g), .O(n14));
  nand2 g02(.a(i), .b(h), .O(n15));
  nand2 g03(.a(n15), .b(n14), .O(n16));
  nand2 g04(.a(n16), .b(j), .O(n17));
  inv1  g05(.a(j), .O(n18));
  nand2 g06(.a(n13), .b(e), .O(n19));
  nand2 g07(.a(i), .b(f), .O(n20));
  nand2 g08(.a(n20), .b(n19), .O(n21));
  nand2 g09(.a(n21), .b(n18), .O(n22));
  nand2 g10(.a(n22), .b(n17), .O(n23));
  nand2 g11(.a(n23), .b(k), .O(n24));
  inv1  g12(.a(k), .O(n25));
  nand2 g13(.a(n13), .b(c), .O(n26));
  nand2 g14(.a(i), .b(d), .O(n27));
  nand2 g15(.a(n27), .b(n26), .O(n28));
  nand2 g16(.a(n28), .b(j), .O(n29));
  nand2 g17(.a(i), .b(b), .O(n30));
  nand2 g18(.a(n13), .b(a), .O(n31));
  nand2 g19(.a(n31), .b(n30), .O(n32));
  nand2 g20(.a(n32), .b(n18), .O(n33));
  nand2 g21(.a(n33), .b(n29), .O(n34));
  nand2 g22(.a(n34), .b(n25), .O(n35));
  nand2 g23(.a(n35), .b(n24), .O(l));
endmodule


