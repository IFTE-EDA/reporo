// Benchmark "C17.iscas" written by ABC on Tue Nov  5 15:01:18 2019

module C17.iscas  ( 
    1GAT(0) , 2GAT(1) , 3GAT(2) , 6GAT(3) , 7GAT(4) ,
    22GAT(10) , 23GAT(9)   );
  input  1GAT(0) , 2GAT(1) , 3GAT(2) , 6GAT(3) , 7GAT(4) ;
  output 22GAT(10) , 23GAT(9) ;
  wire n8, n9, n10, n12;
  nand2 g0(.a(3GAT(2) ), .b(1GAT(0) ), .O(n8));
  nand2 g1(.a(6GAT(3) ), .b(3GAT(2) ), .O(n9));
  nand2 g2(.a(n9), .b(2GAT(1) ), .O(n10));
  nand2 g3(.a(n10), .b(n8), .O(22GAT(10) ));
  nand2 g4(.a(n9), .b(7GAT(4) ), .O(n12));
  nand2 g5(.a(n12), .b(n10), .O(23GAT(9) ));
endmodule


