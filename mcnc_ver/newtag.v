// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:25 2019

module source_pla  ( 
    busB<31> , busA<31> , busA<30> , busB<30> , busB<29> , busA<29> ,
    busB<28> , busA<28> ,
    ptagcompare  );
  input  busB<31> , busA<31> , busA<30> , busB<30> , busB<29> ,
    busA<29> , busB<28> , busA<28> ;
  output ptagcompare;
  wire n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20;
  nand2 g00(.a(busA<29> ), .b(busB<29> ), .O(n10));
  nand2 g01(.a(busA<28> ), .b(busB<28> ), .O(n11));
  nand2 g02(.a(n11), .b(n10), .O(n12));
  inv1  g03(.a(busB<29> ), .O(n13));
  inv1  g04(.a(busA<29> ), .O(n14));
  nand2 g05(.a(n14), .b(n13), .O(n15));
  nand2 g06(.a(n15), .b(n12), .O(n16));
  nand2 g07(.a(n16), .b(busB<30> ), .O(n17));
  inv1  g08(.a(busB<31> ), .O(n18));
  nand2 g09(.a(busA<31> ), .b(n18), .O(n19));
  nor2  g10(.a(n19), .b(busA<30> ), .O(n20));
  nand2 g11(.a(n20), .b(n17), .O(ptagcompare));
endmodule


