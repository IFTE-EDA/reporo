// Benchmark "unreg" written by ABC on Tue Nov  5 15:01:28 2019

module unreg ( 
    a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, s, t, u, v, w, x, y,
    z, a0, b0, c0, d0, e0, f0, g0, h0, i0, j0, k0,
    l0, m0, n0, o0, p0, q0, r0, s0, t0, u0, v0, w0, x0, y0, z0, a1  );
  input  a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, s, t, u, v,
    w, x, y, z, a0, b0, c0, d0, e0, f0, g0, h0, i0, j0, k0;
  output l0, m0, n0, o0, p0, q0, r0, s0, t0, u0, v0, w0, x0, y0, z0, a1;
  wire n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n66, n67,
    n68, n69, n70, n71, n72, n74, n75, n76, n77, n78, n79, n80, n82, n83,
    n84, n85, n86, n87, n89, n90, n91, n92, n93, n94, n95, n96, n98, n99,
    n100, n101, n102, n103, n104, n106, n107, n108, n109, n110, n111, n112,
    n114, n115, n116, n117, n118, n119, n121, n122, n123, n124, n125, n126,
    n127, n128, n130, n131, n132, n133, n134, n135, n136, n138, n139, n140,
    n141, n142, n143, n144, n146, n147, n148, n149, n150, n151, n153, n154,
    n155, n156, n157, n158, n159, n160, n162, n163, n164, n165, n166, n167,
    n168, n170, n171, n172, n173, n174, n175, n176, n178, n179, n180, n181,
    n182, n183;
  inv1  g000(.a(u), .O(n53));
  inv1  g001(.a(v), .O(n54));
  nand2 g002(.a(n54), .b(n53), .O(n55));
  inv1  g003(.a(d), .O(n56));
  inv1  g004(.a(t), .O(n57));
  nand2 g005(.a(n57), .b(n56), .O(n58));
  inv1  g006(.a(w), .O(n59));
  nor2  g007(.a(n59), .b(n57), .O(n60));
  inv1  g008(.a(s), .O(n61));
  nand2 g009(.a(u), .b(n61), .O(n62));
  nor2  g010(.a(n62), .b(n60), .O(n63));
  nand2 g011(.a(n63), .b(n58), .O(n64));
  nand2 g012(.a(n64), .b(n55), .O(l0));
  nand2 g013(.a(n59), .b(n53), .O(n66));
  inv1  g014(.a(c), .O(n67));
  nand2 g015(.a(n57), .b(n67), .O(n68));
  inv1  g016(.a(x), .O(n69));
  nor2  g017(.a(n69), .b(n57), .O(n70));
  nor2  g018(.a(n70), .b(n62), .O(n71));
  nand2 g019(.a(n71), .b(n68), .O(n72));
  nand2 g020(.a(n72), .b(n66), .O(m0));
  nand2 g021(.a(n69), .b(n53), .O(n74));
  inv1  g022(.a(b), .O(n75));
  nand2 g023(.a(n57), .b(n75), .O(n76));
  inv1  g024(.a(y), .O(n77));
  nor2  g025(.a(n77), .b(n57), .O(n78));
  nor2  g026(.a(n78), .b(n62), .O(n79));
  nand2 g027(.a(n79), .b(n76), .O(n80));
  nand2 g028(.a(n80), .b(n74), .O(n0));
  nand2 g029(.a(n77), .b(n53), .O(n82));
  inv1  g030(.a(q), .O(n83));
  nand2 g031(.a(t), .b(n83), .O(n84));
  nor2  g032(.a(t), .b(a), .O(n85));
  nor2  g033(.a(n85), .b(n62), .O(n86));
  nand2 g034(.a(n86), .b(n84), .O(n87));
  nand2 g035(.a(n87), .b(n82), .O(o0));
  inv1  g036(.a(z), .O(n89));
  nand2 g037(.a(n89), .b(n53), .O(n90));
  inv1  g038(.a(h), .O(n91));
  nand2 g039(.a(n57), .b(n91), .O(n92));
  inv1  g040(.a(a0), .O(n93));
  nor2  g041(.a(n93), .b(n57), .O(n94));
  nor2  g042(.a(n94), .b(n62), .O(n95));
  nand2 g043(.a(n95), .b(n92), .O(n96));
  nand2 g044(.a(n96), .b(n90), .O(p0));
  nand2 g045(.a(n93), .b(n53), .O(n98));
  inv1  g046(.a(g), .O(n99));
  nand2 g047(.a(n57), .b(n99), .O(n100));
  inv1  g048(.a(b0), .O(n101));
  nor2  g049(.a(n101), .b(n57), .O(n102));
  nor2  g050(.a(n102), .b(n62), .O(n103));
  nand2 g051(.a(n103), .b(n100), .O(n104));
  nand2 g052(.a(n104), .b(n98), .O(q0));
  nand2 g053(.a(n101), .b(n53), .O(n106));
  inv1  g054(.a(f), .O(n107));
  nand2 g055(.a(n57), .b(n107), .O(n108));
  inv1  g056(.a(c0), .O(n109));
  nor2  g057(.a(n109), .b(n57), .O(n110));
  nor2  g058(.a(n110), .b(n62), .O(n111));
  nand2 g059(.a(n111), .b(n108), .O(n112));
  nand2 g060(.a(n112), .b(n106), .O(r0));
  nand2 g061(.a(n109), .b(n53), .O(n114));
  inv1  g062(.a(e), .O(n115));
  nand2 g063(.a(n57), .b(n115), .O(n116));
  nor2  g064(.a(n54), .b(n57), .O(n117));
  nor2  g065(.a(n117), .b(n62), .O(n118));
  nand2 g066(.a(n118), .b(n116), .O(n119));
  nand2 g067(.a(n119), .b(n114), .O(s0));
  inv1  g068(.a(d0), .O(n121));
  nand2 g069(.a(n121), .b(n53), .O(n122));
  inv1  g070(.a(l), .O(n123));
  nand2 g071(.a(n57), .b(n123), .O(n124));
  inv1  g072(.a(e0), .O(n125));
  nor2  g073(.a(n125), .b(n57), .O(n126));
  nor2  g074(.a(n126), .b(n62), .O(n127));
  nand2 g075(.a(n127), .b(n124), .O(n128));
  nand2 g076(.a(n128), .b(n122), .O(t0));
  nand2 g077(.a(n125), .b(n53), .O(n130));
  inv1  g078(.a(k), .O(n131));
  nand2 g079(.a(n57), .b(n131), .O(n132));
  inv1  g080(.a(f0), .O(n133));
  nor2  g081(.a(n133), .b(n57), .O(n134));
  nor2  g082(.a(n134), .b(n62), .O(n135));
  nand2 g083(.a(n135), .b(n132), .O(n136));
  nand2 g084(.a(n136), .b(n130), .O(u0));
  nand2 g085(.a(n133), .b(n53), .O(n138));
  inv1  g086(.a(j), .O(n139));
  nand2 g087(.a(n57), .b(n139), .O(n140));
  inv1  g088(.a(g0), .O(n141));
  nor2  g089(.a(n141), .b(n57), .O(n142));
  nor2  g090(.a(n142), .b(n62), .O(n143));
  nand2 g091(.a(n143), .b(n140), .O(n144));
  nand2 g092(.a(n144), .b(n138), .O(v0));
  nand2 g093(.a(n141), .b(n53), .O(n146));
  inv1  g094(.a(i), .O(n147));
  nand2 g095(.a(n57), .b(n147), .O(n148));
  nor2  g096(.a(n89), .b(n57), .O(n149));
  nor2  g097(.a(n149), .b(n62), .O(n150));
  nand2 g098(.a(n150), .b(n148), .O(n151));
  nand2 g099(.a(n151), .b(n146), .O(w0));
  inv1  g100(.a(h0), .O(n153));
  nand2 g101(.a(n153), .b(n53), .O(n154));
  inv1  g102(.a(p), .O(n155));
  nand2 g103(.a(n57), .b(n155), .O(n156));
  inv1  g104(.a(i0), .O(n157));
  nor2  g105(.a(n157), .b(n57), .O(n158));
  nor2  g106(.a(n158), .b(n62), .O(n159));
  nand2 g107(.a(n159), .b(n156), .O(n160));
  nand2 g108(.a(n160), .b(n154), .O(x0));
  nand2 g109(.a(n157), .b(n53), .O(n162));
  inv1  g110(.a(o), .O(n163));
  nand2 g111(.a(n57), .b(n163), .O(n164));
  inv1  g112(.a(j0), .O(n165));
  nor2  g113(.a(n165), .b(n57), .O(n166));
  nor2  g114(.a(n166), .b(n62), .O(n167));
  nand2 g115(.a(n167), .b(n164), .O(n168));
  nand2 g116(.a(n168), .b(n162), .O(y0));
  nand2 g117(.a(n165), .b(n53), .O(n170));
  inv1  g118(.a(n), .O(n171));
  nand2 g119(.a(n57), .b(n171), .O(n172));
  inv1  g120(.a(k0), .O(n173));
  nor2  g121(.a(n173), .b(n57), .O(n174));
  nor2  g122(.a(n174), .b(n62), .O(n175));
  nand2 g123(.a(n175), .b(n172), .O(n176));
  nand2 g124(.a(n176), .b(n170), .O(z0));
  nand2 g125(.a(n173), .b(n53), .O(n178));
  inv1  g126(.a(m), .O(n179));
  nand2 g127(.a(n57), .b(n179), .O(n180));
  nor2  g128(.a(n121), .b(n57), .O(n181));
  nor2  g129(.a(n181), .b(n62), .O(n182));
  nand2 g130(.a(n182), .b(n180), .O(n183));
  nand2 g131(.a(n183), .b(n178), .O(a1));
endmodule


