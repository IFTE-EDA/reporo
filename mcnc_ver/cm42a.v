// Benchmark "CM42" written by ABC on Tue Nov  5 15:01:19 2019

module CM42 ( 
    a, b, c, d,
    e, f, g, h, i, j, k, l, m, n  );
  input  a, b, c, d;
  output e, f, g, h, i, j, k, l, m, n;
  wire n15, n16, n18, n19, n21, n22, n24, n26, n27, n32, n33, n34;
  nor2  g00(.a(d), .b(c), .O(n15));
  nor2  g01(.a(b), .b(a), .O(n16));
  nand2 g02(.a(n16), .b(n15), .O(e));
  inv1  g03(.a(a), .O(n18));
  nor2  g04(.a(b), .b(n18), .O(n19));
  nand2 g05(.a(n19), .b(n15), .O(f));
  inv1  g06(.a(b), .O(n21));
  nor2  g07(.a(n21), .b(a), .O(n22));
  nand2 g08(.a(n22), .b(n15), .O(g));
  nor2  g09(.a(n21), .b(n18), .O(n24));
  nand2 g10(.a(n24), .b(n15), .O(h));
  inv1  g11(.a(c), .O(n26));
  nor2  g12(.a(d), .b(n26), .O(n27));
  nand2 g13(.a(n27), .b(n16), .O(i));
  nand2 g14(.a(n27), .b(n19), .O(j));
  nand2 g15(.a(n27), .b(n22), .O(k));
  nand2 g16(.a(n27), .b(n24), .O(l));
  inv1  g17(.a(d), .O(n32));
  nand2 g18(.a(n26), .b(n21), .O(n33));
  nor2  g19(.a(n33), .b(n32), .O(n34));
  nand2 g20(.a(n34), .b(n18), .O(m));
  nand2 g21(.a(n34), .b(a), .O(n));
endmodule


