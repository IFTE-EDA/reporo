// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:25 2019

module source_pla  ( 
    GStrap, trapinstr, datapagefINT, winunderflow, winoverflow, SWI,
    intTAGtrap, illegalopc, instrpagefINT, IOINT,
    TRAPreason3, TRAPreason2, TRAPreason1, TRAPreason0  );
  input  GStrap, trapinstr, datapagefINT, winunderflow, winoverflow, SWI,
    intTAGtrap, illegalopc, instrpagefINT, IOINT;
  output TRAPreason3, TRAPreason2, TRAPreason1, TRAPreason0;
  wire n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n30,
    n31, n32, n33, n34, n35, n37, n38, n39, n40, n41, n42, n43, n44, n45,
    n46, n47, n48, n49, n50;
  nor2  g00(.a(IOINT), .b(instrpagefINT), .O(n15));
  nor2  g01(.a(SWI), .b(winoverflow), .O(n16));
  nor2  g02(.a(illegalopc), .b(intTAGtrap), .O(n17));
  nand2 g03(.a(n17), .b(n16), .O(n18));
  inv1  g04(.a(n18), .O(n19));
  inv1  g05(.a(GStrap), .O(n20));
  inv1  g06(.a(trapinstr), .O(n21));
  nand2 g07(.a(n21), .b(n20), .O(n22));
  inv1  g08(.a(datapagefINT), .O(n23));
  inv1  g09(.a(winunderflow), .O(n24));
  nand2 g10(.a(n24), .b(n23), .O(n25));
  nor2  g11(.a(n25), .b(n22), .O(n26));
  nand2 g12(.a(n26), .b(n19), .O(n27));
  nor2  g13(.a(n27), .b(n15), .O(TRAPreason3));
  nor2  g14(.a(n26), .b(n18), .O(TRAPreason2));
  inv1  g15(.a(n17), .O(n30));
  inv1  g16(.a(winoverflow), .O(n31));
  nor2  g17(.a(n25), .b(winoverflow), .O(n32));
  nand2 g18(.a(n32), .b(n22), .O(n33));
  nand2 g19(.a(n33), .b(n31), .O(n34));
  nor2  g20(.a(n34), .b(SWI), .O(n35));
  nor2  g21(.a(n35), .b(n30), .O(TRAPreason1));
  inv1  g22(.a(IOINT), .O(n37));
  nor2  g23(.a(n37), .b(instrpagefINT), .O(n38));
  nor2  g24(.a(n38), .b(GStrap), .O(n39));
  nand2 g25(.a(n23), .b(n21), .O(n40));
  nor2  g26(.a(n40), .b(n39), .O(n41));
  nor2  g27(.a(n41), .b(datapagefINT), .O(n42));
  nand2 g28(.a(n31), .b(n24), .O(n43));
  nor2  g29(.a(n43), .b(n42), .O(n44));
  nor2  g30(.a(n44), .b(winoverflow), .O(n45));
  inv1  g31(.a(SWI), .O(n46));
  inv1  g32(.a(intTAGtrap), .O(n47));
  nand2 g33(.a(n47), .b(n46), .O(n48));
  nor2  g34(.a(n48), .b(n45), .O(n49));
  nor2  g35(.a(n49), .b(intTAGtrap), .O(n50));
  nor2  g36(.a(n50), .b(illegalopc), .O(TRAPreason0));
endmodule


