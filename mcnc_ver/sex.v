// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:27 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7, v8,
    v9.0 , v9.1 , v9.2 , v9.3 , v9.4 , v9.5 , v9.6 , v9.7 , v9.8 ,
    v9.9 , v9.10 , v9.11 , v9.12 , v9.13   );
  input  v0, v1, v2, v3, v4, v5, v6, v7, v8;
  output v9.0 , v9.1 , v9.2 , v9.3 , v9.4 , v9.5 , v9.6 , v9.7 ,
    v9.8 , v9.9 , v9.10 , v9.11 , v9.12 , v9.13 ;
  wire n24, n25, n27, n28, n29, n30, n31, n33, n34, n35, n36, n37, n38, n40,
    n41, n42, n44, n45, n46, n47, n48, n49, n50, n52, n53, n54, n55, n56,
    n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n73,
    n74, n75, n76, n77, n78, n80, n82, n83, n84, n86, n89;
  inv1  g00(.a(v7), .O(n24));
  nand2 g01(.a(n24), .b(v0), .O(n25));
  nor2  g02(.a(n25), .b(v8), .O(v9.0 ));
  nor2  g03(.a(v2), .b(v1), .O(n27));
  nor2  g04(.a(v3), .b(v0), .O(n28));
  inv1  g05(.a(v8), .O(n29));
  nor2  g06(.a(n29), .b(v7), .O(n30));
  nand2 g07(.a(n30), .b(n28), .O(n31));
  nor2  g08(.a(n31), .b(n27), .O(v9.1 ));
  inv1  g09(.a(v5), .O(n33));
  inv1  g10(.a(v6), .O(n34));
  nand2 g11(.a(v8), .b(n34), .O(n35));
  nor2  g12(.a(n35), .b(v4), .O(n36));
  nor2  g13(.a(v8), .b(n34), .O(n37));
  nor2  g14(.a(n37), .b(n36), .O(n38));
  nor2  g15(.a(n38), .b(n33), .O(v9.2 ));
  nand2 g16(.a(v8), .b(v6), .O(n40));
  nor2  g17(.a(v8), .b(v6), .O(n41));
  nand2 g18(.a(n41), .b(n27), .O(n42));
  nand2 g19(.a(n42), .b(n40), .O(v9.3 ));
  inv1  g20(.a(n27), .O(n44));
  inv1  g21(.a(n28), .O(n45));
  nor2  g22(.a(v7), .b(v6), .O(n46));
  inv1  g23(.a(n46), .O(n47));
  nor2  g24(.a(n47), .b(n45), .O(n48));
  nand2 g25(.a(n48), .b(n44), .O(n49));
  nand2 g26(.a(v7), .b(v6), .O(n50));
  nand2 g27(.a(n50), .b(n49), .O(v9.4 ));
  inv1  g28(.a(v4), .O(n52));
  nand2 g29(.a(n24), .b(n52), .O(n53));
  nand2 g30(.a(n53), .b(n34), .O(n54));
  nor2  g31(.a(v7), .b(n34), .O(n55));
  nand2 g32(.a(n55), .b(v0), .O(n56));
  nand2 g33(.a(n56), .b(n54), .O(v9.5 ));
  inv1  g34(.a(n37), .O(n58));
  nand2 g35(.a(n35), .b(v0), .O(n59));
  nand2 g36(.a(n59), .b(n58), .O(n60));
  nand2 g37(.a(n60), .b(n24), .O(n61));
  inv1  g38(.a(v0), .O(n62));
  inv1  g39(.a(v3), .O(n63));
  nand2 g40(.a(v8), .b(n63), .O(n64));
  nand2 g41(.a(n64), .b(v6), .O(n65));
  nand2 g42(.a(n65), .b(n62), .O(n66));
  nor2  g43(.a(n29), .b(v6), .O(n67));
  nor2  g44(.a(n67), .b(v7), .O(n68));
  nand2 g45(.a(n68), .b(n66), .O(n69));
  nand2 g46(.a(n69), .b(v1), .O(n70));
  nand2 g47(.a(n70), .b(n61), .O(v9.6 ));
  inv1  g48(.a(n61), .O(v9.7 ));
  inv1  g49(.a(v1), .O(n73));
  nor2  g50(.a(n29), .b(v3), .O(n74));
  nor2  g51(.a(n74), .b(n34), .O(n75));
  nor2  g52(.a(n75), .b(v0), .O(n76));
  inv1  g53(.a(n68), .O(n77));
  nor2  g54(.a(n77), .b(n76), .O(n78));
  nor2  g55(.a(n78), .b(n73), .O(v9.8 ));
  inv1  g56(.a(v2), .O(n80));
  nor2  g57(.a(n78), .b(n80), .O(v9.9 ));
  nand2 g58(.a(n33), .b(n52), .O(n82));
  nor2  g59(.a(n82), .b(n35), .O(n83));
  nor2  g60(.a(n83), .b(n37), .O(n84));
  nor2  g61(.a(n84), .b(v7), .O(v9.10 ));
  nand2 g62(.a(n46), .b(v8), .O(n86));
  nor2  g63(.a(n86), .b(n82), .O(v9.11 ));
  nor2  g64(.a(n24), .b(v6), .O(v9.12 ));
  inv1  g65(.a(n55), .O(n89));
  nor2  g66(.a(n89), .b(v8), .O(v9.13 ));
endmodule


