// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:23 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5,
    v6.0 , v6.1 , v6.2 , v6.3 , v6.4 , v6.5 , v6.6 , v6.7 , v6.8 ,
    v6.9 , v6.10 , v6.11   );
  input  v0, v1, v2, v3, v4, v5;
  output v6.0 , v6.1 , v6.2 , v6.3 , v6.4 , v6.5 , v6.6 , v6.7 ,
    v6.8 , v6.9 , v6.10 , v6.11 ;
  wire n20, n21, n22, n23, n24, n25, n26, n27, n29, n30, n31, n32, n33, n34,
    n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48,
    n49, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63,
    n64, n66, n67, n68, n69, n70, n71, n72, n74, n75, n77, n78, n79, n80,
    n81, n83, n84, n85, n86, n87, n88, n89, n90, n91, n93, n94, n95, n96,
    n97, n98, n99, n100, n101, n102, n103, n104, n105, n106, n107, n108,
    n109, n110, n111, n113, n114, n115, n116, n117, n118, n119, n120, n121,
    n122, n123, n124, n125, n126;
  inv1  g000(.a(v0), .O(v6.0 ));
  inv1  g001(.a(v1), .O(n20));
  inv1  g002(.a(v2), .O(n21));
  nand2 g003(.a(n21), .b(n20), .O(n22));
  nand2 g004(.a(v5), .b(v4), .O(n23));
  nor2  g005(.a(v2), .b(v1), .O(n24));
  nand2 g006(.a(n24), .b(v3), .O(n25));
  nor2  g007(.a(n25), .b(n23), .O(n26));
  nor2  g008(.a(n26), .b(n22), .O(n27));
  nor2  g009(.a(n27), .b(v0), .O(v6.3 ));
  nor2  g010(.a(n21), .b(v1), .O(n29));
  inv1  g011(.a(n29), .O(n30));
  inv1  g012(.a(v3), .O(n31));
  nand2 g013(.a(v4), .b(n31), .O(n32));
  inv1  g014(.a(v4), .O(n33));
  nand2 g015(.a(n33), .b(v3), .O(n34));
  nand2 g016(.a(n34), .b(n32), .O(n35));
  nand2 g017(.a(n35), .b(n30), .O(n36));
  nor2  g018(.a(n33), .b(n31), .O(n37));
  nand2 g019(.a(n37), .b(n29), .O(n38));
  nand2 g020(.a(n38), .b(n36), .O(n39));
  nor2  g021(.a(n33), .b(v3), .O(n40));
  nor2  g022(.a(v4), .b(n31), .O(n41));
  nor2  g023(.a(n41), .b(n40), .O(n42));
  nand2 g024(.a(n42), .b(v1), .O(n43));
  nor2  g025(.a(v5), .b(n33), .O(n44));
  inv1  g026(.a(n44), .O(n45));
  nor2  g027(.a(n45), .b(n25), .O(n46));
  inv1  g028(.a(n46), .O(n47));
  nand2 g029(.a(n47), .b(n43), .O(n48));
  nor2  g030(.a(n48), .b(n39), .O(n49));
  nor2  g031(.a(n49), .b(v0), .O(v6.4 ));
  nor2  g032(.a(n35), .b(n21), .O(n51));
  inv1  g033(.a(n37), .O(n52));
  nor2  g034(.a(n52), .b(v2), .O(n53));
  nor2  g035(.a(n53), .b(n51), .O(n54));
  nor2  g036(.a(n54), .b(n20), .O(n55));
  nor2  g037(.a(n42), .b(n21), .O(n56));
  inv1  g038(.a(n56), .O(n57));
  nand2 g039(.a(n41), .b(n21), .O(n58));
  nor2  g040(.a(v4), .b(v3), .O(n59));
  nand2 g041(.a(n59), .b(n24), .O(n60));
  nand2 g042(.a(n60), .b(n58), .O(n61));
  nor2  g043(.a(n61), .b(n46), .O(n62));
  nand2 g044(.a(n62), .b(n57), .O(n63));
  nor2  g045(.a(n63), .b(n55), .O(n64));
  nor2  g046(.a(n64), .b(v0), .O(v6.5 ));
  nor2  g047(.a(v1), .b(v0), .O(n66));
  inv1  g048(.a(n66), .O(n67));
  nor2  g049(.a(v3), .b(v2), .O(n68));
  inv1  g050(.a(v5), .O(n69));
  nand2 g051(.a(n69), .b(n33), .O(n70));
  inv1  g052(.a(n70), .O(n71));
  nand2 g053(.a(n71), .b(n68), .O(n72));
  nor2  g054(.a(n72), .b(n67), .O(v6.6 ));
  inv1  g055(.a(n23), .O(n74));
  nand2 g056(.a(n68), .b(n66), .O(n75));
  nor2  g057(.a(n75), .b(n74), .O(v6.7 ));
  nor2  g058(.a(n69), .b(n31), .O(n77));
  nor2  g059(.a(v5), .b(v3), .O(n78));
  nor2  g060(.a(n78), .b(n77), .O(n79));
  nor2  g061(.a(n79), .b(n33), .O(n80));
  nand2 g062(.a(n66), .b(n21), .O(n81));
  nor2  g063(.a(n81), .b(n80), .O(v6.8 ));
  nand2 g064(.a(n70), .b(n23), .O(n83));
  inv1  g065(.a(n83), .O(n84));
  nor2  g066(.a(n31), .b(n21), .O(n85));
  nor2  g067(.a(n85), .b(n84), .O(n86));
  nor2  g068(.a(n83), .b(v3), .O(n87));
  nor2  g069(.a(n70), .b(n31), .O(n88));
  nor2  g070(.a(n88), .b(n87), .O(n89));
  nor2  g071(.a(n89), .b(n21), .O(n90));
  nor2  g072(.a(n90), .b(n86), .O(n91));
  nor2  g073(.a(n91), .b(n67), .O(v6.9 ));
  nor2  g074(.a(n70), .b(v3), .O(n93));
  nor2  g075(.a(n93), .b(n77), .O(n94));
  nor2  g076(.a(n94), .b(v1), .O(n95));
  nand2 g077(.a(v3), .b(v1), .O(n96));
  nor2  g078(.a(n96), .b(n70), .O(n97));
  nor2  g079(.a(n97), .b(n95), .O(n98));
  nor2  g080(.a(n98), .b(v2), .O(n99));
  nand2 g081(.a(n33), .b(n20), .O(n100));
  nand2 g082(.a(n100), .b(n21), .O(n101));
  nand2 g083(.a(n29), .b(n33), .O(n102));
  nand2 g084(.a(n102), .b(n101), .O(n103));
  nand2 g085(.a(n103), .b(n31), .O(n104));
  inv1  g086(.a(n38), .O(n105));
  nor2  g087(.a(n69), .b(v4), .O(n106));
  nand2 g088(.a(n106), .b(v3), .O(n107));
  nor2  g089(.a(n107), .b(n30), .O(n108));
  nor2  g090(.a(n108), .b(n105), .O(n109));
  nand2 g091(.a(n109), .b(n104), .O(n110));
  nor2  g092(.a(n110), .b(n99), .O(n111));
  nor2  g093(.a(n111), .b(v0), .O(v6.10 ));
  nor2  g094(.a(n56), .b(n53), .O(n113));
  nor2  g095(.a(n113), .b(n20), .O(n114));
  nand2 g096(.a(n44), .b(n31), .O(n115));
  nand2 g097(.a(n115), .b(n107), .O(n116));
  nand2 g098(.a(n116), .b(n29), .O(n117));
  nand2 g099(.a(n21), .b(v1), .O(n118));
  nor2  g100(.a(n118), .b(n107), .O(n119));
  nand2 g101(.a(v2), .b(v1), .O(n120));
  nand2 g102(.a(n120), .b(n22), .O(n121));
  nand2 g103(.a(n121), .b(n42), .O(n122));
  nand2 g104(.a(n122), .b(n38), .O(n123));
  nor2  g105(.a(n123), .b(n119), .O(n124));
  nand2 g106(.a(n124), .b(n117), .O(n125));
  nor2  g107(.a(n125), .b(n114), .O(n126));
  nor2  g108(.a(n126), .b(v0), .O(v6.11 ));
  inv1  g109(.a(v0), .O(v6.1 ));
  inv1  g110(.a(v0), .O(v6.2 ));
endmodule


