# Technology information.

# although the LEF defines a site_width of 0.104, encounter seems to snap everything to 0.1
site_width = 0.104
site_width_format = '.3f'
site_height = 0.72
site_height_format = '.2f'

# the placement grid depends on metal1 pitch
grid_width = 0.08
grid_width_format = '.2f'
grid_height = 0.1
grid_height_format = '.1f'

def snap_to_manufacturing_grid(n):
    return math.ceil(n/0.1) * 0.1

# area of gates
area = {}
area['buf1'] = 0.728 * 0.72
area['inv1'] = 0.416 * 0.72
area['min3A'] = 1.144 * 0.72
area['min3A_PSO'] = 0.832 * 0.72
area['min3A_SPR'] = 0.832 * 0.72
area['min3B'] = 1.456 * 0.72
area['mux2'] = 1.352 * 0.72
area['nand2'] = 0.728 * 0.72
area['nor2'] = 0.728 * 0.72
area['xor2'] = 1.768 * 0.72
area['driv1A'] = 0.728 * 0.72
area['driv1B_SPR'] = 0.728 * 0.72

# static power consumption of gates in pW
static_power = {}
static_power['buf1'] = 78.1989179
static_power['inv1'] = 39.0394395
static_power['min3A'] = 108.942231
static_power['min3A_PSO'] = static_power['min3A'] - static_power['inv1']
static_power['min3A_SPR'] = static_power['min3A'] - static_power['inv1']
static_power['min3B'] = 108.942231
static_power['mux2'] = 126.939132
static_power['nand2'] = 53.224133
static_power['nor2'] = 78.0785811
static_power['xor2'] = 160.09392
static_power['driv1A'] = static_power['buf1']
static_power['driv1B_SPR'] = static_power['buf1']
