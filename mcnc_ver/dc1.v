// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:20 2019

module source_pla  ( 
    v0, v1, v2, v3,
    v4.0 , v4.1 , v4.2 , v4.3 , v4.4 , v4.5 , v4.6   );
  input  v0, v1, v2, v3;
  output v4.0 , v4.1 , v4.2 , v4.3 , v4.4 , v4.5 , v4.6 ;
  wire n12, n13, n14, n15, n16, n17, n18, n19, n20, n22, n23, n24, n26, n27,
    n28, n29, n30, n31, n32, n34, n35, n36, n37, n38, n39, n40, n41, n42,
    n44, n45, n46, n47, n48, n49, n51, n52, n53, n54, n55, n57, n58;
  inv1  g00(.a(v0), .O(n12));
  nand2 g01(.a(v3), .b(v2), .O(n13));
  nand2 g02(.a(n13), .b(v1), .O(n14));
  nor2  g03(.a(v3), .b(v2), .O(n15));
  inv1  g04(.a(n15), .O(n16));
  nand2 g05(.a(n16), .b(n14), .O(n17));
  nand2 g06(.a(n17), .b(n12), .O(n18));
  nor2  g07(.a(v2), .b(v1), .O(n19));
  nand2 g08(.a(n19), .b(v0), .O(n20));
  nand2 g09(.a(n20), .b(n18), .O(v4.0 ));
  nand2 g10(.a(v2), .b(n12), .O(n22));
  inv1  g11(.a(n22), .O(n23));
  nor2  g12(.a(n23), .b(n19), .O(n24));
  nor2  g13(.a(n24), .b(v3), .O(v4.1 ));
  inv1  g14(.a(v1), .O(n26));
  nand2 g15(.a(v2), .b(n26), .O(n27));
  nand2 g16(.a(v3), .b(v1), .O(n28));
  nand2 g17(.a(n28), .b(n27), .O(n29));
  nand2 g18(.a(n29), .b(n12), .O(n30));
  nand2 g19(.a(v3), .b(n12), .O(n31));
  nand2 g20(.a(n31), .b(n19), .O(n32));
  nand2 g21(.a(n32), .b(n30), .O(v4.2 ));
  inv1  g22(.a(v2), .O(n34));
  nand2 g23(.a(n34), .b(v0), .O(n35));
  nand2 g24(.a(n35), .b(n22), .O(n36));
  nand2 g25(.a(n36), .b(n26), .O(n37));
  nand2 g26(.a(n34), .b(v1), .O(n38));
  inv1  g27(.a(v3), .O(n39));
  nand2 g28(.a(n39), .b(v2), .O(n40));
  nand2 g29(.a(n40), .b(n38), .O(n41));
  nand2 g30(.a(n41), .b(n12), .O(n42));
  nand2 g31(.a(n42), .b(n37), .O(v4.3 ));
  nand2 g32(.a(n22), .b(n16), .O(n44));
  nand2 g33(.a(n44), .b(n26), .O(n45));
  nor2  g34(.a(v2), .b(n26), .O(n46));
  nand2 g35(.a(n46), .b(v3), .O(n47));
  nand2 g36(.a(n47), .b(n40), .O(n48));
  nand2 g37(.a(n48), .b(n12), .O(n49));
  nand2 g38(.a(n49), .b(n45), .O(v4.4 ));
  inv1  g39(.a(n19), .O(n51));
  nor2  g40(.a(n39), .b(n34), .O(n52));
  nor2  g41(.a(n15), .b(n52), .O(n53));
  nand2 g42(.a(n53), .b(v1), .O(n54));
  nand2 g43(.a(n54), .b(n12), .O(n55));
  nand2 g44(.a(n55), .b(n51), .O(v4.5 ));
  nand2 g45(.a(n39), .b(n26), .O(n57));
  nand2 g46(.a(n57), .b(n12), .O(n58));
  nand2 g47(.a(n58), .b(n51), .O(v4.6 ));
endmodule


