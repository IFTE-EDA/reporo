// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:25 2019

module source_pla  ( 
    CPIPE1s<9> , CPIPE1s<0> , CPIPE1s<1> , CPIPE1s<2> , CPIPE1s<3> ,
    CPIPE1s<4> , CPIPE1s<5> , CPIPE1s<7> ,
    pillegalopc  );
  input  CPIPE1s<9> , CPIPE1s<0> , CPIPE1s<1> , CPIPE1s<2> ,
    CPIPE1s<3> , CPIPE1s<4> , CPIPE1s<5> , CPIPE1s<7> ;
  output pillegalopc;
  wire n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23,
    n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37,
    n38;
  inv1  g00(.a(CPIPE1s<9> ), .O(n10));
  inv1  g01(.a(CPIPE1s<2> ), .O(n11));
  inv1  g02(.a(CPIPE1s<1> ), .O(n12));
  nor2  g03(.a(CPIPE1s<4> ), .b(n12), .O(n13));
  nand2 g04(.a(n13), .b(CPIPE1s<5> ), .O(n14));
  inv1  g05(.a(CPIPE1s<5> ), .O(n15));
  inv1  g06(.a(CPIPE1s<3> ), .O(n16));
  inv1  g07(.a(CPIPE1s<4> ), .O(n17));
  nor2  g08(.a(n17), .b(n16), .O(n18));
  nand2 g09(.a(n18), .b(n15), .O(n19));
  nand2 g10(.a(n19), .b(n14), .O(n20));
  nand2 g11(.a(n20), .b(n11), .O(n21));
  nor2  g12(.a(CPIPE1s<4> ), .b(CPIPE1s<3> ), .O(n22));
  nor2  g13(.a(n22), .b(n18), .O(n23));
  nand2 g14(.a(n15), .b(CPIPE1s<1> ), .O(n24));
  nor2  g15(.a(n24), .b(n23), .O(n25));
  nor2  g16(.a(n16), .b(n11), .O(n26));
  nor2  g17(.a(CPIPE1s<3> ), .b(CPIPE1s<1> ), .O(n27));
  nor2  g18(.a(n27), .b(n26), .O(n28));
  nand2 g19(.a(CPIPE1s<5> ), .b(n17), .O(n29));
  nor2  g20(.a(n29), .b(n28), .O(n30));
  nor2  g21(.a(n30), .b(n25), .O(n31));
  nand2 g22(.a(n31), .b(n21), .O(n32));
  nand2 g23(.a(n32), .b(CPIPE1s<0> ), .O(n33));
  nor2  g24(.a(CPIPE1s<3> ), .b(CPIPE1s<2> ), .O(n34));
  nor2  g25(.a(CPIPE1s<5> ), .b(CPIPE1s<4> ), .O(n35));
  nand2 g26(.a(n35), .b(n34), .O(n36));
  nand2 g27(.a(n36), .b(n33), .O(n37));
  nand2 g28(.a(n37), .b(CPIPE1s<7> ), .O(n38));
  nand2 g29(.a(n38), .b(n10), .O(pillegalopc));
endmodule


