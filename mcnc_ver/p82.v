// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:25 2019

module source_pla  ( 
    v0, v1, v2, v3, v4,
    v5.0 , v5.1 , v5.2 , v5.3 , v5.4 , v5.5 , v5.6 , v5.7 , v5.8 ,
    v5.9 , v5.10 , v5.11 , v5.12 , v5.13   );
  input  v0, v1, v2, v3, v4;
  output v5.0 , v5.1 , v5.2 , v5.3 , v5.4 , v5.5 , v5.6 , v5.7 ,
    v5.8 , v5.9 , v5.10 , v5.11 , v5.12 , v5.13 ;
  wire n20, n21, n22, n23, n24, n25, n27, n28, n29, n30, n31, n32, n35, n36,
    n37, n38, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n51, n52,
    n53, n54, n55, n56, n57, n58, n59, n61, n62, n63, n64, n65, n66, n67,
    n68, n69, n70, n71, n72, n73, n74, n76, n77, n78, n79, n80, n81, n82,
    n83, n84, n85, n86, n87, n88, n90, n91, n92, n93, n94, n95, n96, n98,
    n99, n100, n101, n102, n103, n104, n105, n107, n108, n109, n110, n112,
    n113, n114, n115, n117, n118, n119, n120, n121, n122, n123, n124, n125,
    n126, n127, n128, n129, n131, n132, n133, n134, n135, n136, n137, n138,
    n139, n140, n141, n142;
  nor2  g000(.a(v1), .b(v0), .O(n20));
  inv1  g001(.a(n20), .O(n21));
  nor2  g002(.a(v3), .b(v2), .O(n22));
  inv1  g003(.a(n22), .O(n23));
  nor2  g004(.a(n23), .b(v4), .O(n24));
  inv1  g005(.a(n24), .O(n25));
  nor2  g006(.a(n25), .b(n21), .O(v5.0 ));
  inv1  g007(.a(v4), .O(n27));
  inv1  g008(.a(v1), .O(n28));
  nor2  g009(.a(n28), .b(v0), .O(n29));
  inv1  g010(.a(v3), .O(n30));
  nor2  g011(.a(n30), .b(v2), .O(n31));
  nand2 g012(.a(n31), .b(n29), .O(n32));
  nor2  g013(.a(n32), .b(n27), .O(v5.1 ));
  nor2  g014(.a(n32), .b(v4), .O(v5.2 ));
  nand2 g015(.a(n28), .b(v0), .O(n35));
  nand2 g016(.a(v3), .b(v2), .O(n36));
  inv1  g017(.a(n36), .O(n37));
  nand2 g018(.a(n37), .b(v4), .O(n38));
  nor2  g019(.a(n38), .b(n35), .O(v5.3 ));
  inv1  g020(.a(v2), .O(n40));
  nand2 g021(.a(n29), .b(n40), .O(n41));
  nand2 g022(.a(n41), .b(v1), .O(n42));
  nand2 g023(.a(n42), .b(v4), .O(n43));
  nor2  g024(.a(v4), .b(v2), .O(n44));
  nand2 g025(.a(n44), .b(n29), .O(n45));
  nand2 g026(.a(n45), .b(n43), .O(n46));
  nand2 g027(.a(n46), .b(v3), .O(n47));
  inv1  g028(.a(n35), .O(n48));
  nand2 g029(.a(n48), .b(n24), .O(n49));
  nand2 g030(.a(n49), .b(n47), .O(v5.4 ));
  inv1  g031(.a(v5.2 ), .O(n51));
  nor2  g032(.a(n27), .b(n30), .O(n52));
  inv1  g033(.a(n52), .O(n53));
  nor2  g034(.a(v4), .b(v3), .O(n54));
  inv1  g035(.a(v0), .O(n55));
  nor2  g036(.a(v2), .b(n55), .O(n56));
  nand2 g037(.a(n56), .b(n54), .O(n57));
  nand2 g038(.a(n57), .b(n53), .O(n58));
  nand2 g039(.a(n58), .b(n28), .O(n59));
  nand2 g040(.a(n59), .b(n51), .O(v5.5 ));
  nand2 g041(.a(v4), .b(n28), .O(n61));
  nand2 g042(.a(n27), .b(v1), .O(n62));
  nand2 g043(.a(n62), .b(n61), .O(n63));
  nand2 g044(.a(n36), .b(n23), .O(n64));
  nand2 g045(.a(n64), .b(n63), .O(n65));
  nand2 g046(.a(n40), .b(n28), .O(n66));
  nand2 g047(.a(n66), .b(v4), .O(n67));
  nand2 g048(.a(n44), .b(n28), .O(n68));
  nand2 g049(.a(n68), .b(n67), .O(n69));
  nand2 g050(.a(n69), .b(n30), .O(n70));
  nand2 g051(.a(n70), .b(n65), .O(n71));
  nand2 g052(.a(n71), .b(n55), .O(n72));
  nand2 g053(.a(n23), .b(n27), .O(n73));
  nand2 g054(.a(n73), .b(n48), .O(n74));
  nand2 g055(.a(n74), .b(n72), .O(v5.6 ));
  inv1  g056(.a(n56), .O(n76));
  nand2 g057(.a(n76), .b(n28), .O(n77));
  nand2 g058(.a(v1), .b(n55), .O(n78));
  nor2  g059(.a(n78), .b(v2), .O(n79));
  nand2 g060(.a(n27), .b(v2), .O(n80));
  nor2  g061(.a(n80), .b(n78), .O(n81));
  nor2  g062(.a(n81), .b(n79), .O(n82));
  nand2 g063(.a(n82), .b(n77), .O(n83));
  nand2 g064(.a(n83), .b(n30), .O(n84));
  nor2  g065(.a(n27), .b(v1), .O(n85));
  nand2 g066(.a(n85), .b(n76), .O(n86));
  nand2 g067(.a(n86), .b(n45), .O(n87));
  nand2 g068(.a(n87), .b(v3), .O(n88));
  nand2 g069(.a(n88), .b(n84), .O(v5.7 ));
  nand2 g070(.a(n35), .b(n78), .O(n90));
  nand2 g071(.a(n90), .b(n30), .O(n91));
  nand2 g072(.a(n52), .b(n55), .O(n92));
  nand2 g073(.a(n92), .b(n91), .O(n93));
  nand2 g074(.a(n93), .b(n40), .O(n94));
  nor2  g075(.a(n40), .b(v0), .O(n95));
  nand2 g076(.a(n95), .b(n63), .O(n96));
  nand2 g077(.a(n96), .b(n94), .O(v5.8 ));
  nand2 g078(.a(v4), .b(n30), .O(n98));
  nor2  g079(.a(n98), .b(n28), .O(n99));
  nand2 g080(.a(n27), .b(v3), .O(n100));
  nor2  g081(.a(n100), .b(v1), .O(n101));
  nor2  g082(.a(n101), .b(n99), .O(n102));
  nor2  g083(.a(n102), .b(v0), .O(n103));
  nor2  g084(.a(n100), .b(n35), .O(n104));
  nor2  g085(.a(n104), .b(n103), .O(n105));
  nor2  g086(.a(n105), .b(v2), .O(v5.9 ));
  nor2  g087(.a(n27), .b(v3), .O(n107));
  nor2  g088(.a(v4), .b(n30), .O(n108));
  nor2  g089(.a(n108), .b(n107), .O(n109));
  nand2 g090(.a(n20), .b(n40), .O(n110));
  nor2  g091(.a(n110), .b(n109), .O(v5.10 ));
  nor2  g092(.a(v4), .b(n55), .O(n112));
  nor2  g093(.a(n112), .b(v1), .O(n113));
  nor2  g094(.a(n62), .b(v0), .O(n114));
  nor2  g095(.a(n114), .b(n113), .O(n115));
  nor2  g096(.a(n115), .b(n23), .O(v5.11 ));
  inv1  g097(.a(n109), .O(n117));
  nand2 g098(.a(n29), .b(v2), .O(n118));
  nand2 g099(.a(n48), .b(n40), .O(n119));
  nand2 g100(.a(n119), .b(n118), .O(n120));
  nand2 g101(.a(n120), .b(n117), .O(n121));
  nor2  g102(.a(v4), .b(n28), .O(n122));
  nor2  g103(.a(n122), .b(n85), .O(n123));
  nand2 g104(.a(n123), .b(v2), .O(n124));
  nand2 g105(.a(n124), .b(n55), .O(n125));
  nor2  g106(.a(n27), .b(n40), .O(n126));
  nand2 g107(.a(n126), .b(n48), .O(n127));
  nand2 g108(.a(n127), .b(n125), .O(n128));
  nand2 g109(.a(n128), .b(n30), .O(n129));
  nand2 g110(.a(n129), .b(n121), .O(v5.12 ));
  nand2 g111(.a(n107), .b(n40), .O(n131));
  nand2 g112(.a(n108), .b(v2), .O(n132));
  nand2 g113(.a(n132), .b(n131), .O(n133));
  nand2 g114(.a(n133), .b(v0), .O(n134));
  nand2 g115(.a(n30), .b(v2), .O(n135));
  nor2  g116(.a(v4), .b(v0), .O(n136));
  nand2 g117(.a(n136), .b(n135), .O(n137));
  nand2 g118(.a(n137), .b(n134), .O(n138));
  nand2 g119(.a(n138), .b(n28), .O(n139));
  nand2 g120(.a(n109), .b(v2), .O(n140));
  nand2 g121(.a(n140), .b(n25), .O(n141));
  nand2 g122(.a(n141), .b(n29), .O(n142));
  nand2 g123(.a(n142), .b(n139), .O(v5.13 ));
endmodule


