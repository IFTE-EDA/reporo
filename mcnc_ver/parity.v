// Benchmark "PARITYFDS" written by ABC on Tue Nov  5 15:01:26 2019

module PARITYFDS ( 
    a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p,
    q  );
  input  a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p;
  output q;
  wire n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31,
    n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45,
    n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59,
    n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73,
    n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87,
    n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101,
    n102, n103, n104, n105, n106, n107, n108, n109, n110, n111, n112, n113,
    n114, n115, n116, n117, n118, n119;
  inv1  g000(.a(o), .O(n18));
  nor2  g001(.a(p), .b(n18), .O(n19));
  inv1  g002(.a(p), .O(n20));
  nor2  g003(.a(n20), .b(o), .O(n21));
  nor2  g004(.a(n21), .b(n19), .O(n22));
  inv1  g005(.a(n), .O(n23));
  nand2 g006(.a(n23), .b(m), .O(n24));
  inv1  g007(.a(m), .O(n25));
  nand2 g008(.a(n), .b(n25), .O(n26));
  nand2 g009(.a(n26), .b(n24), .O(n27));
  nand2 g010(.a(n27), .b(n22), .O(n28));
  nand2 g011(.a(n20), .b(o), .O(n29));
  nand2 g012(.a(p), .b(n18), .O(n30));
  nand2 g013(.a(n30), .b(n29), .O(n31));
  nor2  g014(.a(n), .b(n25), .O(n32));
  nor2  g015(.a(n23), .b(m), .O(n33));
  nor2  g016(.a(n33), .b(n32), .O(n34));
  nand2 g017(.a(n34), .b(n31), .O(n35));
  nand2 g018(.a(n35), .b(n28), .O(n36));
  inv1  g019(.a(l), .O(n37));
  nand2 g020(.a(n37), .b(k), .O(n38));
  inv1  g021(.a(k), .O(n39));
  nand2 g022(.a(l), .b(n39), .O(n40));
  nand2 g023(.a(n40), .b(n38), .O(n41));
  inv1  g024(.a(i), .O(n42));
  nor2  g025(.a(j), .b(n42), .O(n43));
  inv1  g026(.a(j), .O(n44));
  nor2  g027(.a(n44), .b(i), .O(n45));
  nor2  g028(.a(n45), .b(n43), .O(n46));
  nor2  g029(.a(n46), .b(n41), .O(n47));
  nor2  g030(.a(l), .b(n39), .O(n48));
  nor2  g031(.a(n37), .b(k), .O(n49));
  nor2  g032(.a(n49), .b(n48), .O(n50));
  nand2 g033(.a(n44), .b(i), .O(n51));
  nand2 g034(.a(j), .b(n42), .O(n52));
  nand2 g035(.a(n52), .b(n51), .O(n53));
  nor2  g036(.a(n53), .b(n50), .O(n54));
  nor2  g037(.a(n54), .b(n47), .O(n55));
  nor2  g038(.a(n55), .b(n36), .O(n56));
  nor2  g039(.a(n34), .b(n31), .O(n57));
  nor2  g040(.a(n27), .b(n22), .O(n58));
  nor2  g041(.a(n58), .b(n57), .O(n59));
  nand2 g042(.a(n53), .b(n50), .O(n60));
  nand2 g043(.a(n46), .b(n41), .O(n61));
  nand2 g044(.a(n61), .b(n60), .O(n62));
  nor2  g045(.a(n62), .b(n59), .O(n63));
  nor2  g046(.a(n63), .b(n56), .O(n64));
  inv1  g047(.a(h), .O(n65));
  nand2 g048(.a(n65), .b(g), .O(n66));
  inv1  g049(.a(g), .O(n67));
  nand2 g050(.a(h), .b(n67), .O(n68));
  nand2 g051(.a(n68), .b(n66), .O(n69));
  inv1  g052(.a(e), .O(n70));
  nor2  g053(.a(f), .b(n70), .O(n71));
  inv1  g054(.a(f), .O(n72));
  nor2  g055(.a(n72), .b(e), .O(n73));
  nor2  g056(.a(n73), .b(n71), .O(n74));
  nor2  g057(.a(n74), .b(n69), .O(n75));
  nor2  g058(.a(h), .b(n67), .O(n76));
  nor2  g059(.a(n65), .b(g), .O(n77));
  nor2  g060(.a(n77), .b(n76), .O(n78));
  nand2 g061(.a(n72), .b(e), .O(n79));
  nand2 g062(.a(f), .b(n70), .O(n80));
  nand2 g063(.a(n80), .b(n79), .O(n81));
  nor2  g064(.a(n81), .b(n78), .O(n82));
  nor2  g065(.a(n82), .b(n75), .O(n83));
  inv1  g066(.a(c), .O(n84));
  nor2  g067(.a(d), .b(n84), .O(n85));
  inv1  g068(.a(d), .O(n86));
  nor2  g069(.a(n86), .b(c), .O(n87));
  nor2  g070(.a(n87), .b(n85), .O(n88));
  inv1  g071(.a(b), .O(n89));
  nand2 g072(.a(n89), .b(a), .O(n90));
  inv1  g073(.a(a), .O(n91));
  nand2 g074(.a(b), .b(n91), .O(n92));
  nand2 g075(.a(n92), .b(n90), .O(n93));
  nand2 g076(.a(n93), .b(n88), .O(n94));
  nand2 g077(.a(n86), .b(c), .O(n95));
  nand2 g078(.a(d), .b(n84), .O(n96));
  nand2 g079(.a(n96), .b(n95), .O(n97));
  nor2  g080(.a(b), .b(n91), .O(n98));
  nor2  g081(.a(n89), .b(a), .O(n99));
  nor2  g082(.a(n99), .b(n98), .O(n100));
  nand2 g083(.a(n100), .b(n97), .O(n101));
  nand2 g084(.a(n101), .b(n94), .O(n102));
  nand2 g085(.a(n102), .b(n83), .O(n103));
  nand2 g086(.a(n81), .b(n78), .O(n104));
  nand2 g087(.a(n74), .b(n69), .O(n105));
  nand2 g088(.a(n105), .b(n104), .O(n106));
  nor2  g089(.a(n100), .b(n97), .O(n107));
  nor2  g090(.a(n93), .b(n88), .O(n108));
  nor2  g091(.a(n108), .b(n107), .O(n109));
  nand2 g092(.a(n109), .b(n106), .O(n110));
  nand2 g093(.a(n110), .b(n103), .O(n111));
  nand2 g094(.a(n111), .b(n64), .O(n112));
  nand2 g095(.a(n62), .b(n59), .O(n113));
  nand2 g096(.a(n55), .b(n36), .O(n114));
  nand2 g097(.a(n114), .b(n113), .O(n115));
  nor2  g098(.a(n109), .b(n106), .O(n116));
  nor2  g099(.a(n102), .b(n83), .O(n117));
  nor2  g100(.a(n117), .b(n116), .O(n118));
  nand2 g101(.a(n118), .b(n115), .O(n119));
  nand2 g102(.a(n119), .b(n112), .O(q));
endmodule


