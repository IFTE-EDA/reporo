// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:25 2019

module source_pla  ( 
    CPIPE1s<7> , SRC1s<0> , SRC1s<1> , SRC1s<2> , SRC1s<3> ,
    SRC1s<4> , SRC1equalDST2, DSTvalid, pbusDtoINA, SRC2equal16,
    SRC2equalDST2, opc2load,
    readRFaccessA1, readRFaccessB1, AIzero1, AIzeroforce, busDtobusAa,
    DSTtobusDa2, preadTBtoA, preadSWPtoA, pForwardtoINB, preadPCtoA  );
  input  CPIPE1s<7> , SRC1s<0> , SRC1s<1> , SRC1s<2> , SRC1s<3> ,
    SRC1s<4> , SRC1equalDST2, DSTvalid, pbusDtoINA, SRC2equal16,
    SRC2equalDST2, opc2load;
  output readRFaccessA1, readRFaccessB1, AIzero1, AIzeroforce, busDtobusAa,
    DSTtobusDa2, preadTBtoA, preadSWPtoA, pForwardtoINB, preadPCtoA;
  wire n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n38,
    n39, n40, n41, n42, n44, n45, n46, n47, n48, n49, n50, n51, n53, n54,
    n55, n56, n57, n58, n59, n60, n61, n62, n64, n65, n66, n67, n68, n69,
    n70, n71, n73, n74, n75, n76, n77, n79, n80, n81, n83, n84, n85, n87;
  inv1  g00(.a(CPIPE1s<7> ), .O(AIzeroforce));
  inv1  g01(.a(SRC1s<4> ), .O(n24));
  nor2  g02(.a(pbusDtoINA), .b(n24), .O(n25));
  inv1  g03(.a(n25), .O(n26));
  nor2  g04(.a(SRC1s<1> ), .b(SRC1s<0> ), .O(n27));
  nor2  g05(.a(SRC1s<3> ), .b(SRC1s<2> ), .O(n28));
  nand2 g06(.a(n28), .b(n27), .O(n29));
  nor2  g07(.a(n29), .b(n26), .O(n30));
  inv1  g08(.a(pbusDtoINA), .O(n31));
  inv1  g09(.a(SRC1equalDST2), .O(n32));
  inv1  g10(.a(DSTvalid), .O(n33));
  nor2  g11(.a(n33), .b(n32), .O(n34));
  nand2 g12(.a(n34), .b(n31), .O(n35));
  nor2  g13(.a(n35), .b(n30), .O(n36));
  nor2  g14(.a(n36), .b(AIzeroforce), .O(readRFaccessA1));
  inv1  g15(.a(SRC2equalDST2), .O(n38));
  nor2  g16(.a(n38), .b(SRC2equal16), .O(n39));
  inv1  g17(.a(n39), .O(n40));
  nand2 g18(.a(n31), .b(DSTvalid), .O(n41));
  nor2  g19(.a(n41), .b(n40), .O(n42));
  nor2  g20(.a(n42), .b(AIzeroforce), .O(readRFaccessB1));
  inv1  g21(.a(SRC1s<1> ), .O(n44));
  inv1  g22(.a(SRC1s<2> ), .O(n45));
  nand2 g23(.a(n45), .b(n44), .O(n46));
  nor2  g24(.a(n46), .b(SRC1s<3> ), .O(n47));
  nor2  g25(.a(SRC1s<0> ), .b(AIzeroforce), .O(n48));
  inv1  g26(.a(n48), .O(n49));
  nor2  g27(.a(n49), .b(n26), .O(n50));
  nand2 g28(.a(n50), .b(n47), .O(n51));
  nand2 g29(.a(n51), .b(CPIPE1s<7> ), .O(AIzero1));
  nor2  g30(.a(SRC1s<2> ), .b(SRC1s<0> ), .O(n53));
  nor2  g31(.a(SRC1s<3> ), .b(SRC1s<1> ), .O(n54));
  nand2 g32(.a(n54), .b(SRC1s<4> ), .O(n55));
  nor2  g33(.a(n55), .b(n53), .O(n56));
  inv1  g34(.a(n55), .O(n57));
  inv1  g35(.a(opc2load), .O(n58));
  nand2 g36(.a(n34), .b(n58), .O(n59));
  nor2  g37(.a(n59), .b(n57), .O(n60));
  nor2  g38(.a(n60), .b(n56), .O(n61));
  nand2 g39(.a(n31), .b(CPIPE1s<7> ), .O(n62));
  nor2  g40(.a(n62), .b(n61), .O(busDtobusAa));
  nand2 g41(.a(n55), .b(n31), .O(n64));
  nand2 g42(.a(n64), .b(n53), .O(n65));
  nand2 g43(.a(n65), .b(SRC1equalDST2), .O(n66));
  nand2 g44(.a(n39), .b(n31), .O(n67));
  nand2 g45(.a(n67), .b(n66), .O(n68));
  nand2 g46(.a(DSTvalid), .b(CPIPE1s<7> ), .O(n69));
  nor2  g47(.a(n69), .b(opc2load), .O(n70));
  nand2 g48(.a(n70), .b(n68), .O(n71));
  nand2 g49(.a(n71), .b(n31), .O(DSTtobusDa2));
  inv1  g50(.a(SRC1s<0> ), .O(n73));
  nor2  g51(.a(n73), .b(AIzeroforce), .O(n74));
  nand2 g52(.a(n74), .b(n44), .O(n75));
  nor2  g53(.a(SRC1s<3> ), .b(n45), .O(n76));
  nand2 g54(.a(n76), .b(n25), .O(n77));
  nor2  g55(.a(n77), .b(n75), .O(preadTBtoA));
  nor2  g56(.a(n24), .b(n45), .O(n79));
  nand2 g57(.a(n79), .b(n31), .O(n80));
  nand2 g58(.a(n54), .b(n48), .O(n81));
  nor2  g59(.a(n81), .b(n80), .O(preadSWPtoA));
  nor2  g60(.a(SRC2equal16), .b(pbusDtoINA), .O(n83));
  nor2  g61(.a(opc2load), .b(n38), .O(n84));
  nand2 g62(.a(n84), .b(n83), .O(n85));
  nor2  g63(.a(n85), .b(n69), .O(pForwardtoINB));
  nand2 g64(.a(n28), .b(n25), .O(n87));
  nor2  g65(.a(n87), .b(n75), .O(preadPCtoA));
endmodule


