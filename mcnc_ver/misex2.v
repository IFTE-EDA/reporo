// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:24 2019

module source_pla  ( 
    a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x,
    y,
    z, a1, b1, c1, d1, e1, f1, g1, h1, i1, j1, k1, l1, m1, n1, o1, p1, q1  );
  input  a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u,
    v, w, x, y;
  output z, a1, b1, c1, d1, e1, f1, g1, h1, i1, j1, k1, l1, m1, n1, o1, p1,
    q1;
  wire n44, n45, n46, n47, n48, n49, n50, n51, n52, n54, n56, n57, n59, n60,
    n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74,
    n75, n76, n78, n79, n80, n81, n83, n84, n87, n88, n89, n90, n92, n93,
    n94, n95, n96, n97, n98, n99, n100, n101, n103, n104, n105, n106, n108,
    n109, n110, n111, n112, n113, n114, n115, n116, n117, n118, n119, n120,
    n121, n122, n123, n124, n125, n127, n128, n129, n130, n131, n132, n133,
    n134, n135, n136, n137, n138, n139, n140, n141, n142, n144, n145, n146,
    n147, n148, n149, n150, n151, n152, n154, n155, n156, n157, n158, n159,
    n160, n161, n162, n163, n164, n165, n167, n168, n171, n172, n173, n174,
    n175;
  inv1  g000(.a(j), .O(n44));
  nor2  g001(.a(b), .b(a), .O(n45));
  inv1  g002(.a(n45), .O(n46));
  nor2  g003(.a(n46), .b(c), .O(n47));
  nand2 g004(.a(n47), .b(n44), .O(n48));
  inv1  g005(.a(k), .O(n49));
  nor2  g006(.a(r), .b(n49), .O(n50));
  nor2  g007(.a(t), .b(s), .O(n51));
  nand2 g008(.a(n51), .b(n50), .O(n52));
  nor2  g009(.a(n52), .b(n48), .O(z));
  nand2 g010(.a(n47), .b(j), .O(n54));
  nor2  g011(.a(n54), .b(n52), .O(a1));
  nor2  g012(.a(r), .b(k), .O(n56));
  nand2 g013(.a(n56), .b(n51), .O(n57));
  nor2  g014(.a(n57), .b(n54), .O(b1));
  inv1  g015(.a(s), .O(n59));
  nor2  g016(.a(t), .b(n59), .O(n60));
  inv1  g017(.a(c), .O(n61));
  inv1  g018(.a(r), .O(n62));
  nand2 g019(.a(n62), .b(n61), .O(n63));
  nor2  g020(.a(n63), .b(n46), .O(n64));
  nand2 g021(.a(n64), .b(n60), .O(n65));
  inv1  g022(.a(m), .O(n66));
  nand2 g023(.a(l), .b(c), .O(n67));
  nor2  g024(.a(n67), .b(n66), .O(n68));
  nor2  g025(.a(n49), .b(j), .O(n69));
  inv1  g026(.a(n69), .O(n70));
  inv1  g027(.a(a), .O(n71));
  inv1  g028(.a(b), .O(n72));
  nor2  g029(.a(n72), .b(n71), .O(n73));
  inv1  g030(.a(n73), .O(n74));
  nor2  g031(.a(n74), .b(n70), .O(n75));
  nand2 g032(.a(n75), .b(n68), .O(n76));
  nand2 g033(.a(n76), .b(n65), .O(c1));
  nor2  g034(.a(n74), .b(n61), .O(n78));
  nand2 g035(.a(n78), .b(n69), .O(n79));
  inv1  g036(.a(l), .O(n80));
  nand2 g037(.a(n66), .b(n80), .O(n81));
  nor2  g038(.a(n81), .b(n79), .O(d1));
  inv1  g039(.a(n78), .O(n83));
  nand2 g040(.a(k), .b(j), .O(n84));
  nor2  g041(.a(n84), .b(n83), .O(e1));
  nor2  g042(.a(n79), .b(n80), .O(f1));
  nand2 g043(.a(n66), .b(l), .O(n87));
  nor2  g044(.a(n87), .b(n70), .O(n88));
  nor2  g045(.a(n88), .b(n72), .O(n89));
  nand2 g046(.a(c), .b(a), .O(n90));
  nor2  g047(.a(n90), .b(n89), .O(g1));
  inv1  g048(.a(e), .O(n92));
  nor2  g049(.a(n92), .b(d), .O(n93));
  inv1  g050(.a(f), .O(n94));
  inv1  g051(.a(g), .O(n95));
  nand2 g052(.a(n95), .b(n94), .O(n96));
  inv1  g053(.a(h), .O(n97));
  inv1  g054(.a(i), .O(n98));
  nand2 g055(.a(n98), .b(n97), .O(n99));
  nor2  g056(.a(n99), .b(n96), .O(n100));
  nand2 g057(.a(n100), .b(n93), .O(n101));
  nor2  g058(.a(n46), .b(n61), .O(q1));
  inv1  g059(.a(t), .O(n103));
  nand2 g060(.a(n59), .b(r), .O(n104));
  nor2  g061(.a(n104), .b(n103), .O(n105));
  nand2 g062(.a(n105), .b(q1), .O(n106));
  nor2  g063(.a(n106), .b(n101), .O(h1));
  nor2  g064(.a(q), .b(p), .O(n108));
  nand2 g065(.a(n108), .b(u), .O(n109));
  nor2  g066(.a(n61), .b(b), .O(n110));
  nand2 g067(.a(m), .b(n80), .O(n111));
  inv1  g068(.a(n), .O(n112));
  inv1  g069(.a(o), .O(n113));
  nand2 g070(.a(n113), .b(n112), .O(n114));
  nor2  g071(.a(n114), .b(n111), .O(n115));
  nand2 g072(.a(n115), .b(n110), .O(n116));
  nor2  g073(.a(n116), .b(n109), .O(n117));
  nand2 g074(.a(s), .b(b), .O(n118));
  nor2  g075(.a(u), .b(t), .O(n119));
  inv1  g076(.a(n119), .O(n120));
  nor2  g077(.a(n120), .b(n118), .O(n121));
  nor2  g078(.a(n121), .b(n117), .O(n122));
  inv1  g079(.a(w), .O(n123));
  nor2  g080(.a(v), .b(a), .O(n124));
  nand2 g081(.a(n124), .b(n123), .O(n125));
  nor2  g082(.a(n125), .b(n122), .O(i1));
  inv1  g083(.a(n60), .O(n127));
  nor2  g084(.a(u), .b(n72), .O(n128));
  inv1  g085(.a(v), .O(n129));
  nor2  g086(.a(n123), .b(n129), .O(n130));
  nand2 g087(.a(n130), .b(n128), .O(n131));
  nor2  g088(.a(n131), .b(n127), .O(n132));
  nor2  g089(.a(n111), .b(n), .O(n133));
  nand2 g090(.a(n133), .b(n110), .O(n134));
  inv1  g091(.a(p), .O(n135));
  nor2  g092(.a(n135), .b(o), .O(n136));
  nand2 g093(.a(u), .b(q), .O(n137));
  nand2 g094(.a(n123), .b(n129), .O(n138));
  nor2  g095(.a(n138), .b(n137), .O(n139));
  nand2 g096(.a(n139), .b(n136), .O(n140));
  nor2  g097(.a(n140), .b(n134), .O(n141));
  nor2  g098(.a(n141), .b(n132), .O(n142));
  nor2  g099(.a(n142), .b(a), .O(j1));
  nand2 g100(.a(n119), .b(v), .O(n144));
  nor2  g101(.a(n144), .b(n118), .O(n145));
  inv1  g102(.a(q), .O(n146));
  nand2 g103(.a(u), .b(n146), .O(n147));
  nor2  g104(.a(n147), .b(v), .O(n148));
  nand2 g105(.a(n148), .b(n136), .O(n149));
  nor2  g106(.a(n149), .b(n134), .O(n150));
  nor2  g107(.a(n150), .b(n145), .O(n151));
  nand2 g108(.a(n123), .b(n71), .O(n152));
  nor2  g109(.a(n152), .b(n151), .O(k1));
  nor2  g110(.a(n49), .b(n61), .O(n154));
  nor2  g111(.a(n154), .b(n74), .O(n155));
  inv1  g112(.a(x), .O(n156));
  nor2  g113(.a(t), .b(c), .O(n157));
  nor2  g114(.a(n157), .b(n156), .O(n158));
  nand2 g115(.a(n103), .b(n61), .O(n159));
  nor2  g116(.a(n159), .b(n62), .O(n160));
  nor2  g117(.a(n160), .b(n158), .O(n161));
  nor2  g118(.a(n161), .b(n46), .O(n162));
  nor2  g119(.a(n162), .b(n155), .O(n163));
  inv1  g120(.a(y), .O(n164));
  nand2 g121(.a(n164), .b(j), .O(n165));
  nor2  g122(.a(n165), .b(n163), .O(l1));
  inv1  g123(.a(n47), .O(n167));
  nand2 g124(.a(n103), .b(r), .O(n168));
  nor2  g125(.a(n168), .b(n167), .O(m1));
  nor2  g126(.a(n57), .b(n48), .O(n1));
  nand2 g127(.a(n49), .b(b), .O(n171));
  nand2 g128(.a(n171), .b(c), .O(n172));
  nand2 g129(.a(n172), .b(a), .O(n173));
  nor2  g130(.a(c), .b(b), .O(n174));
  nand2 g131(.a(n174), .b(t), .O(n175));
  nand2 g132(.a(n175), .b(n173), .O(o1));
  nor2  g133(.a(n72), .b(a), .O(p1));
endmodule


