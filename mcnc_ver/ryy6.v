// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:27 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15,
    v16.0   );
  input  v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,
    v15;
  output v16.0 ;
  wire n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31,
    n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45,
    n46, n47;
  inv1  g00(.a(v0), .O(n18));
  inv1  g01(.a(v1), .O(n19));
  nor2  g02(.a(n19), .b(n18), .O(n20));
  nor2  g03(.a(n20), .b(v2), .O(n21));
  inv1  g04(.a(v9), .O(n22));
  inv1  g05(.a(v10), .O(n23));
  inv1  g06(.a(v11), .O(n24));
  nor2  g07(.a(n24), .b(n23), .O(n25));
  inv1  g08(.a(v7), .O(n26));
  inv1  g09(.a(v8), .O(n27));
  nor2  g10(.a(n27), .b(n26), .O(n28));
  nor2  g11(.a(n28), .b(n25), .O(n29));
  nor2  g12(.a(n29), .b(n22), .O(n30));
  nor2  g13(.a(n30), .b(v1), .O(n31));
  nor2  g14(.a(n31), .b(n21), .O(n32));
  inv1  g15(.a(v12), .O(n33));
  inv1  g16(.a(v13), .O(n34));
  nor2  g17(.a(n34), .b(n33), .O(n35));
  inv1  g18(.a(v14), .O(n36));
  inv1  g19(.a(v15), .O(n37));
  nor2  g20(.a(n37), .b(n36), .O(n38));
  nand2 g21(.a(n38), .b(n35), .O(n39));
  inv1  g22(.a(v5), .O(n40));
  inv1  g23(.a(v6), .O(n41));
  nor2  g24(.a(n41), .b(n40), .O(n42));
  inv1  g25(.a(v3), .O(n43));
  inv1  g26(.a(v4), .O(n44));
  nor2  g27(.a(n44), .b(n43), .O(n45));
  nor2  g28(.a(n45), .b(n42), .O(n46));
  nand2 g29(.a(n46), .b(n39), .O(n47));
  nor2  g30(.a(n47), .b(n32), .O(v16.0 ));
endmodule


