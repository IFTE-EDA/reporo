// Benchmark "tcon" written by ABC on Tue Nov  5 15:01:28 2019

module tcon ( 
    a, b, c, d, e, f, g, h, i, k, l, m, n, o, p, q, r,
    s, t, u, v, w, x, y, z, a0, b0, c0, d0, e0, f0, g0, h0  );
  input  a, b, c, d, e, f, g, h, i, k, l, m, n, o, p, q, r;
  output s, t, u, v, w, x, y, z, a0, b0, c0, d0, e0, f0, g0, h0;
  wire n34, n35, n36, n37, n39, n40, n41, n42, n44, n45, n46, n47, n49, n50,
    n51, n52, n54, n55, n56, n57, n59, n60, n61, n62, n64, n65, n66, n67,
    n69, n70, n71, n72;
  inv1  g00(.a(a), .O(n34));
  nand2 g01(.a(i), .b(n34), .O(n35));
  nand2 g02(.a(n35), .b(k), .O(n36));
  nand2 g03(.a(i), .b(a), .O(n37));
  nand2 g04(.a(n37), .b(n36), .O(a0));
  inv1  g05(.a(b), .O(n39));
  nand2 g06(.a(i), .b(n39), .O(n40));
  nand2 g07(.a(n40), .b(l), .O(n41));
  nand2 g08(.a(i), .b(b), .O(n42));
  nand2 g09(.a(n42), .b(n41), .O(b0));
  inv1  g10(.a(c), .O(n44));
  nand2 g11(.a(i), .b(n44), .O(n45));
  nand2 g12(.a(n45), .b(m), .O(n46));
  nand2 g13(.a(i), .b(c), .O(n47));
  nand2 g14(.a(n47), .b(n46), .O(c0));
  inv1  g15(.a(d), .O(n49));
  nand2 g16(.a(i), .b(n49), .O(n50));
  nand2 g17(.a(n50), .b(n), .O(n51));
  nand2 g18(.a(i), .b(d), .O(n52));
  nand2 g19(.a(n52), .b(n51), .O(d0));
  inv1  g20(.a(e), .O(n54));
  nand2 g21(.a(i), .b(n54), .O(n55));
  nand2 g22(.a(n55), .b(o), .O(n56));
  nand2 g23(.a(i), .b(e), .O(n57));
  nand2 g24(.a(n57), .b(n56), .O(e0));
  inv1  g25(.a(f), .O(n59));
  nand2 g26(.a(i), .b(n59), .O(n60));
  nand2 g27(.a(n60), .b(p), .O(n61));
  nand2 g28(.a(i), .b(f), .O(n62));
  nand2 g29(.a(n62), .b(n61), .O(f0));
  inv1  g30(.a(g), .O(n64));
  nand2 g31(.a(i), .b(n64), .O(n65));
  nand2 g32(.a(n65), .b(q), .O(n66));
  nand2 g33(.a(i), .b(g), .O(n67));
  nand2 g34(.a(n67), .b(n66), .O(g0));
  inv1  g35(.a(h), .O(n69));
  nand2 g36(.a(i), .b(n69), .O(n70));
  nand2 g37(.a(n70), .b(r), .O(n71));
  nand2 g38(.a(i), .b(h), .O(n72));
  nand2 g39(.a(n72), .b(n71), .O(h0));
  buf   g40(.a(k), .O(s));
  buf   g41(.a(l), .O(t));
  buf   g42(.a(m), .O(u));
  buf   g43(.a(n), .O(v));
  buf   g44(.a(o), .O(w));
  buf   g45(.a(p), .O(x));
  buf   g46(.a(q), .O(y));
  buf   g47(.a(r), .O(z));
endmodule


