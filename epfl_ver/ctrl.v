// Benchmark "top" written by ABC on Tue Nov  5 14:58:17 2019

module top ( 
    opcode_0 , opcode_1 , opcode_2 , opcode_3 , opcode_4 ,
    op_ext_0 , op_ext_1 ,
    sel_reg_dst_0 , sel_reg_dst_1 , sel_alu_opB_0 , sel_alu_opB_1 ,
    alu_op_0 , alu_op_1 , alu_op_2 , alu_op_ext_0 ,
    alu_op_ext_1 , alu_op_ext_2 , alu_op_ext_3 , halt, reg_write,
    sel_pc_opA, sel_pc_opB, beqz, bnez, bgez, bltz, jump, Cin, invA, invB,
    sign, mem_write, sel_wb  );
  input  opcode_0 , opcode_1 , opcode_2 , opcode_3 , opcode_4 ,
    op_ext_0 , op_ext_1 ;
  output sel_reg_dst_0 , sel_reg_dst_1 , sel_alu_opB_0 ,
    sel_alu_opB_1 , alu_op_0 , alu_op_1 , alu_op_2 ,
    alu_op_ext_0 , alu_op_ext_1 , alu_op_ext_2 , alu_op_ext_3 ,
    halt, reg_write, sel_pc_opA, sel_pc_opB, beqz, bnez, bgez, bltz, jump,
    Cin, invA, invB, sign, mem_write, sel_wb;
  wire n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47,
    n48, n49, n50, n51, n52, n53, n55, n56, n57, n58, n59, n60, n61, n62,
    n63, n64, n66, n67, n68, n69, n70, n71, n72, n74, n75, n76, n77, n78,
    n79, n80, n81, n82, n83, n85, n86, n87, n88, n89, n90, n91, n92, n93,
    n94, n95, n96, n97, n98, n99, n100, n101, n102, n103, n105, n106, n107,
    n108, n109, n110, n112, n113, n114, n115, n116, n117, n118, n119, n120,
    n121, n122, n123, n125, n126, n127, n128, n129, n130, n131, n133, n134,
    n135, n136, n137, n138, n139, n141, n142, n144, n145, n146, n147, n148,
    n150, n152, n153, n154, n155, n156, n157, n158, n159, n161, n164, n165,
    n167, n168, n169, n170, n172, n174, n177, n178, n179, n180, n181, n182,
    n183, n184, n186, n187, n188, n189, n190, n191, n193, n194, n195, n198,
    n199, n200, n201, n203;
  inv1  g000(.a(opcode_2 ), .O(n34));
  inv1  g001(.a(opcode_0 ), .O(n35));
  nor2  g002(.a(opcode_1 ), .b(n35), .O(n36));
  inv1  g003(.a(opcode_3 ), .O(n37));
  inv1  g004(.a(opcode_4 ), .O(n38));
  nor2  g005(.a(n38), .b(n37), .O(n39));
  nand2 g006(.a(n39), .b(n36), .O(n40));
  inv1  g007(.a(opcode_1 ), .O(n41));
  nor2  g008(.a(n37), .b(n41), .O(n42));
  nand2 g009(.a(n42), .b(opcode_4 ), .O(n43));
  nand2 g010(.a(n43), .b(n40), .O(n44));
  nand2 g011(.a(n44), .b(n34), .O(n45));
  nor2  g012(.a(n37), .b(opcode_1 ), .O(n46));
  nand2 g013(.a(n46), .b(opcode_4 ), .O(n47));
  nor2  g014(.a(opcode_4 ), .b(opcode_3 ), .O(n48));
  nor2  g015(.a(n48), .b(n39), .O(n49));
  inv1  g016(.a(n49), .O(n50));
  nand2 g017(.a(n50), .b(opcode_1 ), .O(n51));
  nand2 g018(.a(n51), .b(n47), .O(n52));
  nand2 g019(.a(n52), .b(opcode_2 ), .O(n53));
  nand2 g020(.a(n53), .b(n45), .O(sel_reg_dst_0 ));
  inv1  g021(.a(n39), .O(n55));
  nor2  g022(.a(n55), .b(opcode_0 ), .O(n56));
  nor2  g023(.a(n56), .b(opcode_1 ), .O(n57));
  nor2  g024(.a(n38), .b(opcode_3 ), .O(n58));
  nor2  g025(.a(n58), .b(n41), .O(n59));
  nor2  g026(.a(n59), .b(n57), .O(n60));
  nor2  g027(.a(n60), .b(opcode_2 ), .O(n61));
  inv1  g028(.a(n48), .O(n62));
  nor2  g029(.a(n62), .b(n41), .O(n63));
  nor2  g030(.a(n63), .b(n34), .O(n64));
  nor2  g031(.a(n64), .b(n61), .O(sel_reg_dst_1 ));
  inv1  g032(.a(n57), .O(n66));
  nand2 g033(.a(n50), .b(n35), .O(n67));
  nand2 g034(.a(n38), .b(opcode_3 ), .O(n68));
  nand2 g035(.a(n68), .b(opcode_0 ), .O(n69));
  nand2 g036(.a(n69), .b(n67), .O(n70));
  nand2 g037(.a(n70), .b(opcode_1 ), .O(n71));
  nand2 g038(.a(n71), .b(n66), .O(n72));
  nor2  g039(.a(n72), .b(opcode_2 ), .O(sel_alu_opB_0 ));
  inv1  g040(.a(n58), .O(n74));
  nor2  g041(.a(opcode_3 ), .b(opcode_0 ), .O(n75));
  nand2 g042(.a(n75), .b(n74), .O(n76));
  inv1  g043(.a(n76), .O(n77));
  nor2  g044(.a(n49), .b(n35), .O(n78));
  nor2  g045(.a(n78), .b(n77), .O(n79));
  nor2  g046(.a(n79), .b(opcode_1 ), .O(n80));
  nor2  g047(.a(n80), .b(n59), .O(n81));
  nor2  g048(.a(n81), .b(opcode_2 ), .O(n82));
  nor2  g049(.a(n58), .b(n34), .O(n83));
  nor2  g050(.a(n83), .b(n82), .O(sel_alu_opB_1 ));
  nand2 g051(.a(opcode_3 ), .b(n35), .O(n85));
  nand2 g052(.a(op_ext_0 ), .b(opcode_4 ), .O(n86));
  nor2  g053(.a(n86), .b(n85), .O(n87));
  inv1  g054(.a(op_ext_1 ), .O(n88));
  nand2 g055(.a(n88), .b(opcode_3 ), .O(n89));
  nor2  g056(.a(n89), .b(n39), .O(n90));
  inv1  g057(.a(op_ext_0 ), .O(n91));
  nand2 g058(.a(n91), .b(opcode_3 ), .O(n92));
  nor2  g059(.a(n92), .b(n39), .O(n93));
  nor2  g060(.a(n91), .b(n37), .O(n94));
  nor2  g061(.a(n94), .b(n93), .O(n95));
  nor2  g062(.a(n95), .b(n88), .O(n96));
  nor2  g063(.a(n96), .b(n90), .O(n97));
  nor2  g064(.a(n97), .b(n35), .O(n98));
  nor2  g065(.a(n98), .b(n87), .O(n99));
  nor2  g066(.a(n99), .b(n41), .O(n100));
  nor2  g067(.a(n100), .b(opcode_2 ), .O(n101));
  nor2  g068(.a(n74), .b(n35), .O(n102));
  nor2  g069(.a(n102), .b(n34), .O(n103));
  nor2  g070(.a(n103), .b(n101), .O(alu_op_0 ));
  nor2  g071(.a(n88), .b(n37), .O(n105));
  nor2  g072(.a(n105), .b(n90), .O(n106));
  nor2  g073(.a(n106), .b(n41), .O(n107));
  nor2  g074(.a(n107), .b(opcode_2 ), .O(n108));
  nor2  g075(.a(n74), .b(n41), .O(n109));
  nor2  g076(.a(n109), .b(n34), .O(n110));
  nor2  g077(.a(n110), .b(n108), .O(alu_op_1 ));
  nor2  g078(.a(n39), .b(opcode_1 ), .O(n112));
  nand2 g079(.a(n112), .b(n62), .O(n113));
  nor2  g080(.a(n39), .b(opcode_0 ), .O(n114));
  nand2 g081(.a(n114), .b(n62), .O(n115));
  nor2  g082(.a(n48), .b(n35), .O(n116));
  inv1  g083(.a(n116), .O(n117));
  nand2 g084(.a(n117), .b(n115), .O(n118));
  nand2 g085(.a(n118), .b(opcode_1 ), .O(n119));
  nand2 g086(.a(n119), .b(n113), .O(n120));
  nand2 g087(.a(n120), .b(n34), .O(n121));
  nor2  g088(.a(n37), .b(n34), .O(n122));
  nand2 g089(.a(n122), .b(opcode_4 ), .O(n123));
  nand2 g090(.a(n123), .b(n121), .O(alu_op_2 ));
  nor2  g091(.a(opcode_2 ), .b(opcode_1 ), .O(n125));
  nand2 g092(.a(n125), .b(n66), .O(n126));
  inv1  g093(.a(n78), .O(n127));
  nand2 g094(.a(n127), .b(n76), .O(n128));
  nand2 g095(.a(n128), .b(opcode_1 ), .O(n129));
  nand2 g096(.a(n129), .b(n40), .O(n130));
  nand2 g097(.a(n130), .b(opcode_2 ), .O(n131));
  nand2 g098(.a(n131), .b(n126), .O(alu_op_ext_0 ));
  nor2  g099(.a(n74), .b(opcode_0 ), .O(n133));
  nor2  g100(.a(n133), .b(n41), .O(n134));
  inv1  g101(.a(n134), .O(n135));
  nor2  g102(.a(opcode_2 ), .b(n41), .O(n136));
  nand2 g103(.a(n136), .b(n135), .O(n137));
  nor2  g104(.a(n34), .b(n41), .O(n138));
  nand2 g105(.a(n138), .b(n50), .O(n139));
  nand2 g106(.a(n139), .b(n137), .O(alu_op_ext_1 ));
  nor2  g107(.a(n134), .b(n112), .O(n141));
  nor2  g108(.a(n141), .b(opcode_2 ), .O(n142));
  nor2  g109(.a(n142), .b(n64), .O(alu_op_ext_2 ));
  nand2 g110(.a(n117), .b(n85), .O(n144));
  inv1  g111(.a(n144), .O(n145));
  nor2  g112(.a(n145), .b(n41), .O(n146));
  nand2 g113(.a(n113), .b(n34), .O(n147));
  nor2  g114(.a(n147), .b(n146), .O(n148));
  nor2  g115(.a(n148), .b(n83), .O(alu_op_ext_3 ));
  nand2 g116(.a(n77), .b(n41), .O(n150));
  nor2  g117(.a(n150), .b(opcode_2 ), .O(halt));
  nand2 g118(.a(n62), .b(opcode_1 ), .O(n152));
  nand2 g119(.a(n144), .b(n41), .O(n153));
  nand2 g120(.a(n153), .b(n152), .O(n154));
  nand2 g121(.a(n154), .b(n34), .O(n155));
  nand2 g122(.a(opcode_4 ), .b(n41), .O(n156));
  nand2 g123(.a(n68), .b(opcode_1 ), .O(n157));
  nand2 g124(.a(n157), .b(n156), .O(n158));
  nand2 g125(.a(n158), .b(opcode_2 ), .O(n159));
  nand2 g126(.a(n159), .b(n155), .O(reg_write));
  nand2 g127(.a(n48), .b(opcode_0 ), .O(n161));
  nor2  g128(.a(n161), .b(n34), .O(sel_pc_opA));
  nor2  g129(.a(n76), .b(n34), .O(sel_pc_opB));
  nor2  g130(.a(n68), .b(opcode_0 ), .O(n164));
  nand2 g131(.a(n164), .b(n41), .O(n165));
  nor2  g132(.a(n165), .b(n34), .O(beqz));
  nor2  g133(.a(n68), .b(n35), .O(n167));
  inv1  g134(.a(n167), .O(n168));
  nor2  g135(.a(n168), .b(opcode_1 ), .O(n169));
  inv1  g136(.a(n169), .O(n170));
  nor2  g137(.a(n170), .b(n34), .O(bnez));
  nand2 g138(.a(n167), .b(opcode_1 ), .O(n172));
  nor2  g139(.a(n172), .b(n34), .O(bgez));
  nand2 g140(.a(n164), .b(opcode_1 ), .O(n174));
  nor2  g141(.a(n174), .b(n34), .O(bltz));
  nor2  g142(.a(n62), .b(n34), .O(jump));
  inv1  g143(.a(n95), .O(n177));
  nor2  g144(.a(n41), .b(n35), .O(n178));
  nand2 g145(.a(n178), .b(n177), .O(n179));
  nand2 g146(.a(n179), .b(n34), .O(n180));
  nor2  g147(.a(n180), .b(n169), .O(n181));
  nor2  g148(.a(n56), .b(n41), .O(n182));
  nor2  g149(.a(n182), .b(n112), .O(n183));
  nor2  g150(.a(n183), .b(n34), .O(n184));
  nor2  g151(.a(n184), .b(n181), .O(Cin));
  nand2 g152(.a(n168), .b(n41), .O(n186));
  nand2 g153(.a(n39), .b(op_ext_0 ), .O(n187));
  nor2  g154(.a(n187), .b(op_ext_1 ), .O(n188));
  nand2 g155(.a(n188), .b(opcode_0 ), .O(n189));
  nand2 g156(.a(n189), .b(opcode_1 ), .O(n190));
  nand2 g157(.a(n190), .b(n186), .O(n191));
  nor2  g158(.a(n191), .b(opcode_2 ), .O(invA));
  inv1  g159(.a(n178), .O(n193));
  nor2  g160(.a(n193), .b(n97), .O(n194));
  nor2  g161(.a(n194), .b(opcode_2 ), .O(n195));
  nor2  g162(.a(n195), .b(n184), .O(invB));
  nor2  g163(.a(n133), .b(opcode_1 ), .O(n198));
  nor2  g164(.a(n102), .b(n41), .O(n199));
  nor2  g165(.a(n199), .b(n198), .O(n200));
  nor2  g166(.a(n200), .b(opcode_2 ), .O(n201));
  nor2  g167(.a(n201), .b(opcode_2 ), .O(mem_write));
  nand2 g168(.a(n102), .b(n41), .O(n203));
  nor2  g169(.a(n203), .b(opcode_2 ), .O(sel_wb));
endmodule


