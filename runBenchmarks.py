#!/usr/bin/env python3

import datetime
from enum import Enum
import math
import os
from PIL import Image,ImageFont,ImageDraw
import pystache
import re
import subprocess
import time

from netlist import Netlist
import technology as tech

class PowerDomainType(Enum):
    HORIZONTALLY_AND_VERTICALLY_CENTERED = 'HVC' # default
    FULL_WIDTH_VERTICALLY_CENTERED = 'FWVC'
    TOP_CENTER = 'TC'

## Tutorial
benchmarks = {
    'epfl_ver': [
        {'filename': 'tutorial-ctrl.v', 'spr_min3A_pso_core_utilization': 0.75, 'spr_min3A_pso_pd_utilization': 0.75}
    ]
}

## settings for PSO, replacement ratio: 30%
#-#benchmarks = {
#-##    'mcnc_ver': [
#-##        {'filename': 'C1355_copy.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.96},
#-##        {'filename': 'C6288_copy.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.86}
#-##    ],
#-#    'epfl_ver': [
#-##         {'filename': 'adder.v', 'spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED,'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.825},
#-##         {'filename': 'arbiter.v', 'spr_min3A_pso_type': PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_pso_core_utilization': 0.78, 'spr_min3A_pso_pd_utilization': 0.84}, #Type1:0.8/0.58
#-##         {'filename': 'bar.v', 'spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED,'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.85},
#-#         {'filename': 'cavlc.v', 'spr_min3A_pso_type': PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED,'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.8}, #Type1:0.8/0.86
#-##         {'filename': 'ctrl.v', 'spr_min3A_pso_type':PowerDomainType.TOP_CENTER,'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.8},
#-##         {'filename': 'dec.v', 'spr_min3A_pso_type':PowerDomainType.TOP_CENTER,'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.74},#Type1:0.8/0.8
#-#         {'filename': 'div.v','spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_driv1B_core_utilization': 0.7, 'spr_min3A_pso_core_utilization': 0.74, 'spr_min3A_pso_pd_utilization': 0.74}, #0.59/0.48;0.53/0.53
#-####         {'filename': 'hyp.v', 'spr_min3A_core_utilization': 0.6, 'spr_min3A_pso_core_utilization': 0.6, 'spr_min3A_pso_pd_utilization': 0.8, 'spr_min3A_pso_preplace_optimization': True},
#-##         {'filename': 'i2c.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.78}, #1.0, 0.9
#-##         {'filename': 'int2float.v','spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.8}, #1.0, 0.9, 0.88, 0.74
#-#         {'filename': 'log2.v', 'spr_min3A_core_utilization': 0.7, 'spr_min3A_driv1B_core_utilization': 0.7,'spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_pso_core_utilization': 0.74, 'spr_min3A_pso_pd_utilization': 0.78},
#-##         {'filename': 'max.v', 'spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED,'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.84}, #1.0
#-##         {'filename': 'mem_ctrl.v', 'spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED,'spr_min3A_core_utilization': 0.75, 'spr_min3A_pso_core_utilization': 0.75, 'spr_min3A_pso_pd_utilization': 0.8},
#-##         {'filename': 'multiplier.v', 'spr_min3A_core_utilization': 0.79, 'spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_pso_core_utilization': 0.78, 'spr_min3A_pso_pd_utilization': 0.8},
#-##         {'filename': 'priority.v','spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.82}, #1.0, 0.84
#-##         {'filename': 'router.v', 'spr_min3A_pso_core_utilization': 0.79, 'spr_min3A_pso_pd_utilization': 0.76}, #1.0, 0.88, 0.8
#-##         {'filename': 'sin.v','spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.78}, #1.0, 0.88
#-#         {'filename': 'sqrt.v', 'spr_min3A_core_utilization': 0.8,'spr_min3A_pso_preplace_optimization': False, 'spr_min3A_pso_type': PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_pso_core_utilization': 0.78, 'spr_min3A_pso_pd_utilization': 0.78}, #1.0
#-#         {'filename': 'square.v', 'spr_min3A_core_utilization': 0.74, 'spr_min3A_driv1B_core_utilization': 0.72, 'spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_pso_core_utilization': 0.74, 'spr_min3A_pso_pd_utilization': 0.84},
#-#         {'filename': 'voter.v', 'spr_min3A_pso_type': PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_pso_core_utilization': 0.79, 'spr_min3A_pso_pd_utilization': 0.84}
#-#     ]
#-#}

## settings for PSO, replacement ratio: 10%
#benchmarks = {
#    'mcnc_ver': [
#        {'filename': 'C1355_copy.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.96},
#        {'filename': 'C6288_copy.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.86}
#    ],
#    'epfl_ver': [
#         {'filename': 'adder.v', 'spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED,'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.825},
#         {'filename': 'arbiter.v', 'spr_min3A_pso_type': PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.84}, #Type1:0.8/0.58
#         {'filename': 'bar.v', 'spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED,'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.85},
#         {'filename': 'cavlc.v', 'spr_min3A_pso_type': PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED,'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.86}, #Type1:0.8/0.86
#         {'filename': 'ctrl.v', 'spr_min3A_pso_type':PowerDomainType.TOP_CENTER,'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.68},
#         {'filename': 'dec.v', 'spr_min3A_pso_type':PowerDomainType.TOP_CENTER,'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.74},#Type1:0.8/0.8
#         {'filename': 'div.v','spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_driv1B_core_utilization': 0.7, 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.8}, #0.59/0.48;0.53/0.53
#         {'filename': 'hyp.v', 'spr_min3A_core_utilization': 0.6, 'spr_min3A_pso_core_utilization': 0.6, 'spr_min3A_pso_pd_utilization': 0.8, 'spr_min3A_pso_preplace_optimization': True},
#         {'filename': 'i2c.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.78}, #1.0, 0.9
#         {'filename': 'int2float.v','spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.70}, #1.0, 0.9, 0.88, 0.74
#         {'filename': 'log2.v', 'spr_min3A_core_utilization': 0.7, 'spr_min3A_driv1B_core_utilization': 0.7,'spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_pso_core_utilization': 0.68, 'spr_min3A_pso_pd_utilization': 0.8},
#         {'filename': 'max.v', 'spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED,'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.84}, #1.0
#         {'filename': 'mem_ctrl.v', 'spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED,'spr_min3A_core_utilization': 0.75, 'spr_min3A_pso_core_utilization': 0.75, 'spr_min3A_pso_pd_utilization': 0.8},
#         {'filename': 'multiplier.v', 'spr_min3A_core_utilization': 0.79, 'spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_pso_core_utilization': 0.78, 'spr_min3A_pso_pd_utilization': 0.8},
#         {'filename': 'priority.v','spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.8}, #1.0, 0.84
#         {'filename': 'router.v', 'spr_min3A_pso_core_utilization': 0.79, 'spr_min3A_pso_pd_utilization': 0.74}, #1.0, 0.88, 0.8
#         {'filename': 'sin.v','spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.78}, #1.0, 0.88
#         {'filename': 'sqrt.v', 'spr_min3A_core_utilization': 0.8,'spr_min3A_pso_preplace_optimization': False, 'spr_min3A_pso_type': PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_pso_core_utilization': 0.5, 'spr_min3A_pso_pd_utilization': 0.5}, #1.0
#         {'filename': 'square.v', 'spr_min3A_core_utilization': 0.74, 'spr_min3A_driv1B_core_utilization': 0.72, 'spr_min3A_pso_type':PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_pso_core_utilization': 0.74, 'spr_min3A_pso_pd_utilization': 0.84},
#         {'filename': 'voter.v', 'spr_min3A_pso_type': PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED, 'spr_min3A_pso_core_utilization': 0.79, 'spr_min3A_pso_pd_utilization': 0.84}
#     ]
# }

## settings for PSO, with a full-width, vertically centered power domain
#benchmarks = {
#    'mcnc_ver': [
#        {'filename': 'C1355_copy.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.96},
#        {'filename': 'C6288_copy.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.86}
#    ],
#   'epfl_ver': [
#        {'filename': 'adder.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 1.0},
#        {'filename': 'arbiter.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.92},
#        {'filename': 'bar.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.86},
#        {'filename': 'cavlc.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 1.0},
#        {'filename': 'ctrl.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 1.0},
#        {'filename': 'dec.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 1.0},
#         {'filename': 'div.v', 'spr_min3A_driv1B_core_utilization': 0.7, 'spr_min3A_pso_core_utilization': 0.6, 'spr_min3A_pso_pd_utilization': 0.48},
########        {'filename': 'hyp.v', 'spr_min3A_core_utilization': 0.6, 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 1.0, 'spr_min3A_pso_preplace_optimization': True},
#         {'filename': 'i2c.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.8},
#         {'filename': 'int2float.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.905},#'spr_min3A_pso_pd_utilization': 0.9 => 4 rows},
#        {'filename': 'log2.v', 'spr_min3A_core_utilization': 0.7, 'spr_min3A_driv1B_core_utilization': 0.7, 'spr_min3A_pso_core_utilization': 0.55, 'spr_min3A_pso_pd_utilization': 0.45},

#        {'filename': 'max.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 1.0},
#        {'filename': 'mem_ctrl.v', 'spr_min3A_core_utilization': 0.75, 'spr_min3A_pso_core_utilization': 0.48, 'spr_min3A_pso_pd_utilization': 0.46},
#        {'filename': 'multiplier.v', 'spr_min3A_core_utilization': 0.79, 'spr_min3A_pso_core_utilization': 0.64, 'spr_min3A_pso_pd_utilization': 0.66},
#        {'filename': 'priority.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 1.0},
#        {'filename': 'router.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 1.0},
#        {'filename': 'sin.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.86},
########         {'filename': 'sqrt.v', 'spr_min3A_core_utilization': 0.8, 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.7},
#        {'filename': 'square.v', 'spr_min3A_core_utilization': 0.74, 'spr_min3A_driv1B_core_utilization': 0.72, 'spr_min3A_pso_core_utilization': 0.73, 'spr_min3A_pso_pd_utilization': 0.84},
#         {'filename': 'voter.v', 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.88}
#    ]
#}

#benchmarks = {
#    'mcnc_ver': [
#        {'filename': 'C1355_copy.v', 'spr_min3A_pso_core_utilization': 0.82, 'spr_min3A_pso_pd_utilization': 0.72},
#        {'filename': 'C6288_copy.v', 'spr_min3A_pso_core_utilization': 0.75, 'spr_min3A_pso_pd_utilization': 0.54}
#    ],
#   'epfl_ver': [
#        {'filename': 'adder.v'},
#        {'filename': 'arbiter.v', 'spr_min3A_pso_core_utilization': 0.7, 'spr_min3A_pso_pd_utilization': 0.55},
#        {'filename': 'bar.v'},
#        {'filename': 'cavlc.v'},
#        {'filename': 'ctrl.v', 'spr_min3A_pso_pd_utilization': 0.7},
#        {'filename': 'dec.v', 'spr_min3A_pso_pd_utilization': 0.7},
#         {'filename': 'div.v', 'spr_min3A_driv1B_core_utilization': 0.7, 'spr_min3A_pso_core_utilization': 0.55, 'spr_min3A_pso_pd_utilization': 0.55},
########        {'filename': 'hyp.v', 'spr_min3A_core_utilization': 0.6, 'spr_min3A_pso_core_utilization': 0.5, 'spr_min3A_pso_preplace_optimization': False},
#        {'filename': 'i2c.v'},
#        {'filename': 'int2float.v', 'spr_min3A_pso_pd_utilization': 0.7},
#        {'filename': 'log2.v', 'spr_min3A_core_utilization': 0.7, 'spr_min3A_driv1B_core_utilization': 0.7, 'spr_min3A_pso_core_utilization': 0.55, 'spr_min3A_pso_pd_utilization': 0.55},
#        {'filename': 'max.v'},
#        {'filename': 'mem_ctrl.v', 'spr_min3A_core_utilization': 0.75, 'spr_min3A_pso_core_utilization': 0.6},#, 'spr_min3A_pso_pd_utilization': 0.5},
#        {'filename': 'multiplier.v', 'spr_min3A_core_utilization': 0.79},#, 'spr_min3A_pso_core_utilization': 0.7, 'spr_min3A_pso_pd_utilization': 0.6},
#        {'filename': 'priority.v'},
#        {'filename': 'router.v'},
#        {'filename': 'sin.v'},
########         {'filename': 'sqrt.v', 'spr_min3A_core_utilization': 0.8},#, 'spr_min3A_pso_pd_utilization': 0.7},
#        {'filename': 'square.v'},#, 'spr_min3A_core_utilization': 0.74, 'spr_min3A_driv1B_core_utilization': 0.72, 'spr_min3A_pso_core_utilization': 0.7, 'spr_min3A_pso_pd_utilization': 0.4},
#         {'filename': 'voter.v', 'spr_min3A_pso_core_utilization': 0.6}#, 'spr_min3A_pso_pd_utilization': 0.75},#, 'spr_min3A_pso_core_utilization': 0.7, 'spr_min3A_pso_pd_utilization': 0.4}
#    ]
#}

# settings for PSO, using the 'bar' benchmark and a replacement ration from 0% to 100%
#benchmarks = {
#    'epfl_ver': [
##        {'filename': 'bar.v', 'replacement_ratio': 0.0},
##        {'filename': 'bar.v', 'replacement_ratio': 0.1, 'spr_min3A_pso_pd_utilization': 0.85},
##        {'filename': 'bar.v', 'replacement_ratio': 0.2, 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.86},
##        {'filename': 'bar.v', 'replacement_ratio': 0.3, 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.88},
##        {'filename': 'bar.v', 'replacement_ratio': 0.4, 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.87},
##        {'filename': 'bar.v', 'replacement_ratio': 0.5, 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.88},
##        {'filename': 'bar.v', 'replacement_ratio': 0.6, 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.87},
##        {'filename': 'bar.v', 'replacement_ratio': 0.7, 'spr_min3A_pso_core_utilization': 0.8, 'spr_min3A_pso_pd_utilization': 0.86},
##        {'filename': 'bar.v', 'replacement_ratio': 0.8, 'spr_min3A_pso_core_utilization': 0.75, 'spr_min3A_pso_pd_utilization': 0.87},
##        {'filename': 'bar.v', 'replacement_ratio': 0.9, 'spr_min3A_pso_core_utilization': 0.73, 'spr_min3A_pso_pd_utilization': 0.87},
#        {'filename': 'bar.v', 'replacement_ratio': 1.0, 'spr_min3A_pso_core_utilization': 0.73, 'spr_min3A_pso_pd_utilization': 0.84},
#    ]
#}


# environment variables for Cadence Encounter
# see /opt/scripts/setup_tools
os.environ['LM_LICENSE_FILE'] = '5280@eeelc1.et.tu-dresden.de::5280@eeelc2.et.tu-dresden.de'
os.environ['EDIPATH'] = '/opt/EDI142'
os.environ['PATH'] += ':' + os.environ['EDIPATH'] + '/bin'
os.environ['PATH'] += ':' + os.environ['EDIPATH'] + '/tools/bin'
os.environ['CDS_AUTO_64BIT'] = 'ALL'
os.environ['LANG'] = 'C'

class SPR:
    """Runs standard cell place & route (SPR) without changing the original netlist."""
    def __init__(self, directory, netlist_name, output_directory, core_utilization, suffix = '', template = 'spr'):
        self.directory = directory
        self.output_directory = output_directory
        os.makedirs(self.output_directory, exist_ok=True)
        # original netlist
        self.netlist = Netlist(directory, netlist_name)
        self.basename = os.path.splitext(netlist_name)[0] + '_' +  suffix + '_' + template
        # path of transformed netlist
        self.netlist_path = os.path.join(output_directory, self.basename + '.v')
        self.cpf_template = 'spr.cpf'
        self.tcl_template = template + '.tcl'
        self.core_utilization = core_utilization
        self.preplace_optimization = True

    def transform_netlist(self):
        pass

    def create_cpf_file(self):
        """Creates CPF file for Cadence Encounter."""
        renderer = pystache.Renderer()
        module_name = self.netlist.module_name

        with open(os.path.join(self.output_directory, self.basename + '.cpf'), 'w') as cpf_file:
            cpf_file.write(renderer.render_name(self.cpf_template, {
                'moduleName': module_name
            }))

    def create_tcl_script(self, additional_settings = {}, stop_after_floorplanning = False, load_floorplan = False):
        """Creates TCL script for Cadence Encounter."""
        renderer = pystache.Renderer()

        settings = {
            'basename': self.basename,
            'stopAfterFloorplanning': stop_after_floorplanning,
            'loadFloorplan': load_floorplan,
            'ratio': 1,
            'coreUtilization': self.core_utilization,
            'preplaceOptimization': self.preplace_optimization,
            'core2left': 5.04, # multiple of 0.08 (metal1)
            'core2bottom': 5,
            'core2right': 5.04, # multiple of 0.08 (metal1)
            'core2top': 5,
            'ringWidth': 1,
            'ringSpacing': 1,
            'ringOffset': 1
        }
        settings.update(additional_settings)

        with open(os.path.join(self.output_directory, self.basename + '.tcl'), 'w') as tcl_script:
            tcl_script.write(renderer.render_name(self.tcl_template, settings))

    def run_encounter(self):
        """Run Cadence Encounter."""
        cp = subprocess.run(['encounter', '-lic_startup', 'invs', '-cpus', '8', '-batch', '-file', self.basename + '.tcl', '-log', self.basename + '.log', '-overwrite'], cwd=self.output_directory)
        assert cp.returncode == 0, f'Encounter did not exit cleanly! Return value: {cp.returncode}'

    def get_floorplan_size(self):
        """Get floorplan size (core and die)."""
        die_box = re.compile(r'Head Box: (\S+) (\S+) (\S+) (\S+)')
        core_box = re.compile(r'Core Box: (\S+) (\S+) (\S+) (\S+)')
        die_width = 0.0
        die_height = 0.0
        core_width = 0.0
        core_height = 0.0
        with open(os.path.join(self.output_directory, self.basename + '.fp'), 'r') as floorplan:
            for line in floorplan:
                match = re.match(die_box, line)
                if match:
                    die_width = float(match.group(3)) - float(match.group(1))
                    die_height = float(match.group(4)) - float(match.group(2))

                match = re.match(core_box, line)
                if match:
                    core_width = float(match.group(3)) - float(match.group(1))
                    core_height = float(match.group(4)) - float(match.group(2))
                    break
        fp = {
            'die_width'  : die_width,
            'die_height' : die_height,
            'core_width' : core_width,
            'core_height': core_height
        }
        return fp

    def get_wire_length(self):
        wire_length = re.compile(r'#Post Route wire spread is done.(\r\n|\r|\n)#Total wire length = (\d+) um.', re.MULTILINE)
        wl = 0
        with open(os.path.join(self.output_directory, self.basename + '.log'), 'r') as log:
            content = log.read()
        match = re.search(wire_length, content)
        wl = float(match.group(2))
        return wl

    def get_dynamic_power(self):
        unit = re.compile(r'^\*\s+Power Units = 1mW', re.MULTILINE)
        floating_point_number = r'[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?'
        dynamic_power = re.compile(r'^Total \(\s*\d+\s+of\s+\d+\s*\)\s+' + floating_point_number + r'\s+' + floating_point_number + r'\s+(?P<dp>' + floating_point_number + r')\s+' + floating_point_number, re.MULTILINE)
        with open(os.path.join(self.output_directory, self.basename + '.rpt'), 'r') as report:
            content = report.read()
        match = re.search(unit, content)
        if not match:
            raise AssertionError('Expected milliwatts (mW) as power unit in {filename}!'.format(filename=self.basename + '.rpt'))
        match = re.search(dynamic_power, content)
        dp = float(match.group('dp'))
        return dp

    def run(self):
        self.transform_netlist()
        total_gate_area = self.netlist.calculate_area()
        print("***********************************")
        print("* Total Gate Area: " + str(total_gate_area))
        print("***********************************")
        sp = self.netlist.calculate_static_power()
        self.netlist.write_transformed_netlist(self.netlist_path)
        self.create_cpf_file()
        self.create_tcl_script()
        self.run_encounter()
        fp = self.get_floorplan_size()
        wl = self.get_wire_length()
        dp = self.get_dynamic_power()
        return {'floorplan': fp, 'wire_length': wl, 'static_power': sp, 'dynamic_power': dp}



class SPR_min3A(SPR):
    """Replaces some of the nand2/nor2 gates with min3A gates (that include an
    inverter for P/PBAR generation); connects their additional input pin s with
    the net n_reconf."""
    def __init__(self, directory, netlist_name, output_directory, replacement_ratio, core_utilization, suffix = '', template = 'spr_min3A'):
        SPR.__init__(self, directory, netlist_name, output_directory, core_utilization, suffix, template)
        self.replacement_ratio = replacement_ratio

    def transform_netlist(self):
        self.netlist.add_wire('n_reconf')
        self.netlist.replace_gate_ratio(self.replacement_ratio, ['nand2', 'nor2'], 'min3A', '.s(n_reconf)')

class SPR_min3A_driv1B(SPR):
    """Replaces some of the nand2/nor2 gates with min3A_SPR gates (that do not
    include an inverter for P/PBAR generation, but have additional inputs for
    P/PBAR); adds driver gate driv1B_SPR and connects its input to n_reconf and
    outputs to n_s and n_sbar."""
    def __init__(self, directory, netlist_name, output_directory, replacement_ratio, core_utilization, suffix = '', template = 'spr_min3A_driv1B'):
        SPR.__init__(self, directory, netlist_name, output_directory, core_utilization, suffix, template)
        self.replacement_ratio = replacement_ratio

    def transform_netlist(self):
        gates_to_replace = ['nand2', 'nor2']
        num_relevant_gates = self.netlist.count_instances(gates_to_replace)
        matches = self.netlist.get_some_instances(self.replacement_ratio, gates_to_replace)
        num_gates_to_replace = len(matches)
        # calculate required number of driver cells: 1 driver cell per 5 minority gates
        num_drivers = math.ceil(num_gates_to_replace / 5.0)

        print('************************************')
        print('* Replacement ratio: ' + str(self.replacement_ratio))
        print('* Found ' + str(num_relevant_gates) + ' relevant gates')
        print('* Replacing ' + str(num_gates_to_replace) + ' of them')
        print('* Creating ' + str(num_drivers) + ' driver cells')
        print('************************************')

        self.netlist.add_wire('n_reconf')
        for i in range(num_drivers):
            self.netlist.add_wire('n_s' + str(i))
            self.netlist.add_wire('n_sbar' + str(i))

        for i in range(num_drivers):
            print(str(i) + ". driver cell powers: ", matches[i::num_drivers])
            self.netlist.replace_gates(matches[i::num_drivers], 'min3A_SPR', '.s(n_s{}), .s_bar(n_sbar{})'.format(i,i))

        for i in range(num_drivers):
            self.netlist.add_gate('driv1B_SPR', 'gDRIVER' + str(i), '.s(n_reconf), .O(n_s{}), .O_bar(n_sbar{})'.format(i, i))

class SPR_min3A_PSO(SPR):
    """Replaces some of the nand2/nor2 gates with min3A_PSO gates (that do not
    include an inverter for P/PBAR generation, but get these signal over their
    metal1 rails); uses a separate power domain with power-switch-off gate."""
    def __init__(self, directory, netlist_name, output_directory, replacement_ratio, core_utilization, suffix = '', template = 'spr_min3A_pso'):
        SPR.__init__(self, directory, netlist_name, output_directory, core_utilization, suffix, template)
        self.replacement_ratio = replacement_ratio
        self.pd_utilization = core_utilization
        self.pd_type = PowerDomainType.HORIZONTALLY_AND_VERTICALLY_CENTERED
        self.cpf_template = template + '.cpf'

    def transform_netlist(self):
        self.netlist.add_input('s1')
        self.netlist.replace_gate_ratio(self.replacement_ratio, ['nand2', 'nor2'], 'min3A_PSO')

    def create_cpf_file(self):
        """Creates CPF file for Cadence Encounter."""
        renderer = pystache.Renderer()
        module_name = self.netlist.module_name
        replaced_gates = self.netlist.replaced_gates

        with open(os.path.join(self.output_directory, self.basename + '.cpf'), 'w') as cpf_file:
            cpf_file.write(renderer.render_name(self.cpf_template, {
                'moduleName': module_name,
                'gates': ' '.join(replaced_gates),
            }))

    def run(self):
        self.transform_netlist()
        sp = self.netlist.calculate_static_power()
        self.netlist.write_transformed_netlist(self.netlist_path)

        self.create_cpf_file()

        # calculate required number of driver cells: 1 driver cell per 5 minority gates
        # this number may be increased due to the regular grid of driver cells in the power domain
        num_drivers = math.ceil(len(self.netlist.replaced_gates) / 5.0)

        # calculate size of the power domain
        minority_gate_area = len(self.netlist.replaced_gates) * tech.area['min3A_PSO']

        # manually calculate the core and power domain size
        # Encounter doesn't consider the area of the power domain and its gates
        # when calculating the floorplan area for the given utilization. This
        # results in a very high utilization outside the power domain, if the
        # utilization in the power domain is lower than in the rest of the
        # chip. Here, we calculate a new utilization that compensates for this
        # effect.
        total_gate_area = self.netlist.calculate_area()
        # gate area outside of the power domain
        pd0_gate_area = total_gate_area - minority_gate_area

        core_aspect_ratio = 1
        pd_aspect_ratio = 1

        # whitespace: empty rows above and below of power domain
        whitespace_area = 0.0
        core_area = 0.0

        while True:
            # calculate the size of the core
            old_core_area = core_area

            pd_gate_area = minority_gate_area + num_drivers * tech.area['driv1B_SPR']
            print("pd_gate_area: ", pd_gate_area)
            # utilization
            pd_area = pd_gate_area/self.pd_utilization

            core_area = pd0_gate_area/self.core_utilization + pd_area + whitespace_area
            core_height = math.sqrt(core_area/core_aspect_ratio)
            # first, snap height to the next multiple of site_height; use round and not ceil
            # for a better square (encounter does the same)
            core_height = round(core_height/tech.site_height) * tech.site_height
            core_width = core_area/core_height

            # fix core_height (add empty rows above and below of the power domain)
            # core_height = (round(core_height / tech.site_height) + 2) * tech.site_height

            # width should be a multiple of grid_width; use ceil and not round for
            # an area that is larger than required
            core_width = math.ceil(core_width/tech.grid_width) * tech.grid_width

            if pd_type == PowerDomainType.HORIZONTALLY_AND_VERTICALLY_CENTERED or pd_type == PowerDomainType.TOP_CENTER:
                pd_height = math.sqrt(pd_area/pd_aspect_ratio)
                print("pd_height: ", pd_height)
                # first, snap height to the next multiple of site_height; use round and not ceil
                # for a better square (encounter does the same)
                pd_height = round(pd_height/tech.site_height) * tech.site_height
                print("pd_height: ", pd_height)
                pd_width = pd_area/pd_height
                print("pd_width: ", pd_width)
                # width should be a multiple of grid_width; use ceil and not round for
                # an area that is larger than required
                pd_width = math.ceil(pd_width/tech.grid_width) * tech.grid_width
                print("pd_width: ", pd_width)
                whitespace_area = pd_width * 2 * tech.site_height
                pd_x1 = round(0.5*(core_width-pd_width)/tech.grid_width) * tech.grid_width
                pd_y1 = round(0.5*(core_height-pd_height)/tech.site_height) * tech.site_height
                if pd_type == PowerDomainType.TOP_CENTER:
                    whitespace_area = pd_width * tech.site_height
                    pd_y1 = 0
            elif pd_type == PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED:
                pd_width = core_width
                pd_height = pd_area/pd_width
                pd_height = math.ceil(pd_height/tech.site_height) * tech.site_height
                whitespace_area = core_width * 2 * tech.site_height
                pd_x1 = 0
                pd_y1 = round(0.5*(core_height-pd_height)/tech.site_height) * tech.site_height
            else:
                raise AssertionError('Unknown power domain type!')

            pd_area = pd_width * pd_height

            num_rows = round(pd_height/tech.site_height)
            # number of rows to skip when placing a switch in a column
            skip_rows = 2;
            num_vertical_drivers = math.ceil(num_rows / (skip_rows+1))
            num_horizontal_drivers = math.ceil(num_drivers / num_vertical_drivers)
            # update number of drivers
            num_drivers = num_vertical_drivers * num_horizontal_drivers
            horizontal_pitch = pd_width / num_horizontal_drivers
            horizontal_pitch = math.ceil(horizontal_pitch/tech.site_width) * tech.site_width

            if abs(core_area-old_core_area) < 0.01 * old_core_area:
                break

        original_core_utilization = self.core_utilization
        # A_FP = A_G / util
        # A_PD0 = A_G,PD0 / util_PD0
        # A_FP = A_PD1 + A_PD0
        # => new_util = A_G / A_FP = A_G / (A_PD1 + A_PD0) = A_G / (A_PD1 + A_G,PD0 / util_PD0)
        # the term width*tech.site_height compensates for the empty row below the power domain
        #self.core_utilization = total_gate_area / (pd_area + pd_width * tech.site_height + pd0_gate_area / self.core_utilization)

        # snap core width and height to placement grid
        # otherwise, it is snapped by encounter and we possibly get an
        # incomplete row at the top
        snapped_core_width = math.ceil(core_width/tech.grid_width) * tech.grid_width
        snapped_core_height = math.ceil(core_height/tech.grid_height) * tech.grid_height

        print('*****************************')
        print('* Total gate area (w/o drivers): ', format(total_gate_area, '.3f'))
        print('* Power domain PD1 gate area (w/ drivers): ', format(pd_gate_area, '.3f'))
        print('* PD0 gate area: ', format(pd0_gate_area, '.3f'))
        print('* Original core utilization: ', str(original_core_utilization))
        print('* Core area: ', format(core_area, '.3f'))
        #print('* Adapted core utilization: ' + format(self.core_utilization, '.3f'))
        print('* Core: ' + format(snapped_core_width, tech.grid_width_format) + ' x ' + format(snapped_core_height, tech.grid_height_format) + ' (w x h)')
        print('* Power Domain: ' + format(pd_width, '.3f') + ' x ' + format(pd_height, '.3f') + ' (w x h)')
        print('* Number of Minority Gates: ', len(self.netlist.replaced_gates))
        print('* Number of Driver Cells: ', num_drivers)
        print('* Number of vertical drivers: ', num_vertical_drivers)
        print('* Number of horizontal drivers: ', num_horizontal_drivers)
        print('* Skip Rows: ', skip_rows)
        print('* Horizontal Pitch: ', format(horizontal_pitch, '.3f'))
        print('*****************************')


        tcl_settings = {
            'coreWidth': format(snapped_core_width, tech.grid_width_format),
            'coreHeight': format(snapped_core_height, tech.grid_height_format),
            'PDx1': format(pd_x1, '.3f'),
            'PDy1': format(pd_y1, '.3f'),
            'PDx2': format(pd_x1+pd_width, '.3f'),
            'PDy2': format(pd_y1+pd_height, '.3f'),
            'horizontalPitch': format(horizontal_pitch, '.3f'),
            'skipRows': skip_rows
        }

        # First, stop after floorplanning
        self.create_tcl_script(tcl_settings, stop_after_floorplanning = True)
        self.run_encounter()
        fp = self.get_floorplan_size()

        self.create_tcl_script(tcl_settings, load_floorplan = True)
        self.run_encounter()
        wl = self.get_wire_length()
        dp = self.get_dynamic_power()
        return {'floorplan': fp, 'wire_length': wl, 'static_power': sp, 'dynamic_power': dp}


if __name__ == '__main__':

    #nl = Netlist('mcnc_ver', 'C1355_copy.v')
    #print(nl.count_relevant_gates(['nand2', 'nor2']))

    #spr = SPR('mcnc_ver', '5xp1.v', 'mcnc_ver_processed')
    #result = spr.run()
    ##{'floorplan': {'die_width': 19.99, 'die_height': 18.64, 'core_width': 9.91, 'core_height': 8.64}, 'wire_length': '465', 'static_power': 8009.220450699987}
    #print(result)

    #baseline = BaselineSPR('mcnc_ver', '5xp1.v', 'mcnc_ver_processed', 0.1)
    #result = baseline.run()
    ##{'floorplan': {'die_width': 19.7, 'die_height': 19.36, 'core_width': 9.620000000000001, 'core_height': 9.36}, 'wire_length': '462', 'static_power': 8603.274282399985}
    #print(result)

    #basename = 'spr_min3A'
    #klass = SPR_min3A

    #basename = 'spr_min3A_driv1B'
    #klass = SPR_min3A_driv1B

    #basename = 'spr_min3A_pso'
    #klass = SPR_min3A_PSO

    configurations = [
        {
            "basename": "spr_min3A",
            "klass": SPR_min3A
        },
        {
            "basename": "spr_min3A_driv1B",
            "klass": SPR_min3A_driv1B
        },
        {
            "basename": "spr_min3A_pso",
            "klass": SPR_min3A_PSO,
            "pd_type": PowerDomainType.HORIZONTALLY_AND_VERTICALLY_CENTERED
        },
        {
            "basename": "spr_min3A_pso",
            "klass": SPR_min3A_PSO,
            "pd_type": PowerDomainType.FULL_WIDTH_VERTICALLY_CENTERED
        },
        {
            "basename": "spr_min3A_pso",
            "klass": SPR_min3A_PSO,
            "pd_type": PowerDomainType.TOP_CENTER
        }
    ]

    default_replacement_ratio = 0.3

    for config in configurations:
        basename = config["basename"]
        klass = config["klass"]
        pd_type = config.setdefault("pd_type", PowerDomainType.HORIZONTALLY_AND_VERTICALLY_CENTERED)
        csv_filename = datetime.date.today().isoformat() + '_' + basename + '.csv'
        with open(csv_filename, 'a') as csv:
            csv.write('Directory, Netlist, Replacement Ratio, Core Utilization, Power Domain Type, Power Domain Utilization, Static Power [pW], Dynamic Power [mW], Core Area, Core Width, Core Height, Die Width, Die Height, Wire Length [µm]\n')
            for directory in benchmarks:
                for benchmark in benchmarks[directory]:
                    filename = benchmark['filename']
                    replacement_ratio = benchmark.setdefault('replacement_ratio', default_replacement_ratio)
                    # skip emtpy power domain
                    if replacement_ratio == 0.0 and klass == SPR_min3A_PSO:
                        continue
                    # get the core_utilization (default is 0.8)
                    core_utilization = benchmark.setdefault(basename + '_core_utilization', 0.8)
                    # get the power domain utilization (default is core_utilization)
                    pd_utilization = benchmark.setdefault(basename + '_pd_utilization', core_utilization)
                    # get the power domain type (default is horizontally_and_vertically_centered)
                    #pd_type = benchmark.setdefault(basename + '_type', PowerDomainType.HORIZONTALLY_AND_VERTICALLY_CENTERED)
                    # should we perform preplace optimization?
                    preplace_optimization = benchmark.setdefault(basename + '_preplace_optimization', True)
                    print('*****************************')
                    print('* Benchmark: ', os.path.join(directory, filename))
                    print('* Configuration: ', klass.__name__)
                    print('* Core Utilization: ', core_utilization)
                    print('* PD1 Utilization: ', pd_utilization)
                    print('* Preplace Optimization: ', preplace_optimization)
                    print('*****************************')
                    suffix = str(int(replacement_ratio*100)) + 'pct'
                    if pd_type:
                        suffix += '_' + pd_type.value
                    bench = klass(directory, filename, directory + '_processed', replacement_ratio, core_utilization, suffix)
                    #bench.netlist.partition(0.1, ['nand2', 'nor2'])
                    bench.pd_utilization = pd_utilization
                    bench.pd_type = pd_type
                    bench.preplace_optimization = preplace_optimization
                    result = bench.run()
                    gif = os.path.join(bench.output_directory, bench.basename + '.gif')
                    gif_placement = os.path.join(bench.output_directory, bench.basename + '_placement.gif')
                    gif_cells = os.path.join(bench.output_directory, bench.basename + '_cells.gif')
                    # add additional information to the image files
                    font = ImageFont.load("cherry-10-r.pil")
                    for path in [gif, gif_placement, gif_cells]:
                        image = Image.open(path)
                        draw=ImageDraw.Draw(image)
                        info = "core/pd util:%g/%g;stat/dyn pow:%.2fpW/%gmW;core:%.2fx%.2f=%.2f;die:%.2fx%.2f=%.2f;wl=%gµm" % \
                            (core_utilization,
                             pd_utilization,
                             result['static_power'],
                             result['dynamic_power'],
                             result['floorplan']['core_width'],
                             result['floorplan']['core_height'],
                             result['floorplan']['core_width'] * result['floorplan']['core_height'],
                             result['floorplan']['die_width'],
                             result['floorplan']['die_height'],
                             result['floorplan']['die_width'] * result['floorplan']['die_height'],
                             result['wire_length'])
                        draw.text((0,0), info , 255, font=font)
                        # add date at the bottom
                        draw.text((0,image.height-11), time.asctime(), 255, font=font)
                        image.info['comment'] = info.encode()
                        image.save(path)
                    #subprocess.Popen(['eog', gif_placement])
                    #subprocess.Popen(['eog', gif])
                    #subprocess.Popen(['eog', gif_cells])
                    csv.write(', '.join(str(e) for e in [
                        directory,
                        filename,
                        replacement_ratio,
                        core_utilization,
                        pd_type.name,
                        pd_utilization,
                        format(result['static_power'], '.2f'),
                        result['dynamic_power'],
                        format(result['floorplan']['core_width'] * result['floorplan']['core_height'], '.2f'),
                        format(result['floorplan']['core_width'], '.2f'),
                        format(result['floorplan']['core_height'], '.2f'),
                        format(result['floorplan']['die_width'], '.2f'),
                        format(result['floorplan']['die_height'], '.2f'),
                        result['wire_length']]) + '\n')

