// Benchmark "decod" written by ABC on Tue Nov  5 15:01:20 2019

module decod ( 
    a, b, c, d, e,
    f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u  );
  input  a, b, c, d, e;
  output f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u;
  wire n22, n23, n24, n25, n26, n28, n29, n31, n32, n34, n35, n37, n38, n40,
    n42, n44, n46, n47;
  nand2 g00(.a(e), .b(a), .O(n22));
  inv1  g01(.a(c), .O(n23));
  inv1  g02(.a(d), .O(n24));
  nor2  g03(.a(n24), .b(n23), .O(n25));
  nand2 g04(.a(n25), .b(b), .O(n26));
  nor2  g05(.a(n26), .b(n22), .O(f));
  nor2  g06(.a(d), .b(n23), .O(n28));
  nand2 g07(.a(n28), .b(b), .O(n29));
  nor2  g08(.a(n29), .b(n22), .O(g));
  nor2  g09(.a(n24), .b(c), .O(n31));
  nand2 g10(.a(n31), .b(b), .O(n32));
  nor2  g11(.a(n32), .b(n22), .O(h));
  nor2  g12(.a(d), .b(c), .O(n34));
  nand2 g13(.a(n34), .b(b), .O(n35));
  nor2  g14(.a(n35), .b(n22), .O(i));
  inv1  g15(.a(b), .O(n37));
  nand2 g16(.a(n25), .b(n37), .O(n38));
  nor2  g17(.a(n38), .b(n22), .O(j));
  nand2 g18(.a(n28), .b(n37), .O(n40));
  nor2  g19(.a(n40), .b(n22), .O(k));
  nand2 g20(.a(n31), .b(n37), .O(n42));
  nor2  g21(.a(n42), .b(n22), .O(l));
  nand2 g22(.a(n34), .b(n37), .O(n44));
  nor2  g23(.a(n44), .b(n22), .O(m));
  inv1  g24(.a(a), .O(n46));
  nand2 g25(.a(e), .b(n46), .O(n47));
  nor2  g26(.a(n47), .b(n26), .O(n));
  nor2  g27(.a(n47), .b(n29), .O(o));
  nor2  g28(.a(n47), .b(n32), .O(p));
  nor2  g29(.a(n47), .b(n35), .O(q));
  nor2  g30(.a(n47), .b(n38), .O(r));
  nor2  g31(.a(n47), .b(n40), .O(s));
  nor2  g32(.a(n47), .b(n42), .O(t));
  nor2  g33(.a(n47), .b(n44), .O(u));
endmodule


