// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:17 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,
    v15.0 , v15.1 , v15.2 , v15.3 , v15.4 , v15.5 , v15.6 , v15.7 ,
    v15.8   );
  input  v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14;
  output v15.0 , v15.1 , v15.2 , v15.3 , v15.4 , v15.5 , v15.6 ,
    v15.7 , v15.8 ;
  wire n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n38, n39,
    n40, n41, n42, n43, n44, n45, n46, n47, n49, n50, n51, n52, n53, n54,
    n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68,
    n70, n71, n72, n73, n75, n76, n77, n78, n79, n80, n82, n83, n84, n85,
    n87, n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100,
    n101, n102, n103, n104, n105, n106, n107, n108, n109, n110, n112, n113,
    n114, n115, n116, n117, n119, n120, n121, n122, n123, n124;
  inv1  g000(.a(v1), .O(n25));
  inv1  g001(.a(v4), .O(n26));
  nor2  g002(.a(v5), .b(n26), .O(n27));
  nor2  g003(.a(v3), .b(v2), .O(n28));
  nor2  g004(.a(n28), .b(n27), .O(n29));
  inv1  g005(.a(v3), .O(n30));
  nor2  g006(.a(n30), .b(v2), .O(n31));
  nor2  g007(.a(n31), .b(n29), .O(n32));
  nor2  g008(.a(n32), .b(n25), .O(n33));
  nand2 g009(.a(v3), .b(v2), .O(n34));
  nor2  g010(.a(n34), .b(v4), .O(n35));
  nor2  g011(.a(n35), .b(n33), .O(n36));
  nor2  g012(.a(n36), .b(v0), .O(v15.0 ));
  nand2 g013(.a(v6), .b(v5), .O(n38));
  nor2  g014(.a(n38), .b(n25), .O(n39));
  nand2 g015(.a(n26), .b(n30), .O(n40));
  nand2 g016(.a(n40), .b(v2), .O(n41));
  nor2  g017(.a(n41), .b(n39), .O(n42));
  inv1  g018(.a(v2), .O(n43));
  nor2  g019(.a(n38), .b(n43), .O(n44));
  nand2 g020(.a(v3), .b(v1), .O(n45));
  nor2  g021(.a(n45), .b(n44), .O(n46));
  nor2  g022(.a(n46), .b(n42), .O(n47));
  nor2  g023(.a(n47), .b(v0), .O(v15.1 ));
  nor2  g024(.a(v8), .b(v7), .O(n49));
  inv1  g025(.a(n49), .O(n50));
  nand2 g026(.a(n50), .b(v0), .O(n51));
  inv1  g027(.a(v9), .O(n52));
  inv1  g028(.a(v10), .O(n53));
  nand2 g029(.a(n53), .b(n52), .O(n54));
  nor2  g030(.a(v3), .b(v1), .O(n55));
  nand2 g031(.a(n55), .b(n54), .O(n56));
  nand2 g032(.a(n56), .b(n43), .O(n57));
  nand2 g033(.a(n57), .b(n51), .O(n58));
  inv1  g034(.a(v0), .O(n59));
  nand2 g035(.a(v7), .b(n30), .O(n60));
  inv1  g036(.a(v7), .O(n61));
  nor2  g037(.a(v10), .b(n52), .O(n62));
  nand2 g038(.a(n62), .b(n61), .O(n63));
  nand2 g039(.a(n63), .b(n60), .O(n64));
  nand2 g040(.a(n64), .b(n59), .O(n65));
  nand2 g041(.a(n62), .b(n49), .O(n66));
  nand2 g042(.a(n66), .b(n65), .O(n67));
  nand2 g043(.a(n67), .b(n25), .O(n68));
  nand2 g044(.a(n68), .b(n58), .O(v15.2 ));
  nand2 g045(.a(v11), .b(v7), .O(n70));
  inv1  g046(.a(n70), .O(n71));
  inv1  g047(.a(v12), .O(n72));
  nor2  g048(.a(n72), .b(v0), .O(n73));
  nor2  g049(.a(n73), .b(n71), .O(v15.3 ));
  inv1  g050(.a(v8), .O(n75));
  nor2  g051(.a(n75), .b(n59), .O(n76));
  nand2 g052(.a(n76), .b(v9), .O(n77));
  nand2 g053(.a(n77), .b(n70), .O(n78));
  inv1  g054(.a(v11), .O(n79));
  nand2 g055(.a(n79), .b(v7), .O(n80));
  nand2 g056(.a(n80), .b(n78), .O(v15.4 ));
  nor2  g057(.a(v7), .b(v0), .O(n82));
  inv1  g058(.a(v13), .O(n83));
  nand2 g059(.a(n72), .b(n53), .O(n84));
  nor2  g060(.a(n84), .b(n83), .O(n85));
  nand2 g061(.a(n85), .b(n82), .O(v15.5 ));
  nor2  g062(.a(v2), .b(v1), .O(n87));
  inv1  g063(.a(n87), .O(n88));
  nand2 g064(.a(v9), .b(n30), .O(n89));
  nand2 g065(.a(n52), .b(v3), .O(n90));
  nand2 g066(.a(n90), .b(n89), .O(n91));
  nor2  g067(.a(n91), .b(n88), .O(n92));
  inv1  g068(.a(v14), .O(n93));
  nor2  g069(.a(n93), .b(v0), .O(n94));
  nor2  g070(.a(n94), .b(v8), .O(n95));
  nor2  g071(.a(v14), .b(v0), .O(n96));
  nor2  g072(.a(n96), .b(n95), .O(n97));
  nor2  g073(.a(n97), .b(n92), .O(n98));
  nor2  g074(.a(n52), .b(v3), .O(n99));
  nor2  g075(.a(v9), .b(n30), .O(n100));
  nor2  g076(.a(n100), .b(n99), .O(n101));
  nor2  g077(.a(n43), .b(n25), .O(n102));
  nor2  g078(.a(n102), .b(n101), .O(n103));
  nand2 g079(.a(v2), .b(n25), .O(n104));
  nand2 g080(.a(n43), .b(v1), .O(n105));
  nand2 g081(.a(n105), .b(n104), .O(n106));
  nor2  g082(.a(n106), .b(n103), .O(n107));
  nor2  g083(.a(n107), .b(n76), .O(n108));
  nor2  g084(.a(n108), .b(n98), .O(n109));
  nand2 g085(.a(n53), .b(n61), .O(n110));
  nor2  g086(.a(n110), .b(n109), .O(v15.6 ));
  nand2 g087(.a(n31), .b(n25), .O(n112));
  nand2 g088(.a(n112), .b(v8), .O(n113));
  nand2 g089(.a(n113), .b(v9), .O(n114));
  inv1  g090(.a(n112), .O(n115));
  nor2  g091(.a(n115), .b(v8), .O(n116));
  nor2  g092(.a(n116), .b(n110), .O(n117));
  nand2 g093(.a(n117), .b(n114), .O(v15.7 ));
  nand2 g094(.a(n52), .b(v0), .O(n119));
  nor2  g095(.a(v12), .b(n30), .O(n120));
  nand2 g096(.a(n120), .b(n87), .O(n121));
  nand2 g097(.a(n121), .b(n119), .O(n122));
  nor2  g098(.a(v9), .b(v0), .O(n123));
  nor2  g099(.a(n123), .b(n110), .O(n124));
  nand2 g100(.a(n124), .b(n122), .O(v15.8 ));
endmodule


