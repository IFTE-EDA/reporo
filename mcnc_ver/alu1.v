// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:16 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11,
    v12.0 , v12.1 , v12.2 , v12.3 , v12.4 , v12.5 , v12.6 , v12.7   );
  input  v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11;
  output v12.0 , v12.1 , v12.2 , v12.3 , v12.4 , v12.5 , v12.6 ,
    v12.7 ;
  wire n21, n22, n23, n24, n25, n26, n28, n29, n30, n31, n32, n34, n35, n36,
    n37, n38, n40, n41, n42, n43, n44, n46, n47, n48, n50, n51, n52, n54,
    n55, n56, n58;
  inv1  g00(.a(v4), .O(n21));
  inv1  g01(.a(v10), .O(n22));
  nand2 g02(.a(n22), .b(n21), .O(n23));
  inv1  g03(.a(v0), .O(n24));
  nor2  g04(.a(v11), .b(n21), .O(n25));
  nor2  g05(.a(n25), .b(n24), .O(n26));
  nand2 g06(.a(n26), .b(n23), .O(v12.0 ));
  inv1  g07(.a(v5), .O(n28));
  nand2 g08(.a(n22), .b(n28), .O(n29));
  inv1  g09(.a(v1), .O(n30));
  nor2  g10(.a(v11), .b(n28), .O(n31));
  nor2  g11(.a(n31), .b(n30), .O(n32));
  nand2 g12(.a(n32), .b(n29), .O(v12.1 ));
  inv1  g13(.a(v6), .O(n34));
  nand2 g14(.a(n22), .b(n34), .O(n35));
  inv1  g15(.a(v2), .O(n36));
  nor2  g16(.a(v11), .b(n34), .O(n37));
  nor2  g17(.a(n37), .b(n36), .O(n38));
  nand2 g18(.a(n38), .b(n35), .O(v12.2 ));
  inv1  g19(.a(v7), .O(n40));
  nand2 g20(.a(n22), .b(n40), .O(n41));
  inv1  g21(.a(v3), .O(n42));
  nor2  g22(.a(v11), .b(n40), .O(n43));
  nor2  g23(.a(n43), .b(n42), .O(n44));
  nand2 g24(.a(n44), .b(n41), .O(v12.3 ));
  nor2  g25(.a(v8), .b(n21), .O(n46));
  nor2  g26(.a(v9), .b(v4), .O(n47));
  nor2  g27(.a(n47), .b(n46), .O(n48));
  nor2  g28(.a(n48), .b(v0), .O(v12.4 ));
  nor2  g29(.a(v8), .b(n28), .O(n50));
  nor2  g30(.a(v9), .b(v5), .O(n51));
  nor2  g31(.a(n51), .b(n50), .O(n52));
  nor2  g32(.a(n52), .b(v1), .O(v12.5 ));
  nor2  g33(.a(v8), .b(n34), .O(n54));
  nor2  g34(.a(v9), .b(v6), .O(n55));
  nor2  g35(.a(n55), .b(n54), .O(n56));
  nor2  g36(.a(n56), .b(v2), .O(v12.6 ));
  nand2 g37(.a(v7), .b(n42), .O(n58));
  nor2  g38(.a(n58), .b(v8), .O(v12.7 ));
endmodule


