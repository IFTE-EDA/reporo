// Benchmark "traffic_cl" written by ABC on Tue Nov  5 15:01:24 2019

module traffic_cl ( 
    a, b, c, d, e,
    f  );
  input  a, b, c, d, e;
  output f;
  wire n7, n8, n9, n10, n11, n12, n13, n14, n15, n16, n17;
  inv1  g00(.a(d), .O(n7));
  inv1  g01(.a(a), .O(n8));
  inv1  g02(.a(c), .O(n9));
  inv1  g03(.a(e), .O(n10));
  nor2  g04(.a(n10), .b(n9), .O(n11));
  nand2 g05(.a(n11), .b(b), .O(n12));
  nand2 g06(.a(n12), .b(n8), .O(n13));
  nor2  g07(.a(n11), .b(b), .O(n14));
  nor2  g08(.a(e), .b(c), .O(n15));
  nor2  g09(.a(n15), .b(n14), .O(n16));
  nand2 g10(.a(n16), .b(n13), .O(n17));
  nand2 g11(.a(n17), .b(n7), .O(f));
endmodule


