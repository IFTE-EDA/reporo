// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:25 2019

module source_pla  ( 
    WAIT, CPIPE1s<0> , CPIPE1s<1> , CPIPE1s<2> , CPIPE1s<3> ,
    CPIPE1s<4> , CPIPE1s<5> , CPIPE1s<7> , RESET,
    changeCWP2t, trap, lastPCload, pDATABUSintoLOADL, enableINTS1,
    changeCWP1, PCtoMAL1, pALUtoMAL, pPCIncr, pALUtoPC, DST2step1,
    PCstuffoncall1, SRC2smin1, DST1min1, CPIPE1flush, CPIPE1load1  );
  input  WAIT, CPIPE1s<0> , CPIPE1s<1> , CPIPE1s<2> , CPIPE1s<3> ,
    CPIPE1s<4> , CPIPE1s<5> , CPIPE1s<7> , RESET;
  output changeCWP2t, trap, lastPCload, pDATABUSintoLOADL, enableINTS1,
    changeCWP1, PCtoMAL1, pALUtoMAL, pPCIncr, pALUtoPC, DST2step1,
    PCstuffoncall1, SRC2smin1, DST1min1, CPIPE1flush, CPIPE1load1;
  wire n26, n27, n28, n29, n30, n31, n32, n34, n35, n36, n37, n38, n39, n40,
    n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56,
    n57, n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n70, n72,
    n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87,
    n88, n89, n90, n91, n92, n94, n95, n96, n97, n98, n99, n100, n101,
    n102, n103, n104, n105, n106, n107, n108, n110, n111, n112, n113, n114,
    n115, n116, n117, n118, n120, n121, n122, n123, n125, n126, n127, n128,
    n129, n130, n131, n133, n134, n135, n136, n137, n138, n140, n141, n142,
    n144, n145, n146, n147, n148, n150, n151;
  inv1  g000(.a(WAIT), .O(n26));
  nand2 g001(.a(CPIPE1s<0> ), .b(n26), .O(n27));
  inv1  g002(.a(CPIPE1s<3> ), .O(n28));
  nor2  g003(.a(CPIPE1s<4> ), .b(n28), .O(n29));
  inv1  g004(.a(CPIPE1s<7> ), .O(n30));
  nor2  g005(.a(n30), .b(CPIPE1s<5> ), .O(n31));
  nand2 g006(.a(n31), .b(n29), .O(n32));
  nor2  g007(.a(n32), .b(n27), .O(changeCWP2t));
  nor2  g008(.a(CPIPE1s<5> ), .b(CPIPE1s<4> ), .O(n34));
  nand2 g009(.a(n34), .b(CPIPE1s<7> ), .O(n35));
  inv1  g010(.a(CPIPE1s<0> ), .O(n36));
  nor2  g011(.a(CPIPE1s<1> ), .b(n36), .O(n37));
  inv1  g012(.a(CPIPE1s<2> ), .O(n38));
  nor2  g013(.a(CPIPE1s<3> ), .b(n38), .O(n39));
  nand2 g014(.a(n39), .b(n37), .O(n40));
  nor2  g015(.a(n40), .b(n35), .O(trap));
  nor2  g016(.a(trap), .b(WAIT), .O(lastPCload));
  inv1  g017(.a(CPIPE1s<5> ), .O(n43));
  nor2  g018(.a(CPIPE1s<2> ), .b(CPIPE1s<0> ), .O(n44));
  inv1  g019(.a(n44), .O(n45));
  nor2  g020(.a(n45), .b(n43), .O(n46));
  nand2 g021(.a(n43), .b(CPIPE1s<2> ), .O(n47));
  nor2  g022(.a(n47), .b(n28), .O(n48));
  nor2  g023(.a(n48), .b(n46), .O(n49));
  nor2  g024(.a(n49), .b(CPIPE1s<1> ), .O(n50));
  inv1  g025(.a(CPIPE1s<4> ), .O(n51));
  nor2  g026(.a(n43), .b(n51), .O(n52));
  nand2 g027(.a(n52), .b(n28), .O(n53));
  nand2 g028(.a(n53), .b(n47), .O(n54));
  nand2 g029(.a(n54), .b(CPIPE1s<1> ), .O(n55));
  nor2  g030(.a(n51), .b(CPIPE1s<3> ), .O(n56));
  nand2 g031(.a(CPIPE1s<5> ), .b(n38), .O(n57));
  nand2 g032(.a(n57), .b(n56), .O(n58));
  nand2 g033(.a(n58), .b(n55), .O(n59));
  nor2  g034(.a(n59), .b(n50), .O(n60));
  nor2  g035(.a(n60), .b(n30), .O(n61));
  nor2  g036(.a(n30), .b(n43), .O(n62));
  nand2 g037(.a(n62), .b(n56), .O(n63));
  nand2 g038(.a(n63), .b(CPIPE1s<5> ), .O(n64));
  nand2 g039(.a(n64), .b(CPIPE1s<0> ), .O(n65));
  nor2  g040(.a(n30), .b(n51), .O(n66));
  nand2 g041(.a(n66), .b(n65), .O(n67));
  nor2  g042(.a(n67), .b(n61), .O(n68));
  nor2  g043(.a(n68), .b(WAIT), .O(pDATABUSintoLOADL));
  nand2 g044(.a(n29), .b(CPIPE1s<7> ), .O(n70));
  nor2  g045(.a(n70), .b(n47), .O(enableINTS1));
  nand2 g046(.a(n43), .b(n26), .O(n72));
  nor2  g047(.a(n72), .b(CPIPE1s<7> ), .O(changeCWP1));
  nand2 g048(.a(CPIPE1s<2> ), .b(n36), .O(n74));
  nor2  g049(.a(n74), .b(CPIPE1s<4> ), .O(n75));
  nor2  g050(.a(CPIPE1s<5> ), .b(n51), .O(n76));
  nor2  g051(.a(n76), .b(n75), .O(n77));
  nor2  g052(.a(n77), .b(CPIPE1s<3> ), .O(n78));
  nor2  g053(.a(CPIPE1s<2> ), .b(CPIPE1s<1> ), .O(n79));
  nor2  g054(.a(n79), .b(n51), .O(n80));
  nor2  g055(.a(n80), .b(CPIPE1s<0> ), .O(n81));
  inv1  g056(.a(n79), .O(n82));
  nor2  g057(.a(n82), .b(n28), .O(n83));
  nand2 g058(.a(CPIPE1s<2> ), .b(CPIPE1s<1> ), .O(n84));
  nor2  g059(.a(n84), .b(CPIPE1s<3> ), .O(n85));
  nor2  g060(.a(n85), .b(n83), .O(n86));
  nor2  g061(.a(n86), .b(CPIPE1s<4> ), .O(n87));
  nor2  g062(.a(n87), .b(n81), .O(n88));
  nor2  g063(.a(n88), .b(n43), .O(n89));
  nor2  g064(.a(n89), .b(n78), .O(n90));
  nor2  g065(.a(n30), .b(WAIT), .O(n91));
  inv1  g066(.a(n91), .O(n92));
  nor2  g067(.a(n92), .b(n90), .O(PCtoMAL1));
  inv1  g068(.a(CPIPE1s<1> ), .O(n94));
  nand2 g069(.a(CPIPE1s<2> ), .b(n94), .O(n95));
  nor2  g070(.a(n95), .b(n30), .O(n96));
  nor2  g071(.a(n96), .b(n36), .O(n97));
  nor2  g072(.a(n97), .b(n28), .O(n98));
  nor2  g073(.a(n98), .b(n30), .O(n99));
  nor2  g074(.a(n99), .b(CPIPE1s<5> ), .O(n100));
  inv1  g075(.a(n52), .O(n101));
  nand2 g076(.a(n101), .b(n32), .O(n102));
  nand2 g077(.a(n102), .b(CPIPE1s<0> ), .O(n103));
  nand2 g078(.a(n82), .b(n66), .O(n104));
  nand2 g079(.a(n104), .b(CPIPE1s<7> ), .O(n105));
  nand2 g080(.a(n105), .b(CPIPE1s<5> ), .O(n106));
  nand2 g081(.a(n106), .b(n103), .O(n107));
  nor2  g082(.a(n107), .b(n100), .O(n108));
  nor2  g083(.a(n108), .b(WAIT), .O(pALUtoMAL));
  nor2  g084(.a(CPIPE1s<4> ), .b(CPIPE1s<0> ), .O(n110));
  nor2  g085(.a(CPIPE1s<5> ), .b(CPIPE1s<1> ), .O(n111));
  nor2  g086(.a(n111), .b(n110), .O(n112));
  nor2  g087(.a(n112), .b(n38), .O(n113));
  nor2  g088(.a(n113), .b(n76), .O(n114));
  nor2  g089(.a(n114), .b(CPIPE1s<3> ), .O(n115));
  nor2  g090(.a(n115), .b(n89), .O(n116));
  inv1  g091(.a(RESET), .O(n117));
  nand2 g092(.a(n91), .b(n117), .O(n118));
  nor2  g093(.a(n118), .b(n116), .O(pPCIncr));
  inv1  g094(.a(n29), .O(n120));
  nor2  g095(.a(n120), .b(CPIPE1s<5> ), .O(n121));
  nor2  g096(.a(n121), .b(n30), .O(n122));
  nand2 g097(.a(n117), .b(n26), .O(n123));
  nor2  g098(.a(n123), .b(n122), .O(pALUtoPC));
  inv1  g099(.a(n47), .O(n125));
  nand2 g100(.a(n28), .b(n94), .O(n126));
  nand2 g101(.a(n126), .b(n125), .O(n127));
  nand2 g102(.a(n127), .b(CPIPE1s<2> ), .O(n128));
  nor2  g103(.a(n128), .b(n78), .O(n129));
  nor2  g104(.a(n129), .b(n30), .O(n130));
  nor2  g105(.a(n130), .b(CPIPE1s<5> ), .O(n131));
  nor2  g106(.a(n131), .b(WAIT), .O(DST2step1));
  nor2  g107(.a(n43), .b(n38), .O(n133));
  nor2  g108(.a(n44), .b(CPIPE1s<5> ), .O(n134));
  nor2  g109(.a(n134), .b(n94), .O(n135));
  nor2  g110(.a(n135), .b(n133), .O(n136));
  nor2  g111(.a(n51), .b(n28), .O(n137));
  nand2 g112(.a(n137), .b(n91), .O(n138));
  nor2  g113(.a(n138), .b(n136), .O(SRC2smin1));
  nor2  g114(.a(n82), .b(CPIPE1s<0> ), .O(n140));
  inv1  g115(.a(n53), .O(n141));
  nand2 g116(.a(n91), .b(n141), .O(n142));
  nor2  g117(.a(n142), .b(n140), .O(DST1min1));
  inv1  g118(.a(n37), .O(n144));
  nor2  g119(.a(n144), .b(n38), .O(n145));
  nor2  g120(.a(n145), .b(CPIPE1s<3> ), .O(n146));
  nor2  g121(.a(RESET), .b(n30), .O(n147));
  nand2 g122(.a(n147), .b(n34), .O(n148));
  nor2  g123(.a(n148), .b(n146), .O(CPIPE1flush));
  inv1  g124(.a(n62), .O(n150));
  nor2  g125(.a(n150), .b(n51), .O(n151));
  nor2  g126(.a(n151), .b(n123), .O(CPIPE1load1));
  nor2  g127(.a(n72), .b(CPIPE1s<7> ), .O(PCstuffoncall1));
endmodule


