// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:23 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6,
    v7.0 , v7.1 , v7.2 , v7.3 , v7.4 , v7.5 , v7.6 , v7.7 , v7.8   );
  input  v0, v1, v2, v3, v4, v5, v6;
  output v7.0 , v7.1 , v7.2 , v7.3 , v7.4 , v7.5 , v7.6 , v7.7 ,
    v7.8 ;
  wire n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30,
    n31, n32, n33, n34, n35, n36, n37, n38, n39, n41, n42, n43, n44, n45,
    n46, n47, n48, n49, n50, n51, n52, n53, n55, n56, n57, n58, n59, n60,
    n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74,
    n75, n76, n77, n78, n79, n80, n81, n82, n84, n85, n86, n87, n88, n89,
    n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101, n102,
    n103, n104, n105, n106, n107, n108, n109, n110, n111, n112, n113, n114,
    n116, n117, n118, n119, n120, n121, n122, n123, n124, n125, n126, n127,
    n128, n129, n130, n131, n132, n133, n134, n136, n137, n138, n139, n140,
    n141, n143, n144, n145, n146, n147, n149, n150, n151, n152, n153, n154,
    n155, n156, n157, n158, n160;
  inv1  g000(.a(v1), .O(n17));
  inv1  g001(.a(v2), .O(n18));
  inv1  g002(.a(v6), .O(n19));
  nand2 g003(.a(n19), .b(v3), .O(n20));
  inv1  g004(.a(n20), .O(n21));
  nand2 g005(.a(n21), .b(n18), .O(n22));
  nand2 g006(.a(n22), .b(n17), .O(n23));
  inv1  g007(.a(v3), .O(n24));
  nand2 g008(.a(n18), .b(v1), .O(n25));
  inv1  g009(.a(n25), .O(n26));
  nand2 g010(.a(n26), .b(n24), .O(n27));
  nand2 g011(.a(n27), .b(n23), .O(n28));
  nand2 g012(.a(n28), .b(v0), .O(n29));
  inv1  g013(.a(v5), .O(n30));
  nor2  g014(.a(n30), .b(n24), .O(n31));
  inv1  g015(.a(n31), .O(n32));
  nand2 g016(.a(v6), .b(n24), .O(n33));
  nand2 g017(.a(n33), .b(n32), .O(n34));
  nand2 g018(.a(n34), .b(n18), .O(n35));
  nand2 g019(.a(n21), .b(v2), .O(n36));
  nand2 g020(.a(n36), .b(n35), .O(n37));
  nor2  g021(.a(n17), .b(v0), .O(n38));
  nand2 g022(.a(n38), .b(n37), .O(n39));
  nand2 g023(.a(n39), .b(n29), .O(v7.0 ));
  nor2  g024(.a(n18), .b(v1), .O(n41));
  nand2 g025(.a(n41), .b(n31), .O(n42));
  nand2 g026(.a(n42), .b(n27), .O(n43));
  nand2 g027(.a(n43), .b(v0), .O(n44));
  inv1  g028(.a(v0), .O(n45));
  nand2 g029(.a(n20), .b(v2), .O(n46));
  nor2  g030(.a(v5), .b(v2), .O(n47));
  nand2 g031(.a(n47), .b(n33), .O(n48));
  nand2 g032(.a(n48), .b(n46), .O(n49));
  nand2 g033(.a(n49), .b(v1), .O(n50));
  nand2 g034(.a(n41), .b(v5), .O(n51));
  nand2 g035(.a(n51), .b(n50), .O(n52));
  nand2 g036(.a(n52), .b(n45), .O(n53));
  nand2 g037(.a(n53), .b(n44), .O(v7.1 ));
  inv1  g038(.a(v4), .O(n55));
  nand2 g039(.a(v6), .b(n55), .O(n56));
  nand2 g040(.a(n56), .b(n38), .O(n57));
  nand2 g041(.a(n57), .b(v1), .O(n58));
  nand2 g042(.a(n58), .b(n24), .O(n59));
  nor2  g043(.a(v5), .b(v1), .O(n60));
  inv1  g044(.a(n60), .O(n61));
  nand2 g045(.a(v6), .b(v1), .O(n62));
  nand2 g046(.a(n62), .b(n61), .O(n63));
  nand2 g047(.a(n63), .b(n45), .O(n64));
  nand2 g048(.a(n60), .b(v0), .O(n65));
  nand2 g049(.a(n65), .b(n64), .O(n66));
  nand2 g050(.a(n66), .b(v3), .O(n67));
  nand2 g051(.a(n67), .b(n59), .O(n68));
  nand2 g052(.a(n68), .b(v2), .O(n69));
  nor2  g053(.a(n19), .b(n55), .O(n70));
  nand2 g054(.a(n70), .b(n24), .O(n71));
  nand2 g055(.a(n71), .b(n20), .O(n72));
  nand2 g056(.a(n72), .b(v0), .O(n73));
  nand2 g057(.a(n31), .b(n45), .O(n74));
  nand2 g058(.a(n74), .b(n73), .O(n75));
  nand2 g059(.a(n75), .b(n17), .O(n76));
  nand2 g060(.a(n19), .b(n30), .O(n77));
  inv1  g061(.a(n38), .O(n78));
  nor2  g062(.a(n78), .b(v3), .O(n79));
  nand2 g063(.a(n79), .b(n77), .O(n80));
  nand2 g064(.a(n80), .b(n76), .O(n81));
  nand2 g065(.a(n81), .b(n18), .O(n82));
  nand2 g066(.a(n82), .b(n69), .O(v7.2 ));
  nand2 g067(.a(v4), .b(n18), .O(n84));
  nor2  g068(.a(v4), .b(n18), .O(n85));
  nor2  g069(.a(n19), .b(v5), .O(n86));
  nand2 g070(.a(n86), .b(n85), .O(n87));
  nand2 g071(.a(n87), .b(n84), .O(n88));
  nand2 g072(.a(n88), .b(n17), .O(n89));
  nand2 g073(.a(n56), .b(n30), .O(n90));
  nor2  g074(.a(n18), .b(n17), .O(n91));
  nand2 g075(.a(n91), .b(n90), .O(n92));
  nand2 g076(.a(n92), .b(n89), .O(n93));
  nand2 g077(.a(n93), .b(n24), .O(n94));
  nand2 g078(.a(n41), .b(v6), .O(n95));
  nand2 g079(.a(n95), .b(n25), .O(n96));
  nand2 g080(.a(n96), .b(v5), .O(n97));
  nor2  g081(.a(n30), .b(v2), .O(n98));
  nor2  g082(.a(n98), .b(n62), .O(n99));
  nor2  g083(.a(n99), .b(n60), .O(n100));
  nand2 g084(.a(n100), .b(n97), .O(n101));
  nand2 g085(.a(n101), .b(v3), .O(n102));
  nand2 g086(.a(n102), .b(n94), .O(n103));
  nand2 g087(.a(n103), .b(n45), .O(n104));
  nor2  g088(.a(v1), .b(n45), .O(n105));
  nand2 g089(.a(v5), .b(n24), .O(n106));
  nand2 g090(.a(n30), .b(v3), .O(n107));
  nand2 g091(.a(n107), .b(n106), .O(n108));
  nand2 g092(.a(n108), .b(v2), .O(n109));
  nand2 g093(.a(v4), .b(n24), .O(n110));
  nor2  g094(.a(n19), .b(v2), .O(n111));
  nand2 g095(.a(n111), .b(n110), .O(n112));
  nand2 g096(.a(n112), .b(n109), .O(n113));
  nand2 g097(.a(n113), .b(n105), .O(n114));
  nand2 g098(.a(n114), .b(n104), .O(v7.3 ));
  nor2  g099(.a(n56), .b(n17), .O(n116));
  nor2  g100(.a(n116), .b(v5), .O(n117));
  nor2  g101(.a(n19), .b(v4), .O(n118));
  nand2 g102(.a(n118), .b(v1), .O(n119));
  nand2 g103(.a(v5), .b(n17), .O(n120));
  nand2 g104(.a(n120), .b(n119), .O(n121));
  nor2  g105(.a(n121), .b(n117), .O(n122));
  nor2  g106(.a(n122), .b(v3), .O(n123));
  nor2  g107(.a(n24), .b(v1), .O(n124));
  nor2  g108(.a(n124), .b(n123), .O(n125));
  nor2  g109(.a(n125), .b(n18), .O(n126));
  nor2  g110(.a(n107), .b(n17), .O(n127));
  inv1  g111(.a(n124), .O(n128));
  nor2  g112(.a(v6), .b(v3), .O(n129));
  nand2 g113(.a(n129), .b(v1), .O(n130));
  nand2 g114(.a(n130), .b(n128), .O(n131));
  nor2  g115(.a(n131), .b(n127), .O(n132));
  nor2  g116(.a(n132), .b(v2), .O(n133));
  nor2  g117(.a(n133), .b(n126), .O(n134));
  nor2  g118(.a(n134), .b(v0), .O(v7.4 ));
  nand2 g119(.a(v2), .b(v0), .O(n136));
  nor2  g120(.a(n136), .b(n31), .O(n137));
  nand2 g121(.a(n55), .b(n24), .O(n138));
  nand2 g122(.a(n18), .b(n45), .O(n139));
  nor2  g123(.a(n139), .b(n138), .O(n140));
  nor2  g124(.a(n140), .b(n137), .O(n141));
  nor2  g125(.a(n141), .b(v1), .O(v7.5 ));
  inv1  g126(.a(n90), .O(n143));
  inv1  g127(.a(n120), .O(n144));
  nor2  g128(.a(n144), .b(n143), .O(n145));
  nor2  g129(.a(n18), .b(v0), .O(n146));
  nand2 g130(.a(n146), .b(n24), .O(n147));
  nor2  g131(.a(n147), .b(n145), .O(v7.6 ));
  nand2 g132(.a(v6), .b(v4), .O(n149));
  nor2  g133(.a(n149), .b(v3), .O(n150));
  nor2  g134(.a(n150), .b(n21), .O(n151));
  nor2  g135(.a(n151), .b(n45), .O(n152));
  nand2 g136(.a(n24), .b(n45), .O(n153));
  nor2  g137(.a(n153), .b(n55), .O(n154));
  nor2  g138(.a(n154), .b(n152), .O(n155));
  nor2  g139(.a(n155), .b(v1), .O(n156));
  nor2  g140(.a(n78), .b(n33), .O(n157));
  nor2  g141(.a(n157), .b(n156), .O(n158));
  nor2  g142(.a(n158), .b(v2), .O(v7.7 ));
  nor2  g143(.a(n24), .b(n18), .O(n160));
  nor2  g144(.a(n160), .b(n78), .O(v7.8 ));
endmodule


