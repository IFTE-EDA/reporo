// Benchmark "CM138" written by ABC on Tue Nov  5 15:01:19 2019

module CM138 ( 
    a, b, c, d, e, f,
    g, h, i, j, k, l, m, n  );
  input  a, b, c, d, e, f;
  output g, h, i, j, k, l, m, n;
  wire n15, n16, n17, n18, n19, n20, n22, n23, n25, n26, n28, n30, n31;
  inv1  g00(.a(f), .O(n15));
  inv1  g01(.a(d), .O(n16));
  nor2  g02(.a(e), .b(n16), .O(n17));
  nand2 g03(.a(n17), .b(n15), .O(n18));
  nor2  g04(.a(n18), .b(c), .O(n19));
  nor2  g05(.a(b), .b(a), .O(n20));
  nand2 g06(.a(n20), .b(n19), .O(g));
  inv1  g07(.a(a), .O(n22));
  nor2  g08(.a(b), .b(n22), .O(n23));
  nand2 g09(.a(n23), .b(n19), .O(h));
  inv1  g10(.a(b), .O(n25));
  nor2  g11(.a(n25), .b(a), .O(n26));
  nand2 g12(.a(n26), .b(n19), .O(i));
  nor2  g13(.a(n25), .b(n22), .O(n28));
  nand2 g14(.a(n28), .b(n19), .O(j));
  inv1  g15(.a(c), .O(n30));
  nor2  g16(.a(n18), .b(n30), .O(n31));
  nand2 g17(.a(n31), .b(n20), .O(k));
  nand2 g18(.a(n31), .b(n23), .O(l));
  nand2 g19(.a(n31), .b(n26), .O(m));
  nand2 g20(.a(n31), .b(n28), .O(n));
endmodule


