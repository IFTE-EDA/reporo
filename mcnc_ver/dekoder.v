// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:20 2019

module source_pla  ( 
    v0, v1, v2, v3,
    v4.0 , v4.1 , v4.2 , v4.3 , v4.4 , v4.5 , v4.6   );
  input  v0, v1, v2, v3;
  output v4.0 , v4.1 , v4.2 , v4.3 , v4.4 , v4.5 , v4.6 ;
  wire n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n26,
    n27, n28, n29, n30, n31, n32, n33, n35, n36, n37, n38, n39, n41, n42,
    n43, n44, n45, n46, n47, n48, n50, n51, n52, n54, n55, n56, n57, n58,
    n59, n60, n61, n62, n63, n65, n66, n67, n68;
  nand2 g00(.a(v3), .b(v1), .O(n12));
  nor2  g01(.a(v3), .b(v1), .O(n13));
  inv1  g02(.a(n13), .O(n14));
  nand2 g03(.a(n14), .b(n12), .O(n15));
  nor2  g04(.a(v2), .b(v0), .O(n16));
  nand2 g05(.a(n16), .b(n15), .O(n17));
  inv1  g06(.a(v2), .O(n18));
  nor2  g07(.a(n18), .b(v0), .O(n19));
  inv1  g08(.a(v0), .O(n20));
  inv1  g09(.a(v1), .O(n21));
  nand2 g10(.a(n18), .b(n21), .O(n22));
  nor2  g11(.a(n22), .b(n20), .O(n23));
  nor2  g12(.a(n23), .b(n19), .O(n24));
  nand2 g13(.a(n24), .b(n17), .O(v4.0 ));
  nand2 g14(.a(v2), .b(v0), .O(n26));
  nand2 g15(.a(n26), .b(n21), .O(n27));
  nand2 g16(.a(v3), .b(v2), .O(n28));
  nor2  g17(.a(v3), .b(v2), .O(n29));
  inv1  g18(.a(n29), .O(n30));
  nand2 g19(.a(n30), .b(n28), .O(n31));
  nor2  g20(.a(n21), .b(v0), .O(n32));
  nand2 g21(.a(n32), .b(n31), .O(n33));
  nand2 g22(.a(n33), .b(n27), .O(v4.1 ));
  nand2 g23(.a(v2), .b(n21), .O(n35));
  nand2 g24(.a(n35), .b(n20), .O(n36));
  nand2 g25(.a(n21), .b(n20), .O(n37));
  nor2  g26(.a(n37), .b(n28), .O(n38));
  nor2  g27(.a(n38), .b(n23), .O(n39));
  nand2 g28(.a(n39), .b(n36), .O(v4.2 ));
  nand2 g29(.a(n29), .b(n20), .O(n41));
  nor2  g30(.a(v2), .b(n20), .O(n42));
  nor2  g31(.a(n42), .b(n19), .O(n43));
  nand2 g32(.a(n43), .b(n41), .O(n44));
  nand2 g33(.a(n44), .b(n21), .O(n45));
  inv1  g34(.a(n28), .O(n46));
  nor2  g35(.a(n29), .b(n46), .O(n47));
  nand2 g36(.a(n32), .b(n47), .O(n48));
  nand2 g37(.a(n48), .b(n45), .O(v4.3 ));
  nor2  g38(.a(v2), .b(n21), .O(n50));
  nor2  g39(.a(n50), .b(v0), .O(n51));
  nor2  g40(.a(n51), .b(n23), .O(n52));
  nor2  g41(.a(n52), .b(v3), .O(v4.4 ));
  nand2 g42(.a(n13), .b(n20), .O(n54));
  nor2  g43(.a(v1), .b(n20), .O(n55));
  nor2  g44(.a(n55), .b(n32), .O(n56));
  nand2 g45(.a(n56), .b(n54), .O(n57));
  nand2 g46(.a(n57), .b(n18), .O(n58));
  nand2 g47(.a(v1), .b(n20), .O(n59));
  inv1  g48(.a(v3), .O(n60));
  nand2 g49(.a(n60), .b(v2), .O(n61));
  nor2  g50(.a(n61), .b(n59), .O(n62));
  inv1  g51(.a(n62), .O(n63));
  nand2 g52(.a(n63), .b(n58), .O(v4.5 ));
  nand2 g53(.a(n18), .b(v1), .O(n65));
  nand2 g54(.a(n65), .b(n35), .O(n66));
  nand2 g55(.a(n66), .b(n20), .O(n67));
  nor2  g56(.a(n62), .b(n23), .O(n68));
  nand2 g57(.a(n68), .b(n67), .O(v4.6 ));
endmodule


