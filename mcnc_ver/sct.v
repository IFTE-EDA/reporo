// Benchmark "sct" written by ABC on Tue Nov  5 15:01:27 2019

module sct ( 
    a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s,
    t, u, v, w, x, y, z, a0, b0, c0, d0, e0, f0, g0, h0  );
  input  a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s;
  output t, u, v, w, x, y, z, a0, b0, c0, d0, e0, f0, g0, h0;
  wire n35, n36, n37, n38, n39, n40, n42, n43, n44, n45, n46, n47, n48, n50,
    n51, n52, n53, n54, n55, n57, n58, n59, n60, n61, n63, n64, n65, n66,
    n67, n68, n69, n70, n72, n73, n74, n75, n76, n78, n79, n80, n81, n82,
    n84, n85, n86, n87, n88, n90, n91, n92, n93, n94, n95, n96, n97, n98,
    n99, n100, n102, n103, n104, n105, n106, n107, n108, n109, n110, n111,
    n112, n113, n115, n116, n117, n118, n119, n120, n121, n122, n123, n125,
    n126, n127, n129;
  inv1  g00(.a(c), .O(n35));
  inv1  g01(.a(b), .O(n36));
  nand2 g02(.a(o), .b(n36), .O(n37));
  nand2 g03(.a(n37), .b(n35), .O(n38));
  inv1  g04(.a(o), .O(n39));
  nand2 g05(.a(n39), .b(n36), .O(n40));
  nand2 g06(.a(n40), .b(n38), .O(t));
  inv1  g07(.a(e), .O(n42));
  inv1  g08(.a(s), .O(n43));
  nor2  g09(.a(n43), .b(p), .O(n44));
  nor2  g10(.a(n44), .b(f), .O(n45));
  nor2  g11(.a(n45), .b(n42), .O(n46));
  inv1  g12(.a(n44), .O(n47));
  nor2  g13(.a(n47), .b(f), .O(n48));
  nor2  g14(.a(n48), .b(n46), .O(u));
  inv1  g15(.a(d), .O(n50));
  nand2 g16(.a(n50), .b(c), .O(n51));
  nand2 g17(.a(n51), .b(q), .O(n52));
  inv1  g18(.a(n52), .O(n53));
  inv1  g19(.a(g), .O(n54));
  nand2 g20(.a(n54), .b(e), .O(n55));
  nor2  g21(.a(n55), .b(n53), .O(v));
  inv1  g22(.a(h), .O(n57));
  nand2 g23(.a(n57), .b(n54), .O(n58));
  nor2  g24(.a(n57), .b(n54), .O(n59));
  nor2  g25(.a(n59), .b(n42), .O(n60));
  nand2 g26(.a(n60), .b(n58), .O(n61));
  nor2  g27(.a(n61), .b(n53), .O(w));
  nand2 g28(.a(n52), .b(e), .O(n63));
  nand2 g29(.a(i), .b(h), .O(n64));
  nor2  g30(.a(n64), .b(n54), .O(n65));
  inv1  g31(.a(n65), .O(n66));
  inv1  g32(.a(i), .O(n67));
  inv1  g33(.a(n59), .O(n68));
  nand2 g34(.a(n68), .b(n67), .O(n69));
  nand2 g35(.a(n69), .b(n66), .O(n70));
  nor2  g36(.a(n70), .b(n63), .O(x));
  nand2 g37(.a(n66), .b(j), .O(n72));
  nor2  g38(.a(j), .b(n67), .O(n73));
  nand2 g39(.a(n73), .b(n59), .O(n74));
  inv1  g40(.a(n74), .O(n75));
  nor2  g41(.a(n75), .b(n63), .O(n76));
  nand2 g42(.a(n76), .b(n72), .O(y));
  nand2 g43(.a(n74), .b(k), .O(n78));
  nor2  g44(.a(k), .b(j), .O(n79));
  nand2 g45(.a(n79), .b(n65), .O(n80));
  inv1  g46(.a(n80), .O(n81));
  nor2  g47(.a(n81), .b(n63), .O(n82));
  nand2 g48(.a(n82), .b(n78), .O(z));
  nand2 g49(.a(n80), .b(l), .O(n84));
  inv1  g50(.a(l), .O(n85));
  nand2 g51(.a(n79), .b(n85), .O(n86));
  nor2  g52(.a(n86), .b(n66), .O(n87));
  nor2  g53(.a(n87), .b(n63), .O(n88));
  nand2 g54(.a(n88), .b(n84), .O(a0));
  inv1  g55(.a(n86), .O(n90));
  nand2 g56(.a(n90), .b(n65), .O(n91));
  nand2 g57(.a(n91), .b(m), .O(n92));
  inv1  g58(.a(k), .O(n93));
  nor2  g59(.a(j), .b(n54), .O(n94));
  nand2 g60(.a(n94), .b(n93), .O(n95));
  inv1  g61(.a(n64), .O(n96));
  nor2  g62(.a(m), .b(l), .O(n97));
  nand2 g63(.a(n97), .b(n96), .O(n98));
  nor2  g64(.a(n98), .b(n95), .O(n99));
  nor2  g65(.a(n99), .b(n63), .O(n100));
  nand2 g66(.a(n100), .b(n92), .O(b0));
  inv1  g67(.a(j), .O(n102));
  nand2 g68(.a(n102), .b(g), .O(n103));
  nor2  g69(.a(n103), .b(k), .O(n104));
  inv1  g70(.a(n97), .O(n105));
  nor2  g71(.a(n105), .b(n64), .O(n106));
  nand2 g72(.a(n106), .b(n104), .O(n107));
  nand2 g73(.a(n107), .b(n), .O(n108));
  nor2  g74(.a(l), .b(k), .O(n109));
  nor2  g75(.a(n), .b(m), .O(n110));
  nand2 g76(.a(n110), .b(n109), .O(n111));
  nor2  g77(.a(n111), .b(n74), .O(n112));
  nor2  g78(.a(n112), .b(n63), .O(n113));
  nand2 g79(.a(n113), .b(n108), .O(c0));
  nor2  g80(.a(n39), .b(n42), .O(n115));
  nand2 g81(.a(n115), .b(n53), .O(n116));
  inv1  g82(.a(a), .O(n117));
  nand2 g83(.a(n111), .b(n117), .O(n118));
  nand2 g84(.a(n73), .b(h), .O(n119));
  nor2  g85(.a(n54), .b(n42), .O(n120));
  nand2 g86(.a(n120), .b(n52), .O(n121));
  nor2  g87(.a(n121), .b(n119), .O(n122));
  nand2 g88(.a(n122), .b(n118), .O(n123));
  nand2 g89(.a(n123), .b(n116), .O(d0));
  inv1  g90(.a(q), .O(n125));
  nor2  g91(.a(n125), .b(c), .O(n126));
  nor2  g92(.a(n126), .b(d), .O(n127));
  nor2  g93(.a(n127), .b(n42), .O(f0));
  inv1  g94(.a(r), .O(n129));
  nor2  g95(.a(n129), .b(n42), .O(h0));
  buf   g96(.a(c), .O(e0));
  buf   g97(.a(e), .O(g0));
endmodule


