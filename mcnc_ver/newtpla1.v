// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:25 2019

module source_pla  ( 
    CPIPE1s<6> , CPIPE1s<0> , CPIPE1s<1> , CPIPE1s<2> , CPIPE1s<3> ,
    CPIPE1s<4> , CPIPE1s<5> , CPIPE1s<7> , AIprocessed<31> ,
    AIprocessed<30> ,
    shiftAbus31, shiftAbus30  );
  input  CPIPE1s<6> , CPIPE1s<0> , CPIPE1s<1> , CPIPE1s<2> ,
    CPIPE1s<3> , CPIPE1s<4> , CPIPE1s<5> , CPIPE1s<7> ,
    AIprocessed<31> , AIprocessed<30> ;
  output shiftAbus31, shiftAbus30;
  wire n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26,
    n28, n29, n30, n31, n32, n33, n34, n35, n36;
  nand2 g00(.a(CPIPE1s<7> ), .b(CPIPE1s<5> ), .O(n13));
  inv1  g01(.a(n13), .O(n14));
  nand2 g02(.a(n14), .b(AIprocessed<31> ), .O(n15));
  inv1  g03(.a(CPIPE1s<6> ), .O(n16));
  inv1  g04(.a(CPIPE1s<1> ), .O(n17));
  nand2 g05(.a(n17), .b(n16), .O(n18));
  inv1  g06(.a(CPIPE1s<0> ), .O(n19));
  inv1  g07(.a(CPIPE1s<2> ), .O(n20));
  nand2 g08(.a(n20), .b(n19), .O(n21));
  inv1  g09(.a(CPIPE1s<3> ), .O(n22));
  inv1  g10(.a(CPIPE1s<4> ), .O(n23));
  nand2 g11(.a(n23), .b(n22), .O(n24));
  nor2  g12(.a(n24), .b(n21), .O(n25));
  nand2 g13(.a(n25), .b(n18), .O(n26));
  nor2  g14(.a(n26), .b(n15), .O(shiftAbus31));
  inv1  g15(.a(AIprocessed<31> ), .O(n28));
  nor2  g16(.a(n28), .b(CPIPE1s<6> ), .O(n29));
  inv1  g17(.a(AIprocessed<30> ), .O(n30));
  nand2 g18(.a(CPIPE1s<1> ), .b(CPIPE1s<6> ), .O(n31));
  nor2  g19(.a(n31), .b(n30), .O(n32));
  nor2  g20(.a(n32), .b(n29), .O(n33));
  nor2  g21(.a(CPIPE1s<2> ), .b(CPIPE1s<0> ), .O(n34));
  nor2  g22(.a(n24), .b(n13), .O(n35));
  nand2 g23(.a(n35), .b(n34), .O(n36));
  nor2  g24(.a(n36), .b(n33), .O(shiftAbus30));
endmodule


