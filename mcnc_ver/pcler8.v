// Benchmark "pcler8_cl" written by ABC on Tue Nov  5 15:01:26 2019

module pcler8_cl ( 
    a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x,
    y, z, a0,
    b0, c0, d0, e0, f0, g0, h0, i0, j0, k0, l0, m0, n0, o0, p0, q0, r0  );
  input  a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u,
    v, w, x, y, z, a0;
  output b0, c0, d0, e0, f0, g0, h0, i0, j0, k0, l0, m0, n0, o0, p0, q0, r0;
  wire n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58,
    n59, n60, n62, n64, n66, n68, n70, n72, n74, n76, n78, n79, n80, n82,
    n83, n84, n85, n86, n87, n88, n89, n91, n92, n93, n94, n95, n96, n97,
    n99, n100, n101, n102, n103, n104, n105, n106, n108, n109, n110, n111,
    n112, n113, n114, n116, n117, n118, n119, n120, n121, n122, n123, n125,
    n126, n127, n128, n129, n130, n131, n133, n134, n135, n136, n137, n138,
    n139, n140;
  inv1  g00(.a(z), .O(n45));
  inv1  g01(.a(x), .O(n46));
  inv1  g02(.a(v), .O(n47));
  nand2 g03(.a(u), .b(t), .O(n48));
  nor2  g04(.a(n48), .b(n47), .O(n49));
  nand2 g05(.a(n49), .b(w), .O(n50));
  nor2  g06(.a(n50), .b(n46), .O(n51));
  nand2 g07(.a(n51), .b(y), .O(n52));
  nor2  g08(.a(n52), .b(n45), .O(n53));
  inv1  g09(.a(a0), .O(n54));
  inv1  g10(.a(i), .O(n55));
  nand2 g11(.a(j), .b(n55), .O(n56));
  nor2  g12(.a(n56), .b(k), .O(n57));
  inv1  g13(.a(n57), .O(n58));
  nor2  g14(.a(n58), .b(n54), .O(n59));
  nand2 g15(.a(n59), .b(n53), .O(n60));
  inv1  g16(.a(n60), .O(b0));
  inv1  g17(.a(a), .O(n62));
  nor2  g18(.a(n55), .b(n62), .O(c0));
  inv1  g19(.a(b), .O(n64));
  nor2  g20(.a(n55), .b(n64), .O(d0));
  inv1  g21(.a(c), .O(n66));
  nor2  g22(.a(n55), .b(n66), .O(e0));
  inv1  g23(.a(d), .O(n68));
  nor2  g24(.a(n55), .b(n68), .O(f0));
  inv1  g25(.a(e), .O(n70));
  nor2  g26(.a(n55), .b(n70), .O(g0));
  inv1  g27(.a(f), .O(n72));
  nor2  g28(.a(n55), .b(n72), .O(h0));
  inv1  g29(.a(g), .O(n74));
  nor2  g30(.a(n55), .b(n74), .O(i0));
  inv1  g31(.a(h), .O(n76));
  nor2  g32(.a(n55), .b(n76), .O(j0));
  nand2 g33(.a(b0), .b(l), .O(n78));
  nor2  g34(.a(n58), .b(t), .O(n79));
  nor2  g35(.a(n79), .b(c0), .O(n80));
  nand2 g36(.a(n80), .b(n78), .O(k0));
  nand2 g37(.a(b0), .b(m), .O(n82));
  inv1  g38(.a(u), .O(n83));
  nor2  g39(.a(n83), .b(t), .O(n84));
  inv1  g40(.a(t), .O(n85));
  nor2  g41(.a(u), .b(n85), .O(n86));
  nor2  g42(.a(n86), .b(n84), .O(n87));
  nor2  g43(.a(n87), .b(n58), .O(n88));
  nor2  g44(.a(n88), .b(d0), .O(n89));
  nand2 g45(.a(n89), .b(n82), .O(l0));
  nand2 g46(.a(b0), .b(n), .O(n91));
  nor2  g47(.a(n48), .b(v), .O(n92));
  nor2  g48(.a(n83), .b(n85), .O(n93));
  nor2  g49(.a(n93), .b(n47), .O(n94));
  nor2  g50(.a(n94), .b(n92), .O(n95));
  nor2  g51(.a(n95), .b(n58), .O(n96));
  nor2  g52(.a(n96), .b(e0), .O(n97));
  nand2 g53(.a(n97), .b(n91), .O(m0));
  nand2 g54(.a(b0), .b(o), .O(n99));
  nand2 g55(.a(n93), .b(v), .O(n100));
  nor2  g56(.a(n100), .b(w), .O(n101));
  inv1  g57(.a(w), .O(n102));
  nor2  g58(.a(n49), .b(n102), .O(n103));
  nor2  g59(.a(n103), .b(n101), .O(n104));
  nor2  g60(.a(n104), .b(n58), .O(n105));
  nor2  g61(.a(n105), .b(f0), .O(n106));
  nand2 g62(.a(n106), .b(n99), .O(n0));
  nand2 g63(.a(b0), .b(p), .O(n108));
  nor2  g64(.a(n50), .b(x), .O(n109));
  nor2  g65(.a(n100), .b(n102), .O(n110));
  nor2  g66(.a(n110), .b(n46), .O(n111));
  nor2  g67(.a(n111), .b(n109), .O(n112));
  nor2  g68(.a(n112), .b(n58), .O(n113));
  nor2  g69(.a(n113), .b(g0), .O(n114));
  nand2 g70(.a(n114), .b(n108), .O(o0));
  nand2 g71(.a(b0), .b(q), .O(n116));
  nand2 g72(.a(n110), .b(x), .O(n117));
  nor2  g73(.a(n117), .b(y), .O(n118));
  inv1  g74(.a(y), .O(n119));
  nor2  g75(.a(n51), .b(n119), .O(n120));
  nor2  g76(.a(n120), .b(n118), .O(n121));
  nor2  g77(.a(n121), .b(n58), .O(n122));
  nor2  g78(.a(n122), .b(h0), .O(n123));
  nand2 g79(.a(n123), .b(n116), .O(p0));
  nand2 g80(.a(b0), .b(r), .O(n125));
  nor2  g81(.a(n52), .b(z), .O(n126));
  nor2  g82(.a(n117), .b(n119), .O(n127));
  nor2  g83(.a(n127), .b(n45), .O(n128));
  nor2  g84(.a(n128), .b(n126), .O(n129));
  nor2  g85(.a(n129), .b(n58), .O(n130));
  nor2  g86(.a(n130), .b(i0), .O(n131));
  nand2 g87(.a(n131), .b(n125), .O(q0));
  nand2 g88(.a(n53), .b(n54), .O(n133));
  nand2 g89(.a(n127), .b(z), .O(n134));
  nand2 g90(.a(n134), .b(a0), .O(n135));
  nand2 g91(.a(n135), .b(n133), .O(n136));
  nand2 g92(.a(n136), .b(n57), .O(n137));
  inv1  g93(.a(s), .O(n138));
  nor2  g94(.a(n60), .b(n138), .O(n139));
  nor2  g95(.a(n139), .b(j0), .O(n140));
  nand2 g96(.a(n140), .b(n137), .O(r0));
endmodule


