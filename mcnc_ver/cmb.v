// Benchmark "cmb" written by ABC on Tue Nov  5 15:01:19 2019

module cmb ( 
    a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p,
    q, r, s, t  );
  input  a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p;
  output q, r, s, t;
  wire n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34,
    n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49,
    n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63,
    n64, n65, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78, n79,
    n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91;
  inv1  g00(.a(i), .O(n21));
  inv1  g01(.a(j), .O(n22));
  nor2  g02(.a(n22), .b(n21), .O(n23));
  inv1  g03(.a(k), .O(n24));
  inv1  g04(.a(l), .O(n25));
  nor2  g05(.a(n25), .b(n24), .O(n26));
  nand2 g06(.a(n26), .b(n23), .O(n27));
  nand2 g07(.a(f), .b(e), .O(n28));
  nand2 g08(.a(h), .b(g), .O(n29));
  nor2  g09(.a(n29), .b(n28), .O(n30));
  nand2 g10(.a(b), .b(a), .O(n31));
  nand2 g11(.a(d), .b(c), .O(n32));
  nor2  g12(.a(n32), .b(n31), .O(n33));
  nand2 g13(.a(n33), .b(n30), .O(n34));
  nor2  g14(.a(n34), .b(n27), .O(q));
  inv1  g15(.a(p), .O(n36));
  inv1  g16(.a(e), .O(n37));
  inv1  g17(.a(f), .O(n38));
  inv1  g18(.a(h), .O(n39));
  nor2  g19(.a(n39), .b(g), .O(n40));
  nor2  g20(.a(n40), .b(n38), .O(n41));
  nor2  g21(.a(h), .b(g), .O(n42));
  nor2  g22(.a(n42), .b(n41), .O(n43));
  nor2  g23(.a(n43), .b(n37), .O(n44));
  inv1  g24(.a(n42), .O(n45));
  nor2  g25(.a(n45), .b(f), .O(n46));
  nor2  g26(.a(n46), .b(n44), .O(n47));
  nand2 g27(.a(l), .b(n24), .O(n48));
  nand2 g28(.a(k), .b(n22), .O(n49));
  nand2 g29(.a(n49), .b(n48), .O(n50));
  nand2 g30(.a(i), .b(n39), .O(n51));
  nand2 g31(.a(j), .b(n21), .O(n52));
  nand2 g32(.a(n52), .b(n51), .O(n53));
  nor2  g33(.a(n53), .b(n50), .O(n54));
  nand2 g34(.a(n36), .b(e), .O(n55));
  inv1  g35(.a(n), .O(n56));
  nand2 g36(.a(o), .b(n56), .O(n57));
  nand2 g37(.a(n57), .b(n55), .O(n58));
  inv1  g38(.a(m), .O(n59));
  nand2 g39(.a(n), .b(n59), .O(n60));
  nand2 g40(.a(m), .b(n25), .O(n61));
  nand2 g41(.a(n61), .b(n60), .O(n62));
  nor2  g42(.a(n62), .b(n58), .O(n63));
  nand2 g43(.a(n63), .b(n54), .O(n64));
  nor2  g44(.a(n64), .b(n47), .O(n65));
  nand2 g45(.a(n65), .b(n36), .O(r));
  nand2 g46(.a(n65), .b(o), .O(s));
  inv1  g47(.a(g), .O(n68));
  nand2 g48(.a(h), .b(n68), .O(n69));
  nand2 g49(.a(n69), .b(f), .O(n70));
  nand2 g50(.a(n45), .b(n70), .O(n71));
  nand2 g51(.a(n71), .b(e), .O(n72));
  inv1  g52(.a(n46), .O(n73));
  nand2 g53(.a(n73), .b(n72), .O(n74));
  nor2  g54(.a(n25), .b(k), .O(n75));
  nor2  g55(.a(n24), .b(j), .O(n76));
  nor2  g56(.a(n76), .b(n75), .O(n77));
  nor2  g57(.a(n21), .b(h), .O(n78));
  nor2  g58(.a(n22), .b(i), .O(n79));
  nor2  g59(.a(n79), .b(n78), .O(n80));
  nand2 g60(.a(n80), .b(n77), .O(n81));
  nor2  g61(.a(p), .b(n37), .O(n82));
  inv1  g62(.a(o), .O(n83));
  nor2  g63(.a(n83), .b(n), .O(n84));
  nor2  g64(.a(n84), .b(n82), .O(n85));
  nor2  g65(.a(n56), .b(m), .O(n86));
  nor2  g66(.a(n59), .b(l), .O(n87));
  nor2  g67(.a(n87), .b(n86), .O(n88));
  nand2 g68(.a(n88), .b(n85), .O(n89));
  nor2  g69(.a(n89), .b(n81), .O(n90));
  nand2 g70(.a(n90), .b(n74), .O(n91));
  nor2  g71(.a(n91), .b(p), .O(t));
endmodule


