// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:25 2019

module source_pla  ( 
    SRC1s<0> , SRC1s<1> , SRC1s<2> , SRC1s<3> , SRC1s<4> ,
    CPIPE1s<7> , pbusDtoINA, SRC2equal16, SRC2equalDST2, opc2load,
    DSTvalid, SRC1equalDST2,
    pbusStobusA, pSHAtobusA, pSHBtobusA, preadPSWtoA, preadCWPtoA,
    LoadforwtoINB1, LoadforwtoINA1  );
  input  SRC1s<0> , SRC1s<1> , SRC1s<2> , SRC1s<3> , SRC1s<4> ,
    CPIPE1s<7> , pbusDtoINA, SRC2equal16, SRC2equalDST2, opc2load,
    DSTvalid, SRC1equalDST2;
  output pbusStobusA, pSHAtobusA, pSHBtobusA, preadPSWtoA, preadCWPtoA,
    LoadforwtoINB1, LoadforwtoINA1;
  wire n20, n21, n22, n23, n24, n25, n26, n27, n29, n30, n31, n32, n33, n34,
    n35, n37, n38, n40, n41, n43, n45, n46, n47, n48, n49, n50, n52, n53,
    n54, n55, n56, n57, n58, n59;
  inv1  g00(.a(pbusDtoINA), .O(n20));
  nand2 g01(.a(n20), .b(CPIPE1s<7> ), .O(n21));
  inv1  g02(.a(SRC1s<1> ), .O(n22));
  inv1  g03(.a(SRC1s<2> ), .O(n23));
  nor2  g04(.a(n23), .b(n22), .O(n24));
  inv1  g05(.a(SRC1s<4> ), .O(n25));
  nor2  g06(.a(n25), .b(SRC1s<3> ), .O(n26));
  nand2 g07(.a(n26), .b(n24), .O(n27));
  nor2  g08(.a(n27), .b(n21), .O(pbusStobusA));
  inv1  g09(.a(CPIPE1s<7> ), .O(n29));
  nor2  g10(.a(n29), .b(n25), .O(n30));
  nand2 g11(.a(n30), .b(n20), .O(n31));
  inv1  g12(.a(SRC1s<0> ), .O(n32));
  nor2  g13(.a(n22), .b(n32), .O(n33));
  nor2  g14(.a(SRC1s<3> ), .b(SRC1s<2> ), .O(n34));
  nand2 g15(.a(n34), .b(n33), .O(n35));
  nor2  g16(.a(n35), .b(n31), .O(pSHAtobusA));
  nor2  g17(.a(n22), .b(SRC1s<0> ), .O(n37));
  nand2 g18(.a(n37), .b(n34), .O(n38));
  nor2  g19(.a(n38), .b(n31), .O(pSHBtobusA));
  nor2  g20(.a(SRC1s<3> ), .b(n23), .O(n40));
  nand2 g21(.a(n40), .b(n33), .O(n41));
  nor2  g22(.a(n41), .b(n31), .O(preadPSWtoA));
  nand2 g23(.a(n40), .b(n37), .O(n43));
  nor2  g24(.a(n43), .b(n31), .O(preadCWPtoA));
  inv1  g25(.a(SRC2equalDST2), .O(n45));
  nor2  g26(.a(n45), .b(SRC2equal16), .O(n46));
  inv1  g27(.a(opc2load), .O(n47));
  inv1  g28(.a(DSTvalid), .O(n48));
  nor2  g29(.a(n48), .b(n47), .O(n49));
  nand2 g30(.a(n49), .b(n46), .O(n50));
  nor2  g31(.a(n50), .b(n21), .O(LoadforwtoINB1));
  nand2 g32(.a(n22), .b(n32), .O(n52));
  nand2 g33(.a(n34), .b(SRC1s<4> ), .O(n53));
  nor2  g34(.a(n53), .b(n52), .O(n54));
  nor2  g35(.a(pbusDtoINA), .b(n29), .O(n55));
  inv1  g36(.a(SRC1equalDST2), .O(n56));
  nand2 g37(.a(DSTvalid), .b(opc2load), .O(n57));
  nor2  g38(.a(n57), .b(n56), .O(n58));
  nand2 g39(.a(n58), .b(n55), .O(n59));
  nor2  g40(.a(n59), .b(n54), .O(LoadforwtoINA1));
endmodule


