// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:21 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15,
    v16.0 , v16.1 , v16.2 , v16.3 , v16.4   );
  input  v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,
    v15;
  output v16.0 , v16.1 , v16.2 , v16.3 , v16.4 ;
  wire n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35,
    n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49,
    n50, n51, n52, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64,
    n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78,
    n79, n80, n81, n82, n83, n84, n86, n87, n88, n89, n90, n91, n92, n93,
    n94, n95, n96, n97, n98, n99, n100, n101, n102, n103, n104, n105, n106,
    n107, n108, n109, n110, n111, n112, n113, n114, n115, n117, n118, n119,
    n120, n121, n122, n123, n124, n125, n126, n127, n128, n129, n130, n131,
    n132, n133, n134, n135, n136, n137, n138, n139, n140, n141, n142, n143,
    n144, n145, n147, n148, n149, n150, n151, n152;
  inv1  g000(.a(v4), .O(n22));
  inv1  g001(.a(v12), .O(n23));
  nand2 g002(.a(n23), .b(v5), .O(n24));
  nand2 g003(.a(n24), .b(n22), .O(n25));
  nor2  g004(.a(v5), .b(n22), .O(n26));
  nand2 g005(.a(n26), .b(n23), .O(n27));
  nand2 g006(.a(n27), .b(n25), .O(n28));
  nand2 g007(.a(n28), .b(v6), .O(n29));
  nand2 g008(.a(v12), .b(v7), .O(n30));
  inv1  g009(.a(v7), .O(n31));
  nand2 g010(.a(n23), .b(n31), .O(n32));
  nand2 g011(.a(n32), .b(n30), .O(n33));
  inv1  g012(.a(v5), .O(n34));
  nor2  g013(.a(n34), .b(n22), .O(n35));
  nand2 g014(.a(n35), .b(n33), .O(n36));
  nand2 g015(.a(n36), .b(n29), .O(n37));
  nand2 g016(.a(n37), .b(v8), .O(n38));
  nand2 g017(.a(v4), .b(v0), .O(n39));
  nor2  g018(.a(n39), .b(v5), .O(n40));
  nand2 g019(.a(v12), .b(v5), .O(n41));
  nor2  g020(.a(n41), .b(v4), .O(n42));
  nor2  g021(.a(n42), .b(n40), .O(n43));
  nor2  g022(.a(n43), .b(v6), .O(n44));
  nor2  g023(.a(n33), .b(n34), .O(n45));
  nand2 g024(.a(v6), .b(n34), .O(n46));
  nor2  g025(.a(n46), .b(n23), .O(n47));
  nor2  g026(.a(n47), .b(n45), .O(n48));
  inv1  g027(.a(v8), .O(n49));
  nand2 g028(.a(n49), .b(v4), .O(n50));
  nor2  g029(.a(n50), .b(n48), .O(n51));
  nor2  g030(.a(n51), .b(n44), .O(n52));
  nand2 g031(.a(n52), .b(n38), .O(v16.0 ));
  inv1  g032(.a(v9), .O(n54));
  inv1  g033(.a(v13), .O(n55));
  nor2  g034(.a(n55), .b(n54), .O(n56));
  nor2  g035(.a(v13), .b(v9), .O(n57));
  nor2  g036(.a(n57), .b(n56), .O(n58));
  inv1  g037(.a(n58), .O(n59));
  nand2 g038(.a(v7), .b(v0), .O(n60));
  nand2 g039(.a(v12), .b(v8), .O(n61));
  nand2 g040(.a(n61), .b(n60), .O(n62));
  nand2 g041(.a(n62), .b(n59), .O(n63));
  inv1  g042(.a(v0), .O(n64));
  nor2  g043(.a(n31), .b(n64), .O(n65));
  nor2  g044(.a(n23), .b(n49), .O(n66));
  nor2  g045(.a(n66), .b(n65), .O(n67));
  nand2 g046(.a(n67), .b(n58), .O(n68));
  nand2 g047(.a(n68), .b(n63), .O(n69));
  nand2 g048(.a(n69), .b(v5), .O(n70));
  nand2 g049(.a(n58), .b(v6), .O(n71));
  inv1  g050(.a(v6), .O(n72));
  nand2 g051(.a(n72), .b(v1), .O(n73));
  nand2 g052(.a(n73), .b(n71), .O(n74));
  nand2 g053(.a(n74), .b(n34), .O(n75));
  nand2 g054(.a(n75), .b(n70), .O(n76));
  nand2 g055(.a(n76), .b(v4), .O(n77));
  nand2 g056(.a(n54), .b(v6), .O(n78));
  nor2  g057(.a(n55), .b(n34), .O(n79));
  nand2 g058(.a(n79), .b(n78), .O(n80));
  nor2  g059(.a(n54), .b(n72), .O(n81));
  nand2 g060(.a(n81), .b(n34), .O(n82));
  nand2 g061(.a(n82), .b(n80), .O(n83));
  nand2 g062(.a(n83), .b(n22), .O(n84));
  nand2 g063(.a(n84), .b(n77), .O(v16.1 ));
  nand2 g064(.a(v14), .b(v10), .O(n86));
  inv1  g065(.a(n86), .O(n87));
  nor2  g066(.a(v14), .b(v10), .O(n88));
  nor2  g067(.a(n88), .b(n87), .O(n89));
  inv1  g068(.a(n89), .O(n90));
  inv1  g069(.a(n56), .O(n91));
  nand2 g070(.a(n62), .b(v1), .O(n92));
  nand2 g071(.a(n92), .b(n91), .O(n93));
  nand2 g072(.a(n93), .b(n90), .O(n94));
  inv1  g073(.a(v1), .O(n95));
  nor2  g074(.a(n67), .b(n95), .O(n96));
  nor2  g075(.a(n96), .b(n56), .O(n97));
  nand2 g076(.a(n97), .b(n89), .O(n98));
  nand2 g077(.a(n98), .b(n94), .O(n99));
  nand2 g078(.a(n99), .b(v5), .O(n100));
  nand2 g079(.a(n89), .b(v6), .O(n101));
  nand2 g080(.a(n72), .b(v2), .O(n102));
  nand2 g081(.a(n102), .b(n101), .O(n103));
  nand2 g082(.a(n103), .b(n34), .O(n104));
  nand2 g083(.a(n104), .b(n100), .O(n105));
  nand2 g084(.a(n105), .b(v4), .O(n106));
  inv1  g085(.a(v10), .O(n107));
  nand2 g086(.a(n107), .b(v6), .O(n108));
  inv1  g087(.a(v14), .O(n109));
  nor2  g088(.a(n109), .b(n34), .O(n110));
  nand2 g089(.a(n110), .b(n108), .O(n111));
  nor2  g090(.a(n107), .b(n72), .O(n112));
  nand2 g091(.a(n112), .b(n34), .O(n113));
  nand2 g092(.a(n113), .b(n111), .O(n114));
  nand2 g093(.a(n114), .b(n22), .O(n115));
  nand2 g094(.a(n115), .b(n106), .O(v16.2 ));
  nand2 g095(.a(v15), .b(v11), .O(n117));
  inv1  g096(.a(n117), .O(n118));
  nor2  g097(.a(v15), .b(v11), .O(n119));
  nor2  g098(.a(n119), .b(n118), .O(n120));
  inv1  g099(.a(n120), .O(n121));
  nand2 g100(.a(n93), .b(v2), .O(n122));
  nand2 g101(.a(n122), .b(n86), .O(n123));
  nand2 g102(.a(n123), .b(n121), .O(n124));
  inv1  g103(.a(v2), .O(n125));
  nor2  g104(.a(n97), .b(n125), .O(n126));
  nor2  g105(.a(n126), .b(n87), .O(n127));
  nand2 g106(.a(n127), .b(n120), .O(n128));
  nand2 g107(.a(n128), .b(n124), .O(n129));
  nand2 g108(.a(n129), .b(v5), .O(n130));
  nand2 g109(.a(n120), .b(v6), .O(n131));
  nand2 g110(.a(n72), .b(v3), .O(n132));
  nand2 g111(.a(n132), .b(n131), .O(n133));
  nand2 g112(.a(n133), .b(n34), .O(n134));
  nand2 g113(.a(n134), .b(n130), .O(n135));
  nand2 g114(.a(n135), .b(v4), .O(n136));
  inv1  g115(.a(v11), .O(n137));
  nand2 g116(.a(n137), .b(v6), .O(n138));
  inv1  g117(.a(v15), .O(n139));
  nor2  g118(.a(n139), .b(n34), .O(n140));
  nand2 g119(.a(n140), .b(n138), .O(n141));
  nor2  g120(.a(n137), .b(n72), .O(n142));
  nand2 g121(.a(n142), .b(n34), .O(n143));
  nand2 g122(.a(n143), .b(n141), .O(n144));
  nand2 g123(.a(n144), .b(n22), .O(n145));
  nand2 g124(.a(n145), .b(n136), .O(v16.3 ));
  nand2 g125(.a(n66), .b(v1), .O(n147));
  nor2  g126(.a(n65), .b(n56), .O(n148));
  nand2 g127(.a(n148), .b(n147), .O(n149));
  nand2 g128(.a(n149), .b(v2), .O(n150));
  nand2 g129(.a(n150), .b(n86), .O(n151));
  nand2 g130(.a(n151), .b(v3), .O(n152));
  nand2 g131(.a(n152), .b(n117), .O(v16.4 ));
endmodule


