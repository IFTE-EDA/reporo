// Benchmark "CM151" written by ABC on Tue Nov  5 15:01:19 2019

module CM151 ( 
    a, b, c, d, e, f, g, h, i, j, k, l,
    m, n  );
  input  a, b, c, d, e, f, g, h, i, j, k, l;
  output m, n;
  wire n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28,
    n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42,
    n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56,
    n57, n58, n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71,
    n72, n73, n74, n75;
  inv1  g00(.a(k), .O(n15));
  inv1  g01(.a(j), .O(n16));
  inv1  g02(.a(h), .O(n17));
  inv1  g03(.a(i), .O(n18));
  nor2  g04(.a(n18), .b(n17), .O(n19));
  nor2  g05(.a(n19), .b(g), .O(n20));
  nor2  g06(.a(n18), .b(h), .O(n21));
  nor2  g07(.a(n21), .b(n20), .O(n22));
  nor2  g08(.a(n22), .b(n16), .O(n23));
  inv1  g09(.a(e), .O(n24));
  nand2 g10(.a(i), .b(f), .O(n25));
  nand2 g11(.a(n25), .b(n24), .O(n26));
  nor2  g12(.a(n18), .b(f), .O(n27));
  inv1  g13(.a(n27), .O(n28));
  nand2 g14(.a(n28), .b(n26), .O(n29));
  nor2  g15(.a(n29), .b(n23), .O(n30));
  inv1  g16(.a(g), .O(n31));
  nand2 g17(.a(i), .b(h), .O(n32));
  nand2 g18(.a(n32), .b(n31), .O(n33));
  nand2 g19(.a(i), .b(n17), .O(n34));
  nand2 g20(.a(n34), .b(n33), .O(n35));
  nor2  g21(.a(n35), .b(n16), .O(n36));
  nor2  g22(.a(n36), .b(n30), .O(n37));
  nor2  g23(.a(n37), .b(n15), .O(n38));
  inv1  g24(.a(c), .O(n39));
  nand2 g25(.a(i), .b(d), .O(n40));
  nand2 g26(.a(n40), .b(n39), .O(n41));
  nor2  g27(.a(n18), .b(d), .O(n42));
  inv1  g28(.a(n42), .O(n43));
  nand2 g29(.a(n43), .b(n41), .O(n44));
  nand2 g30(.a(n44), .b(j), .O(n45));
  inv1  g31(.a(b), .O(n46));
  nor2  g32(.a(n18), .b(n46), .O(n47));
  nor2  g33(.a(n47), .b(a), .O(n48));
  nor2  g34(.a(n18), .b(b), .O(n49));
  nor2  g35(.a(n49), .b(n48), .O(n50));
  nand2 g36(.a(n50), .b(n45), .O(n51));
  nor2  g37(.a(n44), .b(n16), .O(n52));
  inv1  g38(.a(n52), .O(n53));
  nand2 g39(.a(n53), .b(n51), .O(n54));
  nor2  g40(.a(n54), .b(n38), .O(n55));
  inv1  g41(.a(l), .O(n56));
  nand2 g42(.a(n37), .b(k), .O(n57));
  nand2 g43(.a(n57), .b(n56), .O(n58));
  nor2  g44(.a(n58), .b(n55), .O(m));
  nand2 g45(.a(n35), .b(j), .O(n60));
  inv1  g46(.a(n26), .O(n61));
  nor2  g47(.a(n27), .b(n61), .O(n62));
  nand2 g48(.a(n62), .b(n60), .O(n63));
  inv1  g49(.a(n36), .O(n64));
  nand2 g50(.a(n64), .b(n63), .O(n65));
  nand2 g51(.a(n65), .b(k), .O(n66));
  inv1  g52(.a(n41), .O(n67));
  nor2  g53(.a(n42), .b(n67), .O(n68));
  nor2  g54(.a(n68), .b(n16), .O(n69));
  inv1  g55(.a(n50), .O(n70));
  nor2  g56(.a(n70), .b(n69), .O(n71));
  nor2  g57(.a(n52), .b(n71), .O(n72));
  nand2 g58(.a(n72), .b(n66), .O(n73));
  nor2  g59(.a(n65), .b(n15), .O(n74));
  nor2  g60(.a(n74), .b(l), .O(n75));
  nand2 g61(.a(n75), .b(n73), .O(n));
endmodule


