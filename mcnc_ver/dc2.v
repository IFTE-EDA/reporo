// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:20 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7,
    v8.0 , v8.1 , v8.2 , v8.3 , v8.4 , v8.5 , v8.6   );
  input  v0, v1, v2, v3, v4, v5, v6, v7;
  output v8.0 , v8.1 , v8.2 , v8.3 , v8.4 , v8.5 , v8.6 ;
  wire n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29,
    n30, n31, n32, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44,
    n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n57, n58, n59,
    n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73,
    n74, n75, n76, n77, n78, n79, n81, n82, n83, n84, n85, n86, n87, n88,
    n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101,
    n102, n103, n104, n105, n106, n107, n108, n109, n110, n111, n112, n114,
    n115, n116, n117, n118, n119, n120, n121, n122, n123, n124, n125, n126,
    n127, n128, n129, n130, n131, n132, n134, n135, n136, n137, n138, n139,
    n140, n141, n142;
  inv1  g000(.a(v2), .O(n16));
  inv1  g001(.a(v0), .O(n17));
  nor2  g002(.a(v1), .b(n17), .O(n18));
  nand2 g003(.a(n18), .b(n16), .O(n19));
  inv1  g004(.a(v3), .O(n20));
  inv1  g005(.a(v5), .O(n21));
  nor2  g006(.a(n21), .b(v4), .O(n22));
  nor2  g007(.a(v6), .b(v5), .O(n23));
  nand2 g008(.a(n23), .b(v4), .O(n24));
  inv1  g009(.a(n24), .O(n25));
  nor2  g010(.a(n25), .b(n22), .O(n26));
  nand2 g011(.a(n26), .b(n20), .O(n27));
  inv1  g012(.a(v1), .O(n28));
  nor2  g013(.a(n28), .b(v0), .O(n29));
  inv1  g014(.a(n29), .O(n30));
  nor2  g015(.a(n30), .b(n16), .O(n31));
  nand2 g016(.a(n31), .b(n27), .O(n32));
  nand2 g017(.a(n32), .b(n19), .O(v8.0 ));
  inv1  g018(.a(v4), .O(n34));
  inv1  g019(.a(v6), .O(n35));
  nor2  g020(.a(n35), .b(n21), .O(n36));
  nand2 g021(.a(n36), .b(n34), .O(n37));
  nand2 g022(.a(n37), .b(n24), .O(n38));
  nor2  g023(.a(v2), .b(n17), .O(n39));
  nand2 g024(.a(n39), .b(n38), .O(n40));
  nand2 g025(.a(n35), .b(n21), .O(n41));
  nand2 g026(.a(n41), .b(n34), .O(n42));
  nand2 g027(.a(n42), .b(n24), .O(n43));
  nor2  g028(.a(n16), .b(v0), .O(n44));
  nand2 g029(.a(n44), .b(n43), .O(n45));
  nand2 g030(.a(n45), .b(n40), .O(n46));
  nand2 g031(.a(n46), .b(n28), .O(n47));
  nand2 g032(.a(n29), .b(n16), .O(n48));
  nand2 g033(.a(n48), .b(n47), .O(n49));
  nand2 g034(.a(n49), .b(v3), .O(n50));
  nor2  g035(.a(v5), .b(v4), .O(n51));
  inv1  g036(.a(n51), .O(n52));
  nand2 g037(.a(n52), .b(v2), .O(n53));
  nor2  g038(.a(n30), .b(v3), .O(n54));
  nand2 g039(.a(n54), .b(n53), .O(n55));
  nand2 g040(.a(n55), .b(n50), .O(v8.1 ));
  nor2  g041(.a(n34), .b(v3), .O(n57));
  nand2 g042(.a(n57), .b(n23), .O(n58));
  nand2 g043(.a(n58), .b(n20), .O(n59));
  nand2 g044(.a(n59), .b(v1), .O(n60));
  nor2  g045(.a(n20), .b(v1), .O(n61));
  nand2 g046(.a(n61), .b(n38), .O(n62));
  nand2 g047(.a(n62), .b(n60), .O(n63));
  nand2 g048(.a(n63), .b(n16), .O(n64));
  nor2  g049(.a(v4), .b(n20), .O(n65));
  nand2 g050(.a(n65), .b(n23), .O(n66));
  nand2 g051(.a(n66), .b(v3), .O(n67));
  nand2 g052(.a(n67), .b(n28), .O(n68));
  nor2  g053(.a(v3), .b(n28), .O(n69));
  nand2 g054(.a(n69), .b(n51), .O(n70));
  nand2 g055(.a(n70), .b(n68), .O(n71));
  nand2 g056(.a(n71), .b(v2), .O(n72));
  nand2 g057(.a(n72), .b(n64), .O(n73));
  nand2 g058(.a(n73), .b(n17), .O(n74));
  inv1  g059(.a(n19), .O(n75));
  inv1  g060(.a(n36), .O(n76));
  nand2 g061(.a(n65), .b(n76), .O(n77));
  nand2 g062(.a(n77), .b(v3), .O(n78));
  nand2 g063(.a(n78), .b(n75), .O(n79));
  nand2 g064(.a(n79), .b(n74), .O(v8.2 ));
  nand2 g065(.a(v6), .b(v2), .O(n81));
  nor2  g066(.a(v4), .b(v1), .O(n82));
  nand2 g067(.a(n82), .b(n81), .O(n83));
  nor2  g068(.a(v6), .b(n34), .O(n84));
  nor2  g069(.a(n16), .b(n28), .O(n85));
  nand2 g070(.a(n85), .b(n84), .O(n86));
  nand2 g071(.a(n86), .b(n83), .O(n87));
  nand2 g072(.a(n87), .b(v3), .O(n88));
  nor2  g073(.a(v6), .b(n20), .O(n89));
  nand2 g074(.a(n34), .b(v1), .O(n90));
  nor2  g075(.a(n90), .b(n89), .O(n91));
  nand2 g076(.a(n35), .b(v4), .O(n92));
  nand2 g077(.a(n20), .b(n28), .O(n93));
  nor2  g078(.a(n93), .b(n92), .O(n94));
  nor2  g079(.a(n94), .b(n91), .O(n95));
  nand2 g080(.a(n95), .b(n88), .O(n96));
  nand2 g081(.a(n96), .b(n21), .O(n97));
  nand2 g082(.a(n20), .b(v2), .O(n98));
  nand2 g083(.a(n89), .b(n16), .O(n99));
  nand2 g084(.a(n99), .b(n98), .O(n100));
  nand2 g085(.a(n100), .b(n28), .O(n101));
  nand2 g086(.a(v6), .b(v3), .O(n102));
  nand2 g087(.a(n102), .b(n16), .O(n103));
  nand2 g088(.a(v3), .b(v2), .O(n104));
  nand2 g089(.a(n104), .b(n103), .O(n105));
  nand2 g090(.a(n105), .b(v1), .O(n106));
  nand2 g091(.a(n106), .b(n101), .O(n107));
  nand2 g092(.a(n107), .b(n22), .O(n108));
  nand2 g093(.a(n108), .b(n97), .O(n109));
  nand2 g094(.a(n109), .b(n17), .O(n110));
  nand2 g095(.a(n77), .b(n58), .O(n111));
  nand2 g096(.a(n111), .b(n75), .O(n112));
  nand2 g097(.a(n112), .b(n110), .O(v8.3 ));
  nand2 g098(.a(v6), .b(n21), .O(n114));
  nand2 g099(.a(n35), .b(v5), .O(n115));
  nand2 g100(.a(n115), .b(n114), .O(n116));
  nand2 g101(.a(n116), .b(v3), .O(n117));
  nor2  g102(.a(n21), .b(v3), .O(n118));
  inv1  g103(.a(n118), .O(n119));
  nand2 g104(.a(n119), .b(n117), .O(n120));
  nor2  g105(.a(n28), .b(n17), .O(n121));
  nor2  g106(.a(n121), .b(v2), .O(n122));
  nand2 g107(.a(n122), .b(n120), .O(n123));
  nor2  g108(.a(n35), .b(v5), .O(n124));
  nor2  g109(.a(v6), .b(n21), .O(n125));
  nor2  g110(.a(n125), .b(n124), .O(n126));
  nor2  g111(.a(n126), .b(n20), .O(n127));
  nor2  g112(.a(n118), .b(n127), .O(n128));
  nand2 g113(.a(n128), .b(n44), .O(n129));
  nand2 g114(.a(n129), .b(n123), .O(n130));
  nand2 g115(.a(n130), .b(n34), .O(n131));
  nand2 g116(.a(n44), .b(n25), .O(n132));
  nand2 g117(.a(n132), .b(n131), .O(v8.4 ));
  nand2 g118(.a(n19), .b(v0), .O(n134));
  nor2  g119(.a(n35), .b(v3), .O(n135));
  nor2  g120(.a(n135), .b(n89), .O(n136));
  nor2  g121(.a(n136), .b(v4), .O(n137));
  nand2 g122(.a(n137), .b(n134), .O(n138));
  inv1  g123(.a(n89), .O(n139));
  nand2 g124(.a(n21), .b(v4), .O(n140));
  nor2  g125(.a(n140), .b(n139), .O(n141));
  nand2 g126(.a(n141), .b(n134), .O(n142));
  nand2 g127(.a(n142), .b(n138), .O(v8.5 ));
  buf   g128(.a(v7), .O(v8.6 ));
endmodule


