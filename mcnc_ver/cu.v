// Benchmark "cu" written by ABC on Tue Nov  5 15:01:20 2019

module cu ( 
    a, b, c, d, e, f, g, i, j, k, l, m, n, o,
    p, q, r, s, t, u, v, w, x, y, z  );
  input  a, b, c, d, e, f, g, i, j, k, l, m, n, o;
  output p, q, r, s, t, u, v, w, x, y, z;
  wire n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n41,
    n42, n43, n44, n45, n46, n47, n48, n50, n51, n53, n54, n56, n57, n59,
    n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73,
    n74, n75, n76, n77, n78, n80, n82, n83, n84, n85, n87, n89, n90, n91;
  nand2 g00(.a(f), .b(e), .O(n26));
  nor2  g01(.a(f), .b(e), .O(n27));
  nor2  g02(.a(n27), .b(d), .O(n28));
  nand2 g03(.a(n28), .b(n26), .O(n29));
  inv1  g04(.a(c), .O(n30));
  inv1  g05(.a(e), .O(n31));
  nor2  g06(.a(f), .b(n31), .O(n32));
  nor2  g07(.a(n32), .b(n30), .O(n33));
  inv1  g08(.a(f), .O(n34));
  nor2  g09(.a(n34), .b(e), .O(n35));
  nor2  g10(.a(n35), .b(c), .O(n36));
  nor2  g11(.a(n36), .b(n33), .O(n37));
  inv1  g12(.a(n37), .O(n38));
  nor2  g13(.a(n38), .b(n29), .O(q));
  inv1  g14(.a(q), .O(p));
  inv1  g15(.a(o), .O(n41));
  nand2 g16(.a(n35), .b(n41), .O(n42));
  nor2  g17(.a(d), .b(c), .O(n43));
  inv1  g18(.a(a), .O(n44));
  inv1  g19(.a(b), .O(n45));
  nand2 g20(.a(n45), .b(n44), .O(n46));
  inv1  g21(.a(n46), .O(n47));
  nand2 g22(.a(n47), .b(n43), .O(n48));
  nor2  g23(.a(n48), .b(n42), .O(r));
  nor2  g24(.a(b), .b(n44), .O(n50));
  nand2 g25(.a(n50), .b(n43), .O(n51));
  nor2  g26(.a(n51), .b(n42), .O(s));
  nor2  g27(.a(n45), .b(a), .O(n53));
  nand2 g28(.a(n53), .b(n43), .O(n54));
  nor2  g29(.a(n54), .b(n42), .O(t));
  nor2  g30(.a(n45), .b(n44), .O(n56));
  nand2 g31(.a(n56), .b(n43), .O(n57));
  nor2  g32(.a(n57), .b(n42), .O(u));
  nand2 g33(.a(n41), .b(n34), .O(n59));
  nand2 g34(.a(n59), .b(c), .O(n60));
  nor2  g35(.a(n31), .b(d), .O(n61));
  nand2 g36(.a(n61), .b(n60), .O(n62));
  inv1  g37(.a(k), .O(n63));
  nor2  g38(.a(n63), .b(b), .O(n64));
  inv1  g39(.a(m), .O(n65));
  nor2  g40(.a(n65), .b(n45), .O(n66));
  nor2  g41(.a(n66), .b(n64), .O(n67));
  nor2  g42(.a(n67), .b(n44), .O(n68));
  nand2 g43(.a(n53), .b(l), .O(n69));
  inv1  g44(.a(j), .O(n70));
  nor2  g45(.a(n46), .b(n70), .O(n71));
  nor2  g46(.a(i), .b(n34), .O(n72));
  nor2  g47(.a(n41), .b(n), .O(n73));
  nand2 g48(.a(n73), .b(n72), .O(n74));
  nor2  g49(.a(n74), .b(n71), .O(n75));
  nand2 g50(.a(n75), .b(n69), .O(n76));
  nor2  g51(.a(n76), .b(n68), .O(n77));
  nor2  g52(.a(n77), .b(c), .O(n78));
  nor2  g53(.a(n78), .b(n62), .O(v));
  inv1  g54(.a(n43), .O(n80));
  nor2  g55(.a(n80), .b(n42), .O(w));
  inv1  g56(.a(n), .O(n82));
  nand2 g57(.a(n82), .b(f), .O(n83));
  nor2  g58(.a(n83), .b(n41), .O(n84));
  nor2  g59(.a(n84), .b(c), .O(n85));
  nor2  g60(.a(n85), .b(n62), .O(x));
  inv1  g61(.a(g), .O(n87));
  nor2  g62(.a(n41), .b(n87), .O(y));
  nor2  g63(.a(n34), .b(n30), .O(n89));
  inv1  g64(.a(d), .O(n90));
  nand2 g65(.a(g), .b(n90), .O(n91));
  nor2  g66(.a(n91), .b(n89), .O(z));
endmodule


