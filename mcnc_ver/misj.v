// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:24 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15,
    v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29,
    v30, v31, v32, v33, v34,
    v35.0 , v35.1 , v35.2 , v35.3 , v35.4 , v35.5 , v35.6 , v35.7 ,
    v35.8 , v35.9 , v35.10 , v35.11 , v35.12 , v35.13   );
  input  v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14,
    v15, v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28,
    v29, v30, v31, v32, v33, v34;
  output v35.0 , v35.1 , v35.2 , v35.3 , v35.4 , v35.5 , v35.6 ,
    v35.7 , v35.8 , v35.9 , v35.10 , v35.11 , v35.12 , v35.13 ;
  wire n50, n51, n53, n54, n55, n56, n58, n59, n60, n61, n62, n65, n66, n68,
    n69, n70, n71, n72, n75, n76, n79, n80, n81, n82, n83, n84, n85, n86,
    n87, n88, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n101, n102,
    n103, n104, n105, n106;
  nand2 g00(.a(v1), .b(v0), .O(n50));
  nand2 g01(.a(v3), .b(v2), .O(n51));
  nor2  g02(.a(n51), .b(n50), .O(v35.0 ));
  inv1  g03(.a(v7), .O(n53));
  inv1  g04(.a(v8), .O(n54));
  nand2 g05(.a(v3), .b(v1), .O(n55));
  inv1  g06(.a(v4), .O(n56));
  inv1  g07(.a(v15), .O(v35.7 ));
  nor2  g08(.a(v35.7 ), .b(n56), .O(n58));
  nand2 g09(.a(n58), .b(v16), .O(n59));
  nor2  g10(.a(n59), .b(n55), .O(n60));
  nor2  g11(.a(n60), .b(n54), .O(n61));
  nor2  g12(.a(n61), .b(v5), .O(n62));
  nor2  g13(.a(n62), .b(n53), .O(v35.1 ));
  inv1  g14(.a(v5), .O(v35.2 ));
  nand2 g15(.a(v11), .b(v10), .O(n65));
  nand2 g16(.a(n65), .b(v35.2 ), .O(n66));
  nand2 g17(.a(n66), .b(v7), .O(v35.3 ));
  inv1  g18(.a(v12), .O(n68));
  inv1  g19(.a(v13), .O(n69));
  nor2  g20(.a(n69), .b(n68), .O(n70));
  nand2 g21(.a(n70), .b(v14), .O(n71));
  nand2 g22(.a(n71), .b(v35.2 ), .O(n72));
  nand2 g23(.a(n72), .b(v7), .O(v35.4 ));
  nand2 g24(.a(v18), .b(v17), .O(v35.5 ));
  inv1  g25(.a(v20), .O(n75));
  nor2  g26(.a(v19), .b(v9), .O(n76));
  nand2 g27(.a(n76), .b(n75), .O(v35.6 ));
  nand2 g28(.a(v21), .b(v6), .O(v35.8 ));
  nand2 g29(.a(v31), .b(v30), .O(n79));
  nand2 g30(.a(v33), .b(v32), .O(n80));
  nor2  g31(.a(n80), .b(n79), .O(n81));
  nand2 g32(.a(v28), .b(v27), .O(n82));
  inv1  g33(.a(v22), .O(n83));
  nor2  g34(.a(v23), .b(n83), .O(n84));
  inv1  g35(.a(v29), .O(n85));
  nor2  g36(.a(n85), .b(v25), .O(n86));
  nand2 g37(.a(n86), .b(n84), .O(n87));
  nor2  g38(.a(n87), .b(n82), .O(n88));
  nand2 g39(.a(n88), .b(n81), .O(v35.9 ));
  nand2 g40(.a(v32), .b(v31), .O(n90));
  nand2 g41(.a(v34), .b(v33), .O(n91));
  nor2  g42(.a(n91), .b(n90), .O(n92));
  inv1  g43(.a(v24), .O(n93));
  nor2  g44(.a(n93), .b(n83), .O(n94));
  inv1  g45(.a(v26), .O(n95));
  inv1  g46(.a(v30), .O(n96));
  nor2  g47(.a(n96), .b(n95), .O(n97));
  nand2 g48(.a(n97), .b(n94), .O(n98));
  nor2  g49(.a(n98), .b(n82), .O(n99));
  nand2 g50(.a(n99), .b(n92), .O(v35.10 ));
  inv1  g51(.a(n55), .O(n101));
  inv1  g52(.a(v16), .O(n102));
  nand2 g53(.a(v15), .b(v4), .O(n103));
  nor2  g54(.a(n103), .b(n102), .O(n104));
  nand2 g55(.a(n104), .b(n101), .O(n105));
  nand2 g56(.a(n105), .b(v8), .O(n106));
  nor2  g57(.a(n106), .b(n53), .O(v35.11 ));
  zero  g58(.O(v35.12 ));
  zero  g59(.O(v35.13 ));
endmodule


