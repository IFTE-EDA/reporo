// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:25 2019

module source_pla  ( 
    DST2s<0> , DST2s<1> , DST2s<2> , DST2s<3> , DST2s<4> , DSTvalid,
    writetoSHA1, writetoSHB1, writetoPSW1, writetoCWP1, pwritetoTB,
    pwritetoSWP, pwritetoPC  );
  input  DST2s<0> , DST2s<1> , DST2s<2> , DST2s<3> , DST2s<4> ,
    DSTvalid;
  output writetoSHA1, writetoSHB1, writetoPSW1, writetoCWP1, pwritetoTB,
    pwritetoSWP, pwritetoPC;
  wire n14, n15, n16, n17, n18, n19, n21, n22, n24, n25, n26, n29, n30, n32;
  nand2 g00(.a(DST2s<1> ), .b(DST2s<0> ), .O(n14));
  nor2  g01(.a(DST2s<3> ), .b(DST2s<2> ), .O(n15));
  inv1  g02(.a(DST2s<4> ), .O(n16));
  inv1  g03(.a(DSTvalid), .O(n17));
  nor2  g04(.a(n17), .b(n16), .O(n18));
  nand2 g05(.a(n18), .b(n15), .O(n19));
  nor2  g06(.a(n19), .b(n14), .O(writetoSHA1));
  inv1  g07(.a(DST2s<0> ), .O(n21));
  nand2 g08(.a(DST2s<1> ), .b(n21), .O(n22));
  nor2  g09(.a(n22), .b(n19), .O(writetoSHB1));
  inv1  g10(.a(DST2s<2> ), .O(n24));
  nor2  g11(.a(DST2s<3> ), .b(n24), .O(n25));
  nand2 g12(.a(n25), .b(n18), .O(n26));
  nor2  g13(.a(n26), .b(n14), .O(writetoPSW1));
  nor2  g14(.a(n26), .b(n22), .O(writetoCWP1));
  inv1  g15(.a(DST2s<1> ), .O(n29));
  nand2 g16(.a(n29), .b(DST2s<0> ), .O(n30));
  nor2  g17(.a(n30), .b(n26), .O(pwritetoTB));
  nand2 g18(.a(n29), .b(n21), .O(n32));
  nor2  g19(.a(n32), .b(n26), .O(pwritetoSWP));
  nor2  g20(.a(n30), .b(n19), .O(pwritetoPC));
endmodule


