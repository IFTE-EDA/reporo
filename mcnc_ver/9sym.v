// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:15 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7, v8,
    v9.0   );
  input  v0, v1, v2, v3, v4, v5, v6, v7, v8;
  output v9.0 ;
  wire n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24,
    n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38,
    n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52,
    n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n65, n66,
    n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78, n79, n80,
    n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93, n94,
    n95, n96, n97, n98, n99, n100, n101, n102, n103, n104, n105, n106,
    n107, n108, n109, n110, n111, n112, n113, n114, n115, n116, n117, n118,
    n119, n120, n121, n122, n123, n124, n125, n126, n127, n128, n129, n130,
    n131, n132, n133, n134, n135, n136, n137, n138, n139, n140, n141, n142,
    n143, n144, n145, n146, n147, n148, n149, n150, n151, n152, n153, n154,
    n155, n156, n157, n158, n159, n160, n161, n162, n163, n164, n165, n166,
    n167, n168, n169, n170, n171, n172, n173, n174, n175, n176, n177, n178,
    n179, n180, n181, n182, n183, n184, n185, n186, n187, n188, n189, n190,
    n191, n192, n193, n194, n195, n196, n197, n198, n199, n200, n201, n202,
    n203, n204, n205, n206, n207, n208, n209, n210, n211, n212, n213, n214,
    n215, n216, n217, n218, n219, n220, n221, n222, n223, n224, n225, n226,
    n227, n228, n229, n230, n231, n232, n233, n234, n235, n236, n237, n238,
    n239, n240, n241, n242, n243, n244, n245, n246, n247, n248, n249, n250,
    n251, n252, n253, n254, n255, n256, n257, n258, n259, n260, n261, n262,
    n263;
  inv1  g000(.a(v6), .O(n11));
  inv1  g001(.a(v3), .O(n12));
  nand2 g002(.a(v8), .b(n12), .O(n13));
  inv1  g003(.a(n13), .O(n14));
  inv1  g004(.a(v5), .O(n15));
  nor2  g005(.a(v8), .b(n15), .O(n16));
  nor2  g006(.a(n16), .b(n14), .O(n17));
  nor2  g007(.a(n17), .b(v0), .O(n18));
  inv1  g008(.a(v8), .O(n19));
  nand2 g009(.a(n19), .b(v7), .O(n20));
  nor2  g010(.a(n20), .b(v5), .O(n21));
  nor2  g011(.a(n21), .b(n18), .O(n22));
  nor2  g012(.a(n22), .b(n11), .O(n23));
  inv1  g013(.a(v0), .O(n24));
  nand2 g014(.a(v8), .b(n15), .O(n25));
  nor2  g015(.a(n25), .b(v3), .O(n26));
  nand2 g016(.a(n19), .b(v6), .O(n27));
  nor2  g017(.a(n27), .b(v4), .O(n28));
  nor2  g018(.a(n28), .b(n26), .O(n29));
  nor2  g019(.a(n29), .b(n24), .O(n30));
  inv1  g020(.a(v7), .O(n31));
  nand2 g021(.a(n31), .b(v6), .O(n32));
  nor2  g022(.a(n32), .b(v5), .O(n33));
  nor2  g023(.a(n20), .b(v6), .O(n34));
  nor2  g024(.a(n34), .b(n33), .O(n35));
  nor2  g025(.a(n35), .b(n12), .O(n36));
  nor2  g026(.a(n36), .b(n30), .O(n37));
  inv1  g027(.a(v4), .O(n38));
  nand2 g028(.a(v6), .b(n15), .O(n39));
  nor2  g029(.a(n39), .b(v0), .O(n40));
  nand2 g030(.a(v5), .b(n12), .O(n41));
  nor2  g031(.a(n41), .b(v6), .O(n42));
  nor2  g032(.a(n42), .b(n40), .O(n43));
  nor2  g033(.a(n43), .b(n38), .O(n44));
  nand2 g034(.a(v5), .b(n38), .O(n45));
  nor2  g035(.a(n45), .b(v3), .O(n46));
  nand2 g036(.a(v8), .b(n11), .O(n47));
  nor2  g037(.a(n47), .b(v5), .O(n48));
  nor2  g038(.a(n48), .b(n46), .O(n49));
  nor2  g039(.a(n49), .b(n31), .O(n50));
  nor2  g040(.a(n50), .b(n44), .O(n51));
  nand2 g041(.a(n51), .b(n37), .O(n52));
  nor2  g042(.a(n52), .b(n23), .O(n53));
  nor2  g043(.a(n53), .b(v2), .O(n54));
  nand2 g044(.a(n31), .b(n11), .O(n55));
  nor2  g045(.a(n55), .b(n15), .O(n56));
  nand2 g046(.a(n12), .b(v2), .O(n57));
  nor2  g047(.a(n57), .b(v8), .O(n58));
  nor2  g048(.a(n58), .b(n56), .O(n59));
  nor2  g049(.a(n59), .b(n24), .O(n60));
  nand2 g050(.a(n19), .b(n11), .O(n61));
  nand2 g051(.a(v5), .b(v2), .O(n62));
  nor2  g052(.a(n62), .b(n61), .O(n63));
  nor2  g053(.a(n63), .b(n60), .O(n64));
  nor2  g054(.a(n64), .b(v4), .O(n65));
  nor2  g055(.a(n65), .b(n54), .O(n66));
  nand2 g056(.a(v7), .b(n15), .O(n67));
  inv1  g057(.a(n67), .O(n68));
  nor2  g058(.a(v7), .b(n15), .O(n69));
  nor2  g059(.a(n69), .b(n68), .O(n70));
  nor2  g060(.a(n70), .b(n38), .O(n71));
  nand2 g061(.a(v6), .b(n38), .O(n72));
  nand2 g062(.a(n11), .b(v4), .O(n73));
  nand2 g063(.a(n73), .b(n72), .O(n74));
  nand2 g064(.a(n74), .b(v2), .O(n75));
  inv1  g065(.a(n20), .O(n76));
  nand2 g066(.a(n76), .b(v5), .O(n77));
  nand2 g067(.a(n77), .b(n75), .O(n78));
  nor2  g068(.a(n78), .b(n71), .O(n79));
  nor2  g069(.a(n79), .b(v3), .O(n80));
  nor2  g070(.a(v7), .b(n12), .O(n81));
  inv1  g071(.a(n81), .O(n82));
  nand2 g072(.a(n82), .b(n47), .O(n83));
  nand2 g073(.a(n15), .b(v4), .O(n84));
  nand2 g074(.a(n84), .b(n45), .O(n85));
  nand2 g075(.a(n85), .b(n83), .O(n86));
  nor2  g076(.a(n19), .b(v7), .O(n87));
  nor2  g077(.a(v5), .b(n12), .O(n88));
  nand2 g078(.a(n88), .b(n87), .O(n89));
  nand2 g079(.a(n89), .b(n86), .O(n90));
  nor2  g080(.a(n90), .b(n80), .O(n91));
  nor2  g081(.a(n91), .b(v0), .O(n92));
  nor2  g082(.a(n31), .b(v4), .O(n93));
  nor2  g083(.a(v8), .b(n38), .O(n94));
  nor2  g084(.a(n94), .b(n93), .O(n95));
  nor2  g085(.a(n95), .b(n24), .O(n96));
  inv1  g086(.a(v2), .O(n97));
  nor2  g087(.a(n47), .b(n97), .O(n98));
  nor2  g088(.a(n98), .b(n96), .O(n99));
  nor2  g089(.a(n99), .b(v3), .O(n100));
  nand2 g090(.a(n19), .b(v3), .O(n101));
  nand2 g091(.a(n101), .b(n13), .O(n102));
  nand2 g092(.a(n102), .b(v2), .O(n103));
  nor2  g093(.a(n12), .b(n24), .O(n104));
  nand2 g094(.a(n104), .b(n38), .O(n105));
  nand2 g095(.a(n105), .b(n103), .O(n106));
  nand2 g096(.a(n106), .b(n31), .O(n107));
  nor2  g097(.a(v6), .b(n97), .O(n108));
  nand2 g098(.a(n108), .b(n76), .O(n109));
  nand2 g099(.a(n109), .b(n107), .O(n110));
  nor2  g100(.a(n110), .b(n100), .O(n111));
  nor2  g101(.a(n111), .b(v5), .O(n112));
  nor2  g102(.a(n112), .b(n92), .O(n113));
  nand2 g103(.a(n113), .b(n66), .O(n114));
  nand2 g104(.a(n114), .b(v1), .O(n115));
  nor2  g105(.a(n70), .b(v4), .O(n116));
  nor2  g106(.a(n116), .b(n33), .O(n117));
  nor2  g107(.a(n117), .b(n12), .O(n118));
  nand2 g108(.a(n41), .b(n20), .O(n119));
  nand2 g109(.a(n119), .b(v2), .O(n120));
  nor2  g110(.a(n38), .b(v2), .O(n121));
  nand2 g111(.a(n121), .b(v7), .O(n122));
  nand2 g112(.a(n122), .b(n120), .O(n123));
  nand2 g113(.a(n123), .b(n11), .O(n124));
  nor2  g114(.a(n41), .b(n38), .O(n125));
  nor2  g115(.a(n67), .b(n19), .O(n126));
  nor2  g116(.a(n126), .b(n125), .O(n127));
  nor2  g117(.a(n127), .b(v2), .O(n128));
  nor2  g118(.a(n72), .b(n20), .O(n129));
  nor2  g119(.a(n129), .b(n128), .O(n130));
  nand2 g120(.a(n130), .b(n124), .O(n131));
  nor2  g121(.a(n131), .b(n118), .O(n132));
  nor2  g122(.a(n132), .b(v1), .O(n133));
  nor2  g123(.a(n15), .b(v3), .O(n134));
  nand2 g124(.a(n47), .b(n32), .O(n135));
  nand2 g125(.a(n135), .b(n134), .O(n136));
  nor2  g126(.a(n19), .b(v6), .O(n137));
  nor2  g127(.a(v5), .b(n97), .O(n138));
  nand2 g128(.a(n138), .b(n137), .O(n139));
  nand2 g129(.a(n139), .b(n136), .O(n140));
  nand2 g130(.a(n140), .b(n38), .O(n141));
  nor2  g131(.a(n19), .b(v2), .O(n142));
  nor2  g132(.a(v8), .b(n97), .O(n143));
  nor2  g133(.a(n143), .b(n142), .O(n144));
  inv1  g134(.a(n39), .O(n145));
  nor2  g135(.a(n81), .b(n145), .O(n146));
  nor2  g136(.a(n146), .b(v4), .O(n147));
  nor2  g137(.a(n55), .b(n38), .O(n148));
  nor2  g138(.a(n148), .b(n147), .O(n149));
  nor2  g139(.a(n149), .b(n144), .O(n150));
  inv1  g140(.a(n45), .O(n151));
  nor2  g141(.a(n31), .b(v6), .O(n152));
  nand2 g142(.a(n152), .b(n151), .O(n153));
  nor2  g143(.a(v8), .b(n11), .O(n154));
  nand2 g144(.a(v4), .b(n12), .O(n155));
  inv1  g145(.a(n155), .O(n156));
  nand2 g146(.a(n156), .b(n154), .O(n157));
  nand2 g147(.a(n157), .b(n153), .O(n158));
  nand2 g148(.a(n158), .b(n97), .O(n159));
  nand2 g149(.a(n148), .b(n88), .O(n160));
  nand2 g150(.a(n160), .b(n159), .O(n161));
  nor2  g151(.a(n161), .b(n150), .O(n162));
  nand2 g152(.a(n162), .b(n141), .O(n163));
  nor2  g153(.a(n163), .b(n133), .O(n164));
  nor2  g154(.a(n164), .b(n24), .O(n165));
  nor2  g155(.a(n94), .b(n137), .O(n166));
  nor2  g156(.a(n166), .b(v0), .O(n167));
  nor2  g157(.a(n39), .b(v3), .O(n168));
  nor2  g158(.a(n168), .b(n167), .O(n169));
  nor2  g159(.a(n169), .b(n31), .O(n170));
  nor2  g160(.a(n19), .b(v5), .O(n171));
  nor2  g161(.a(n154), .b(n171), .O(n172));
  nor2  g162(.a(n172), .b(n12), .O(n173));
  nand2 g163(.a(v8), .b(v6), .O(n174));
  nor2  g164(.a(n174), .b(v3), .O(n175));
  nor2  g165(.a(n175), .b(n173), .O(n176));
  nor2  g166(.a(n176), .b(v7), .O(n177));
  nor2  g167(.a(n177), .b(n170), .O(n178));
  nor2  g168(.a(n178), .b(v1), .O(n179));
  inv1  g169(.a(v1), .O(n180));
  nand2 g170(.a(n152), .b(n180), .O(n181));
  nand2 g171(.a(n154), .b(n24), .O(n182));
  nand2 g172(.a(n182), .b(n181), .O(n183));
  nand2 g173(.a(n183), .b(n38), .O(n184));
  nor2  g174(.a(v8), .b(n12), .O(n185));
  nor2  g175(.a(n185), .b(n137), .O(n186));
  nand2 g176(.a(n31), .b(n24), .O(n187));
  nor2  g177(.a(n187), .b(n186), .O(n188));
  nor2  g178(.a(n155), .b(n61), .O(n189));
  nor2  g179(.a(n189), .b(n188), .O(n190));
  nand2 g180(.a(n190), .b(n184), .O(n191));
  nand2 g181(.a(n191), .b(v5), .O(n192));
  nor2  g182(.a(n148), .b(n21), .O(n193));
  nand2 g183(.a(v3), .b(n24), .O(n194));
  nor2  g184(.a(n194), .b(n193), .O(n195));
  nor2  g185(.a(n137), .b(n154), .O(n196));
  nand2 g186(.a(n156), .b(n31), .O(n197));
  nor2  g187(.a(n197), .b(n196), .O(n198));
  nor2  g188(.a(n198), .b(n195), .O(n199));
  nand2 g189(.a(n199), .b(n192), .O(n200));
  nor2  g190(.a(n200), .b(n179), .O(n201));
  nor2  g191(.a(n201), .b(n97), .O(n202));
  nor2  g192(.a(n11), .b(v2), .O(n203));
  inv1  g193(.a(n203), .O(n204));
  nor2  g194(.a(n19), .b(v4), .O(n205));
  nor2  g195(.a(n205), .b(n16), .O(n206));
  nor2  g196(.a(n206), .b(n204), .O(n207));
  nor2  g197(.a(n19), .b(n15), .O(n208));
  inv1  g198(.a(n208), .O(n209));
  nand2 g199(.a(n38), .b(n12), .O(n210));
  nor2  g200(.a(n210), .b(n209), .O(n211));
  nor2  g201(.a(n211), .b(n207), .O(n212));
  nor2  g202(.a(n212), .b(n31), .O(n213));
  nand2 g203(.a(v8), .b(n31), .O(n214));
  nand2 g204(.a(n203), .b(v3), .O(n215));
  nor2  g205(.a(n215), .b(n214), .O(n216));
  nor2  g206(.a(n216), .b(n213), .O(n217));
  nor2  g207(.a(n217), .b(v1), .O(n218));
  nor2  g208(.a(n218), .b(n202), .O(n219));
  nor2  g209(.a(n203), .b(n134), .O(n220));
  nor2  g210(.a(n220), .b(v0), .O(n221));
  nor2  g211(.a(n221), .b(n48), .O(n222));
  nor2  g212(.a(n222), .b(n31), .O(n223));
  nand2 g213(.a(n87), .b(n11), .O(n224));
  nand2 g214(.a(n154), .b(n15), .O(n225));
  nand2 g215(.a(n225), .b(n224), .O(n226));
  nand2 g216(.a(n226), .b(v3), .O(n227));
  nand2 g217(.a(n134), .b(n154), .O(n228));
  nand2 g218(.a(n228), .b(n227), .O(n229));
  nor2  g219(.a(n229), .b(n223), .O(n230));
  nor2  g220(.a(n230), .b(v1), .O(n231));
  nand2 g221(.a(n208), .b(n97), .O(n232));
  nand2 g222(.a(n76), .b(v3), .O(n233));
  nand2 g223(.a(n233), .b(n232), .O(n234));
  nor2  g224(.a(v6), .b(v0), .O(n235));
  nand2 g225(.a(n235), .b(n234), .O(n236));
  inv1  g226(.a(n175), .O(n237));
  nand2 g227(.a(n16), .b(v3), .O(n238));
  nand2 g228(.a(n238), .b(n237), .O(n239));
  nor2  g229(.a(v7), .b(v2), .O(n240));
  nand2 g230(.a(n240), .b(n239), .O(n241));
  nand2 g231(.a(n241), .b(n236), .O(n242));
  nor2  g232(.a(n242), .b(n231), .O(n243));
  nor2  g233(.a(n243), .b(n38), .O(n244));
  nor2  g234(.a(v7), .b(n11), .O(n245));
  nor2  g235(.a(n93), .b(n245), .O(n246));
  nor2  g236(.a(n246), .b(v1), .O(n247));
  nor2  g237(.a(n214), .b(v2), .O(n248));
  nor2  g238(.a(n248), .b(n247), .O(n249));
  nor2  g239(.a(n249), .b(n12), .O(n250));
  nand2 g240(.a(n38), .b(n97), .O(n251));
  nor2  g241(.a(n251), .b(n174), .O(n252));
  nor2  g242(.a(n252), .b(n250), .O(n253));
  nor2  g243(.a(n253), .b(n15), .O(n254));
  nor2  g244(.a(n11), .b(v1), .O(n255));
  nor2  g245(.a(n255), .b(n205), .O(n256));
  nand2 g246(.a(n88), .b(v7), .O(n257));
  nor2  g247(.a(n257), .b(n256), .O(n258));
  nor2  g248(.a(n258), .b(n254), .O(n259));
  nor2  g249(.a(n259), .b(v0), .O(n260));
  nor2  g250(.a(n260), .b(n244), .O(n261));
  nand2 g251(.a(n261), .b(n219), .O(n262));
  nor2  g252(.a(n262), .b(n165), .O(n263));
  nand2 g253(.a(n263), .b(n115), .O(v9.0 ));
endmodule


