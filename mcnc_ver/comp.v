// Benchmark "comp" written by ABC on Tue Nov  5 15:01:19 2019

module comp ( 
    a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x,
    y, z, a0, b0, c0, d0, e0, f0,
    g0, h0, i0  );
  input  a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u,
    v, w, x, y, z, a0, b0, c0, d0, e0, f0;
  output g0, h0, i0;
  wire n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49,
    n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63,
    n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77,
    n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91,
    n92, n93, n94, n95, n96, n97, n98, n99, n100, n101, n102, n103, n104,
    n105, n106, n107, n108, n109, n110, n111, n112, n113, n114, n115, n116,
    n117, n118, n119, n120, n121, n122, n123, n124, n125, n126, n127, n128,
    n129, n130, n131, n132, n133, n134, n135, n136, n137, n138, n139, n140,
    n141, n142, n143, n144, n145, n146, n147, n148, n149, n150, n151, n152,
    n153, n154, n155, n156, n157, n158, n159, n160, n161, n162, n163, n165,
    n166, n167, n168, n169, n170, n171, n172;
  inv1  g000(.a(a), .O(n36));
  nor2  g001(.a(q), .b(n36), .O(n37));
  nand2 g002(.a(q), .b(n36), .O(n38));
  inv1  g003(.a(n38), .O(n39));
  nor2  g004(.a(n39), .b(n37), .O(n40));
  inv1  g005(.a(n40), .O(n41));
  inv1  g006(.a(b), .O(n42));
  nor2  g007(.a(r), .b(n42), .O(n43));
  nand2 g008(.a(r), .b(n42), .O(n44));
  inv1  g009(.a(n44), .O(n45));
  nor2  g010(.a(n45), .b(n43), .O(n46));
  inv1  g011(.a(n46), .O(n47));
  inv1  g012(.a(c), .O(n48));
  nor2  g013(.a(s), .b(n48), .O(n49));
  inv1  g014(.a(n49), .O(n50));
  nand2 g015(.a(s), .b(n48), .O(n51));
  nand2 g016(.a(n51), .b(n50), .O(n52));
  inv1  g017(.a(t), .O(n53));
  nand2 g018(.a(n53), .b(d), .O(n54));
  nor2  g019(.a(n54), .b(n52), .O(n55));
  nor2  g020(.a(n55), .b(n49), .O(n56));
  nor2  g021(.a(n56), .b(n47), .O(n57));
  nor2  g022(.a(n57), .b(n43), .O(n58));
  nor2  g023(.a(n58), .b(n41), .O(n59));
  nor2  g024(.a(n59), .b(n37), .O(n60));
  inv1  g025(.a(n54), .O(n61));
  nor2  g026(.a(n53), .b(d), .O(n62));
  nor2  g027(.a(n62), .b(n61), .O(n63));
  nand2 g028(.a(n63), .b(n40), .O(n64));
  inv1  g029(.a(n52), .O(n65));
  nand2 g030(.a(n65), .b(n46), .O(n66));
  nor2  g031(.a(n66), .b(n64), .O(n67));
  inv1  g032(.a(e), .O(n68));
  nor2  g033(.a(u), .b(n68), .O(n69));
  nand2 g034(.a(u), .b(n68), .O(n70));
  inv1  g035(.a(n70), .O(n71));
  nor2  g036(.a(n71), .b(n69), .O(n72));
  inv1  g037(.a(n72), .O(n73));
  inv1  g038(.a(f), .O(n74));
  nor2  g039(.a(v), .b(n74), .O(n75));
  inv1  g040(.a(n75), .O(n76));
  nand2 g041(.a(v), .b(n74), .O(n77));
  nand2 g042(.a(n77), .b(n76), .O(n78));
  inv1  g043(.a(w), .O(n79));
  nand2 g044(.a(n79), .b(g), .O(n80));
  inv1  g045(.a(n80), .O(n81));
  inv1  g046(.a(g), .O(n82));
  nand2 g047(.a(w), .b(n82), .O(n83));
  nand2 g048(.a(n83), .b(n80), .O(n84));
  inv1  g049(.a(x), .O(n85));
  nand2 g050(.a(n85), .b(h), .O(n86));
  nor2  g051(.a(n86), .b(n84), .O(n87));
  nor2  g052(.a(n87), .b(n81), .O(n88));
  nor2  g053(.a(n88), .b(n78), .O(n89));
  nor2  g054(.a(n89), .b(n75), .O(n90));
  nor2  g055(.a(n90), .b(n73), .O(n91));
  nor2  g056(.a(n91), .b(n69), .O(n92));
  inv1  g057(.a(n86), .O(n93));
  nor2  g058(.a(n85), .b(h), .O(n94));
  nor2  g059(.a(n94), .b(n93), .O(n95));
  nand2 g060(.a(n95), .b(n72), .O(n96));
  inv1  g061(.a(n78), .O(n97));
  inv1  g062(.a(n84), .O(n98));
  nand2 g063(.a(n98), .b(n97), .O(n99));
  nor2  g064(.a(n99), .b(n96), .O(n100));
  inv1  g065(.a(i), .O(n101));
  nor2  g066(.a(y), .b(n101), .O(n102));
  inv1  g067(.a(y), .O(n103));
  nor2  g068(.a(n103), .b(i), .O(n104));
  nor2  g069(.a(n104), .b(n102), .O(n105));
  inv1  g070(.a(n105), .O(n106));
  inv1  g071(.a(j), .O(n107));
  nor2  g072(.a(z), .b(n107), .O(n108));
  nand2 g073(.a(z), .b(n107), .O(n109));
  inv1  g074(.a(n109), .O(n110));
  nor2  g075(.a(n110), .b(n108), .O(n111));
  inv1  g076(.a(n111), .O(n112));
  inv1  g077(.a(a0), .O(n113));
  nand2 g078(.a(n113), .b(k), .O(n114));
  inv1  g079(.a(n114), .O(n115));
  inv1  g080(.a(k), .O(n116));
  nand2 g081(.a(a0), .b(n116), .O(n117));
  nand2 g082(.a(n117), .b(n114), .O(n118));
  inv1  g083(.a(b0), .O(n119));
  nand2 g084(.a(n119), .b(l), .O(n120));
  nor2  g085(.a(n120), .b(n118), .O(n121));
  nor2  g086(.a(n121), .b(n115), .O(n122));
  nor2  g087(.a(n122), .b(n112), .O(n123));
  nor2  g088(.a(n123), .b(n108), .O(n124));
  nor2  g089(.a(n124), .b(n106), .O(n125));
  nor2  g090(.a(n125), .b(n102), .O(n126));
  inv1  g091(.a(n120), .O(n127));
  nor2  g092(.a(n119), .b(l), .O(n128));
  nor2  g093(.a(n128), .b(n127), .O(n129));
  nand2 g094(.a(n129), .b(n105), .O(n130));
  inv1  g095(.a(n118), .O(n131));
  nand2 g096(.a(n131), .b(n111), .O(n132));
  nor2  g097(.a(n132), .b(n130), .O(n133));
  inv1  g098(.a(m), .O(n134));
  nor2  g099(.a(c0), .b(n134), .O(n135));
  inv1  g100(.a(n135), .O(n136));
  inv1  g101(.a(c0), .O(n137));
  nor2  g102(.a(n137), .b(m), .O(n138));
  nor2  g103(.a(n138), .b(n135), .O(n139));
  inv1  g104(.a(d0), .O(n140));
  nand2 g105(.a(n140), .b(n), .O(n141));
  inv1  g106(.a(n141), .O(n142));
  nor2  g107(.a(n140), .b(n), .O(n143));
  nor2  g108(.a(n143), .b(n142), .O(n144));
  inv1  g109(.a(o), .O(n145));
  nor2  g110(.a(e0), .b(n145), .O(n146));
  inv1  g111(.a(n146), .O(n147));
  inv1  g112(.a(e0), .O(n148));
  nor2  g113(.a(n148), .b(o), .O(n149));
  nor2  g114(.a(n149), .b(n146), .O(n150));
  inv1  g115(.a(p), .O(n151));
  nor2  g116(.a(f0), .b(n151), .O(n152));
  nand2 g117(.a(n152), .b(n150), .O(n153));
  nand2 g118(.a(n153), .b(n147), .O(n154));
  nand2 g119(.a(n154), .b(n144), .O(n155));
  nand2 g120(.a(n155), .b(n141), .O(n156));
  nand2 g121(.a(n156), .b(n139), .O(n157));
  nand2 g122(.a(n157), .b(n136), .O(n158));
  nand2 g123(.a(n158), .b(n133), .O(n159));
  nand2 g124(.a(n159), .b(n126), .O(n160));
  nand2 g125(.a(n160), .b(n100), .O(n161));
  nand2 g126(.a(n161), .b(n92), .O(n162));
  nand2 g127(.a(n162), .b(n67), .O(n163));
  nand2 g128(.a(n163), .b(n60), .O(i0));
  inv1  g129(.a(f0), .O(n165));
  nor2  g130(.a(n165), .b(p), .O(n166));
  nor2  g131(.a(n166), .b(n152), .O(n167));
  nand2 g132(.a(n167), .b(n139), .O(n168));
  nand2 g133(.a(n150), .b(n144), .O(n169));
  nor2  g134(.a(n169), .b(n168), .O(n170));
  nand2 g135(.a(n170), .b(n67), .O(n171));
  nand2 g136(.a(n133), .b(n100), .O(n172));
  nor2  g137(.a(n172), .b(n171), .O(h0));
  nor2  g138(.a(h0), .b(i0), .O(g0));
endmodule


