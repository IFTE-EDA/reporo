// Benchmark "CM162" written by ABC on Tue Nov  5 15:01:19 2019

module CM162 ( 
    a, b, c, d, e, f, g, h, i, j, k, l, m, n,
    o, p, q, r, s  );
  input  a, b, c, d, e, f, g, h, i, j, k, l, m, n;
  output o, p, q, r, s;
  wire n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33,
    n34, n35, n36, n37, n38, n39, n40, n41, n43, n44, n45, n46, n47, n48,
    n49, n50, n51, n53, n54, n55, n56, n57, n58, n59, n60, n61, n63, n64,
    n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n76;
  inv1  g00(.a(i), .O(n20));
  inv1  g01(.a(c), .O(n21));
  inv1  g02(.a(d), .O(n22));
  nor2  g03(.a(n22), .b(n21), .O(n23));
  nand2 g04(.a(n23), .b(e), .O(n24));
  nor2  g05(.a(n24), .b(n20), .O(n25));
  inv1  g06(.a(e), .O(n26));
  nand2 g07(.a(d), .b(c), .O(n27));
  nor2  g08(.a(n27), .b(n26), .O(n28));
  nor2  g09(.a(n28), .b(i), .O(n29));
  nor2  g10(.a(n29), .b(n25), .O(n30));
  inv1  g11(.a(j), .O(n31));
  inv1  g12(.a(n), .O(n32));
  nor2  g13(.a(n32), .b(n31), .O(n33));
  nand2 g14(.a(n33), .b(n28), .O(n34));
  inv1  g15(.a(f), .O(n35));
  nor2  g16(.a(n35), .b(n22), .O(n36));
  nand2 g17(.a(n36), .b(n34), .O(n37));
  nor2  g18(.a(n37), .b(n30), .O(n38));
  inv1  g19(.a(a), .O(n39));
  nand2 g20(.a(f), .b(n22), .O(n40));
  nor2  g21(.a(n40), .b(n39), .O(n41));
  nor2  g22(.a(n41), .b(n38), .O(o));
  inv1  g23(.a(k), .O(n43));
  nand2 g24(.a(n28), .b(n20), .O(n44));
  nor2  g25(.a(n44), .b(n43), .O(n45));
  nor2  g26(.a(n24), .b(i), .O(n46));
  nor2  g27(.a(n46), .b(k), .O(n47));
  nor2  g28(.a(n47), .b(n45), .O(n48));
  nor2  g29(.a(n48), .b(n37), .O(n49));
  inv1  g30(.a(b), .O(n50));
  nor2  g31(.a(n40), .b(n50), .O(n51));
  nor2  g32(.a(n51), .b(n49), .O(p));
  inv1  g33(.a(l), .O(n53));
  nand2 g34(.a(n46), .b(n43), .O(n54));
  nor2  g35(.a(n54), .b(n53), .O(n55));
  nor2  g36(.a(n44), .b(k), .O(n56));
  nor2  g37(.a(n56), .b(l), .O(n57));
  nor2  g38(.a(n57), .b(n55), .O(n58));
  nor2  g39(.a(n58), .b(n37), .O(n59));
  inv1  g40(.a(g), .O(n60));
  nor2  g41(.a(n40), .b(n60), .O(n61));
  nor2  g42(.a(n61), .b(n59), .O(q));
  inv1  g43(.a(m), .O(n63));
  nor2  g44(.a(k), .b(i), .O(n64));
  nand2 g45(.a(n64), .b(n53), .O(n65));
  inv1  g46(.a(n65), .O(n66));
  nand2 g47(.a(n66), .b(n28), .O(n67));
  nor2  g48(.a(n67), .b(n63), .O(n68));
  nor2  g49(.a(n65), .b(n24), .O(n69));
  nor2  g50(.a(n69), .b(m), .O(n70));
  nor2  g51(.a(n70), .b(n68), .O(n71));
  nor2  g52(.a(n71), .b(n37), .O(n72));
  inv1  g53(.a(h), .O(n73));
  nor2  g54(.a(n40), .b(n73), .O(n74));
  nor2  g55(.a(n74), .b(n72), .O(r));
  inv1  g56(.a(n33), .O(n76));
  nor2  g57(.a(n76), .b(n26), .O(s));
endmodule


