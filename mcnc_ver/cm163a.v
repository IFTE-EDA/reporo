// Benchmark "CM163" written by ABC on Tue Nov  5 15:01:19 2019

module CM163 ( 
    a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p,
    q, r, s, t, u  );
  input  a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p;
  output q, r, s, t, u;
  wire n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35,
    n37, n38, n39, n40, n41, n42, n43, n44, n45, n47, n48, n49, n50, n51,
    n52, n53, n54, n55, n57, n58, n59, n60, n61, n62, n63, n64, n65, n66,
    n67, n68, n69, n71, n72, n73, n74, n75;
  inv1  g00(.a(j), .O(n22));
  nand2 g01(.a(d), .b(c), .O(n23));
  nor2  g02(.a(n23), .b(n22), .O(n24));
  inv1  g03(.a(c), .O(n25));
  inv1  g04(.a(d), .O(n26));
  nor2  g05(.a(n26), .b(n25), .O(n27));
  nor2  g06(.a(n27), .b(j), .O(n28));
  nor2  g07(.a(n28), .b(n24), .O(n29));
  nand2 g08(.a(f), .b(e), .O(n30));
  nor2  g09(.a(n30), .b(n29), .O(n31));
  inv1  g10(.a(a), .O(n32));
  inv1  g11(.a(e), .O(n33));
  nand2 g12(.a(f), .b(n33), .O(n34));
  nor2  g13(.a(n34), .b(n32), .O(n35));
  nor2  g14(.a(n35), .b(n31), .O(q));
  inv1  g15(.a(l), .O(n37));
  nand2 g16(.a(n27), .b(n22), .O(n38));
  nor2  g17(.a(n38), .b(n37), .O(n39));
  nor2  g18(.a(n23), .b(j), .O(n40));
  nor2  g19(.a(n40), .b(l), .O(n41));
  nor2  g20(.a(n41), .b(n39), .O(n42));
  nor2  g21(.a(n42), .b(n30), .O(n43));
  inv1  g22(.a(b), .O(n44));
  nor2  g23(.a(n34), .b(n44), .O(n45));
  nor2  g24(.a(n45), .b(n43), .O(r));
  inv1  g25(.a(m), .O(n47));
  nand2 g26(.a(n40), .b(n37), .O(n48));
  nor2  g27(.a(n48), .b(n47), .O(n49));
  nor2  g28(.a(n38), .b(l), .O(n50));
  nor2  g29(.a(n50), .b(m), .O(n51));
  nor2  g30(.a(n51), .b(n49), .O(n52));
  nor2  g31(.a(n52), .b(n30), .O(n53));
  inv1  g32(.a(g), .O(n54));
  nor2  g33(.a(n34), .b(n54), .O(n55));
  nor2  g34(.a(n55), .b(n53), .O(s));
  inv1  g35(.a(n), .O(n57));
  nand2 g36(.a(n37), .b(n22), .O(n58));
  nor2  g37(.a(n58), .b(m), .O(n59));
  nand2 g38(.a(n59), .b(n27), .O(n60));
  nor2  g39(.a(n60), .b(n57), .O(n61));
  nor2  g40(.a(l), .b(j), .O(n62));
  nand2 g41(.a(n62), .b(n47), .O(n63));
  nor2  g42(.a(n63), .b(n23), .O(n64));
  nor2  g43(.a(n64), .b(n), .O(n65));
  nor2  g44(.a(n65), .b(n61), .O(n66));
  nor2  g45(.a(n66), .b(n30), .O(n67));
  inv1  g46(.a(h), .O(n68));
  nor2  g47(.a(n34), .b(n68), .O(n69));
  nor2  g48(.a(n69), .b(n67), .O(t));
  nand2 g49(.a(i), .b(d), .O(n71));
  inv1  g50(.a(k), .O(n72));
  inv1  g51(.a(o), .O(n73));
  nor2  g52(.a(n73), .b(n72), .O(n74));
  nand2 g53(.a(n74), .b(p), .O(n75));
  nor2  g54(.a(n75), .b(n71), .O(u));
endmodule


