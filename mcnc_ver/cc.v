// Benchmark "cc" written by ABC on Tue Nov  5 15:01:19 2019

module cc ( 
    a, b, c, d, e, f, g, h, i, j, k, l, m, o, p, q, r, s, t, u, v,
    w, x, y, z, a0, b0, c0, d0, e0, f0, g0, h0, i0, j0, k0, l0, m0, n0, o0,
    p0  );
  input  a, b, c, d, e, f, g, h, i, j, k, l, m, o, p, q, r, s, t, u, v;
  output w, x, y, z, a0, b0, c0, d0, e0, f0, g0, h0, i0, j0, k0, l0, m0, n0,
    o0, p0;
  wire n42, n43, n45, n46, n47, n49, n50, n51, n52, n53, n54, n56, n57, n58,
    n63, n64, n65, n66, n67, n68, n70, n71, n72, n73, n74, n75, n77, n78,
    n79, n80, n81, n83, n84, n85, n86, n87, n88, n90, n91, n92, n93, n95,
    n96, n97, n98, n100, n101, n102, n103, n105, n106, n107, n108;
  inv1  g00(.a(l), .O(n42));
  inv1  g01(.a(v), .O(n43));
  nor2  g02(.a(n43), .b(n42), .O(w));
  nand2 g03(.a(k), .b(i), .O(n45));
  inv1  g04(.a(p), .O(n46));
  nand2 g05(.a(q), .b(n46), .O(n47));
  nor2  g06(.a(n47), .b(n45), .O(x));
  inv1  g07(.a(i), .O(n49));
  inv1  g08(.a(q), .O(n50));
  nand2 g09(.a(n50), .b(k), .O(n51));
  nor2  g10(.a(n51), .b(n49), .O(n52));
  nor2  g11(.a(n52), .b(p), .O(n53));
  nand2 g12(.a(m), .b(n42), .O(n54));
  nor2  g13(.a(n54), .b(n53), .O(y));
  inv1  g14(.a(m), .O(n56));
  nor2  g15(.a(p), .b(n56), .O(n57));
  nand2 g16(.a(n57), .b(q), .O(n58));
  nor2  g17(.a(n58), .b(n45), .O(z));
  inv1  g18(.a(t), .O(a0));
  nand2 g19(.a(j), .b(i), .O(g0));
  inv1  g20(.a(g0), .O(f0));
  nor2  g21(.a(n52), .b(o), .O(n63));
  inv1  g22(.a(k), .O(n64));
  nor2  g23(.a(q), .b(n64), .O(n65));
  nor2  g24(.a(n49), .b(a), .O(n66));
  nand2 g25(.a(n66), .b(n65), .O(n67));
  nand2 g26(.a(n67), .b(m), .O(n68));
  nor2  g27(.a(n68), .b(n63), .O(i0));
  nor2  g28(.a(n45), .b(n50), .O(n70));
  nor2  g29(.a(n70), .b(p), .O(n71));
  inv1  g30(.a(b), .O(n72));
  nor2  g31(.a(n49), .b(n72), .O(n73));
  nand2 g32(.a(n73), .b(n65), .O(n74));
  nand2 g33(.a(n74), .b(m), .O(n75));
  nor2  g34(.a(n75), .b(n71), .O(j0));
  inv1  g35(.a(c), .O(n77));
  nor2  g36(.a(q), .b(n77), .O(n78));
  nor2  g37(.a(n78), .b(n45), .O(n79));
  nand2 g38(.a(n45), .b(n50), .O(n80));
  nand2 g39(.a(n80), .b(m), .O(n81));
  nor2  g40(.a(n81), .b(n79), .O(k0));
  inv1  g41(.a(r), .O(n83));
  nor2  g42(.a(n83), .b(p), .O(n84));
  nor2  g43(.a(n84), .b(n52), .O(n85));
  nor2  g44(.a(n49), .b(d), .O(n86));
  nand2 g45(.a(n86), .b(n65), .O(n87));
  nand2 g46(.a(n87), .b(m), .O(n88));
  nor2  g47(.a(n88), .b(n85), .O(l0));
  nor2  g48(.a(n52), .b(s), .O(n90));
  nor2  g49(.a(n49), .b(e), .O(n91));
  nand2 g50(.a(n91), .b(n65), .O(n92));
  nand2 g51(.a(n92), .b(m), .O(n93));
  nor2  g52(.a(n93), .b(n90), .O(m0));
  nor2  g53(.a(n52), .b(t), .O(n95));
  nor2  g54(.a(n49), .b(f), .O(n96));
  nand2 g55(.a(n96), .b(n65), .O(n97));
  nand2 g56(.a(n97), .b(m), .O(n98));
  nor2  g57(.a(n98), .b(n95), .O(n0));
  nor2  g58(.a(n52), .b(u), .O(n100));
  nor2  g59(.a(n49), .b(g), .O(n101));
  nand2 g60(.a(n101), .b(n65), .O(n102));
  nand2 g61(.a(n102), .b(m), .O(n103));
  nor2  g62(.a(n103), .b(n100), .O(o0));
  nor2  g63(.a(n52), .b(v), .O(n105));
  nor2  g64(.a(n49), .b(h), .O(n106));
  nand2 g65(.a(n106), .b(n65), .O(n107));
  nand2 g66(.a(n107), .b(m), .O(n108));
  nor2  g67(.a(n108), .b(n105), .O(p0));
  buf   g68(.a(u), .O(b0));
  buf   g69(.a(q), .O(c0));
  buf   g70(.a(s), .O(d0));
  buf   g71(.a(r), .O(e0));
  buf   g72(.a(p), .O(h0));
endmodule


