// Benchmark "pcle_cl" written by ABC on Tue Nov  5 15:01:26 2019

module pcle_cl ( 
    a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s,
    t, u, v, w, x, y, z, a0, b0  );
  input  a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s;
  output t, u, v, w, x, y, z, a0, b0;
  wire n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42,
    n44, n45, n47, n48, n49, n50, n51, n53, n54, n55, n56, n57, n58, n59,
    n61, n62, n63, n64, n65, n66, n68, n69, n70, n71, n72, n73, n74, n76,
    n77, n78, n79, n80, n81, n83, n84, n85, n86, n87, n88, n89, n91, n92,
    n93, n94, n95, n96, n97;
  inv1  g00(.a(q), .O(n29));
  inv1  g01(.a(o), .O(n30));
  inv1  g02(.a(l), .O(n31));
  inv1  g03(.a(m), .O(n32));
  nor2  g04(.a(n32), .b(n31), .O(n33));
  nand2 g05(.a(n33), .b(n), .O(n34));
  nor2  g06(.a(n34), .b(n30), .O(n35));
  nand2 g07(.a(n35), .b(p), .O(n36));
  nor2  g08(.a(n36), .b(n29), .O(n37));
  nand2 g09(.a(n37), .b(r), .O(n38));
  inv1  g10(.a(i), .O(n39));
  nand2 g11(.a(j), .b(n39), .O(n40));
  nor2  g12(.a(n40), .b(k), .O(n41));
  nand2 g13(.a(n41), .b(s), .O(n42));
  nor2  g14(.a(n42), .b(n38), .O(t));
  nand2 g15(.a(i), .b(a), .O(n44));
  nand2 g16(.a(n41), .b(n31), .O(n45));
  nand2 g17(.a(n45), .b(n44), .O(u));
  nand2 g18(.a(m), .b(n31), .O(n47));
  nand2 g19(.a(n32), .b(l), .O(n48));
  nand2 g20(.a(n48), .b(n47), .O(n49));
  nand2 g21(.a(n49), .b(n41), .O(n50));
  nand2 g22(.a(i), .b(b), .O(n51));
  nand2 g23(.a(n51), .b(n50), .O(v));
  inv1  g24(.a(n), .O(n53));
  nand2 g25(.a(n33), .b(n53), .O(n54));
  nand2 g26(.a(m), .b(l), .O(n55));
  nand2 g27(.a(n55), .b(n), .O(n56));
  nand2 g28(.a(n56), .b(n54), .O(n57));
  nand2 g29(.a(n57), .b(n41), .O(n58));
  nand2 g30(.a(i), .b(c), .O(n59));
  nand2 g31(.a(n59), .b(n58), .O(w));
  nor2  g32(.a(n55), .b(n53), .O(n61));
  nand2 g33(.a(n61), .b(n30), .O(n62));
  nand2 g34(.a(n34), .b(o), .O(n63));
  nand2 g35(.a(n63), .b(n62), .O(n64));
  nand2 g36(.a(n64), .b(n41), .O(n65));
  nand2 g37(.a(i), .b(d), .O(n66));
  nand2 g38(.a(n66), .b(n65), .O(x));
  inv1  g39(.a(p), .O(n68));
  nand2 g40(.a(n35), .b(n68), .O(n69));
  nand2 g41(.a(n61), .b(o), .O(n70));
  nand2 g42(.a(n70), .b(p), .O(n71));
  nand2 g43(.a(n71), .b(n69), .O(n72));
  nand2 g44(.a(n72), .b(n41), .O(n73));
  nand2 g45(.a(i), .b(e), .O(n74));
  nand2 g46(.a(n74), .b(n73), .O(y));
  nor2  g47(.a(n70), .b(n68), .O(n76));
  nand2 g48(.a(n76), .b(n29), .O(n77));
  nand2 g49(.a(n36), .b(q), .O(n78));
  nand2 g50(.a(n78), .b(n77), .O(n79));
  nand2 g51(.a(n79), .b(n41), .O(n80));
  nand2 g52(.a(i), .b(f), .O(n81));
  nand2 g53(.a(n81), .b(n80), .O(z));
  inv1  g54(.a(r), .O(n83));
  nand2 g55(.a(n37), .b(n83), .O(n84));
  nand2 g56(.a(n76), .b(q), .O(n85));
  nand2 g57(.a(n85), .b(r), .O(n86));
  nand2 g58(.a(n86), .b(n84), .O(n87));
  nand2 g59(.a(n87), .b(n41), .O(n88));
  nand2 g60(.a(i), .b(g), .O(n89));
  nand2 g61(.a(n89), .b(n88), .O(a0));
  inv1  g62(.a(s), .O(n91));
  nor2  g63(.a(n85), .b(n83), .O(n92));
  nand2 g64(.a(n92), .b(n91), .O(n93));
  nand2 g65(.a(n38), .b(s), .O(n94));
  nand2 g66(.a(n94), .b(n93), .O(n95));
  nand2 g67(.a(n95), .b(n41), .O(n96));
  nand2 g68(.a(i), .b(h), .O(n97));
  nand2 g69(.a(n97), .b(n96), .O(b0));
endmodule


