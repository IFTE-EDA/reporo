// Benchmark "frg1" written by ABC on Tue Nov  5 15:01:21 2019

module frg1 ( 
    a, b, c, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y,
    z, a0, b0, c0,
    d0, e0, f0  );
  input  a, b, c, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v,
    w, x, y, z, a0, b0, c0;
  output d0, e0, f0;
  wire n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45,
    n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59,
    n60, n61, n62, n63, n64, n65, n66, n67, n68, n69, n70, n71, n72, n73,
    n74, n75, n76, n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87,
    n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101,
    n102, n103, n104, n105, n106, n107, n108, n109, n110, n111, n112, n113,
    n114, n115, n116, n117, n118, n119, n120, n121, n122, n123, n124, n125,
    n126, n127, n128, n129, n130, n131, n132, n133, n134, n135, n136, n137,
    n138, n139, n140, n141, n142, n143, n144, n145, n146, n147, n148, n149,
    n150, n151, n152, n153, n154, n155, n156, n157, n158, n159, n160, n161,
    n162, n163, n164, n165, n166, n167, n168, n169, n170, n171, n172, n173,
    n174, n175, n176, n177, n178, n179, n180, n182, n183, n184, n185, n187,
    n188, n189;
  inv1  g000(.a(c), .O(n32));
  nor2  g001(.a(e), .b(a), .O(n33));
  inv1  g002(.a(n33), .O(n34));
  inv1  g003(.a(v), .O(n35));
  inv1  g004(.a(r), .O(n36));
  inv1  g005(.a(z), .O(n37));
  inv1  g006(.a(h), .O(n38));
  nor2  g007(.a(s), .b(p), .O(n39));
  nor2  g008(.a(x), .b(t), .O(n40));
  nand2 g009(.a(n40), .b(n39), .O(n41));
  nand2 g010(.a(n41), .b(j), .O(n42));
  nand2 g011(.a(n42), .b(n38), .O(n43));
  nor2  g012(.a(x), .b(w), .O(n44));
  inv1  g013(.a(p), .O(n45));
  inv1  g014(.a(s), .O(n46));
  nand2 g015(.a(n46), .b(n45), .O(n47));
  inv1  g016(.a(o), .O(n48));
  inv1  g017(.a(t), .O(n49));
  nand2 g018(.a(n49), .b(n48), .O(n50));
  nor2  g019(.a(n50), .b(n47), .O(n51));
  nand2 g020(.a(n51), .b(n44), .O(n52));
  nand2 g021(.a(n52), .b(j), .O(n53));
  nor2  g022(.a(u), .b(q), .O(n54));
  inv1  g023(.a(n54), .O(n55));
  nor2  g024(.a(n55), .b(y), .O(n56));
  nand2 g025(.a(n56), .b(n53), .O(n57));
  nand2 g026(.a(n57), .b(n43), .O(n58));
  nand2 g027(.a(n58), .b(n37), .O(n59));
  inv1  g028(.a(m), .O(n60));
  nor2  g029(.a(t), .b(s), .O(n61));
  nand2 g030(.a(n61), .b(n45), .O(n62));
  nand2 g031(.a(n62), .b(j), .O(n63));
  nand2 g032(.a(n63), .b(n38), .O(n64));
  nor2  g033(.a(t), .b(o), .O(n65));
  nand2 g034(.a(n65), .b(n39), .O(n66));
  nand2 g035(.a(n66), .b(j), .O(n67));
  nand2 g036(.a(n67), .b(n54), .O(n68));
  nand2 g037(.a(n68), .b(n64), .O(n69));
  nand2 g038(.a(n69), .b(n60), .O(n70));
  nand2 g039(.a(n70), .b(n59), .O(n71));
  nand2 g040(.a(n71), .b(n36), .O(n72));
  inv1  g041(.a(n61), .O(n73));
  nand2 g042(.a(n73), .b(j), .O(n74));
  inv1  g043(.a(u), .O(n75));
  nor2  g044(.a(n75), .b(n38), .O(n76));
  nor2  g045(.a(n76), .b(l), .O(n77));
  nand2 g046(.a(n77), .b(n74), .O(n78));
  nand2 g047(.a(n78), .b(n72), .O(n79));
  nand2 g048(.a(n79), .b(n35), .O(n80));
  nor2  g049(.a(z), .b(y), .O(n81));
  nor2  g050(.a(n81), .b(n60), .O(n82));
  nor2  g051(.a(n82), .b(j), .O(n83));
  nand2 g052(.a(n45), .b(n48), .O(n84));
  inv1  g053(.a(w), .O(n85));
  inv1  g054(.a(x), .O(n86));
  nand2 g055(.a(n86), .b(n85), .O(n87));
  inv1  g056(.a(y), .O(n88));
  nand2 g057(.a(n37), .b(n88), .O(n89));
  nor2  g058(.a(n89), .b(n87), .O(n90));
  nor2  g059(.a(n90), .b(n60), .O(n91));
  nor2  g060(.a(n91), .b(n84), .O(n92));
  nor2  g061(.a(n92), .b(n83), .O(n93));
  nor2  g062(.a(n93), .b(r), .O(n94));
  inv1  g063(.a(j), .O(n95));
  nand2 g064(.a(y), .b(m), .O(n96));
  nand2 g065(.a(n96), .b(n95), .O(n97));
  inv1  g066(.a(n97), .O(n98));
  nor2  g067(.a(y), .b(w), .O(n99));
  nor2  g068(.a(n99), .b(n60), .O(n100));
  nor2  g069(.a(n100), .b(o), .O(n101));
  nor2  g070(.a(n101), .b(n98), .O(n102));
  nor2  g071(.a(n102), .b(g), .O(n103));
  nor2  g072(.a(n103), .b(n94), .O(n104));
  nor2  g073(.a(n104), .b(q), .O(n105));
  nor2  g074(.a(n37), .b(n60), .O(n106));
  nor2  g075(.a(n106), .b(j), .O(n107));
  nor2  g076(.a(z), .b(x), .O(n108));
  nor2  g077(.a(n108), .b(n60), .O(n109));
  nor2  g078(.a(n109), .b(p), .O(n110));
  nor2  g079(.a(n110), .b(n107), .O(n111));
  nand2 g080(.a(n36), .b(n38), .O(n112));
  nor2  g081(.a(n112), .b(n111), .O(n113));
  nor2  g082(.a(n113), .b(n105), .O(n114));
  nor2  g083(.a(n114), .b(k), .O(n115));
  inv1  g084(.a(i), .O(n116));
  inv1  g085(.a(g), .O(n117));
  nand2 g086(.a(w), .b(m), .O(n118));
  nand2 g087(.a(n118), .b(n117), .O(n119));
  inv1  g088(.a(k), .O(n120));
  nor2  g089(.a(n61), .b(n120), .O(n121));
  inv1  g090(.a(n121), .O(n122));
  nor2  g091(.a(n44), .b(n60), .O(n123));
  nor2  g092(.a(n123), .b(p), .O(n124));
  nand2 g093(.a(n124), .b(n122), .O(n125));
  nand2 g094(.a(n125), .b(n119), .O(n126));
  nand2 g095(.a(n126), .b(n48), .O(n127));
  nand2 g096(.a(x), .b(m), .O(n128));
  nand2 g097(.a(n128), .b(n45), .O(n129));
  nor2  g098(.a(n129), .b(n121), .O(n130));
  nor2  g099(.a(x), .b(n), .O(n131));
  nor2  g100(.a(n131), .b(n130), .O(n132));
  nor2  g101(.a(n132), .b(h), .O(n133));
  inv1  g102(.a(l), .O(n134));
  nand2 g103(.a(n73), .b(g), .O(n135));
  nand2 g104(.a(n135), .b(n134), .O(n136));
  nor2  g105(.a(n86), .b(n117), .O(n137));
  inv1  g106(.a(n), .O(n138));
  nand2 g107(.a(n85), .b(n138), .O(n139));
  nor2  g108(.a(n139), .b(n137), .O(n140));
  nor2  g109(.a(n140), .b(n95), .O(n141));
  nand2 g110(.a(n141), .b(n136), .O(n142));
  nor2  g111(.a(n142), .b(n133), .O(n143));
  nand2 g112(.a(n143), .b(n127), .O(n144));
  nand2 g113(.a(n144), .b(n116), .O(n145));
  nor2  g114(.a(n86), .b(n95), .O(n146));
  nor2  g115(.a(n146), .b(h), .O(n147));
  nor2  g116(.a(n44), .b(n95), .O(n148));
  nor2  g117(.a(n148), .b(y), .O(n149));
  nor2  g118(.a(n149), .b(n147), .O(n150));
  nor2  g119(.a(n150), .b(z), .O(n151));
  nor2  g120(.a(n151), .b(n60), .O(n152));
  nor2  g121(.a(n152), .b(n), .O(n153));
  nand2 g122(.a(n75), .b(n117), .O(n154));
  nor2  g123(.a(n138), .b(n120), .O(n155));
  nand2 g124(.a(n155), .b(n154), .O(n156));
  nand2 g125(.a(n156), .b(n134), .O(n157));
  nand2 g126(.a(n88), .b(n85), .O(n158));
  nand2 g127(.a(n158), .b(m), .O(n159));
  nand2 g128(.a(n159), .b(n48), .O(n160));
  nand2 g129(.a(n160), .b(n97), .O(n161));
  nand2 g130(.a(n161), .b(n54), .O(n162));
  nor2  g131(.a(n85), .b(n95), .O(n163));
  nand2 g132(.a(n88), .b(n138), .O(n164));
  nor2  g133(.a(n164), .b(n163), .O(n165));
  nor2  g134(.a(n165), .b(n38), .O(n166));
  nand2 g135(.a(n166), .b(n162), .O(n167));
  nand2 g136(.a(n167), .b(n117), .O(n168));
  nand2 g137(.a(n168), .b(n157), .O(n169));
  nor2  g138(.a(n169), .b(n153), .O(n170));
  nand2 g139(.a(n170), .b(n145), .O(n171));
  nor2  g140(.a(n171), .b(n115), .O(n172));
  nand2 g141(.a(n172), .b(n80), .O(n173));
  nand2 g142(.a(n173), .b(n34), .O(n174));
  inv1  g143(.a(c0), .O(n175));
  nand2 g144(.a(n33), .b(n175), .O(n176));
  nand2 g145(.a(n176), .b(n174), .O(n177));
  nand2 g146(.a(n177), .b(n32), .O(n178));
  inv1  g147(.a(b), .O(n179));
  nand2 g148(.a(c), .b(n179), .O(n180));
  nand2 g149(.a(n180), .b(n178), .O(d0));
  inv1  g150(.a(e), .O(n182));
  nand2 g151(.a(a0), .b(n182), .O(n183));
  nand2 g152(.a(n183), .b(f), .O(n184));
  nor2  g153(.a(c), .b(a), .O(n185));
  nand2 g154(.a(n185), .b(n184), .O(e0));
  inv1  g155(.a(b0), .O(n187));
  inv1  g156(.a(n185), .O(n188));
  nor2  g157(.a(n188), .b(n187), .O(n189));
  nor2  g158(.a(n189), .b(e), .O(f0));
endmodule


