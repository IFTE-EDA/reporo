// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:29 2019

module source_pla  ( 
    v0, v1, v2, v3,
    v4.0 , v4.1 , v4.2 , v4.3 , v4.4 , v4.5 , v4.6   );
  input  v0, v1, v2, v3;
  output v4.0 , v4.1 , v4.2 , v4.3 , v4.4 , v4.5 , v4.6 ;
  wire n12, n13, n14, n15, n16, n17, n18, n19, n21, n22, n23, n24, n25, n26,
    n27, n28, n29, n30, n31, n32, n34, n35, n36, n37, n38, n39, n41, n42,
    n43, n46, n47, n48, n49, n50, n52, n53, n54, n55, n56, n57, n58, n59,
    n60;
  nor2  g00(.a(v2), .b(v1), .O(n12));
  nor2  g01(.a(n12), .b(v0), .O(n13));
  nand2 g02(.a(n12), .b(v0), .O(n14));
  inv1  g03(.a(n14), .O(n15));
  nor2  g04(.a(n15), .b(n13), .O(n16));
  nor2  g05(.a(v1), .b(v0), .O(n17));
  nor2  g06(.a(v3), .b(v2), .O(n18));
  nand2 g07(.a(n18), .b(n17), .O(n19));
  nand2 g08(.a(n19), .b(n16), .O(v4.0 ));
  inv1  g09(.a(v2), .O(n21));
  inv1  g10(.a(v3), .O(n22));
  nand2 g11(.a(n17), .b(n22), .O(n23));
  inv1  g12(.a(v1), .O(n24));
  nor2  g13(.a(n24), .b(v0), .O(n25));
  inv1  g14(.a(v0), .O(n26));
  nor2  g15(.a(v1), .b(n26), .O(n27));
  nor2  g16(.a(n27), .b(n25), .O(n28));
  nand2 g17(.a(n28), .b(n23), .O(n29));
  nand2 g18(.a(n29), .b(n21), .O(n30));
  nor2  g19(.a(v3), .b(n21), .O(n31));
  nand2 g20(.a(n31), .b(n25), .O(n32));
  nand2 g21(.a(n32), .b(n30), .O(v4.1 ));
  nand2 g22(.a(v2), .b(v0), .O(n34));
  nand2 g23(.a(n34), .b(n24), .O(n35));
  inv1  g24(.a(n18), .O(n36));
  nand2 g25(.a(v3), .b(v2), .O(n37));
  nand2 g26(.a(n37), .b(n36), .O(n38));
  nand2 g27(.a(n38), .b(n25), .O(n39));
  nand2 g28(.a(n39), .b(n35), .O(v4.2 ));
  nor2  g29(.a(v2), .b(n24), .O(n41));
  nor2  g30(.a(n41), .b(v0), .O(n42));
  nor2  g31(.a(n42), .b(n15), .O(n43));
  nor2  g32(.a(n43), .b(v3), .O(v4.3 ));
  inv1  g33(.a(n16), .O(v4.4 ));
  nand2 g34(.a(v2), .b(n24), .O(n46));
  nand2 g35(.a(n46), .b(n26), .O(n47));
  inv1  g36(.a(n17), .O(n48));
  nor2  g37(.a(n37), .b(n48), .O(n49));
  nor2  g38(.a(n49), .b(n15), .O(n50));
  nand2 g39(.a(n50), .b(n47), .O(v4.5 ));
  nand2 g40(.a(n18), .b(n26), .O(n52));
  nor2  g41(.a(v2), .b(n26), .O(n53));
  nor2  g42(.a(n21), .b(v0), .O(n54));
  nor2  g43(.a(n54), .b(n53), .O(n55));
  nand2 g44(.a(n55), .b(n52), .O(n56));
  nand2 g45(.a(n56), .b(n24), .O(n57));
  inv1  g46(.a(n37), .O(n58));
  nor2  g47(.a(n58), .b(n18), .O(n59));
  nand2 g48(.a(n59), .b(n25), .O(n60));
  nand2 g49(.a(n60), .b(n57), .O(v4.6 ));
endmodule


