// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:25 2019

module source_pla  ( 
    EX_INSpass, byteEX, s1, s0, phi3,
    ex3, ex2, ex1, ex0, ins3, ins2, ins1, ins0  );
  input  EX_INSpass, byteEX, s1, s0, phi3;
  output ex3, ex2, ex1, ex0, ins3, ins2, ins1, ins0;
  wire n14, n15, n16, n17, n19, n20, n22, n23, n26, n27, n30;
  inv1  g00(.a(byteEX), .O(n14));
  nor2  g01(.a(n14), .b(EX_INSpass), .O(n15));
  nand2 g02(.a(n15), .b(s1), .O(n16));
  nand2 g03(.a(phi3), .b(s0), .O(n17));
  nor2  g04(.a(n17), .b(n16), .O(ex3));
  inv1  g05(.a(s0), .O(n19));
  nand2 g06(.a(phi3), .b(n19), .O(n20));
  nor2  g07(.a(n20), .b(n16), .O(ex2));
  inv1  g08(.a(s1), .O(n22));
  nand2 g09(.a(n15), .b(n22), .O(n23));
  nor2  g10(.a(n23), .b(n17), .O(ex1));
  nor2  g11(.a(n23), .b(n20), .O(ex0));
  nor2  g12(.a(byteEX), .b(EX_INSpass), .O(n26));
  nand2 g13(.a(n26), .b(s1), .O(n27));
  nor2  g14(.a(n27), .b(n17), .O(ins3));
  nor2  g15(.a(n27), .b(n20), .O(ins2));
  nand2 g16(.a(n26), .b(n22), .O(n30));
  nor2  g17(.a(n30), .b(n17), .O(ins1));
  nor2  g18(.a(n30), .b(n20), .O(ins0));
endmodule


