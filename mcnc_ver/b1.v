// Benchmark "b1" written by ABC on Tue Nov  5 15:01:17 2019

module b1 ( 
    a, b, c,
    d, e, f, g  );
  input  a, b, c;
  output d, e, f, g;
  wire n8, n9, n10, n11, n14, n15, n16, n17, n18, n19, n20, n21;
  inv1  g00(.a(a), .O(n8));
  inv1  g01(.a(b), .O(n9));
  nor2  g02(.a(n9), .b(n8), .O(n10));
  nor2  g03(.a(b), .b(a), .O(n11));
  nor2  g04(.a(n11), .b(n10), .O(e));
  inv1  g05(.a(c), .O(g));
  nor2  g06(.a(g), .b(b), .O(n14));
  nor2  g07(.a(n14), .b(a), .O(n15));
  nand2 g08(.a(g), .b(b), .O(n16));
  nand2 g09(.a(n16), .b(a), .O(n17));
  nor2  g10(.a(c), .b(b), .O(n18));
  nor2  g11(.a(g), .b(n9), .O(n19));
  nor2  g12(.a(n19), .b(n18), .O(n20));
  nand2 g13(.a(n20), .b(n17), .O(n21));
  nor2  g14(.a(n21), .b(n15), .O(f));
  buf   g15(.a(c), .O(d));
endmodule


