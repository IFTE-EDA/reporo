// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:16 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7, v8, v9,
    v10.0 , v10.1 , v10.2 , v10.3 , v10.4 , v10.5 , v10.6 , v10.7   );
  input  v0, v1, v2, v3, v4, v5, v6, v7, v8, v9;
  output v10.0 , v10.1 , v10.2 , v10.3 , v10.4 , v10.5 , v10.6 ,
    v10.7 ;
  wire n19, n20, n21, n22, n23, n24, n25, n26, n28, n29, n30, n31, n32, n33,
    n34, n35, n36, n37, n38, n39, n40, n41, n42, n44, n45, n46, n47, n48,
    n49, n50, n51, n52, n53, n54, n56, n57, n58, n59, n60, n61, n62, n63,
    n64, n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77,
    n78, n79, n80, n81, n82, n83, n84, n85, n87, n88, n89, n90, n92, n93,
    n94, n95, n96, n97, n98, n99, n101, n102, n103, n104, n105, n107;
  inv1  g00(.a(v2), .O(n19));
  nor2  g01(.a(v6), .b(n19), .O(n20));
  inv1  g02(.a(n20), .O(n21));
  inv1  g03(.a(v0), .O(n22));
  nor2  g04(.a(v1), .b(n22), .O(n23));
  inv1  g05(.a(n23), .O(n24));
  nand2 g06(.a(n24), .b(n21), .O(n25));
  nand2 g07(.a(n23), .b(n20), .O(n26));
  nand2 g08(.a(n26), .b(n25), .O(v10.0 ));
  inv1  g09(.a(v3), .O(n28));
  nor2  g10(.a(v7), .b(n28), .O(n29));
  inv1  g11(.a(n29), .O(n30));
  inv1  g12(.a(v1), .O(n31));
  nor2  g13(.a(v6), .b(v0), .O(n32));
  nor2  g14(.a(n32), .b(n19), .O(n33));
  nand2 g15(.a(n33), .b(n31), .O(n34));
  nand2 g16(.a(n34), .b(n30), .O(n35));
  inv1  g17(.a(v6), .O(n36));
  nand2 g18(.a(v2), .b(v0), .O(n37));
  nand2 g19(.a(n37), .b(n36), .O(n38));
  nor2  g20(.a(n28), .b(v1), .O(n39));
  inv1  g21(.a(n39), .O(n40));
  nor2  g22(.a(n40), .b(v7), .O(n41));
  nand2 g23(.a(n41), .b(n38), .O(n42));
  nand2 g24(.a(n42), .b(n35), .O(v10.1 ));
  inv1  g25(.a(v7), .O(n44));
  nand2 g26(.a(n38), .b(v3), .O(n45));
  nand2 g27(.a(n45), .b(n44), .O(n46));
  inv1  g28(.a(v4), .O(n47));
  nor2  g29(.a(v8), .b(n47), .O(n48));
  nor2  g30(.a(n48), .b(v1), .O(n49));
  nand2 g31(.a(n49), .b(n46), .O(n50));
  inv1  g32(.a(n33), .O(n51));
  nand2 g33(.a(n51), .b(n44), .O(n52));
  nand2 g34(.a(n52), .b(n39), .O(n53));
  nand2 g35(.a(n53), .b(n48), .O(n54));
  nand2 g36(.a(n54), .b(n50), .O(v10.2 ));
  nand2 g37(.a(v8), .b(n28), .O(n56));
  nand2 g38(.a(n48), .b(v7), .O(n57));
  nand2 g39(.a(n57), .b(n56), .O(n58));
  nand2 g40(.a(n58), .b(n19), .O(n59));
  nor2  g41(.a(v8), .b(v7), .O(n60));
  nand2 g42(.a(v4), .b(v3), .O(n61));
  nor2  g43(.a(n61), .b(n36), .O(n62));
  nand2 g44(.a(n62), .b(n60), .O(n63));
  nand2 g45(.a(n63), .b(n59), .O(n64));
  nand2 g46(.a(n64), .b(n22), .O(n65));
  nand2 g47(.a(v4), .b(v0), .O(n66));
  nor2  g48(.a(n66), .b(v6), .O(n67));
  inv1  g49(.a(n60), .O(n68));
  nand2 g50(.a(v3), .b(v2), .O(n69));
  nor2  g51(.a(n69), .b(n68), .O(n70));
  nand2 g52(.a(n70), .b(n67), .O(n71));
  nand2 g53(.a(n71), .b(n65), .O(n72));
  nand2 g54(.a(v5), .b(n31), .O(n73));
  nor2  g55(.a(n73), .b(v9), .O(n74));
  nand2 g56(.a(n74), .b(n72), .O(n75));
  inv1  g57(.a(v9), .O(n76));
  nand2 g58(.a(n76), .b(v5), .O(n77));
  nor2  g59(.a(n20), .b(n31), .O(n78));
  nor2  g60(.a(n48), .b(n29), .O(n79));
  nand2 g61(.a(n79), .b(n78), .O(n80));
  nor2  g62(.a(v2), .b(v0), .O(n81));
  nor2  g63(.a(v4), .b(v3), .O(n82));
  nand2 g64(.a(n82), .b(n81), .O(n83));
  nand2 g65(.a(n83), .b(n80), .O(n84));
  nand2 g66(.a(n84), .b(n77), .O(n85));
  nand2 g67(.a(n85), .b(n75), .O(v10.3 ));
  inv1  g68(.a(v8), .O(n87));
  nand2 g69(.a(n46), .b(v4), .O(n88));
  nand2 g70(.a(n88), .b(n87), .O(n89));
  nand2 g71(.a(n89), .b(v5), .O(n90));
  nand2 g72(.a(n90), .b(n76), .O(v10.4 ));
  inv1  g73(.a(n77), .O(n92));
  nor2  g74(.a(n92), .b(v1), .O(n93));
  nand2 g75(.a(n93), .b(n89), .O(n94));
  nand2 g76(.a(n52), .b(v3), .O(n95));
  nand2 g77(.a(n95), .b(n87), .O(n96));
  nor2  g78(.a(n47), .b(v1), .O(n97));
  nand2 g79(.a(n97), .b(n96), .O(n98));
  nand2 g80(.a(n98), .b(n92), .O(n99));
  nand2 g81(.a(n99), .b(n94), .O(v10.5 ));
  nand2 g82(.a(n44), .b(n36), .O(n101));
  nand2 g83(.a(n101), .b(v3), .O(n102));
  nand2 g84(.a(n102), .b(n87), .O(n103));
  nand2 g85(.a(n103), .b(v4), .O(n104));
  nand2 g86(.a(n104), .b(n76), .O(n105));
  nand2 g87(.a(n105), .b(v5), .O(v10.6 ));
  nand2 g88(.a(v5), .b(v4), .O(n107));
  nor2  g89(.a(n107), .b(n69), .O(v10.7 ));
endmodule


