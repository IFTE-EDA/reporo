// Benchmark "mux" written by ABC on Tue Nov  5 15:01:24 2019

module mux ( 
    a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u,
    v  );
  input  a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u;
  output v;
  wire n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36,
    n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50,
    n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64,
    n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78,
    n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92,
    n93, n94, n95, n96, n97, n98, n99, n100, n101, n102, n103, n104, n105,
    n106, n107, n108, n109, n110, n111, n112, n113, n114, n115, n116, n117,
    n118, n119, n120, n121, n122, n123, n124, n125, n126, n127, n128, n129,
    n130, n131, n132, n133, n134, n135, n136, n137, n138, n139, n140, n141,
    n142, n143, n144, n145, n146, n147, n148, n149, n150, n151, n152, n153,
    n154, n155, n156, n157, n158;
  inv1  g000(.a(u), .O(n23));
  inv1  g001(.a(q), .O(n24));
  inv1  g002(.a(r), .O(n25));
  nor2  g003(.a(d), .b(c), .O(n26));
  nor2  g004(.a(n26), .b(s), .O(n27));
  nor2  g005(.a(n27), .b(b), .O(n28));
  inv1  g006(.a(s), .O(n29));
  nand2 g007(.a(n29), .b(c), .O(n30));
  nand2 g008(.a(n30), .b(t), .O(n31));
  inv1  g009(.a(n31), .O(n32));
  nor2  g010(.a(n32), .b(n28), .O(n33));
  nor2  g011(.a(n33), .b(a), .O(n34));
  inv1  g012(.a(t), .O(n35));
  inv1  g013(.a(b), .O(n36));
  nand2 g014(.a(n29), .b(d), .O(n37));
  nand2 g015(.a(n37), .b(n36), .O(n38));
  nor2  g016(.a(s), .b(d), .O(n39));
  inv1  g017(.a(n39), .O(n40));
  nand2 g018(.a(n40), .b(n38), .O(n41));
  nand2 g019(.a(n41), .b(n35), .O(n42));
  inv1  g020(.a(d), .O(n43));
  nor2  g021(.a(t), .b(n43), .O(n44));
  nor2  g022(.a(s), .b(c), .O(n45));
  inv1  g023(.a(n45), .O(n46));
  nor2  g024(.a(n46), .b(n44), .O(n47));
  inv1  g025(.a(n47), .O(n48));
  nand2 g026(.a(n48), .b(n42), .O(n49));
  nor2  g027(.a(n49), .b(n34), .O(n50));
  nor2  g028(.a(n50), .b(n25), .O(n51));
  nor2  g029(.a(n51), .b(n24), .O(n52));
  nor2  g030(.a(p), .b(o), .O(n53));
  nor2  g031(.a(n53), .b(s), .O(n54));
  nor2  g032(.a(n54), .b(n), .O(n55));
  nand2 g033(.a(n29), .b(o), .O(n56));
  nand2 g034(.a(n56), .b(t), .O(n57));
  inv1  g035(.a(n57), .O(n58));
  nor2  g036(.a(n58), .b(n55), .O(n59));
  nor2  g037(.a(n59), .b(m), .O(n60));
  inv1  g038(.a(n), .O(n61));
  nand2 g039(.a(n29), .b(p), .O(n62));
  nand2 g040(.a(n62), .b(n61), .O(n63));
  nor2  g041(.a(s), .b(p), .O(n64));
  inv1  g042(.a(n64), .O(n65));
  nand2 g043(.a(n65), .b(n63), .O(n66));
  nand2 g044(.a(n66), .b(n35), .O(n67));
  nand2 g045(.a(n35), .b(p), .O(n68));
  nor2  g046(.a(s), .b(o), .O(n69));
  nand2 g047(.a(n69), .b(n68), .O(n70));
  nand2 g048(.a(n70), .b(n67), .O(n71));
  nor2  g049(.a(n71), .b(n60), .O(n72));
  inv1  g050(.a(n72), .O(n73));
  inv1  g051(.a(a), .O(n74));
  inv1  g052(.a(c), .O(n75));
  nand2 g053(.a(n43), .b(n75), .O(n76));
  nand2 g054(.a(n76), .b(n29), .O(n77));
  nand2 g055(.a(n77), .b(n36), .O(n78));
  nand2 g056(.a(n31), .b(n78), .O(n79));
  nand2 g057(.a(n79), .b(n74), .O(n80));
  nor2  g058(.a(s), .b(n43), .O(n81));
  nor2  g059(.a(n81), .b(b), .O(n82));
  nor2  g060(.a(n39), .b(n82), .O(n83));
  nor2  g061(.a(n83), .b(t), .O(n84));
  nor2  g062(.a(n47), .b(n84), .O(n85));
  nand2 g063(.a(n85), .b(n80), .O(n86));
  inv1  g064(.a(i), .O(n87));
  inv1  g065(.a(j), .O(n88));
  inv1  g066(.a(k), .O(n89));
  inv1  g067(.a(l), .O(n90));
  nand2 g068(.a(n90), .b(n89), .O(n91));
  nand2 g069(.a(n91), .b(n29), .O(n92));
  nand2 g070(.a(n92), .b(n88), .O(n93));
  nand2 g071(.a(n29), .b(k), .O(n94));
  nand2 g072(.a(n94), .b(t), .O(n95));
  nand2 g073(.a(n95), .b(n93), .O(n96));
  nand2 g074(.a(n96), .b(n87), .O(n97));
  nor2  g075(.a(s), .b(n90), .O(n98));
  nor2  g076(.a(n98), .b(j), .O(n99));
  nor2  g077(.a(s), .b(l), .O(n100));
  nor2  g078(.a(n100), .b(n99), .O(n101));
  nor2  g079(.a(n101), .b(t), .O(n102));
  nor2  g080(.a(t), .b(n90), .O(n103));
  nor2  g081(.a(s), .b(k), .O(n104));
  inv1  g082(.a(n104), .O(n105));
  nor2  g083(.a(n105), .b(n103), .O(n106));
  nor2  g084(.a(n106), .b(n102), .O(n107));
  nand2 g085(.a(n107), .b(n97), .O(n108));
  nor2  g086(.a(n108), .b(n86), .O(n109));
  nor2  g087(.a(n109), .b(n25), .O(n110));
  nor2  g088(.a(n110), .b(n73), .O(n111));
  nor2  g089(.a(n111), .b(n52), .O(n112));
  inv1  g090(.a(e), .O(n113));
  inv1  g091(.a(f), .O(n114));
  inv1  g092(.a(g), .O(n115));
  inv1  g093(.a(h), .O(n116));
  nand2 g094(.a(n116), .b(n115), .O(n117));
  nand2 g095(.a(n117), .b(n29), .O(n118));
  nand2 g096(.a(n118), .b(n114), .O(n119));
  nand2 g097(.a(n29), .b(g), .O(n120));
  nand2 g098(.a(n120), .b(t), .O(n121));
  nand2 g099(.a(n121), .b(n119), .O(n122));
  nand2 g100(.a(n122), .b(n113), .O(n123));
  nor2  g101(.a(s), .b(n116), .O(n124));
  nor2  g102(.a(n124), .b(f), .O(n125));
  nor2  g103(.a(s), .b(h), .O(n126));
  nor2  g104(.a(n126), .b(n125), .O(n127));
  nor2  g105(.a(n127), .b(t), .O(n128));
  nor2  g106(.a(t), .b(n116), .O(n129));
  nand2 g107(.a(n29), .b(n115), .O(n130));
  nor2  g108(.a(n130), .b(n129), .O(n131));
  nor2  g109(.a(n131), .b(n128), .O(n132));
  nand2 g110(.a(n132), .b(n123), .O(n133));
  nor2  g111(.a(n133), .b(n112), .O(n134));
  nand2 g112(.a(n108), .b(r), .O(n135));
  nand2 g113(.a(n135), .b(n72), .O(n136));
  nor2  g114(.a(l), .b(k), .O(n137));
  nor2  g115(.a(n137), .b(s), .O(n138));
  nor2  g116(.a(n138), .b(j), .O(n139));
  inv1  g117(.a(n95), .O(n140));
  nor2  g118(.a(n140), .b(n139), .O(n141));
  nor2  g119(.a(n141), .b(i), .O(n142));
  nand2 g120(.a(n29), .b(l), .O(n143));
  nand2 g121(.a(n143), .b(n88), .O(n144));
  inv1  g122(.a(n100), .O(n145));
  nand2 g123(.a(n145), .b(n144), .O(n146));
  nand2 g124(.a(n146), .b(n35), .O(n147));
  inv1  g125(.a(n106), .O(n148));
  nand2 g126(.a(n148), .b(n147), .O(n149));
  nor2  g127(.a(n149), .b(n142), .O(n150));
  nand2 g128(.a(n150), .b(r), .O(n151));
  nand2 g129(.a(n151), .b(n136), .O(n152));
  nand2 g130(.a(n152), .b(n24), .O(n153));
  nand2 g131(.a(n108), .b(n24), .O(n154));
  nor2  g132(.a(n86), .b(n25), .O(n155));
  nand2 g133(.a(n155), .b(n154), .O(n156));
  nand2 g134(.a(n156), .b(n153), .O(n157));
  nor2  g135(.a(n157), .b(n134), .O(n158));
  nor2  g136(.a(n158), .b(n23), .O(v));
endmodule


