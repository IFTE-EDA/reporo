// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:25 2019

module source_pla  ( 
    CPIPE2s<0> , CPIPE2s<1> , CPIPE2s<2> , CPIPE2s<3> , CPIPE2s<4> ,
    CPIPE2s<5> , CPIPE2s<7> ,
    writeRFaccess2, lastPCtobusD1, busDtobusB2, busDtobusA2, DSTtobusD2,
    nillonreturn, pLOADwrite, opc2load, DSTvalid, pbusDtoINA  );
  input  CPIPE2s<0> , CPIPE2s<1> , CPIPE2s<2> , CPIPE2s<3> ,
    CPIPE2s<4> , CPIPE2s<5> , CPIPE2s<7> ;
  output writeRFaccess2, lastPCtobusD1, busDtobusB2, busDtobusA2, DSTtobusD2,
    nillonreturn, pLOADwrite, opc2load, DSTvalid, pbusDtoINA;
  wire n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31,
    n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45,
    n47, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61,
    n62, n63, n64, n65, n66, n69, n70, n71, n73, n74, n76, n77, n78, n79,
    n80, n81, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93, n94,
    n95, n97, n98, n99, n100, n101, n102, n103, n104, n105;
  inv1  g00(.a(CPIPE2s<5> ), .O(n18));
  nand2 g01(.a(n18), .b(CPIPE2s<3> ), .O(n19));
  nand2 g02(.a(n19), .b(CPIPE2s<4> ), .O(n20));
  nand2 g03(.a(n20), .b(CPIPE2s<0> ), .O(n21));
  inv1  g04(.a(CPIPE2s<4> ), .O(n22));
  inv1  g05(.a(n19), .O(n23));
  nand2 g06(.a(n23), .b(n22), .O(n24));
  nand2 g07(.a(n24), .b(n21), .O(n25));
  nand2 g08(.a(n25), .b(CPIPE2s<1> ), .O(n26));
  nor2  g09(.a(CPIPE2s<4> ), .b(CPIPE2s<3> ), .O(n27));
  nand2 g10(.a(n18), .b(CPIPE2s<4> ), .O(n28));
  nand2 g11(.a(CPIPE2s<3> ), .b(CPIPE2s<0> ), .O(n29));
  nor2  g12(.a(n29), .b(n28), .O(n30));
  nor2  g13(.a(n30), .b(n27), .O(n31));
  nor2  g14(.a(n31), .b(CPIPE2s<2> ), .O(n32));
  inv1  g15(.a(CPIPE2s<3> ), .O(n33));
  nor2  g16(.a(n22), .b(n33), .O(n34));
  nor2  g17(.a(n34), .b(n18), .O(n35));
  nor2  g18(.a(n35), .b(n32), .O(n36));
  nand2 g19(.a(n36), .b(n26), .O(n37));
  nand2 g20(.a(n37), .b(CPIPE2s<7> ), .O(n38));
  inv1  g21(.a(CPIPE2s<7> ), .O(n39));
  inv1  g22(.a(CPIPE2s<0> ), .O(n40));
  nor2  g23(.a(CPIPE2s<1> ), .b(n40), .O(n41));
  inv1  g24(.a(n41), .O(n42));
  nand2 g25(.a(n27), .b(CPIPE2s<2> ), .O(n43));
  nor2  g26(.a(n43), .b(n42), .O(n44));
  nor2  g27(.a(n44), .b(n39), .O(n45));
  nor2  g28(.a(n45), .b(CPIPE2s<5> ), .O(lastPCtobusD1));
  inv1  g29(.a(lastPCtobusD1), .O(n47));
  nand2 g30(.a(n47), .b(n38), .O(writeRFaccess2));
  inv1  g31(.a(CPIPE2s<2> ), .O(n49));
  nor2  g32(.a(CPIPE2s<3> ), .b(n49), .O(n50));
  nand2 g33(.a(n50), .b(CPIPE2s<5> ), .O(n51));
  nand2 g34(.a(n51), .b(n19), .O(n52));
  nand2 g35(.a(n52), .b(CPIPE2s<1> ), .O(n53));
  inv1  g36(.a(CPIPE2s<1> ), .O(n54));
  nor2  g37(.a(n33), .b(CPIPE2s<2> ), .O(n55));
  nand2 g38(.a(n55), .b(n54), .O(n56));
  nand2 g39(.a(n56), .b(CPIPE2s<0> ), .O(n57));
  nand2 g40(.a(n57), .b(CPIPE2s<5> ), .O(n58));
  nand2 g41(.a(n58), .b(n53), .O(n59));
  nand2 g42(.a(n59), .b(CPIPE2s<7> ), .O(n60));
  nor2  g43(.a(CPIPE2s<5> ), .b(CPIPE2s<3> ), .O(n61));
  nor2  g44(.a(n42), .b(n49), .O(n62));
  nand2 g45(.a(n62), .b(n61), .O(n63));
  nand2 g46(.a(n63), .b(n60), .O(n64));
  nand2 g47(.a(n64), .b(n22), .O(n65));
  nand2 g48(.a(n39), .b(n18), .O(n66));
  nand2 g49(.a(n66), .b(n65), .O(busDtobusB2));
  nor2  g50(.a(n60), .b(CPIPE2s<4> ), .O(DSTtobusD2));
  nand2 g51(.a(CPIPE2s<3> ), .b(CPIPE2s<1> ), .O(n69));
  nor2  g52(.a(CPIPE2s<5> ), .b(CPIPE2s<4> ), .O(n70));
  nand2 g53(.a(n70), .b(CPIPE2s<7> ), .O(n71));
  nor2  g54(.a(n71), .b(n69), .O(nillonreturn));
  nand2 g55(.a(CPIPE2s<4> ), .b(n33), .O(n73));
  nand2 g56(.a(CPIPE2s<7> ), .b(CPIPE2s<5> ), .O(n74));
  nor2  g57(.a(n74), .b(n73), .O(pLOADwrite));
  nand2 g58(.a(CPIPE2s<5> ), .b(CPIPE2s<4> ), .O(n76));
  inv1  g59(.a(n76), .O(n77));
  nand2 g60(.a(n77), .b(CPIPE2s<7> ), .O(n78));
  nor2  g61(.a(CPIPE2s<1> ), .b(CPIPE2s<0> ), .O(n79));
  nor2  g62(.a(CPIPE2s<3> ), .b(CPIPE2s<2> ), .O(n80));
  nand2 g63(.a(n80), .b(n79), .O(n81));
  nor2  g64(.a(n81), .b(n78), .O(opc2load));
  inv1  g65(.a(n20), .O(n83));
  nor2  g66(.a(n83), .b(n54), .O(n84));
  inv1  g67(.a(n55), .O(n85));
  nor2  g68(.a(n85), .b(n28), .O(n86));
  nor2  g69(.a(n86), .b(n84), .O(n87));
  nor2  g70(.a(n87), .b(n40), .O(n88));
  nand2 g71(.a(n22), .b(n49), .O(n89));
  nand2 g72(.a(n89), .b(n76), .O(n90));
  nand2 g73(.a(n90), .b(n33), .O(n91));
  inv1  g74(.a(n61), .O(n92));
  nand2 g75(.a(n92), .b(n22), .O(n93));
  nand2 g76(.a(n93), .b(n91), .O(n94));
  nor2  g77(.a(n94), .b(n88), .O(n95));
  nor2  g78(.a(n95), .b(n39), .O(DSTvalid));
  nor2  g79(.a(n33), .b(CPIPE2s<1> ), .O(n97));
  nor2  g80(.a(n97), .b(CPIPE2s<5> ), .O(n98));
  nor2  g81(.a(n98), .b(n49), .O(n99));
  inv1  g82(.a(n79), .O(n100));
  nand2 g83(.a(n100), .b(CPIPE2s<5> ), .O(n101));
  nand2 g84(.a(n23), .b(n40), .O(n102));
  nand2 g85(.a(n102), .b(n101), .O(n103));
  nor2  g86(.a(n103), .b(n99), .O(n104));
  nand2 g87(.a(CPIPE2s<7> ), .b(CPIPE2s<4> ), .O(n105));
  nor2  g88(.a(n105), .b(n104), .O(pbusDtoINA));
  nand2 g89(.a(n66), .b(n65), .O(busDtobusA2));
endmodule


