// Benchmark "apex7" written by ABC on Tue Nov  5 15:01:16 2019

module apex7 ( 
    CAPSD, CAT0, CAT1, CAT2, CAT3, CAT4, CAT5, VACC, MMERR, IBT0, IBT1,
    IBT2, ICLR, LSD, ACCRPY, VERR_N, RATR, MARSSR, VLENESR, VSUMESR,
    PLUTO0, PLUTO1, PLUTO2, PLUTO3, PLUTO4, PLUTO5, ORWD_N, OWL_N, PY, END,
    FBI, WATCH, OVACC, KBG_N, DEL1, COMPPAR, VST0, VST1, STAR0, STAR1,
    STAR2, STAR3, BULL0, BULL1, BULL2, BULL3, BULL4, BULL5, BULL6,
    SDO, LSD_P, ACCRPY_P, VERR_F, RATR_P, MARSSR_P, VLENESR_P, VSUMESR_P,
    PLUTO0_P, PLUTO1_P, PLUTO2_P, PLUTO3_P, PLUTO4_P, PLUTO5_P, ORWD_F,
    OWL_F, PY_P, END_P, FBI_P, WATCH_P, OVACC_P, KBG_F, DEL1_P, COMPPAR_P,
    VST0_P, VST1_P, STAR0_P, STAR1_P, STAR2_P, STAR3_P, BULL0_P, BULL1_P,
    BULL2_P, BULL3_P, BULL4_P, BULL5_P, BULL6_P  );
  input  CAPSD, CAT0, CAT1, CAT2, CAT3, CAT4, CAT5, VACC, MMERR, IBT0,
    IBT1, IBT2, ICLR, LSD, ACCRPY, VERR_N, RATR, MARSSR, VLENESR, VSUMESR,
    PLUTO0, PLUTO1, PLUTO2, PLUTO3, PLUTO4, PLUTO5, ORWD_N, OWL_N, PY, END,
    FBI, WATCH, OVACC, KBG_N, DEL1, COMPPAR, VST0, VST1, STAR0, STAR1,
    STAR2, STAR3, BULL0, BULL1, BULL2, BULL3, BULL4, BULL5, BULL6;
  output SDO, LSD_P, ACCRPY_P, VERR_F, RATR_P, MARSSR_P, VLENESR_P, VSUMESR_P,
    PLUTO0_P, PLUTO1_P, PLUTO2_P, PLUTO3_P, PLUTO4_P, PLUTO5_P, ORWD_F,
    OWL_F, PY_P, END_P, FBI_P, WATCH_P, OVACC_P, KBG_F, DEL1_P, COMPPAR_P,
    VST0_P, VST1_P, STAR0_P, STAR1_P, STAR2_P, STAR3_P, BULL0_P, BULL1_P,
    BULL2_P, BULL3_P, BULL4_P, BULL5_P, BULL6_P;
  wire n87, n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98, n99, n100,
    n101, n102, n103, n104, n105, n106, n107, n108, n109, n110, n111, n112,
    n113, n114, n115, n116, n117, n118, n119, n120, n121, n122, n123, n124,
    n125, n126, n127, n128, n129, n130, n132, n133, n134, n135, n136, n137,
    n138, n139, n140, n141, n142, n143, n144, n145, n146, n147, n148, n150,
    n152, n153, n154, n155, n156, n157, n158, n159, n160, n161, n162, n163,
    n164, n165, n166, n167, n168, n169, n171, n172, n173, n174, n175, n176,
    n177, n178, n179, n180, n181, n183, n184, n185, n186, n188, n189, n191,
    n192, n194, n195, n196, n197, n198, n199, n200, n201, n202, n203, n205,
    n206, n208, n209, n210, n211, n213, n214, n215, n217, n218, n219, n221,
    n222, n223, n225, n226, n227, n229, n230, n231, n232, n235, n236, n237,
    n238, n239, n240, n242, n243, n245, n247, n248, n249, n250, n252, n254,
    n255, n256, n257, n258, n259, n261, n262, n264, n265, n267, n268, n269,
    n270, n271, n272, n274, n275, n276, n277, n278, n279, n280, n281, n282,
    n283, n284, n286, n287, n288, n289, n290, n291, n292, n294, n295, n296,
    n297, n298, n299, n300, n302, n303, n304, n305, n307, n308, n309, n310,
    n311, n312, n314, n315, n316, n318, n319, n320, n321, n322, n323, n324,
    n326, n327, n328, n329, n330, n331, n332, n334, n335, n336, n337, n339,
    n340, n341, n342, n343, n344, n345, n346, n347, n348, n349;
  inv1  g000(.a(STAR3), .O(n87));
  inv1  g001(.a(IBT1), .O(n88));
  nor2  g002(.a(IBT2), .b(n88), .O(n89));
  inv1  g003(.a(n89), .O(n90));
  inv1  g004(.a(IBT0), .O(n91));
  nor2  g005(.a(n91), .b(CAT1), .O(n92));
  nor2  g006(.a(IBT0), .b(CAT0), .O(n93));
  nor2  g007(.a(n93), .b(n92), .O(n94));
  nor2  g008(.a(n94), .b(n90), .O(n95));
  inv1  g009(.a(n95), .O(n96));
  inv1  g010(.a(CAT5), .O(n97));
  nand2 g011(.a(IBT1), .b(n97), .O(n98));
  inv1  g012(.a(CAT3), .O(n99));
  nand2 g013(.a(n88), .b(n99), .O(n100));
  nand2 g014(.a(n100), .b(n98), .O(n101));
  nand2 g015(.a(n101), .b(IBT0), .O(n102));
  inv1  g016(.a(CAT4), .O(n103));
  nand2 g017(.a(IBT1), .b(n103), .O(n104));
  inv1  g018(.a(CAT2), .O(n105));
  nand2 g019(.a(n88), .b(n105), .O(n106));
  nand2 g020(.a(n106), .b(n104), .O(n107));
  nand2 g021(.a(n107), .b(n91), .O(n108));
  nand2 g022(.a(n108), .b(n102), .O(n109));
  nand2 g023(.a(n109), .b(IBT2), .O(n110));
  nand2 g024(.a(n110), .b(n96), .O(n111));
  inv1  g025(.a(OWL_N), .O(n112));
  inv1  g026(.a(WATCH), .O(n113));
  nor2  g027(.a(n113), .b(n112), .O(n114));
  nand2 g028(.a(n114), .b(n111), .O(n115));
  inv1  g029(.a(n115), .O(n116));
  inv1  g030(.a(FBI), .O(n117));
  inv1  g031(.a(STAR2), .O(n118));
  inv1  g032(.a(STAR0), .O(n119));
  inv1  g033(.a(STAR1), .O(n120));
  nor2  g034(.a(n120), .b(n119), .O(n121));
  nand2 g035(.a(n121), .b(n118), .O(n122));
  nor2  g036(.a(n122), .b(n117), .O(n123));
  nand2 g037(.a(n123), .b(n116), .O(n124));
  nand2 g038(.a(OWL_N), .b(LSD), .O(n125));
  nand2 g039(.a(n125), .b(n124), .O(n126));
  nand2 g040(.a(n126), .b(n87), .O(n127));
  inv1  g041(.a(n123), .O(n128));
  inv1  g042(.a(n125), .O(n129));
  nand2 g043(.a(n129), .b(n128), .O(n130));
  nand2 g044(.a(n130), .b(n127), .O(LSD_P));
  nor2  g045(.a(n117), .b(n112), .O(n132));
  inv1  g046(.a(IBT2), .O(n133));
  nor2  g047(.a(n88), .b(CAT5), .O(n134));
  nor2  g048(.a(IBT1), .b(CAT3), .O(n135));
  nor2  g049(.a(n135), .b(n134), .O(n136));
  nor2  g050(.a(n136), .b(n91), .O(n137));
  nor2  g051(.a(n88), .b(CAT4), .O(n138));
  nor2  g052(.a(IBT1), .b(CAT2), .O(n139));
  nor2  g053(.a(n139), .b(n138), .O(n140));
  nor2  g054(.a(n140), .b(IBT0), .O(n141));
  nor2  g055(.a(n141), .b(n137), .O(n142));
  nor2  g056(.a(n142), .b(n133), .O(n143));
  nor2  g057(.a(n143), .b(n95), .O(n144));
  nor2  g058(.a(n144), .b(n113), .O(n145));
  nor2  g059(.a(n145), .b(n122), .O(n146));
  nand2 g060(.a(n146), .b(n132), .O(n147));
  nand2 g061(.a(OWL_N), .b(ACCRPY), .O(n148));
  nand2 g062(.a(n148), .b(n147), .O(ACCRPY_P));
  inv1  g063(.a(n122), .O(n150));
  nand2 g064(.a(n111), .b(WATCH), .O(ORWD_F));
  nand2 g065(.a(ORWD_F), .b(n150), .O(n152));
  nand2 g066(.a(n152), .b(n132), .O(n153));
  inv1  g067(.a(BULL4), .O(n154));
  nor2  g068(.a(BULL5), .b(n154), .O(n155));
  nand2 g069(.a(n155), .b(BULL6), .O(n156));
  inv1  g070(.a(BULL1), .O(n157));
  nor2  g071(.a(n157), .b(BULL0), .O(n158));
  nor2  g072(.a(BULL3), .b(BULL2), .O(n159));
  nand2 g073(.a(n159), .b(n158), .O(n160));
  nor2  g074(.a(n160), .b(n156), .O(n161));
  nand2 g075(.a(n161), .b(WATCH), .O(n162));
  nand2 g076(.a(n162), .b(n153), .O(n163));
  inv1  g077(.a(n161), .O(n164));
  nor2  g078(.a(n122), .b(n87), .O(n165));
  nor2  g079(.a(n165), .b(ORWD_F), .O(n166));
  nand2 g080(.a(n166), .b(n164), .O(n167));
  nand2 g081(.a(n167), .b(n163), .O(n168));
  nand2 g082(.a(n168), .b(VERR_N), .O(n169));
  nand2 g083(.a(n169), .b(OWL_N), .O(VERR_F));
  inv1  g084(.a(COMPPAR), .O(n171));
  nand2 g085(.a(END), .b(OWL_N), .O(n172));
  inv1  g086(.a(n172), .O(n173));
  nand2 g087(.a(n173), .b(n171), .O(n174));
  inv1  g088(.a(MMERR), .O(n175));
  inv1  g089(.a(VST0), .O(n176));
  nand2 g090(.a(n176), .b(n175), .O(n177));
  nor2  g091(.a(n177), .b(n172), .O(n178));
  inv1  g092(.a(RATR), .O(n179));
  nor2  g093(.a(n112), .b(n179), .O(n180));
  nor2  g094(.a(n180), .b(n178), .O(n181));
  nand2 g095(.a(n181), .b(n174), .O(RATR_P));
  nand2 g096(.a(OWL_N), .b(MARSSR), .O(n183));
  inv1  g097(.a(n114), .O(n184));
  nor2  g098(.a(n164), .b(n184), .O(n185));
  inv1  g099(.a(n185), .O(n186));
  nand2 g100(.a(n186), .b(n183), .O(MARSSR_P));
  inv1  g101(.a(KBG_N), .O(n188));
  nor2  g102(.a(n188), .b(VLENESR), .O(n189));
  nor2  g103(.a(n189), .b(n112), .O(VLENESR_P));
  nand2 g104(.a(OWL_N), .b(VSUMESR), .O(n191));
  nand2 g105(.a(n173), .b(VST1), .O(n192));
  nand2 g106(.a(n192), .b(n191), .O(VSUMESR_P));
  nand2 g107(.a(OWL_N), .b(PLUTO0), .O(n194));
  inv1  g108(.a(VST1), .O(n195));
  nand2 g109(.a(n195), .b(COMPPAR), .O(n196));
  nand2 g110(.a(n196), .b(n173), .O(n197));
  nor2  g111(.a(KBG_N), .b(n112), .O(n198));
  nor2  g112(.a(n198), .b(n178), .O(n199));
  nand2 g113(.a(n199), .b(n197), .O(n200));
  nor2  g114(.a(n200), .b(n185), .O(n201));
  nor2  g115(.a(n201), .b(n90), .O(n202));
  nand2 g116(.a(n202), .b(n91), .O(n203));
  nand2 g117(.a(n203), .b(n194), .O(PLUTO0_P));
  nand2 g118(.a(OWL_N), .b(PLUTO1), .O(n205));
  nand2 g119(.a(n202), .b(IBT0), .O(n206));
  nand2 g120(.a(n206), .b(n205), .O(PLUTO1_P));
  nand2 g121(.a(OWL_N), .b(PLUTO2), .O(n208));
  nor2  g122(.a(n201), .b(n133), .O(n209));
  nor2  g123(.a(IBT1), .b(IBT0), .O(n210));
  nand2 g124(.a(n210), .b(n209), .O(n211));
  nand2 g125(.a(n211), .b(n208), .O(PLUTO2_P));
  nand2 g126(.a(OWL_N), .b(PLUTO3), .O(n213));
  nor2  g127(.a(IBT1), .b(n91), .O(n214));
  nand2 g128(.a(n214), .b(n209), .O(n215));
  nand2 g129(.a(n215), .b(n213), .O(PLUTO3_P));
  nand2 g130(.a(OWL_N), .b(PLUTO4), .O(n217));
  nor2  g131(.a(n88), .b(IBT0), .O(n218));
  nand2 g132(.a(n218), .b(n209), .O(n219));
  nand2 g133(.a(n219), .b(n217), .O(PLUTO4_P));
  nand2 g134(.a(OWL_N), .b(PLUTO5), .O(n221));
  nor2  g135(.a(n88), .b(n91), .O(n222));
  nand2 g136(.a(n222), .b(n209), .O(n223));
  nand2 g137(.a(n223), .b(n221), .O(PLUTO5_P));
  inv1  g138(.a(n162), .O(n225));
  nor2  g139(.a(END), .b(ICLR), .O(n226));
  nand2 g140(.a(n226), .b(KBG_N), .O(n227));
  nor2  g141(.a(n227), .b(n225), .O(OWL_F));
  nor2  g142(.a(n117), .b(ICLR), .O(n229));
  nand2 g143(.a(n229), .b(DEL1), .O(n230));
  nor2  g144(.a(FBI), .b(ICLR), .O(n231));
  nand2 g145(.a(n231), .b(PY), .O(n232));
  nand2 g146(.a(n232), .b(n230), .O(PY_P));
  nand2 g147(.a(n172), .b(n147), .O(END_P));
  inv1  g148(.a(n132), .O(n235));
  nor2  g149(.a(n115), .b(ORWD_N), .O(n236));
  inv1  g150(.a(n236), .O(n237));
  nand2 g151(.a(n237), .b(n235), .O(n238));
  nand2 g152(.a(n238), .b(n116), .O(n239));
  nand2 g153(.a(n132), .b(n122), .O(n240));
  nand2 g154(.a(n240), .b(n239), .O(FBI_P));
  nor2  g155(.a(n112), .b(VACC), .O(n242));
  nand2 g156(.a(n242), .b(OVACC), .O(n243));
  nand2 g157(.a(n243), .b(n184), .O(WATCH_P));
  inv1  g158(.a(VACC), .O(n245));
  nor2  g159(.a(ICLR), .b(n245), .O(OVACC_P));
  nor2  g160(.a(n146), .b(n235), .O(n247));
  inv1  g161(.a(n166), .O(n248));
  nand2 g162(.a(n248), .b(n247), .O(n249));
  nand2 g163(.a(n249), .b(KBG_N), .O(n250));
  nand2 g164(.a(n250), .b(OWL_N), .O(KBG_F));
  inv1  g165(.a(CAPSD), .O(n252));
  nor2  g166(.a(ICLR), .b(n252), .O(DEL1_P));
  nand2 g167(.a(DEL1), .b(FBI), .O(n254));
  nor2  g168(.a(n171), .b(n112), .O(n255));
  nand2 g169(.a(n255), .b(n254), .O(n256));
  inv1  g170(.a(DEL1), .O(n257));
  nor2  g171(.a(COMPPAR), .b(n257), .O(n258));
  nand2 g172(.a(n258), .b(n132), .O(n259));
  nand2 g173(.a(n259), .b(n256), .O(COMPPAR_P));
  nand2 g174(.a(n229), .b(VST1), .O(n261));
  nand2 g175(.a(n231), .b(VST0), .O(n262));
  nand2 g176(.a(n262), .b(n261), .O(VST0_P));
  nand2 g177(.a(n231), .b(VST1), .O(n264));
  nand2 g178(.a(n229), .b(PY), .O(n265));
  nand2 g179(.a(n265), .b(n264), .O(VST1_P));
  nor2  g180(.a(ORWD_F), .b(ORWD_N), .O(n267));
  nor2  g181(.a(FBI), .b(n112), .O(n268));
  inv1  g182(.a(n268), .O(n269));
  nor2  g183(.a(n269), .b(n267), .O(n270));
  nand2 g184(.a(n270), .b(STAR0), .O(n271));
  nand2 g185(.a(n238), .b(n119), .O(n272));
  nand2 g186(.a(n272), .b(n271), .O(STAR0_P));
  nand2 g187(.a(n236), .b(n120), .O(n274));
  inv1  g188(.a(n121), .O(n275));
  nand2 g189(.a(n132), .b(n275), .O(n276));
  nand2 g190(.a(n276), .b(n274), .O(n277));
  nand2 g191(.a(n277), .b(STAR0), .O(n278));
  inv1  g192(.a(ORWD_N), .O(n279));
  nand2 g193(.a(n145), .b(n279), .O(n280));
  nand2 g194(.a(n268), .b(n280), .O(n281));
  nand2 g195(.a(n119), .b(OWL_N), .O(n282));
  nand2 g196(.a(n282), .b(n281), .O(n283));
  nand2 g197(.a(n283), .b(STAR1), .O(n284));
  nand2 g198(.a(n284), .b(n278), .O(STAR1_P));
  nor2  g199(.a(n121), .b(n112), .O(n286));
  inv1  g200(.a(n286), .O(n287));
  nand2 g201(.a(n287), .b(n281), .O(n288));
  nand2 g202(.a(n288), .b(STAR2), .O(n289));
  nor2  g203(.a(n237), .b(n122), .O(n290));
  nor2  g204(.a(n235), .b(n122), .O(n291));
  nor2  g205(.a(n291), .b(n290), .O(n292));
  nand2 g206(.a(n292), .b(n289), .O(STAR2_P));
  nor2  g207(.a(n286), .b(n270), .O(n294));
  nand2 g208(.a(n118), .b(OWL_N), .O(n295));
  nand2 g209(.a(n295), .b(n294), .O(n296));
  nand2 g210(.a(n296), .b(STAR3), .O(n297));
  nand2 g211(.a(n87), .b(STAR2), .O(n298));
  nor2  g212(.a(n298), .b(n275), .O(n299));
  nand2 g213(.a(n299), .b(n238), .O(n300));
  nand2 g214(.a(n300), .b(n297), .O(STAR3_P));
  inv1  g215(.a(BULL0), .O(n302));
  nand2 g216(.a(n114), .b(n302), .O(n303));
  nor2  g217(.a(WATCH), .b(n112), .O(n304));
  nand2 g218(.a(n304), .b(BULL0), .O(n305));
  nand2 g219(.a(n305), .b(n303), .O(BULL0_P));
  nand2 g220(.a(BULL0), .b(WATCH), .O(n307));
  nor2  g221(.a(n307), .b(n157), .O(n308));
  nor2  g222(.a(n308), .b(n112), .O(n309));
  nand2 g223(.a(n309), .b(BULL1), .O(n310));
  nor2  g224(.a(n184), .b(n302), .O(n311));
  nand2 g225(.a(n311), .b(n157), .O(n312));
  nand2 g226(.a(n312), .b(n310), .O(BULL1_P));
  nand2 g227(.a(n309), .b(BULL2), .O(n314));
  nor2  g228(.a(BULL2), .b(n157), .O(n315));
  nand2 g229(.a(n315), .b(n311), .O(n316));
  nand2 g230(.a(n316), .b(n314), .O(BULL2_P));
  inv1  g231(.a(BULL3), .O(n318));
  nand2 g232(.a(BULL2), .b(BULL1), .O(n319));
  nor2  g233(.a(n319), .b(n307), .O(n320));
  nor2  g234(.a(n320), .b(n318), .O(n321));
  inv1  g235(.a(n320), .O(n322));
  nor2  g236(.a(n322), .b(BULL3), .O(n323));
  nor2  g237(.a(n323), .b(n321), .O(n324));
  nor2  g238(.a(n324), .b(n112), .O(BULL3_P));
  nor2  g239(.a(n318), .b(n112), .O(n326));
  nand2 g240(.a(n326), .b(n320), .O(n327));
  nor2  g241(.a(n327), .b(BULL4), .O(n328));
  nand2 g242(.a(n320), .b(BULL3), .O(n329));
  nand2 g243(.a(n329), .b(OWL_N), .O(n330));
  nor2  g244(.a(n330), .b(n154), .O(n331));
  nor2  g245(.a(n331), .b(n328), .O(n332));
  inv1  g246(.a(n332), .O(BULL4_P));
  nand2 g247(.a(n332), .b(n330), .O(n334));
  nand2 g248(.a(n334), .b(BULL5), .O(n335));
  inv1  g249(.a(n327), .O(n336));
  nand2 g250(.a(n336), .b(n155), .O(n337));
  nand2 g251(.a(n337), .b(n335), .O(BULL5_P));
  nor2  g252(.a(n327), .b(BULL6), .O(n339));
  inv1  g253(.a(BULL5), .O(n340));
  nor2  g254(.a(n340), .b(n154), .O(n341));
  nand2 g255(.a(n341), .b(n339), .O(n342));
  inv1  g256(.a(n309), .O(n343));
  inv1  g257(.a(BULL2), .O(n344));
  nor2  g258(.a(n318), .b(n344), .O(n345));
  nand2 g259(.a(n345), .b(n341), .O(n346));
  nand2 g260(.a(n346), .b(OWL_N), .O(n347));
  nand2 g261(.a(n347), .b(n343), .O(n348));
  nand2 g262(.a(n348), .b(BULL6), .O(n349));
  nand2 g263(.a(n349), .b(n342), .O(BULL6_P));
  buf   g264(.a(VST0), .O(SDO));
endmodule


