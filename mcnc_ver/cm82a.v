// Benchmark "CM82" written by ABC on Tue Nov  5 15:01:19 2019

module CM82 ( 
    a, b, c, d, e,
    f, g, h  );
  input  a, b, c, d, e;
  output f, g, h;
  wire n9, n10, n11, n12, n13, n14, n15, n16, n17, n18, n19, n21, n22, n23,
    n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37,
    n38, n40, n41, n42;
  inv1  g00(.a(a), .O(n9));
  inv1  g01(.a(b), .O(n10));
  nand2 g02(.a(c), .b(n10), .O(n11));
  inv1  g03(.a(c), .O(n12));
  nand2 g04(.a(n12), .b(b), .O(n13));
  nand2 g05(.a(n13), .b(n11), .O(n14));
  nand2 g06(.a(n14), .b(n9), .O(n15));
  nor2  g07(.a(n12), .b(b), .O(n16));
  nor2  g08(.a(c), .b(n10), .O(n17));
  nor2  g09(.a(n17), .b(n16), .O(n18));
  nand2 g10(.a(n18), .b(a), .O(n19));
  nand2 g11(.a(n19), .b(n15), .O(f));
  nor2  g12(.a(c), .b(b), .O(n21));
  inv1  g13(.a(n21), .O(n22));
  nand2 g14(.a(c), .b(b), .O(n23));
  nand2 g15(.a(n23), .b(n9), .O(n24));
  nand2 g16(.a(n24), .b(n22), .O(n25));
  inv1  g17(.a(d), .O(n26));
  nand2 g18(.a(e), .b(n26), .O(n27));
  inv1  g19(.a(e), .O(n28));
  nand2 g20(.a(n28), .b(d), .O(n29));
  nand2 g21(.a(n29), .b(n27), .O(n30));
  nand2 g22(.a(n30), .b(n25), .O(n31));
  nor2  g23(.a(n12), .b(n10), .O(n32));
  nor2  g24(.a(n32), .b(a), .O(n33));
  nor2  g25(.a(n33), .b(n21), .O(n34));
  nor2  g26(.a(n28), .b(d), .O(n35));
  nor2  g27(.a(e), .b(n26), .O(n36));
  nor2  g28(.a(n36), .b(n35), .O(n37));
  nand2 g29(.a(n37), .b(n34), .O(n38));
  nand2 g30(.a(n38), .b(n31), .O(g));
  nand2 g31(.a(n28), .b(n26), .O(n40));
  nand2 g32(.a(n40), .b(n34), .O(n41));
  nand2 g33(.a(e), .b(d), .O(n42));
  nand2 g34(.a(n42), .b(n41), .O(h));
endmodule


