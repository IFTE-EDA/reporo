// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:27 2019

module source_pla  ( 
    i_0_, i_1_, i_2_, i_3_, i_4_,
    o_0_, o_1_, o_2_, o_3_, o_4_, o_5_, o_6_, o_7_  );
  input  i_0_, i_1_, i_2_, i_3_, i_4_;
  output o_0_, o_1_, o_2_, o_3_, o_4_, o_5_, o_6_, o_7_;
  wire n14, n15, n16, n17, n18, n19, n20, n22, n24, n25, n26, n27, n28, n29,
    n30, n31, n32, n33, n34, n36, n37, n38, n39, n40, n41, n42, n43, n44,
    n45, n46, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59,
    n60, n61, n62, n63, n64, n66, n67, n68, n69, n70, n71, n72, n73, n74,
    n75, n76, n78, n79, n81, n82;
  inv1  g00(.a(i_0_), .O(n14));
  inv1  g01(.a(i_2_), .O(n15));
  nor2  g02(.a(n15), .b(i_1_), .O(n16));
  inv1  g03(.a(n16), .O(n17));
  nand2 g04(.a(i_4_), .b(i_3_), .O(n18));
  nor2  g05(.a(n18), .b(n17), .O(n19));
  nor2  g06(.a(n19), .b(i_1_), .O(n20));
  nor2  g07(.a(n20), .b(n14), .O(o_0_));
  inv1  g08(.a(i_3_), .O(n22));
  nor2  g09(.a(i_4_), .b(n22), .O(o_7_));
  nand2 g10(.a(o_7_), .b(n16), .O(n24));
  inv1  g11(.a(n24), .O(n25));
  inv1  g12(.a(i_1_), .O(n26));
  nor2  g13(.a(i_3_), .b(n15), .O(n27));
  nand2 g14(.a(n27), .b(n26), .O(n28));
  nand2 g15(.a(i_2_), .b(i_1_), .O(n29));
  inv1  g16(.a(n29), .O(n30));
  nor2  g17(.a(i_2_), .b(i_1_), .O(n31));
  nor2  g18(.a(n31), .b(n30), .O(n32));
  nand2 g19(.a(n32), .b(n28), .O(n33));
  nor2  g20(.a(n33), .b(n25), .O(n34));
  nor2  g21(.a(n34), .b(n14), .O(o_1_));
  nand2 g22(.a(n15), .b(n14), .O(n36));
  nand2 g23(.a(n36), .b(i_3_), .O(n37));
  nand2 g24(.a(n27), .b(n14), .O(n38));
  nand2 g25(.a(n38), .b(n37), .O(n39));
  nand2 g26(.a(n39), .b(i_1_), .O(n40));
  inv1  g27(.a(n27), .O(n41));
  nand2 g28(.a(n26), .b(i_0_), .O(n42));
  nor2  g29(.a(n42), .b(n41), .O(n43));
  nand2 g30(.a(o_7_), .b(i_2_), .O(n44));
  nor2  g31(.a(n44), .b(n42), .O(n45));
  nor2  g32(.a(n45), .b(n43), .O(n46));
  nand2 g33(.a(n46), .b(n40), .O(o_2_));
  nand2 g34(.a(n22), .b(n15), .O(n48));
  inv1  g35(.a(i_4_), .O(n49));
  nor2  g36(.a(n49), .b(n26), .O(n50));
  nand2 g37(.a(n50), .b(n48), .O(n51));
  nand2 g38(.a(n51), .b(n24), .O(n52));
  nand2 g39(.a(n52), .b(i_0_), .O(n53));
  nor2  g40(.a(n22), .b(i_1_), .O(n54));
  nor2  g41(.a(i_3_), .b(n26), .O(n55));
  nor2  g42(.a(n55), .b(n54), .O(n56));
  nor2  g43(.a(n56), .b(n14), .O(n57));
  nor2  g44(.a(n26), .b(i_0_), .O(n58));
  nor2  g45(.a(n58), .b(n57), .O(n59));
  nor2  g46(.a(n59), .b(i_2_), .O(n60));
  inv1  g47(.a(n58), .O(n61));
  nand2 g48(.a(i_3_), .b(i_2_), .O(n62));
  nor2  g49(.a(n62), .b(n61), .O(n63));
  nor2  g50(.a(n63), .b(n60), .O(n64));
  nand2 g51(.a(n64), .b(n53), .O(o_3_));
  nand2 g52(.a(i_3_), .b(n14), .O(n66));
  nand2 g53(.a(n22), .b(i_0_), .O(n67));
  nand2 g54(.a(n67), .b(n66), .O(n68));
  nand2 g55(.a(n68), .b(n32), .O(n69));
  nor2  g56(.a(n29), .b(i_0_), .O(n70));
  inv1  g57(.a(n31), .O(n71));
  nor2  g58(.a(n71), .b(n14), .O(n72));
  nor2  g59(.a(n72), .b(n70), .O(n73));
  nand2 g60(.a(n73), .b(n69), .O(n74));
  nand2 g61(.a(n74), .b(i_4_), .O(n75));
  nand2 g62(.a(n32), .b(o_7_), .O(n76));
  nand2 g63(.a(n76), .b(n75), .O(o_4_));
  nand2 g64(.a(n32), .b(i_4_), .O(n78));
  nand2 g65(.a(n27), .b(n49), .O(n79));
  nand2 g66(.a(n79), .b(n78), .O(o_5_));
  nor2  g67(.a(n22), .b(i_2_), .O(n81));
  nor2  g68(.a(n81), .b(n27), .O(n82));
  nor2  g69(.a(n82), .b(n49), .O(o_6_));
endmodule


