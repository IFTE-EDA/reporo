// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:19 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10,
    v11.0 , v11.1 , v11.2 , v11.3 , v11.4   );
  input  v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10;
  output v11.0 , v11.1 , v11.2 , v11.3 , v11.4 ;
  wire n17, n18, n19, n20, n22, n24, n26, n27, n28, n30;
  inv1  g00(.a(v1), .O(n17));
  inv1  g01(.a(v3), .O(n18));
  inv1  g02(.a(v2), .O(n19));
  nand2 g03(.a(v4), .b(v0), .O(n20));
  nand2 g04(.a(n20), .b(n19), .O(v11.2 ));
  nand2 g05(.a(v11.2 ), .b(v5), .O(n22));
  nand2 g06(.a(n22), .b(n18), .O(v11.1 ));
  nand2 g07(.a(v11.1 ), .b(v6), .O(n24));
  nand2 g08(.a(n24), .b(n17), .O(v11.0 ));
  inv1  g09(.a(v10), .O(n26));
  inv1  g10(.a(v7), .O(n27));
  nand2 g11(.a(v11.0 ), .b(v8), .O(n28));
  nand2 g12(.a(n28), .b(n27), .O(v11.4 ));
  nand2 g13(.a(v11.4 ), .b(v9), .O(n30));
  nand2 g14(.a(n30), .b(n26), .O(v11.3 ));
endmodule


