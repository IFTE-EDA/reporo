// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:27 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7,
    v8.0 , v8.1 , v8.2 , v8.3 , v8.4 , v8.5 , v8.6 , v8.7 , v8.8 ,
    v8.9 , v8.10 , v8.11 , v8.12 , v8.13 , v8.14 , v8.15 , v8.16 ,
    v8.17 , v8.18 , v8.19 , v8.20 , v8.21 , v8.22 , v8.23 , v8.24 ,
    v8.25 , v8.26 , v8.27 , v8.28 , v8.29 , v8.30   );
  input  v0, v1, v2, v3, v4, v5, v6, v7;
  output v8.0 , v8.1 , v8.2 , v8.3 , v8.4 , v8.5 , v8.6 , v8.7 ,
    v8.8 , v8.9 , v8.10 , v8.11 , v8.12 , v8.13 , v8.14 , v8.15 ,
    v8.16 , v8.17 , v8.18 , v8.19 , v8.20 , v8.21 , v8.22 , v8.23 ,
    v8.24 , v8.25 , v8.26 , v8.27 , v8.28 , v8.29 , v8.30 ;
  wire n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n53, n54,
    n55, n56, n57, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n69,
    n70, n71, n72, n74, n75, n76, n77, n78, n79, n80, n82, n83, n84, n85,
    n86, n87, n88, n90, n91, n93, n95, n96, n97, n98, n100, n101, n103,
    n105, n107, n109, n110, n112, n113, n118, n119, n121, n122, n124, n127,
    n129, n130, n131, n134, n136, n137, n138, n139, n141, n143, n145;
  inv1  g000(.a(v5), .O(n40));
  nor2  g001(.a(v6), .b(n40), .O(n41));
  nor2  g002(.a(n41), .b(n40), .O(n42));
  inv1  g003(.a(v1), .O(n43));
  nor2  g004(.a(v2), .b(n43), .O(n44));
  nor2  g005(.a(v4), .b(v3), .O(n45));
  nand2 g006(.a(n45), .b(n44), .O(n46));
  nor2  g007(.a(n46), .b(n42), .O(n47));
  nand2 g008(.a(v2), .b(n43), .O(n48));
  nand2 g009(.a(v4), .b(v3), .O(n49));
  nor2  g010(.a(n49), .b(n48), .O(n50));
  nor2  g011(.a(n50), .b(n47), .O(n51));
  nor2  g012(.a(n51), .b(v0), .O(v8.0 ));
  nor2  g013(.a(v1), .b(v0), .O(n53));
  nand2 g014(.a(n53), .b(v2), .O(n54));
  inv1  g015(.a(v3), .O(n55));
  nor2  g016(.a(v4), .b(n55), .O(n56));
  nand2 g017(.a(n56), .b(v5), .O(n57));
  nor2  g018(.a(n57), .b(n54), .O(v8.1 ));
  inv1  g019(.a(v4), .O(n59));
  nand2 g020(.a(v5), .b(n59), .O(n60));
  nand2 g021(.a(n60), .b(n43), .O(n61));
  nand2 g022(.a(n61), .b(v3), .O(n62));
  nor2  g023(.a(v3), .b(n43), .O(n63));
  inv1  g024(.a(n63), .O(n64));
  nand2 g025(.a(n64), .b(n62), .O(n65));
  inv1  g026(.a(v2), .O(n66));
  nor2  g027(.a(n66), .b(v0), .O(n67));
  nand2 g028(.a(n67), .b(n65), .O(n68));
  inv1  g029(.a(v0), .O(n69));
  nor2  g030(.a(n43), .b(n69), .O(n70));
  nor2  g031(.a(v3), .b(v2), .O(n71));
  nand2 g032(.a(n71), .b(n70), .O(n72));
  nand2 g033(.a(n72), .b(n68), .O(v8.2 ));
  nor2  g034(.a(v7), .b(v3), .O(n74));
  nor2  g035(.a(v4), .b(v1), .O(n75));
  inv1  g036(.a(n75), .O(n76));
  nor2  g037(.a(n76), .b(n74), .O(n77));
  nor2  g038(.a(n77), .b(n63), .O(n78));
  nor2  g039(.a(v2), .b(n69), .O(n79));
  inv1  g040(.a(n79), .O(n80));
  nor2  g041(.a(n80), .b(n78), .O(v8.3 ));
  nand2 g042(.a(n66), .b(n69), .O(n82));
  nor2  g043(.a(n82), .b(n55), .O(n83));
  nor2  g044(.a(n66), .b(n69), .O(n84));
  inv1  g045(.a(n84), .O(n85));
  nand2 g046(.a(n71), .b(n69), .O(n86));
  nand2 g047(.a(n86), .b(n85), .O(n87));
  nor2  g048(.a(n87), .b(n83), .O(n88));
  nor2  g049(.a(n88), .b(v1), .O(v8.4 ));
  inv1  g050(.a(n53), .O(n90));
  nand2 g051(.a(v3), .b(v2), .O(n91));
  nor2  g052(.a(n91), .b(n90), .O(v8.5 ));
  nand2 g053(.a(n55), .b(v2), .O(n93));
  nor2  g054(.a(n93), .b(n90), .O(v8.6 ));
  inv1  g055(.a(n60), .O(n95));
  nor2  g056(.a(n95), .b(n55), .O(n96));
  nor2  g057(.a(v5), .b(v3), .O(n97));
  nor2  g058(.a(n97), .b(n96), .O(n98));
  nor2  g059(.a(n98), .b(n54), .O(v8.7 ));
  nor2  g060(.a(n66), .b(n43), .O(n100));
  inv1  g061(.a(n100), .O(n101));
  nor2  g062(.a(n101), .b(v0), .O(v8.8 ));
  inv1  g063(.a(n47), .O(n103));
  nor2  g064(.a(n103), .b(v0), .O(v8.9 ));
  inv1  g065(.a(n71), .O(n105));
  nor2  g066(.a(n43), .b(v0), .O(v8.27 ));
  inv1  g067(.a(v8.27 ), .O(n107));
  nor2  g068(.a(n107), .b(n105), .O(v8.10 ));
  nor2  g069(.a(n105), .b(v4), .O(n109));
  nand2 g070(.a(n109), .b(v8.27 ), .O(n110));
  nor2  g071(.a(n110), .b(n41), .O(v8.11 ));
  inv1  g072(.a(v8.10 ), .O(n112));
  nor2  g073(.a(n41), .b(v4), .O(n113));
  nor2  g074(.a(n113), .b(n112), .O(v8.12 ));
  nor2  g075(.a(n107), .b(v2), .O(v8.13 ));
  nand2 g076(.a(n53), .b(n66), .O(v8.15 ));
  inv1  g077(.a(v8.15 ), .O(v8.14 ));
  nor2  g078(.a(v3), .b(v1), .O(n118));
  nand2 g079(.a(n118), .b(v5), .O(n119));
  nor2  g080(.a(n119), .b(n85), .O(v8.16 ));
  inv1  g081(.a(n97), .O(n121));
  nand2 g082(.a(n84), .b(n43), .O(n122));
  nor2  g083(.a(n122), .b(n121), .O(v8.17 ));
  inv1  g084(.a(n45), .O(n124));
  nor2  g085(.a(v8.15 ), .b(n124), .O(v8.18 ));
  nor2  g086(.a(v8.15 ), .b(n55), .O(v8.19 ));
  inv1  g087(.a(n70), .O(n127));
  nor2  g088(.a(n127), .b(n66), .O(v8.20 ));
  nand2 g089(.a(n56), .b(n43), .O(n129));
  inv1  g090(.a(n129), .O(n130));
  nor2  g091(.a(n130), .b(n63), .O(n131));
  nor2  g092(.a(n131), .b(n80), .O(v8.21 ));
  nor2  g093(.a(n129), .b(n80), .O(v8.22 ));
  nand2 g094(.a(n79), .b(n43), .O(n134));
  nor2  g095(.a(n134), .b(n124), .O(v8.23 ));
  nor2  g096(.a(v2), .b(v1), .O(n136));
  inv1  g097(.a(n136), .O(n137));
  nor2  g098(.a(n137), .b(n60), .O(n138));
  inv1  g099(.a(n138), .O(n139));
  nor2  g100(.a(n139), .b(n69), .O(v8.24 ));
  nand2 g101(.a(n79), .b(v3), .O(n141));
  nor2  g102(.a(n141), .b(n75), .O(v8.25 ));
  nor2  g103(.a(n138), .b(n100), .O(n143));
  nor2  g104(.a(n143), .b(n69), .O(v8.26 ));
  nand2 g105(.a(v5), .b(n55), .O(n145));
  nor2  g106(.a(n145), .b(n54), .O(v8.28 ));
  nor2  g107(.a(n129), .b(n85), .O(v8.29 ));
  nor2  g108(.a(n122), .b(n124), .O(v8.30 ));
endmodule


