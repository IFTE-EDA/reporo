// Benchmark "CM85" written by ABC on Tue Nov  5 15:01:19 2019

module CM85 ( 
    a, b, c, d, e, f, g, h, i, j, k,
    l, m, n  );
  input  a, b, c, d, e, f, g, h, i, j, k;
  output l, m, n;
  wire n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27, n28,
    n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41, n42,
    n43, n44, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55, n56, n58,
    n59, n60, n61, n62, n63, n64, n65, n66, n67;
  inv1  g00(.a(b), .O(n15));
  nor2  g01(.a(e), .b(d), .O(n16));
  inv1  g02(.a(d), .O(n17));
  inv1  g03(.a(e), .O(n18));
  nor2  g04(.a(n18), .b(n17), .O(n19));
  nor2  g05(.a(n19), .b(n16), .O(n20));
  nor2  g06(.a(n20), .b(n15), .O(n21));
  inv1  g07(.a(g), .O(n22));
  nor2  g08(.a(n22), .b(f), .O(n23));
  inv1  g09(.a(f), .O(n24));
  nor2  g10(.a(g), .b(n24), .O(n25));
  nor2  g11(.a(n25), .b(n23), .O(n26));
  nand2 g12(.a(n26), .b(n21), .O(n27));
  nor2  g13(.a(i), .b(h), .O(n28));
  inv1  g14(.a(h), .O(n29));
  inv1  g15(.a(i), .O(n30));
  nor2  g16(.a(n30), .b(n29), .O(n31));
  nor2  g17(.a(n31), .b(n28), .O(n32));
  nor2  g18(.a(n32), .b(n27), .O(n33));
  inv1  g19(.a(k), .O(n34));
  nor2  g20(.a(n34), .b(j), .O(n35));
  nand2 g21(.a(n35), .b(n33), .O(n36));
  nand2 g22(.a(i), .b(n29), .O(n37));
  nor2  g23(.a(n37), .b(n27), .O(n38));
  nand2 g24(.a(n23), .b(n21), .O(n39));
  nand2 g25(.a(n17), .b(b), .O(n40));
  nor2  g26(.a(n40), .b(n18), .O(n41));
  nor2  g27(.a(n41), .b(a), .O(n42));
  nand2 g28(.a(n42), .b(n39), .O(n43));
  nor2  g29(.a(n43), .b(n38), .O(n44));
  nand2 g30(.a(n44), .b(n36), .O(l));
  inv1  g31(.a(n16), .O(n46));
  nand2 g32(.a(e), .b(d), .O(n47));
  nand2 g33(.a(n47), .b(n46), .O(n48));
  nand2 g34(.a(n48), .b(b), .O(n49));
  inv1  g35(.a(n26), .O(n50));
  nor2  g36(.a(n50), .b(n49), .O(n51));
  inv1  g37(.a(n32), .O(n52));
  nand2 g38(.a(n52), .b(n51), .O(n53));
  inv1  g39(.a(n35), .O(n54));
  nand2 g40(.a(n34), .b(j), .O(n55));
  nand2 g41(.a(n55), .b(n54), .O(n56));
  nor2  g42(.a(n56), .b(n53), .O(m));
  inv1  g43(.a(n55), .O(n58));
  nand2 g44(.a(n58), .b(n33), .O(n59));
  nand2 g45(.a(n30), .b(h), .O(n60));
  nor2  g46(.a(n60), .b(n27), .O(n61));
  nand2 g47(.a(n25), .b(n21), .O(n62));
  nand2 g48(.a(d), .b(b), .O(n63));
  nor2  g49(.a(n63), .b(e), .O(n64));
  nor2  g50(.a(n64), .b(c), .O(n65));
  nand2 g51(.a(n65), .b(n62), .O(n66));
  nor2  g52(.a(n66), .b(n61), .O(n67));
  nand2 g53(.a(n67), .b(n59), .O(n));
endmodule


