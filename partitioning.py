from sortedcontainers import SortedDict, SortedList

class FMPartitioning:
    """Partition the netlist in two partitions.

    Arguments are the gates that should be considered and the ratio of these
    gates that should be placed in the separate partition.
    Based on the Fiduccia-Mattheyses (FM) algorithm.
    Source: http://limsk.ece.gatech.edu/course/ece6133/project/FM.pdf
    """
    def __init__(self, netlist, replacement_ratio, gates):
        self.netlist = netlist
        self.gates = set(gates)
        # initial partitioning (array of gate instance names)
        self.partition = set(self.netlist.get_some_instances(replacement_ratio, self.gates))
        # initial cut cost
        self.current_cost = self.calculate_cut_cost()

        self.best_cost = self.current_cost
        self.best_partition = self.partition.copy()

        #print("Partition:", self.partition)
        #print("Cut Cost:", self.calculate_cut_cost())

        # for each net, store the number of gates inside and outside the
        # partition
        self.net_to_in_partition_cnt = {}
        self.net_to_out_partition_cnt = {}
        # gain of all gates in the partition
        self.in_partition_gate_to_gain = {}
        # gain of all gates outside the partition
        self.out_partition_gate_to_gain = {}
        # sorted maps from gain to a list of gates that have this gain
        self.sorted_in_partition_gain_to_gates = None
        self.sorted_out_partition_gain_to_gates = None
        self.fixed_gates = set()

    def calculate_cut_cost(self):
        cost = 0
        nets = []
        for gate in self.partition:
            nets += self.netlist.get_connected_nets(gate)
        # remove duplicates
        nets = set(nets)
        for net in nets:
            # if a net connects to an input or output, count it as cut
            if net in self.netlist.inputs or net in self.netlist.outputs:
                cost += 1
            else:
                for gate in self.netlist.get_connected_gates(net):
                    if gate not in self.partition:
                        cost += 1
                        break
        return cost

    def calculate_push_pull(self):
        """For each net, calculate the number of gates in both partitions."""
        for net in self.netlist.get_nets():
            in_partition_cnt = 0
            out_partition_cnt = 0
            # nets that are top-level inputs or outputs are always
            # considered outside the partition (because they are
            # connected to a pad cell)
            if net in self.netlist.inputs or net in self.netlist.outputs:
                out_partition_cnt += 1
            # iterate over all other gates that are connected to that
            # net
            for connected_gate in self.netlist.get_connected_gates(net):
                if connected_gate in self.partition:
                    in_partition_cnt += 1
                else:
                    out_partition_cnt += 1
            self.net_to_in_partition_cnt[net] = in_partition_cnt
            self.net_to_out_partition_cnt[net] = out_partition_cnt

    def calculate_gains(self):
        """Initial calculation of gate gains.

        Calculation is based on the push/pull of connected nets.
        """
        self.calculate_push_pull()
        self.clear_gains()
        for gate in self.netlist.get_gates():
            gain = 0
            if self.netlist.get_cell(gate) in self.gates:
                for connected_net in self.netlist.get_connected_nets(gate):
                    if gate in self.partition:
                        if self.net_to_in_partition_cnt[connected_net] == 1:
                            gain += 1
                        if self.net_to_out_partition_cnt[connected_net] == 0:
                            gain -= 1
                    else:
                        if self.net_to_out_partition_cnt[connected_net] == 1:
                            gain += 1
                        if self.net_to_in_partition_cnt[connected_net] == 0:
                            gain -= 1
                if gate in self.partition:
                    self.in_partition_gate_to_gain[gate] = gain
                    self.sorted_in_partition_gain_to_gates.setdefault(gain, SortedList()).add(gate)
                else:
                    self.out_partition_gate_to_gain[gate] = gain
                    self.sorted_out_partition_gain_to_gates.setdefault(gain, SortedList()).add(gate)

    def update_gain(self, gate, gate_to_gain, sorted_gain_to_gates, delta_gain):
        """Update the gain of a gate."""
        old_gain = gate_to_gain[gate]
        sorted_gain_to_gates[old_gain].remove(gate)
        if not sorted_gain_to_gates[old_gain]:
            del sorted_gain_to_gates[old_gain]
        gate_to_gain[gate] += delta_gain
        sorted_gain_to_gates.setdefault(old_gain+delta_gain, SortedList()).add(gate)

    def clear_gains(self):
        self.in_partition_gate_to_gain = {}
        self.out_partition_gate_to_gain = {}
        self.sorted_in_partition_gain_to_gates = SortedDict()
        self.sorted_out_partition_gain_to_gates = SortedDict()

    def move(self, gate):
        connected_nets = self.netlist.get_connected_nets(gate)
        # define F (FROM) and T (TO)
        if gate in self.partition:
            F = self.net_to_in_partition_cnt
            F_gain = self.in_partition_gate_to_gain
            F_sorted_gain = self.sorted_in_partition_gain_to_gates
            T = self.net_to_out_partition_cnt
            T_gain = self.out_partition_gate_to_gain
            T_sorted_gain = self.sorted_out_partition_gain_to_gates
        else:
            F = self.net_to_out_partition_cnt
            F_gain = self.out_partition_gate_to_gain
            F_sorted_gain = self.sorted_out_partition_gain_to_gates
            T = self.net_to_in_partition_cnt
            T_gain = self.in_partition_gate_to_gain
            T_sorted_gain = self.sorted_in_partition_gain_to_gates

        # update gains of gates that are connected via critical nets
        for connected_net in connected_nets:
            connected_gates = self.netlist.get_connected_gates(connected_net)
            # update gains (depending on T(net) from before the move)
            # T(net) == 0?
            if T[connected_net] == 0:
                for connected_gate in connected_gates:
                    if connected_gate in F_gain:
                        self.update_gain(connected_gate, F_gain, F_sorted_gain, 1)
            # T(net) == 1?
            elif T[connected_net] == 1:
                for connected_gate in connected_gates:
                    if connected_gate in T_gain:
                        self.update_gain(connected_gate, T_gain, T_sorted_gain, -1)
            # update T(net), F(net)
            F[connected_net] -= 1
            T[connected_net] += 1
            # update gains (depending on F(net) from after the move)
            # F(net) == 0?
            if F[connected_net] == 0:
                for connected_gate in connected_gates:
                    if connected_gate in T_gain:
                        self.update_gain(connected_gate, T_gain, T_sorted_gain, -1)
            # F(net) == 1?
            elif F[connected_net] == 1:
                for connected_gate in connected_gates:
                    if connected_gate in F_gain:
                        self.update_gain(connected_gate, F_gain, F_sorted_gain, 1)

        # remove gain from data structures
        gain = F_gain[gate]
        F_sorted_gain[gain].remove(gate)
        # remove empty list
        if not F_sorted_gain[gain]:
            del F_sorted_gain[gain]
        del F_gain[gate]

        if gate in self.partition:
            self.partition.remove(gate)
        else:
            self.partition.add(gate)
        # mark the gate as fixed
        self.fixed_gates.add(gate)

    def optimize(self):
        improvement = True
        while improvement:
            self.partition = self.best_partition.copy()
            self.current_cost = self.best_cost
            print("NEW PASS; current cost: %d" % self.current_cost)
            improvement = False
            # fix all gates that are not of the correct type
            self.fixed_gates = set([gate for gate in self.netlist.get_gates() if self.netlist.get_cell(gate) not in self.gates])
            self.calculate_gains()

            while self.in_partition_gate_to_gain and self.out_partition_gate_to_gain:
                # 1st: move one cell out of the partition
                (gain, best_gates) = self.sorted_in_partition_gain_to_gates.peekitem()
                self.move(best_gates[0])
                self.current_cost -= gain
                # 2nd: move one cell into the partition
                (gain, best_gates) = self.sorted_out_partition_gain_to_gates.peekitem()
                self.move(best_gates[0])
                self.current_cost -= gain
                if self.current_cost < self.best_cost:
                    improvement = True
                    self.best_partition = self.partition.copy()
                    self.best_cost = self.current_cost
        self.partition = self.best_partition.copy()
        self.current_cost = self.best_cost
        assert(self.best_cost == self.calculate_cut_cost())
