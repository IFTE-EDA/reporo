// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:25 2019

module source_pla  ( 
    tagcompare, tCPIPE1s<0> , tCPIPE1s<1> , tCPIPE1s<2> , tCPIPE1s<3> ,
    tCPIPE1s<4> , tCPIPE1s<5> , tCPIPE1s<7> , tbusA<31> , tbusB<31> ,
    tbusB<30> , tbusB<29> , tbusB<28> , tCPIPE1s<6> , tCPIPE1s<8> ,
    GStrap, trapinstr, TAGtrap, pov_unflow, skipCONDenable  );
  input  tagcompare, tCPIPE1s<0> , tCPIPE1s<1> , tCPIPE1s<2> ,
    tCPIPE1s<3> , tCPIPE1s<4> , tCPIPE1s<5> , tCPIPE1s<7> ,
    tbusA<31> , tbusB<31> , tbusB<30> , tbusB<29> , tbusB<28> ,
    tCPIPE1s<6> , tCPIPE1s<8> ;
  output GStrap, trapinstr, TAGtrap, pov_unflow, skipCONDenable;
  wire n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34,
    n35, n36, n37, n38, n39, n40, n41, n42, n44, n45, n46, n47, n48, n49,
    n50, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64,
    n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78,
    n79, n80, n81, n82, n83, n84, n86, n87, n88, n89, n90, n92, n93;
  inv1  g00(.a(tagcompare), .O(n21));
  inv1  g01(.a(tbusB<31> ), .O(n22));
  inv1  g02(.a(tbusB<30> ), .O(n23));
  nand2 g03(.a(n23), .b(n22), .O(n24));
  inv1  g04(.a(tbusB<29> ), .O(n25));
  inv1  g05(.a(tbusB<28> ), .O(n26));
  nand2 g06(.a(n26), .b(n25), .O(n27));
  nor2  g07(.a(n27), .b(n24), .O(n28));
  nor2  g08(.a(n28), .b(n21), .O(n29));
  nor2  g09(.a(tCPIPE1s<1> ), .b(tCPIPE1s<0> ), .O(n30));
  inv1  g10(.a(tCPIPE1s<4> ), .O(n31));
  nor2  g11(.a(n31), .b(tCPIPE1s<2> ), .O(n32));
  nand2 g12(.a(n32), .b(n30), .O(n33));
  nor2  g13(.a(n33), .b(n29), .O(n34));
  inv1  g14(.a(tbusA<31> ), .O(n35));
  nor2  g15(.a(n35), .b(tCPIPE1s<4> ), .O(n36));
  nor2  g16(.a(n36), .b(n34), .O(n37));
  inv1  g17(.a(tCPIPE1s<3> ), .O(n38));
  nor2  g18(.a(tCPIPE1s<5> ), .b(n38), .O(n39));
  nand2 g19(.a(tCPIPE1s<6> ), .b(tCPIPE1s<7> ), .O(n40));
  inv1  g20(.a(n40), .O(n41));
  nand2 g21(.a(n41), .b(n39), .O(n42));
  nor2  g22(.a(n42), .b(n37), .O(GStrap));
  inv1  g23(.a(n30), .O(n44));
  nor2  g24(.a(n44), .b(tCPIPE1s<2> ), .O(n45));
  nand2 g25(.a(tCPIPE1s<4> ), .b(n38), .O(n46));
  inv1  g26(.a(n46), .O(n47));
  inv1  g27(.a(tCPIPE1s<7> ), .O(n48));
  nor2  g28(.a(n48), .b(tCPIPE1s<5> ), .O(n49));
  nand2 g29(.a(n49), .b(n47), .O(n50));
  nor2  g30(.a(n50), .b(n45), .O(trapinstr));
  inv1  g31(.a(tCPIPE1s<8> ), .O(n52));
  nand2 g32(.a(n52), .b(n22), .O(n53));
  nand2 g33(.a(n53), .b(n35), .O(n54));
  inv1  g34(.a(n54), .O(n55));
  inv1  g35(.a(tCPIPE1s<1> ), .O(n56));
  inv1  g36(.a(tCPIPE1s<2> ), .O(n57));
  nand2 g37(.a(n57), .b(n56), .O(n58));
  nor2  g38(.a(n58), .b(n38), .O(n59));
  inv1  g39(.a(tCPIPE1s<0> ), .O(n60));
  nand2 g40(.a(tCPIPE1s<3> ), .b(tCPIPE1s<2> ), .O(n61));
  nand2 g41(.a(n61), .b(n60), .O(n62));
  nor2  g42(.a(n57), .b(n56), .O(n63));
  nand2 g43(.a(n63), .b(n38), .O(n64));
  nand2 g44(.a(n64), .b(n62), .O(n65));
  nor2  g45(.a(n65), .b(n59), .O(n66));
  nand2 g46(.a(tCPIPE1s<5> ), .b(n31), .O(n67));
  nor2  g47(.a(n67), .b(n66), .O(n68));
  nor2  g48(.a(n46), .b(tCPIPE1s<5> ), .O(n69));
  nor2  g49(.a(n69), .b(n68), .O(n70));
  nor2  g50(.a(n70), .b(n55), .O(n71));
  nor2  g51(.a(n54), .b(n38), .O(n72));
  nor2  g52(.a(n53), .b(n35), .O(n73));
  nor2  g53(.a(n73), .b(n72), .O(n74));
  nor2  g54(.a(n74), .b(n57), .O(n75));
  nand2 g55(.a(n57), .b(n60), .O(n76));
  nand2 g56(.a(n35), .b(tCPIPE1s<3> ), .O(n77));
  nor2  g57(.a(n77), .b(n76), .O(n78));
  nor2  g58(.a(n78), .b(n75), .O(n79));
  inv1  g59(.a(tCPIPE1s<5> ), .O(n80));
  nor2  g60(.a(n31), .b(tCPIPE1s<1> ), .O(n81));
  nand2 g61(.a(n81), .b(n80), .O(n82));
  nor2  g62(.a(n82), .b(n79), .O(n83));
  nor2  g63(.a(n83), .b(n71), .O(n84));
  nor2  g64(.a(n84), .b(n40), .O(TAGtrap));
  inv1  g65(.a(n67), .O(n86));
  nand2 g66(.a(n86), .b(n41), .O(n87));
  nand2 g67(.a(tCPIPE1s<1> ), .b(tCPIPE1s<0> ), .O(n88));
  nor2  g68(.a(n38), .b(tCPIPE1s<2> ), .O(n89));
  nand2 g69(.a(n89), .b(n88), .O(n90));
  nor2  g70(.a(n90), .b(n87), .O(pov_unflow));
  nor2  g71(.a(tCPIPE1s<5> ), .b(tCPIPE1s<3> ), .O(n92));
  nand2 g72(.a(n92), .b(tCPIPE1s<7> ), .O(n93));
  nor2  g73(.a(n93), .b(n33), .O(skipCONDenable));
endmodule


