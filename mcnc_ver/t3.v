// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:28 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11,
    v12.0 , v12.1 , v12.2 , v12.3 , v12.4 , v12.5 , v12.6 , v12.7   );
  input  v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11;
  output v12.0 , v12.1 , v12.2 , v12.3 , v12.4 , v12.5 , v12.6 ,
    v12.7 ;
  wire n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34,
    n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n47, n48, n49,
    n50, n51, n52, n53, n54, n55, n57, n58, n59, n60, n61, n62, n63, n64,
    n65, n66, n67, n68, n69, n70, n71, n72, n73, n74, n75, n76, n78, n79,
    n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93,
    n94, n95, n96, n97, n99, n100, n101, n102, n103, n105, n106, n107,
    n108, n110, n111, n112, n114, n115;
  inv1  g00(.a(v4), .O(n21));
  nor2  g01(.a(v9), .b(v5), .O(n22));
  nor2  g02(.a(n22), .b(v11), .O(n23));
  nor2  g03(.a(n23), .b(v6), .O(n24));
  inv1  g04(.a(v7), .O(n25));
  nor2  g05(.a(n23), .b(n25), .O(n26));
  nand2 g06(.a(n25), .b(v5), .O(n27));
  nand2 g07(.a(v11), .b(v9), .O(n28));
  nor2  g08(.a(n28), .b(n27), .O(n29));
  nor2  g09(.a(n29), .b(n26), .O(n30));
  inv1  g10(.a(v8), .O(n31));
  nand2 g11(.a(n31), .b(v6), .O(n32));
  nor2  g12(.a(n32), .b(n30), .O(n33));
  nor2  g13(.a(n33), .b(n24), .O(n34));
  nand2 g14(.a(v10), .b(v4), .O(n35));
  nor2  g15(.a(n35), .b(n34), .O(n36));
  nor2  g16(.a(n36), .b(n21), .O(n37));
  inv1  g17(.a(v3), .O(n38));
  nor2  g18(.a(v1), .b(v0), .O(n39));
  nand2 g19(.a(n39), .b(n38), .O(n40));
  nor2  g20(.a(n40), .b(n37), .O(n41));
  nand2 g21(.a(v4), .b(v0), .O(n42));
  nand2 g22(.a(n31), .b(n25), .O(n43));
  nor2  g23(.a(n43), .b(n42), .O(n44));
  nor2  g24(.a(n44), .b(n41), .O(n45));
  nor2  g25(.a(n45), .b(v2), .O(v12.0 ));
  nor2  g26(.a(v10), .b(v3), .O(n47));
  nand2 g27(.a(n47), .b(n39), .O(n48));
  nor2  g28(.a(n48), .b(n34), .O(n49));
  inv1  g29(.a(v0), .O(n50));
  nand2 g30(.a(v8), .b(v7), .O(n51));
  nor2  g31(.a(n51), .b(n50), .O(n52));
  nor2  g32(.a(n52), .b(n49), .O(n53));
  nor2  g33(.a(n21), .b(v2), .O(n54));
  inv1  g34(.a(n54), .O(n55));
  nor2  g35(.a(n55), .b(n53), .O(v12.1 ));
  inv1  g36(.a(v9), .O(n57));
  nor2  g37(.a(n57), .b(v5), .O(n58));
  inv1  g38(.a(v5), .O(n59));
  nor2  g39(.a(v9), .b(n59), .O(n60));
  nor2  g40(.a(n60), .b(n58), .O(n61));
  nand2 g41(.a(v8), .b(v6), .O(n62));
  inv1  g42(.a(n62), .O(n63));
  nor2  g43(.a(n63), .b(v11), .O(n64));
  nand2 g44(.a(v11), .b(n25), .O(n65));
  nor2  g45(.a(n65), .b(n32), .O(n66));
  nor2  g46(.a(n66), .b(n64), .O(n67));
  nor2  g47(.a(n67), .b(n61), .O(n68));
  inv1  g48(.a(n64), .O(n69));
  nand2 g49(.a(v9), .b(v5), .O(n70));
  nor2  g50(.a(n70), .b(n69), .O(n71));
  nor2  g51(.a(n71), .b(n68), .O(n72));
  nor2  g52(.a(n72), .b(n40), .O(n73));
  nand2 g53(.a(n31), .b(v7), .O(n74));
  nor2  g54(.a(n74), .b(n50), .O(n75));
  nor2  g55(.a(n75), .b(n73), .O(n76));
  nor2  g56(.a(n76), .b(n55), .O(v12.2 ));
  inv1  g57(.a(n22), .O(n78));
  nor2  g58(.a(n78), .b(n21), .O(n79));
  inv1  g59(.a(n39), .O(n80));
  nand2 g60(.a(v6), .b(n38), .O(n81));
  nor2  g61(.a(n81), .b(n80), .O(n82));
  nand2 g62(.a(n82), .b(n79), .O(n83));
  nand2 g63(.a(n21), .b(v0), .O(n84));
  nand2 g64(.a(n84), .b(n83), .O(n85));
  inv1  g65(.a(v2), .O(n86));
  nand2 g66(.a(n25), .b(n86), .O(n87));
  nor2  g67(.a(n87), .b(v8), .O(n88));
  nand2 g68(.a(n88), .b(n85), .O(n89));
  inv1  g69(.a(n81), .O(n90));
  nand2 g70(.a(n90), .b(v8), .O(n91));
  nand2 g71(.a(n54), .b(n39), .O(n92));
  nor2  g72(.a(n92), .b(n91), .O(n93));
  nand2 g73(.a(v2), .b(v0), .O(n94));
  nand2 g74(.a(n31), .b(n21), .O(n95));
  nor2  g75(.a(n95), .b(n94), .O(n96));
  nor2  g76(.a(n96), .b(n93), .O(n97));
  nand2 g77(.a(n97), .b(n89), .O(v12.3 ));
  nand2 g78(.a(v7), .b(n86), .O(n99));
  nor2  g79(.a(n84), .b(n31), .O(n100));
  nand2 g80(.a(n100), .b(n99), .O(n101));
  nor2  g81(.a(n38), .b(v2), .O(n102));
  nand2 g82(.a(n102), .b(n39), .O(n103));
  nand2 g83(.a(n103), .b(n101), .O(v12.4 ));
  nand2 g84(.a(n39), .b(v2), .O(n105));
  nor2  g85(.a(v2), .b(n50), .O(n106));
  nor2  g86(.a(n74), .b(v4), .O(n107));
  nand2 g87(.a(n107), .b(n106), .O(n108));
  nand2 g88(.a(n108), .b(n105), .O(v12.5 ));
  nand2 g89(.a(v1), .b(n50), .O(n110));
  nor2  g90(.a(n51), .b(v4), .O(n111));
  nand2 g91(.a(n111), .b(n106), .O(n112));
  nand2 g92(.a(n112), .b(n110), .O(v12.6 ));
  nor2  g93(.a(n87), .b(n31), .O(n114));
  nor2  g94(.a(n114), .b(v2), .O(n115));
  nor2  g95(.a(n115), .b(n42), .O(v12.7 ));
endmodule


