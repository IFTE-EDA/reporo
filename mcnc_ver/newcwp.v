// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:25 2019

module source_pla  ( 
    CWP<6> , CWP<5> , CWP<4> , changeCWP2,
    CWP+1<2> , CWP+1<1> , CWP+1<0> , CWPm1<1> , CWPm1<2>   );
  input  CWP<6> , CWP<5> , CWP<4> , changeCWP2;
  output CWP+1<2> , CWP+1<1> , CWP+1<0> , CWPm1<1> , CWPm1<2> ;
  wire n10, n11, n12, n14, n16, n17, n18, n19, n20, n21, n22, n23, n24, n26,
    n27, n28, n29, n30, n31, n33, n34, n35;
  inv1  g00(.a(CWP<6> ), .O(n10));
  nand2 g01(.a(CWP<4> ), .b(CWP<5> ), .O(n11));
  inv1  g02(.a(CWP<5> ), .O(n12));
  inv1  g03(.a(CWP<4> ), .O(CWP+1<0> ));
  nand2 g04(.a(CWP+1<0> ), .b(n12), .O(n14));
  nand2 g05(.a(n14), .b(n11), .O(CWPm1<1> ));
  nand2 g06(.a(CWPm1<1> ), .b(n10), .O(n16));
  nor2  g07(.a(CWPm1<1> ), .b(n10), .O(n17));
  inv1  g08(.a(changeCWP2), .O(n18));
  nor2  g09(.a(n18), .b(CWP<4> ), .O(n19));
  nand2 g10(.a(n19), .b(n12), .O(n20));
  nor2  g11(.a(changeCWP2), .b(CWP+1<0> ), .O(n21));
  nand2 g12(.a(n21), .b(CWP<5> ), .O(n22));
  nand2 g13(.a(n22), .b(n20), .O(n23));
  nor2  g14(.a(n23), .b(n17), .O(n24));
  nand2 g15(.a(n24), .b(n16), .O(CWP+1<2> ));
  nand2 g16(.a(changeCWP2), .b(CWP+1<0> ), .O(n26));
  nand2 g17(.a(n18), .b(CWP<4> ), .O(n27));
  nand2 g18(.a(n27), .b(n26), .O(n28));
  nand2 g19(.a(n28), .b(CWP<5> ), .O(n29));
  nor2  g20(.a(n21), .b(n19), .O(n30));
  nand2 g21(.a(n30), .b(n12), .O(n31));
  nand2 g22(.a(n31), .b(n29), .O(CWP+1<1> ));
  nand2 g23(.a(n14), .b(CWP<6> ), .O(n33));
  inv1  g24(.a(n14), .O(n34));
  nand2 g25(.a(n34), .b(n10), .O(n35));
  nand2 g26(.a(n35), .b(n33), .O(CWPm1<2> ));
endmodule


