// Benchmark "cordic" written by ABC on Tue Nov  5 15:01:19 2019

module cordic ( 
    a6, a4, a3, a2, a5, v, x0, x1, x2, x3, y0, y1, y2, y3, z0, z1, z2, ex0,
    ex1, ex2, ey0, ey1, ey2,
    d, dn  );
  input  a6, a4, a3, a2, a5, v, x0, x1, x2, x3, y0, y1, y2, y3, z0, z1,
    z2, ex0, ex1, ex2, ey0, ey1, ey2;
  output d, dn;
  wire n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39,
    n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53,
    n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n65, n66, n67,
    n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78, n79, n80, n81,
    n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93, n95, n96,
    n97, n98, n99, n100, n101, n102, n103, n104, n105, n106, n107, n108,
    n109, n110, n111, n112, n113;
  nor2  g00(.a(a4), .b(a6), .O(n26));
  nor2  g01(.a(a2), .b(a3), .O(n27));
  inv1  g02(.a(n27), .O(n28));
  nor2  g03(.a(n28), .b(a5), .O(n29));
  nand2 g04(.a(n29), .b(n26), .O(n30));
  inv1  g05(.a(n30), .O(n31));
  nand2 g06(.a(a4), .b(a6), .O(n32));
  nor2  g07(.a(n32), .b(n28), .O(n33));
  inv1  g08(.a(a4), .O(n34));
  nand2 g09(.a(n34), .b(a6), .O(n35));
  inv1  g10(.a(a2), .O(n36));
  nand2 g11(.a(n36), .b(a3), .O(n37));
  nor2  g12(.a(n37), .b(n35), .O(n38));
  inv1  g13(.a(y0), .O(n39));
  nor2  g14(.a(y1), .b(n39), .O(n40));
  inv1  g15(.a(y1), .O(n41));
  nor2  g16(.a(n41), .b(y0), .O(n42));
  nor2  g17(.a(n42), .b(n40), .O(n43));
  inv1  g18(.a(y3), .O(n44));
  nand2 g19(.a(n44), .b(y2), .O(n45));
  inv1  g20(.a(y2), .O(n46));
  nand2 g21(.a(y3), .b(n46), .O(n47));
  nand2 g22(.a(n47), .b(n45), .O(n48));
  nand2 g23(.a(n48), .b(n43), .O(n49));
  inv1  g24(.a(z2), .O(n50));
  inv1  g25(.a(z0), .O(n51));
  nand2 g26(.a(z1), .b(n51), .O(n52));
  nor2  g27(.a(n52), .b(n50), .O(n53));
  inv1  g28(.a(z1), .O(n54));
  nand2 g29(.a(n50), .b(n54), .O(n55));
  nor2  g30(.a(n55), .b(n51), .O(n56));
  nor2  g31(.a(n56), .b(n53), .O(n57));
  nand2 g32(.a(n57), .b(n49), .O(n58));
  inv1  g33(.a(x3), .O(n59));
  nand2 g34(.a(n59), .b(x2), .O(n60));
  inv1  g35(.a(x2), .O(n61));
  nand2 g36(.a(x3), .b(n61), .O(n62));
  nand2 g37(.a(n62), .b(n60), .O(n63));
  inv1  g38(.a(x0), .O(n64));
  nor2  g39(.a(x1), .b(n64), .O(n65));
  inv1  g40(.a(x1), .O(n66));
  nor2  g41(.a(n66), .b(x0), .O(n67));
  nor2  g42(.a(n67), .b(n65), .O(n68));
  nand2 g43(.a(n68), .b(n63), .O(n69));
  nor2  g44(.a(n68), .b(n63), .O(n70));
  nor2  g45(.a(n48), .b(n43), .O(n71));
  nor2  g46(.a(n71), .b(n70), .O(n72));
  nand2 g47(.a(n72), .b(n69), .O(n73));
  nor2  g48(.a(n73), .b(n58), .O(n74));
  nor2  g49(.a(n74), .b(n38), .O(n75));
  nor2  g50(.a(ey2), .b(ey0), .O(n76));
  nor2  g51(.a(n76), .b(ey1), .O(n77));
  nand2 g52(.a(ey2), .b(ey0), .O(n78));
  nand2 g53(.a(n78), .b(ey1), .O(n79));
  inv1  g54(.a(n79), .O(n80));
  nor2  g55(.a(n80), .b(n77), .O(n81));
  inv1  g56(.a(ex1), .O(n82));
  inv1  g57(.a(ex0), .O(n83));
  inv1  g58(.a(ex2), .O(n84));
  nor2  g59(.a(n84), .b(n83), .O(n85));
  nor2  g60(.a(n85), .b(n82), .O(n86));
  nor2  g61(.a(ex2), .b(ex0), .O(n87));
  nor2  g62(.a(n87), .b(ex1), .O(n88));
  nor2  g63(.a(n88), .b(n86), .O(n89));
  nand2 g64(.a(n89), .b(n81), .O(n90));
  nor2  g65(.a(n90), .b(n75), .O(n91));
  nor2  g66(.a(n91), .b(n33), .O(n92));
  nor2  g67(.a(n92), .b(v), .O(n93));
  nor2  g68(.a(n93), .b(n31), .O(d));
  inv1  g69(.a(v), .O(n95));
  inv1  g70(.a(n81), .O(n96));
  nand2 g71(.a(n89), .b(n57), .O(n97));
  nor2  g72(.a(n97), .b(n96), .O(n98));
  inv1  g73(.a(n43), .O(n99));
  nor2  g74(.a(n48), .b(n99), .O(n100));
  inv1  g75(.a(n48), .O(n101));
  nor2  g76(.a(n101), .b(n43), .O(n102));
  nor2  g77(.a(n102), .b(n100), .O(n103));
  inv1  g78(.a(n68), .O(n104));
  nor2  g79(.a(n104), .b(n63), .O(n105));
  inv1  g80(.a(n63), .O(n106));
  nor2  g81(.a(n68), .b(n106), .O(n107));
  nor2  g82(.a(n107), .b(n105), .O(n108));
  nor2  g83(.a(n108), .b(n103), .O(n109));
  nand2 g84(.a(n109), .b(n98), .O(n110));
  nor2  g85(.a(n38), .b(n33), .O(n111));
  nand2 g86(.a(n111), .b(n110), .O(n112));
  nand2 g87(.a(n112), .b(n95), .O(n113));
  nand2 g88(.a(n113), .b(n30), .O(dn));
endmodule


