// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:27 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6,
    v7.0 , v7.1 , v7.2   );
  input  v0, v1, v2, v3, v4, v5, v6;
  output v7.0 , v7.1 , v7.2 ;
  wire n11, n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24,
    n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38,
    n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52,
    n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n64, n65, n66, n67,
    n68, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78, n79, n80, n81,
    n82, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93, n94, n95,
    n96, n97, n98, n99, n100, n101, n102, n103, n104, n105, n106, n107,
    n108, n109, n111, n112, n113, n114, n115, n116, n117, n118, n119, n120,
    n121, n122, n123, n124, n125, n126, n127, n128, n129, n130, n131, n132,
    n133, n134, n135, n136, n137, n138, n139, n140, n141, n142, n143, n144,
    n145, n146, n147, n148, n149, n150, n151, n152, n153, n154;
  inv1  g000(.a(v4), .O(n11));
  inv1  g001(.a(v6), .O(n12));
  nor2  g002(.a(n12), .b(n11), .O(n13));
  inv1  g003(.a(n13), .O(n14));
  nor2  g004(.a(n14), .b(v1), .O(n15));
  inv1  g005(.a(v1), .O(n16));
  nor2  g006(.a(v6), .b(v4), .O(n17));
  inv1  g007(.a(n17), .O(n18));
  nor2  g008(.a(n18), .b(n16), .O(n19));
  nor2  g009(.a(n19), .b(n15), .O(n20));
  inv1  g010(.a(v3), .O(n21));
  nor2  g011(.a(n12), .b(v4), .O(n22));
  nand2 g012(.a(n22), .b(n16), .O(n23));
  nor2  g013(.a(v6), .b(n11), .O(n24));
  nand2 g014(.a(n24), .b(v1), .O(n25));
  nand2 g015(.a(n25), .b(n23), .O(n26));
  nand2 g016(.a(n26), .b(n21), .O(n27));
  nand2 g017(.a(n27), .b(n20), .O(n28));
  nand2 g018(.a(n28), .b(v0), .O(n29));
  inv1  g019(.a(n22), .O(n30));
  nor2  g020(.a(n30), .b(n16), .O(n31));
  inv1  g021(.a(n24), .O(n32));
  nor2  g022(.a(n32), .b(v1), .O(n33));
  nor2  g023(.a(n33), .b(n31), .O(n34));
  inv1  g024(.a(v2), .O(n35));
  nand2 g025(.a(n21), .b(n35), .O(n36));
  nor2  g026(.a(n36), .b(v0), .O(n37));
  inv1  g027(.a(v0), .O(n38));
  nand2 g028(.a(n35), .b(n38), .O(n39));
  nor2  g029(.a(n39), .b(n37), .O(n40));
  nor2  g030(.a(n40), .b(v5), .O(n41));
  nand2 g031(.a(v2), .b(v0), .O(n42));
  nand2 g032(.a(v5), .b(n21), .O(n43));
  nor2  g033(.a(n43), .b(n42), .O(n44));
  nor2  g034(.a(n44), .b(n41), .O(n45));
  nor2  g035(.a(n45), .b(n34), .O(n46));
  nand2 g036(.a(n13), .b(v1), .O(n47));
  nand2 g037(.a(n17), .b(n16), .O(n48));
  nand2 g038(.a(n48), .b(n47), .O(n49));
  inv1  g039(.a(n49), .O(n50));
  nand2 g040(.a(v5), .b(n35), .O(n51));
  inv1  g041(.a(n51), .O(n52));
  nor2  g042(.a(v3), .b(n38), .O(n53));
  inv1  g043(.a(n53), .O(n54));
  nor2  g044(.a(n54), .b(n52), .O(n55));
  nand2 g045(.a(v2), .b(n38), .O(n56));
  inv1  g046(.a(v5), .O(n57));
  nand2 g047(.a(n57), .b(v3), .O(n58));
  nor2  g048(.a(n58), .b(n56), .O(n59));
  nor2  g049(.a(n59), .b(n55), .O(n60));
  nor2  g050(.a(n60), .b(n50), .O(n61));
  nor2  g051(.a(n61), .b(n46), .O(n62));
  nand2 g052(.a(n62), .b(n29), .O(v7.0 ));
  nor2  g053(.a(n58), .b(n39), .O(n64));
  nor2  g054(.a(n64), .b(n44), .O(n65));
  nor2  g055(.a(n56), .b(n21), .O(n66));
  nor2  g056(.a(n36), .b(n38), .O(n67));
  nor2  g057(.a(n67), .b(n66), .O(n68));
  nand2 g058(.a(v5), .b(v3), .O(n69));
  nor2  g059(.a(n69), .b(n39), .O(n70));
  nand2 g060(.a(n57), .b(n21), .O(n71));
  nor2  g061(.a(n71), .b(n42), .O(n72));
  nor2  g062(.a(n72), .b(n70), .O(n73));
  nand2 g063(.a(n73), .b(n68), .O(n74));
  nand2 g064(.a(n74), .b(v1), .O(n75));
  nand2 g065(.a(n75), .b(n65), .O(n76));
  nand2 g066(.a(n76), .b(n12), .O(n77));
  nand2 g067(.a(n35), .b(v0), .O(n78));
  nor2  g068(.a(n78), .b(n43), .O(n79));
  nor2  g069(.a(n79), .b(n59), .O(n80));
  nand2 g070(.a(v6), .b(v1), .O(n81));
  inv1  g071(.a(n81), .O(n82));
  nor2  g072(.a(v6), .b(v1), .O(n83));
  nor2  g073(.a(n83), .b(n82), .O(n84));
  nor2  g074(.a(n84), .b(n80), .O(n85));
  nor2  g075(.a(n43), .b(v0), .O(n86));
  nor2  g076(.a(n58), .b(n38), .O(n87));
  nor2  g077(.a(n87), .b(n86), .O(n88));
  nor2  g078(.a(n88), .b(n16), .O(n89));
  nor2  g079(.a(n89), .b(n85), .O(n90));
  nor2  g080(.a(n90), .b(v4), .O(n91));
  inv1  g081(.a(n69), .O(n92));
  nand2 g082(.a(n92), .b(n38), .O(n93));
  nor2  g083(.a(v5), .b(v3), .O(n94));
  nand2 g084(.a(n94), .b(v0), .O(n95));
  nand2 g085(.a(n95), .b(n93), .O(n96));
  nand2 g086(.a(n96), .b(n82), .O(n97));
  nand2 g087(.a(n92), .b(v0), .O(n98));
  nand2 g088(.a(n94), .b(n38), .O(n99));
  nand2 g089(.a(n99), .b(n98), .O(n100));
  nand2 g090(.a(v6), .b(n35), .O(n101));
  nor2  g091(.a(v4), .b(n16), .O(n102));
  nand2 g092(.a(n102), .b(n101), .O(n103));
  nor2  g093(.a(n35), .b(v1), .O(n104));
  nand2 g094(.a(n104), .b(n24), .O(n105));
  nand2 g095(.a(n105), .b(n103), .O(n106));
  nand2 g096(.a(n106), .b(n100), .O(n107));
  nand2 g097(.a(n107), .b(n97), .O(n108));
  nor2  g098(.a(n108), .b(n91), .O(n109));
  nand2 g099(.a(n109), .b(n77), .O(v7.1 ));
  nand2 g100(.a(n16), .b(n38), .O(n111));
  nand2 g101(.a(n111), .b(n57), .O(n112));
  nand2 g102(.a(v1), .b(n38), .O(n113));
  nor2  g103(.a(n113), .b(n57), .O(n114));
  nand2 g104(.a(v1), .b(v0), .O(n115));
  nor2  g105(.a(n115), .b(n51), .O(n116));
  nor2  g106(.a(n116), .b(n114), .O(n117));
  nand2 g107(.a(n117), .b(n112), .O(n118));
  nor2  g108(.a(n12), .b(v3), .O(n119));
  nand2 g109(.a(n119), .b(n118), .O(n120));
  inv1  g110(.a(n83), .O(n121));
  nand2 g111(.a(n121), .b(n81), .O(n122));
  nor2  g112(.a(n43), .b(n38), .O(n123));
  nor2  g113(.a(n58), .b(v0), .O(n124));
  nor2  g114(.a(n124), .b(n123), .O(n125));
  nor2  g115(.a(n125), .b(n122), .O(n126));
  nor2  g116(.a(n84), .b(n65), .O(n127));
  nor2  g117(.a(n127), .b(n126), .O(n128));
  nand2 g118(.a(n128), .b(n120), .O(n129));
  nand2 g119(.a(n129), .b(n11), .O(n130));
  nand2 g120(.a(n71), .b(n69), .O(n131));
  nor2  g121(.a(n131), .b(n86), .O(n132));
  nor2  g122(.a(n132), .b(n35), .O(n133));
  nor2  g123(.a(n43), .b(n39), .O(n134));
  nor2  g124(.a(n134), .b(n133), .O(n135));
  nor2  g125(.a(n135), .b(n20), .O(n136));
  nand2 g126(.a(n57), .b(v2), .O(n137));
  nand2 g127(.a(n137), .b(n51), .O(n138));
  nand2 g128(.a(n138), .b(n53), .O(n139));
  nor2  g129(.a(n35), .b(v0), .O(n140));
  nand2 g130(.a(n92), .b(n140), .O(n141));
  nand2 g131(.a(n141), .b(n139), .O(n142));
  nand2 g132(.a(n142), .b(n49), .O(n143));
  nand2 g133(.a(n113), .b(n57), .O(n144));
  nor2  g134(.a(n111), .b(n57), .O(n145));
  nand2 g135(.a(n16), .b(v0), .O(n146));
  nor2  g136(.a(n146), .b(n51), .O(n147));
  nor2  g137(.a(n147), .b(n145), .O(n148));
  nand2 g138(.a(n148), .b(n144), .O(n149));
  nand2 g139(.a(v4), .b(n21), .O(n150));
  nor2  g140(.a(n150), .b(v6), .O(n151));
  nand2 g141(.a(n151), .b(n149), .O(n152));
  nand2 g142(.a(n152), .b(n143), .O(n153));
  nor2  g143(.a(n153), .b(n136), .O(n154));
  nand2 g144(.a(n154), .b(n130), .O(v7.2 ));
endmodule


