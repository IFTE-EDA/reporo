// Benchmark "pm1" written by ABC on Tue Nov  5 15:01:26 2019

module pm1 ( 
    a, b, c, d, e, g, h, i, j, k, l, m, n, o, p, q,
    r, s, t, u, v, w, x, y, z, a0, b0, c0, d0  );
  input  a, b, c, d, e, g, h, i, j, k, l, m, n, o, p, q;
  output r, s, t, u, v, w, x, y, z, a0, b0, c0, d0;
  wire n30, n31, n33, n35, n36, n37, n38, n39, n40, n42, n43, n44, n45, n46,
    n50, n51, n52, n53, n56, n57, n58, n59, n60, n61, n62, n63, n64, n66,
    n67, n68, n69, n71, n72, n73, n74, n76, n77, n78, n79, n80, n81, n83;
  inv1  g00(.a(n), .O(n30));
  nor2  g01(.a(m), .b(b), .O(n31));
  nand2 g02(.a(n31), .b(n30), .O(r));
  inv1  g03(.a(m), .O(n33));
  nand2 g04(.a(n), .b(n33), .O(s));
  inv1  g05(.a(k), .O(n35));
  nand2 g06(.a(j), .b(i), .O(n36));
  nor2  g07(.a(n36), .b(n35), .O(n37));
  nand2 g08(.a(n), .b(m), .O(n38));
  nand2 g09(.a(h), .b(g), .O(n39));
  nor2  g10(.a(n39), .b(n38), .O(n40));
  nand2 g11(.a(n40), .b(n37), .O(t));
  inv1  g12(.a(n36), .O(n42));
  inv1  g13(.a(n39), .O(n43));
  nand2 g14(.a(n43), .b(n42), .O(n44));
  nand2 g15(.a(m), .b(k), .O(n45));
  nor2  g16(.a(n45), .b(n30), .O(n46));
  nand2 g17(.a(n46), .b(n44), .O(u));
  inv1  g18(.a(p), .O(v));
  inv1  g19(.a(o), .O(w));
  nand2 g20(.a(e), .b(d), .O(n50));
  nand2 g21(.a(s), .b(c), .O(n51));
  nor2  g22(.a(n51), .b(n50), .O(n52));
  nand2 g23(.a(k), .b(b), .O(n53));
  nor2  g24(.a(n53), .b(n52), .O(x));
  inv1  g25(.a(q), .O(y));
  inv1  g26(.a(c), .O(n56));
  inv1  g27(.a(d), .O(n57));
  nor2  g28(.a(n57), .b(n56), .O(n58));
  nand2 g29(.a(k), .b(e), .O(n59));
  nor2  g30(.a(n59), .b(n30), .O(n60));
  nand2 g31(.a(n60), .b(n58), .O(n61));
  inv1  g32(.a(l), .O(n62));
  nand2 g33(.a(n62), .b(a), .O(n63));
  nor2  g34(.a(n63), .b(n33), .O(n64));
  nand2 g35(.a(n64), .b(n61), .O(z));
  nor2  g36(.a(n), .b(n33), .O(n66));
  inv1  g37(.a(a), .O(n67));
  nor2  g38(.a(l), .b(n67), .O(n68));
  nand2 g39(.a(n68), .b(s), .O(n69));
  nor2  g40(.a(n69), .b(n66), .O(a0));
  nor2  g41(.a(n50), .b(n56), .O(n71));
  inv1  g42(.a(n38), .O(n72));
  nor2  g43(.a(n63), .b(n35), .O(n73));
  nand2 g44(.a(n73), .b(n72), .O(n74));
  nor2  g45(.a(n74), .b(n71), .O(b0));
  inv1  g46(.a(b), .O(n76));
  nand2 g47(.a(n), .b(k), .O(n77));
  nor2  g48(.a(n77), .b(n71), .O(n78));
  nor2  g49(.a(n78), .b(n76), .O(n79));
  nand2 g50(.a(n), .b(n76), .O(n80));
  nand2 g51(.a(n80), .b(n64), .O(n81));
  nor2  g52(.a(n81), .b(n79), .O(c0));
  nand2 g53(.a(n68), .b(n35), .O(n83));
  nor2  g54(.a(n83), .b(n38), .O(d0));
endmodule


