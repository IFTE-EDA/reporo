// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:20 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7, v8,
    v9.0 , v9.1 , v9.2 , v9.3 , v9.4 , v9.5 , v9.6 , v9.7 , v9.8   );
  input  v0, v1, v2, v3, v4, v5, v6, v7, v8;
  output v9.0 , v9.1 , v9.2 , v9.3 , v9.4 , v9.5 , v9.6 , v9.7 ,
    v9.8 ;
  wire n19, n20, n21, n22, n23, n24, n25, n26, n27, n28, n29, n30, n31, n32,
    n33, n35, n36, n37, n38, n40, n41, n42, n43, n44, n45, n46, n47, n48,
    n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n60, n61, n63, n64,
    n65, n66, n67, n68, n69, n70, n71, n72, n73, n75, n76, n78, n79, n80,
    n81, n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93, n95;
  inv1  g00(.a(v4), .O(n19));
  nor2  g01(.a(n19), .b(v2), .O(n20));
  inv1  g02(.a(v2), .O(n21));
  nor2  g03(.a(v4), .b(n21), .O(n22));
  nor2  g04(.a(n22), .b(n20), .O(n23));
  inv1  g05(.a(v6), .O(n24));
  nand2 g06(.a(v7), .b(n24), .O(n25));
  nor2  g07(.a(n25), .b(v8), .O(n26));
  nor2  g08(.a(v1), .b(v0), .O(n27));
  inv1  g09(.a(n27), .O(n28));
  inv1  g10(.a(v3), .O(n29));
  inv1  g11(.a(v5), .O(n30));
  nand2 g12(.a(n30), .b(n29), .O(n31));
  nor2  g13(.a(n31), .b(n28), .O(n32));
  nand2 g14(.a(n32), .b(n26), .O(n33));
  nor2  g15(.a(n33), .b(n23), .O(v9.0 ));
  nor2  g16(.a(v7), .b(v6), .O(n35));
  nand2 g17(.a(n35), .b(v8), .O(n36));
  inv1  g18(.a(n36), .O(n37));
  nand2 g19(.a(n37), .b(n32), .O(n38));
  nor2  g20(.a(n38), .b(n23), .O(v9.1 ));
  inv1  g21(.a(v7), .O(n40));
  nand2 g22(.a(v8), .b(n40), .O(n41));
  inv1  g23(.a(v8), .O(n42));
  nand2 g24(.a(n42), .b(v7), .O(n43));
  nand2 g25(.a(n43), .b(n41), .O(n44));
  nand2 g26(.a(n44), .b(n24), .O(n45));
  nor2  g27(.a(n45), .b(n29), .O(n46));
  nand2 g28(.a(v6), .b(n29), .O(n47));
  nor2  g29(.a(n47), .b(n41), .O(n48));
  nor2  g30(.a(n48), .b(n46), .O(n49));
  nor2  g31(.a(n49), .b(v0), .O(n50));
  inv1  g32(.a(v0), .O(n51));
  nor2  g33(.a(v3), .b(n51), .O(n52));
  nand2 g34(.a(n52), .b(n24), .O(n53));
  nor2  g35(.a(n53), .b(n43), .O(n54));
  nor2  g36(.a(n54), .b(n50), .O(n55));
  nor2  g37(.a(v2), .b(v1), .O(n56));
  nor2  g38(.a(v5), .b(v4), .O(n57));
  nand2 g39(.a(n57), .b(n56), .O(n58));
  nor2  g40(.a(n58), .b(n55), .O(v9.2 ));
  inv1  g41(.a(n58), .O(n60));
  nand2 g42(.a(n60), .b(n52), .O(n61));
  nor2  g43(.a(n61), .b(n36), .O(v9.3 ));
  nor2  g44(.a(n24), .b(v5), .O(n63));
  nor2  g45(.a(v6), .b(n30), .O(n64));
  nor2  g46(.a(n64), .b(n63), .O(n65));
  nor2  g47(.a(n65), .b(v1), .O(n66));
  nand2 g48(.a(n30), .b(v1), .O(n67));
  nor2  g49(.a(n67), .b(v6), .O(n68));
  nor2  g50(.a(n68), .b(n66), .O(n69));
  nor2  g51(.a(v2), .b(v0), .O(n70));
  nand2 g52(.a(n19), .b(n29), .O(n71));
  nor2  g53(.a(n71), .b(n43), .O(n72));
  nand2 g54(.a(n72), .b(n70), .O(n73));
  nor2  g55(.a(n73), .b(n69), .O(v9.4 ));
  nor2  g56(.a(n71), .b(n67), .O(n75));
  nand2 g57(.a(n75), .b(n70), .O(n76));
  nor2  g58(.a(n76), .b(n36), .O(v9.5 ));
  nand2 g59(.a(n29), .b(n21), .O(n78));
  nand2 g60(.a(v5), .b(n19), .O(n79));
  nor2  g61(.a(n79), .b(n78), .O(n80));
  nand2 g62(.a(n80), .b(n27), .O(n81));
  nor2  g63(.a(n81), .b(n36), .O(v9.6 ));
  nor2  g64(.a(n45), .b(n19), .O(n83));
  nand2 g65(.a(v6), .b(n19), .O(n84));
  nor2  g66(.a(n84), .b(n41), .O(n85));
  nor2  g67(.a(n85), .b(n83), .O(n86));
  nor2  g68(.a(n86), .b(v3), .O(n87));
  nor2  g69(.a(v4), .b(n29), .O(n88));
  nand2 g70(.a(n88), .b(n24), .O(n89));
  nor2  g71(.a(n89), .b(n41), .O(n90));
  nor2  g72(.a(n90), .b(n87), .O(n91));
  nor2  g73(.a(v5), .b(v2), .O(n92));
  nand2 g74(.a(n92), .b(n27), .O(n93));
  nor2  g75(.a(n93), .b(n91), .O(v9.7 ));
  nand2 g76(.a(n32), .b(n22), .O(n95));
  nor2  g77(.a(n95), .b(n45), .O(v9.8 ));
endmodule


