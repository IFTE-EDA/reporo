// Benchmark "top" written by ABC on Tue Nov  5 14:58:39 2019

module top ( 
    dest_x_0 , dest_x_1 , dest_x_2 , dest_x_3 , dest_x_4 ,
    dest_x_5 , dest_x_6 , dest_x_7 , dest_x_8 , dest_x_9 ,
    dest_x_10 , dest_x_11 , dest_x_12 , dest_x_13 , dest_x_14 ,
    dest_x_15 , dest_x_16 , dest_x_17 , dest_x_18 , dest_x_19 ,
    dest_x_20 , dest_x_21 , dest_x_22 , dest_x_23 , dest_x_24 ,
    dest_x_25 , dest_x_26 , dest_x_27 , dest_x_28 , dest_x_29 ,
    dest_y_0 , dest_y_1 , dest_y_2 , dest_y_3 , dest_y_4 ,
    dest_y_5 , dest_y_6 , dest_y_7 , dest_y_8 , dest_y_9 ,
    dest_y_10 , dest_y_11 , dest_y_12 , dest_y_13 , dest_y_14 ,
    dest_y_15 , dest_y_16 , dest_y_17 , dest_y_18 , dest_y_19 ,
    dest_y_20 , dest_y_21 , dest_y_22 , dest_y_23 , dest_y_24 ,
    dest_y_25 , dest_y_26 , dest_y_27 , dest_y_28 , dest_y_29 ,
    outport_0 , outport_1 , outport_2 , outport_3 , outport_4 ,
    outport_5 , outport_6 , outport_7 , outport_8 , outport_9 ,
    outport_10 , outport_11 , outport_12 , outport_13 ,
    outport_14 , outport_15 , outport_16 , outport_17 ,
    outport_18 , outport_19 , outport_20 , outport_21 ,
    outport_22 , outport_23 , outport_24 , outport_25 ,
    outport_26 , outport_27 , outport_28 , outport_29   );
  input  dest_x_0 , dest_x_1 , dest_x_2 , dest_x_3 , dest_x_4 ,
    dest_x_5 , dest_x_6 , dest_x_7 , dest_x_8 , dest_x_9 ,
    dest_x_10 , dest_x_11 , dest_x_12 , dest_x_13 , dest_x_14 ,
    dest_x_15 , dest_x_16 , dest_x_17 , dest_x_18 , dest_x_19 ,
    dest_x_20 , dest_x_21 , dest_x_22 , dest_x_23 , dest_x_24 ,
    dest_x_25 , dest_x_26 , dest_x_27 , dest_x_28 , dest_x_29 ,
    dest_y_0 , dest_y_1 , dest_y_2 , dest_y_3 , dest_y_4 ,
    dest_y_5 , dest_y_6 , dest_y_7 , dest_y_8 , dest_y_9 ,
    dest_y_10 , dest_y_11 , dest_y_12 , dest_y_13 , dest_y_14 ,
    dest_y_15 , dest_y_16 , dest_y_17 , dest_y_18 , dest_y_19 ,
    dest_y_20 , dest_y_21 , dest_y_22 , dest_y_23 , dest_y_24 ,
    dest_y_25 , dest_y_26 , dest_y_27 , dest_y_28 , dest_y_29 ;
  output outport_0 , outport_1 , outport_2 , outport_3 , outport_4 ,
    outport_5 , outport_6 , outport_7 , outport_8 , outport_9 ,
    outport_10 , outport_11 , outport_12 , outport_13 ,
    outport_14 , outport_15 , outport_16 , outport_17 ,
    outport_18 , outport_19 , outport_20 , outport_21 ,
    outport_22 , outport_23 , outport_24 , outport_25 ,
    outport_26 , outport_27 , outport_28 , outport_29 ;
  wire n91, n92, n93, n94, n95, n96, n97, n98, n99, n100, n101, n102, n103,
    n104, n105, n106, n107, n108, n109, n110, n111, n112, n113, n114, n115,
    n116, n117, n118, n119, n120, n121, n122, n123, n124, n125, n126, n127,
    n128, n129, n130, n131, n132, n133, n134, n135, n136, n137, n138, n139,
    n140, n141, n142, n143, n144, n145, n146, n147, n148, n149, n150, n151,
    n152, n153, n154, n155, n156, n157, n158, n159, n160, n161, n162, n163,
    n164, n165, n166, n167, n168, n169, n170, n171, n172, n173, n174, n175,
    n176, n177, n178, n179, n180, n181, n182, n183, n184, n185, n186, n187,
    n188, n189, n190, n191, n192, n193, n194, n195, n196, n197, n198, n199,
    n200, n201, n202, n203, n204, n205, n206, n207, n208, n209, n210, n211,
    n212, n213, n214, n215, n216, n217, n218, n219, n220, n221, n222, n223,
    n224, n225, n226, n227, n228, n229, n230, n231, n232, n233, n234, n235,
    n236, n237, n238, n239, n240, n241, n242, n243, n244, n245, n246, n247,
    n248, n249, n250, n251, n252, n254, n255, n256, n257, n258, n259, n260,
    n261, n262, n263, n264, n265, n266, n267, n268, n269, n270, n271, n272,
    n273, n274, n275, n276, n277, n278, n279, n280, n281, n282, n283, n284,
    n285, n286, n287, n288, n289, n290, n291, n292, n293, n294, n295, n296,
    n297, n298, n299, n300, n301, n302, n303, n304, n305, n306, n307, n308,
    n309, n310, n311, n312, n313, n314, n315, n316, n317, n318, n319, n320,
    n321, n322, n323, n324, n325, n326, n327, n328, n329, n330, n331, n332,
    n333, n334, n335, n336, n337, n338, n339, n340, n341, n342, n343, n344,
    n345, n346, n347, n348, n349, n350, n351, n352, n353, n354, n355, n356,
    n357, n358, n359, n360, n361, n362, n363, n364, n365, n366, n367, n368,
    n369, n370, n371, n372, n373, n374, n375, n376, n377, n378, n379, n380,
    n381, n382, n383, n384, n385, n386, n387, n388, n389, n390, n391, n392,
    n393, n394, n395, n396, n397, n398, n399, n400, n401, n402, n403, n404,
    n405, n406, n407, n408, n409, n410, n411, n412, n413, n414, n415, n416,
    n417, n418, n419, n420, n421, n422, n423, n424, n425, n426, n427, n429,
    n430, n431;
  inv1  g000(.a(dest_x_29 ), .O(n91));
  inv1  g001(.a(dest_x_27 ), .O(n92));
  inv1  g002(.a(dest_x_25 ), .O(n93));
  inv1  g003(.a(dest_x_23 ), .O(n94));
  inv1  g004(.a(dest_x_21 ), .O(n95));
  inv1  g005(.a(dest_x_19 ), .O(n96));
  inv1  g006(.a(dest_x_17 ), .O(n97));
  inv1  g007(.a(dest_x_15 ), .O(n98));
  inv1  g008(.a(dest_x_13 ), .O(n99));
  inv1  g009(.a(dest_x_11 ), .O(n100));
  nor2  g010(.a(dest_x_10 ), .b(dest_x_9 ), .O(n101));
  nor2  g011(.a(n101), .b(n100), .O(n102));
  nor2  g012(.a(n102), .b(dest_x_12 ), .O(n103));
  nand2 g013(.a(n103), .b(n99), .O(n104));
  nand2 g014(.a(n104), .b(dest_x_14 ), .O(n105));
  nor2  g015(.a(n105), .b(n98), .O(n106));
  nor2  g016(.a(n106), .b(dest_x_16 ), .O(n107));
  nor2  g017(.a(n107), .b(n97), .O(n108));
  nor2  g018(.a(n108), .b(dest_x_18 ), .O(n109));
  nor2  g019(.a(n109), .b(n96), .O(n110));
  nand2 g020(.a(n110), .b(dest_x_20 ), .O(n111));
  nand2 g021(.a(n111), .b(n95), .O(n112));
  nor2  g022(.a(n112), .b(dest_x_22 ), .O(n113));
  nor2  g023(.a(n113), .b(n94), .O(n114));
  nand2 g024(.a(n114), .b(dest_x_24 ), .O(n115));
  nor2  g025(.a(n115), .b(n93), .O(n116));
  nor2  g026(.a(n116), .b(dest_x_26 ), .O(n117));
  nor2  g027(.a(n117), .b(n92), .O(n118));
  nand2 g028(.a(n118), .b(dest_x_28 ), .O(n119));
  inv1  g029(.a(n119), .O(n120));
  nand2 g030(.a(n120), .b(n91), .O(n121));
  nand2 g031(.a(n119), .b(dest_x_29 ), .O(n122));
  nand2 g032(.a(n122), .b(n121), .O(n123));
  inv1  g033(.a(dest_x_28 ), .O(n124));
  nor2  g034(.a(n118), .b(n124), .O(n125));
  inv1  g035(.a(n118), .O(n126));
  nor2  g036(.a(n126), .b(dest_x_28 ), .O(n127));
  nor2  g037(.a(n127), .b(n125), .O(n128));
  inv1  g038(.a(n117), .O(n129));
  nor2  g039(.a(n129), .b(dest_x_27 ), .O(n130));
  nor2  g040(.a(n130), .b(n118), .O(n131));
  nand2 g041(.a(n116), .b(dest_x_26 ), .O(n132));
  inv1  g042(.a(n132), .O(n133));
  nor2  g043(.a(n133), .b(n117), .O(n134));
  nand2 g044(.a(n115), .b(dest_x_25 ), .O(n135));
  nor2  g045(.a(n115), .b(dest_x_25 ), .O(n136));
  inv1  g046(.a(n136), .O(n137));
  nand2 g047(.a(n137), .b(n135), .O(n138));
  inv1  g048(.a(dest_x_24 ), .O(n139));
  nor2  g049(.a(n114), .b(n139), .O(n140));
  inv1  g050(.a(n114), .O(n141));
  nor2  g051(.a(n141), .b(dest_x_24 ), .O(n142));
  nor2  g052(.a(n142), .b(n140), .O(n143));
  inv1  g053(.a(n113), .O(n144));
  nor2  g054(.a(n144), .b(dest_x_23 ), .O(n145));
  nor2  g055(.a(n145), .b(n114), .O(n146));
  nand2 g056(.a(n112), .b(dest_x_22 ), .O(n147));
  inv1  g057(.a(n147), .O(n148));
  nor2  g058(.a(n148), .b(n113), .O(n149));
  nor2  g059(.a(n111), .b(n95), .O(n150));
  inv1  g060(.a(n150), .O(n151));
  nand2 g061(.a(n151), .b(n112), .O(n152));
  inv1  g062(.a(dest_x_20 ), .O(n153));
  nor2  g063(.a(n110), .b(n153), .O(n154));
  inv1  g064(.a(n110), .O(n155));
  nor2  g065(.a(n155), .b(dest_x_20 ), .O(n156));
  nor2  g066(.a(n156), .b(n154), .O(n157));
  inv1  g067(.a(n109), .O(n158));
  nor2  g068(.a(n158), .b(dest_x_19 ), .O(n159));
  nor2  g069(.a(n159), .b(n110), .O(n160));
  nand2 g070(.a(n108), .b(dest_x_18 ), .O(n161));
  inv1  g071(.a(n161), .O(n162));
  nor2  g072(.a(n162), .b(n109), .O(n163));
  inv1  g073(.a(n107), .O(n164));
  nor2  g074(.a(n164), .b(dest_x_17 ), .O(n165));
  nor2  g075(.a(n165), .b(n108), .O(n166));
  nand2 g076(.a(n106), .b(dest_x_16 ), .O(n167));
  inv1  g077(.a(n167), .O(n168));
  nor2  g078(.a(n168), .b(n107), .O(n169));
  nand2 g079(.a(n105), .b(dest_x_15 ), .O(n170));
  nor2  g080(.a(n105), .b(dest_x_15 ), .O(n171));
  inv1  g081(.a(n171), .O(n172));
  nand2 g082(.a(n172), .b(n170), .O(n173));
  nor2  g083(.a(n104), .b(dest_x_14 ), .O(n174));
  inv1  g084(.a(n174), .O(n175));
  nand2 g085(.a(n175), .b(n105), .O(n176));
  nor2  g086(.a(n103), .b(n99), .O(n177));
  inv1  g087(.a(n177), .O(n178));
  nand2 g088(.a(n178), .b(n104), .O(n179));
  nand2 g089(.a(n102), .b(dest_x_12 ), .O(n180));
  inv1  g090(.a(n180), .O(n181));
  nor2  g091(.a(n181), .b(n103), .O(n182));
  inv1  g092(.a(dest_x_9 ), .O(n183));
  inv1  g093(.a(dest_x_10 ), .O(n184));
  nand2 g094(.a(n184), .b(n183), .O(n185));
  nor2  g095(.a(n185), .b(dest_x_11 ), .O(n186));
  nor2  g096(.a(n186), .b(n102), .O(n187));
  inv1  g097(.a(dest_x_0 ), .O(n188));
  inv1  g098(.a(dest_x_1 ), .O(n189));
  nor2  g099(.a(n189), .b(n188), .O(n190));
  nand2 g100(.a(dest_x_3 ), .b(dest_x_2 ), .O(n191));
  nand2 g101(.a(dest_x_5 ), .b(dest_x_4 ), .O(n192));
  nor2  g102(.a(n192), .b(n191), .O(n193));
  nand2 g103(.a(n193), .b(n190), .O(n194));
  nand2 g104(.a(dest_x_10 ), .b(dest_x_9 ), .O(n195));
  nand2 g105(.a(n195), .b(n185), .O(n196));
  nand2 g106(.a(dest_x_7 ), .b(dest_x_6 ), .O(n197));
  nand2 g107(.a(n183), .b(dest_x_8 ), .O(n198));
  nor2  g108(.a(n198), .b(n197), .O(n199));
  nand2 g109(.a(n199), .b(n196), .O(n200));
  nor2  g110(.a(n200), .b(n194), .O(n201));
  nand2 g111(.a(n201), .b(n187), .O(n202));
  nor2  g112(.a(n202), .b(n182), .O(n203));
  nand2 g113(.a(n203), .b(n179), .O(n204));
  nor2  g114(.a(n204), .b(n176), .O(n205));
  nand2 g115(.a(n205), .b(n173), .O(n206));
  nor2  g116(.a(n206), .b(n169), .O(n207));
  nand2 g117(.a(n207), .b(n166), .O(n208));
  nor2  g118(.a(n208), .b(n163), .O(n209));
  nand2 g119(.a(n209), .b(n160), .O(n210));
  nor2  g120(.a(n210), .b(n157), .O(n211));
  nand2 g121(.a(n211), .b(n152), .O(n212));
  nor2  g122(.a(n212), .b(n149), .O(n213));
  nand2 g123(.a(n213), .b(n146), .O(n214));
  nor2  g124(.a(n214), .b(n143), .O(n215));
  nand2 g125(.a(n215), .b(n138), .O(n216));
  nor2  g126(.a(n216), .b(n134), .O(n217));
  nand2 g127(.a(n217), .b(n131), .O(n218));
  nor2  g128(.a(n218), .b(n128), .O(n219));
  nand2 g129(.a(n219), .b(n123), .O(n220));
  nor2  g130(.a(n119), .b(n91), .O(n221));
  inv1  g131(.a(n221), .O(n222));
  nand2 g132(.a(n222), .b(n220), .O(n223));
  nor2  g133(.a(dest_x_2 ), .b(dest_x_1 ), .O(n224));
  inv1  g134(.a(n224), .O(n225));
  nor2  g135(.a(dest_x_4 ), .b(dest_x_3 ), .O(n226));
  nor2  g136(.a(dest_x_6 ), .b(dest_x_5 ), .O(n227));
  nand2 g137(.a(n227), .b(n226), .O(n228));
  nor2  g138(.a(n228), .b(n225), .O(n229));
  nor2  g139(.a(dest_x_8 ), .b(dest_x_7 ), .O(n230));
  nand2 g140(.a(n230), .b(dest_x_9 ), .O(n231));
  nor2  g141(.a(n231), .b(n196), .O(n232));
  nand2 g142(.a(n232), .b(n229), .O(n233));
  nor2  g143(.a(n233), .b(n187), .O(n234));
  nand2 g144(.a(n234), .b(n182), .O(n235));
  nor2  g145(.a(n235), .b(n179), .O(n236));
  nand2 g146(.a(n236), .b(n176), .O(n237));
  nor2  g147(.a(n237), .b(n173), .O(n238));
  nand2 g148(.a(n238), .b(n169), .O(n239));
  nor2  g149(.a(n239), .b(n166), .O(n240));
  nand2 g150(.a(n240), .b(n163), .O(n241));
  nor2  g151(.a(n241), .b(n160), .O(n242));
  nand2 g152(.a(n242), .b(n157), .O(n243));
  nor2  g153(.a(n243), .b(n152), .O(n244));
  nand2 g154(.a(n244), .b(n149), .O(n245));
  nor2  g155(.a(n245), .b(n146), .O(n246));
  nand2 g156(.a(n246), .b(n143), .O(n247));
  nor2  g157(.a(n247), .b(n138), .O(n248));
  nand2 g158(.a(n248), .b(n134), .O(n249));
  nor2  g159(.a(n249), .b(n131), .O(n250));
  nand2 g160(.a(n250), .b(n128), .O(n251));
  nand2 g161(.a(n251), .b(n221), .O(n252));
  nand2 g162(.a(n252), .b(n223), .O(outport_0 ));
  inv1  g163(.a(n252), .O(n254));
  inv1  g164(.a(dest_y_26 ), .O(n255));
  inv1  g165(.a(dest_y_24 ), .O(n256));
  inv1  g166(.a(dest_y_22 ), .O(n257));
  inv1  g167(.a(dest_y_20 ), .O(n258));
  inv1  g168(.a(dest_y_18 ), .O(n259));
  inv1  g169(.a(dest_y_16 ), .O(n260));
  inv1  g170(.a(dest_y_14 ), .O(n261));
  inv1  g171(.a(dest_y_11 ), .O(n262));
  nor2  g172(.a(dest_y_10 ), .b(dest_y_9 ), .O(n263));
  nor2  g173(.a(n263), .b(n262), .O(n264));
  nor2  g174(.a(n264), .b(dest_y_12 ), .O(n265));
  inv1  g175(.a(n265), .O(n266));
  nor2  g176(.a(n266), .b(dest_y_13 ), .O(n267));
  nor2  g177(.a(n267), .b(n261), .O(n268));
  nand2 g178(.a(n268), .b(dest_y_15 ), .O(n269));
  nand2 g179(.a(n269), .b(n260), .O(n270));
  nand2 g180(.a(n270), .b(dest_y_17 ), .O(n271));
  nand2 g181(.a(n271), .b(n259), .O(n272));
  nand2 g182(.a(n272), .b(dest_y_19 ), .O(n273));
  nor2  g183(.a(n273), .b(n258), .O(n274));
  nor2  g184(.a(n274), .b(dest_y_21 ), .O(n275));
  nand2 g185(.a(n275), .b(n257), .O(n276));
  nand2 g186(.a(n276), .b(dest_y_23 ), .O(n277));
  nor2  g187(.a(n277), .b(n256), .O(n278));
  nand2 g188(.a(n278), .b(dest_y_25 ), .O(n279));
  nand2 g189(.a(n279), .b(n255), .O(n280));
  nand2 g190(.a(n280), .b(dest_y_27 ), .O(n281));
  nand2 g191(.a(dest_y_29 ), .b(dest_y_28 ), .O(n282));
  nor2  g192(.a(n282), .b(n281), .O(n283));
  inv1  g193(.a(n283), .O(n284));
  nand2 g194(.a(n281), .b(dest_y_28 ), .O(n285));
  inv1  g195(.a(dest_y_28 ), .O(n286));
  inv1  g196(.a(n281), .O(n287));
  nand2 g197(.a(n287), .b(n286), .O(n288));
  nand2 g198(.a(n288), .b(n285), .O(n289));
  inv1  g199(.a(dest_y_27 ), .O(n290));
  inv1  g200(.a(n280), .O(n291));
  nand2 g201(.a(n291), .b(n290), .O(n292));
  nand2 g202(.a(n292), .b(n281), .O(n293));
  inv1  g203(.a(n279), .O(n294));
  nand2 g204(.a(n294), .b(dest_y_26 ), .O(n295));
  nand2 g205(.a(n295), .b(n280), .O(n296));
  inv1  g206(.a(dest_y_25 ), .O(n297));
  nor2  g207(.a(n278), .b(n297), .O(n298));
  inv1  g208(.a(n278), .O(n299));
  nor2  g209(.a(n299), .b(dest_y_25 ), .O(n300));
  nor2  g210(.a(n300), .b(n298), .O(n301));
  nand2 g211(.a(n277), .b(dest_y_24 ), .O(n302));
  inv1  g212(.a(n277), .O(n303));
  nand2 g213(.a(n303), .b(n256), .O(n304));
  nand2 g214(.a(n304), .b(n302), .O(n305));
  inv1  g215(.a(dest_y_23 ), .O(n306));
  inv1  g216(.a(n276), .O(n307));
  nand2 g217(.a(n307), .b(n306), .O(n308));
  nand2 g218(.a(n308), .b(n277), .O(n309));
  inv1  g219(.a(n275), .O(n310));
  nand2 g220(.a(n310), .b(dest_y_22 ), .O(n311));
  nand2 g221(.a(n311), .b(n276), .O(n312));
  inv1  g222(.a(dest_y_21 ), .O(n313));
  inv1  g223(.a(n274), .O(n314));
  nor2  g224(.a(n314), .b(n313), .O(n315));
  nor2  g225(.a(n315), .b(n275), .O(n316));
  nand2 g226(.a(n273), .b(dest_y_20 ), .O(n317));
  inv1  g227(.a(n273), .O(n318));
  nand2 g228(.a(n318), .b(n258), .O(n319));
  nand2 g229(.a(n319), .b(n317), .O(n320));
  inv1  g230(.a(dest_y_19 ), .O(n321));
  inv1  g231(.a(n272), .O(n322));
  nand2 g232(.a(n322), .b(n321), .O(n323));
  nand2 g233(.a(n323), .b(n273), .O(n324));
  inv1  g234(.a(n271), .O(n325));
  nand2 g235(.a(n325), .b(dest_y_18 ), .O(n326));
  nand2 g236(.a(n326), .b(n272), .O(n327));
  inv1  g237(.a(dest_y_17 ), .O(n328));
  inv1  g238(.a(n270), .O(n329));
  nand2 g239(.a(n329), .b(n328), .O(n330));
  nand2 g240(.a(n330), .b(n271), .O(n331));
  inv1  g241(.a(n269), .O(n332));
  nand2 g242(.a(n332), .b(dest_y_16 ), .O(n333));
  nand2 g243(.a(n333), .b(n270), .O(n334));
  inv1  g244(.a(dest_y_15 ), .O(n335));
  nor2  g245(.a(n268), .b(n335), .O(n336));
  inv1  g246(.a(n268), .O(n337));
  nor2  g247(.a(n337), .b(dest_y_15 ), .O(n338));
  nor2  g248(.a(n338), .b(n336), .O(n339));
  inv1  g249(.a(n267), .O(n340));
  nor2  g250(.a(n340), .b(dest_y_14 ), .O(n341));
  nor2  g251(.a(n341), .b(n268), .O(n342));
  inv1  g252(.a(dest_y_13 ), .O(n343));
  nor2  g253(.a(n265), .b(n343), .O(n344));
  nor2  g254(.a(n344), .b(n267), .O(n345));
  nand2 g255(.a(n264), .b(dest_y_12 ), .O(n346));
  nand2 g256(.a(n346), .b(n266), .O(n347));
  inv1  g257(.a(n264), .O(n348));
  nand2 g258(.a(n263), .b(n262), .O(n349));
  nand2 g259(.a(n349), .b(n348), .O(n350));
  nor2  g260(.a(dest_y_2 ), .b(dest_y_1 ), .O(n351));
  inv1  g261(.a(dest_y_3 ), .O(n352));
  inv1  g262(.a(dest_y_4 ), .O(n353));
  nand2 g263(.a(n353), .b(n352), .O(n354));
  inv1  g264(.a(dest_y_5 ), .O(n355));
  inv1  g265(.a(dest_y_6 ), .O(n356));
  nand2 g266(.a(n356), .b(n355), .O(n357));
  nor2  g267(.a(n357), .b(n354), .O(n358));
  nand2 g268(.a(n358), .b(n351), .O(n359));
  inv1  g269(.a(dest_y_9 ), .O(n360));
  inv1  g270(.a(dest_y_10 ), .O(n361));
  nor2  g271(.a(n361), .b(n360), .O(n362));
  nor2  g272(.a(n362), .b(n263), .O(n363));
  inv1  g273(.a(dest_y_7 ), .O(n364));
  inv1  g274(.a(dest_y_8 ), .O(n365));
  nand2 g275(.a(n365), .b(n364), .O(n366));
  nor2  g276(.a(n366), .b(n360), .O(n367));
  nand2 g277(.a(n367), .b(n363), .O(n368));
  nor2  g278(.a(n368), .b(n359), .O(n369));
  nand2 g279(.a(n369), .b(n350), .O(n370));
  nor2  g280(.a(n370), .b(n347), .O(n371));
  nand2 g281(.a(n371), .b(n345), .O(n372));
  nor2  g282(.a(n372), .b(n342), .O(n373));
  nand2 g283(.a(n373), .b(n339), .O(n374));
  nor2  g284(.a(n374), .b(n334), .O(n375));
  nand2 g285(.a(n375), .b(n331), .O(n376));
  nor2  g286(.a(n376), .b(n327), .O(n377));
  nand2 g287(.a(n377), .b(n324), .O(n378));
  nor2  g288(.a(n378), .b(n320), .O(n379));
  nand2 g289(.a(n379), .b(n316), .O(n380));
  nor2  g290(.a(n380), .b(n312), .O(n381));
  nand2 g291(.a(n381), .b(n309), .O(n382));
  nor2  g292(.a(n382), .b(n305), .O(n383));
  nand2 g293(.a(n383), .b(n301), .O(n384));
  nor2  g294(.a(n384), .b(n296), .O(n385));
  nand2 g295(.a(n385), .b(n293), .O(n386));
  nor2  g296(.a(n386), .b(n289), .O(n387));
  nor2  g297(.a(n387), .b(n284), .O(n388));
  inv1  g298(.a(dest_y_0 ), .O(n389));
  nand2 g299(.a(n389), .b(n188), .O(n390));
  nand2 g300(.a(n390), .b(n283), .O(n391));
  nor2  g301(.a(n355), .b(n353), .O(n392));
  nor2  g302(.a(n364), .b(n356), .O(n393));
  nand2 g303(.a(n393), .b(n392), .O(n394));
  inv1  g304(.a(dest_y_1 ), .O(n395));
  nor2  g305(.a(n395), .b(n389), .O(n396));
  inv1  g306(.a(dest_y_2 ), .O(n397));
  nor2  g307(.a(n352), .b(n397), .O(n398));
  nand2 g308(.a(n398), .b(n396), .O(n399));
  nor2  g309(.a(n399), .b(n394), .O(n400));
  nor2  g310(.a(dest_y_9 ), .b(n365), .O(n401));
  nand2 g311(.a(n401), .b(dest_y_29 ), .O(n402));
  nor2  g312(.a(n402), .b(n363), .O(n403));
  nand2 g313(.a(n403), .b(n400), .O(n404));
  nor2  g314(.a(n404), .b(n350), .O(n405));
  nand2 g315(.a(n405), .b(n347), .O(n406));
  nor2  g316(.a(n406), .b(n345), .O(n407));
  nand2 g317(.a(n407), .b(n342), .O(n408));
  nor2  g318(.a(n408), .b(n339), .O(n409));
  nand2 g319(.a(n409), .b(n334), .O(n410));
  nor2  g320(.a(n410), .b(n331), .O(n411));
  nand2 g321(.a(n411), .b(n327), .O(n412));
  nor2  g322(.a(n412), .b(n324), .O(n413));
  nand2 g323(.a(n413), .b(n320), .O(n414));
  nor2  g324(.a(n414), .b(n316), .O(n415));
  nand2 g325(.a(n415), .b(n312), .O(n416));
  nor2  g326(.a(n416), .b(n309), .O(n417));
  nand2 g327(.a(n417), .b(n305), .O(n418));
  nor2  g328(.a(n418), .b(n301), .O(n419));
  nand2 g329(.a(n419), .b(n296), .O(n420));
  nor2  g330(.a(n420), .b(n293), .O(n421));
  nand2 g331(.a(n421), .b(n289), .O(n422));
  nand2 g332(.a(n422), .b(n391), .O(n423));
  nor2  g333(.a(n423), .b(n388), .O(n424));
  nand2 g334(.a(n284), .b(dest_x_0 ), .O(n425));
  nand2 g335(.a(n425), .b(n223), .O(n426));
  nor2  g336(.a(n426), .b(n424), .O(n427));
  nor2  g337(.a(n427), .b(n254), .O(outport_1 ));
  nand2 g338(.a(dest_y_0 ), .b(dest_x_0 ), .O(n429));
  nor2  g339(.a(n429), .b(n284), .O(n430));
  nor2  g340(.a(n430), .b(n388), .O(n431));
  nor2  g341(.a(n431), .b(outport_0 ), .O(outport_2 ));
endmodule


