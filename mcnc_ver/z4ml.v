// Benchmark "z4ml" written by ABC on Tue Nov  5 15:01:30 2019

module z4ml ( 
    1 , 2, 3, 4, 5, 6, 7,
    24, 25, 26, 27  );
  input  1 , 2, 3, 4, 5, 6, 7;
  output 24, 25, 26, 27;
  wire n12, n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25,
    n26, n27, n28, n29, n30, n32, n33, n34, n35, n36, n37, n38, n39, n40,
    n41, n42, n43, n44, n45, n46, n48, n49, n50, n51, n53, n54, n55, n56;
  inv1  g00(.a(1 ), .O(n12));
  nor2  g01(.a(7), .b(4), .O(n13));
  nor2  g02(.a(n13), .b(n12), .O(n14));
  nand2 g03(.a(7), .b(4), .O(n15));
  inv1  g04(.a(n15), .O(n16));
  nor2  g05(.a(n16), .b(n14), .O(n17));
  inv1  g06(.a(n17), .O(n18));
  nor2  g07(.a(6), .b(3), .O(n19));
  nor2  g08(.a(5), .b(2), .O(n20));
  nor2  g09(.a(n20), .b(n19), .O(n21));
  nand2 g10(.a(n21), .b(n18), .O(n22));
  inv1  g11(.a(2), .O(n23));
  nand2 g12(.a(6), .b(3), .O(n24));
  inv1  g13(.a(n24), .O(n25));
  nor2  g14(.a(n25), .b(5), .O(n26));
  nor2  g15(.a(n26), .b(n23), .O(n27));
  inv1  g16(.a(5), .O(n28));
  nor2  g17(.a(n24), .b(n28), .O(n29));
  nor2  g18(.a(n29), .b(n27), .O(n30));
  nand2 g19(.a(n30), .b(n22), .O(24));
  nor2  g20(.a(n28), .b(2), .O(n32));
  nor2  g21(.a(5), .b(n23), .O(n33));
  nor2  g22(.a(n33), .b(n32), .O(n34));
  nor2  g23(.a(n34), .b(n25), .O(n35));
  nand2 g24(.a(n35), .b(n17), .O(n36));
  inv1  g25(.a(n19), .O(n37));
  nand2 g26(.a(n34), .b(n37), .O(n38));
  nor2  g27(.a(n38), .b(n17), .O(n39));
  nand2 g28(.a(n34), .b(n25), .O(n40));
  nand2 g29(.a(5), .b(n23), .O(n41));
  nand2 g30(.a(n28), .b(2), .O(n42));
  nand2 g31(.a(n42), .b(n41), .O(n43));
  nand2 g32(.a(n43), .b(n19), .O(n44));
  nand2 g33(.a(n44), .b(n40), .O(n45));
  nor2  g34(.a(n45), .b(n39), .O(n46));
  nand2 g35(.a(n46), .b(n36), .O(25));
  nor2  g36(.a(n25), .b(n19), .O(n48));
  inv1  g37(.a(n48), .O(n49));
  nand2 g38(.a(n49), .b(n18), .O(n50));
  nand2 g39(.a(n48), .b(n17), .O(n51));
  nand2 g40(.a(n51), .b(n50), .O(26));
  nor2  g41(.a(n16), .b(n13), .O(n53));
  inv1  g42(.a(n53), .O(n54));
  nand2 g43(.a(n54), .b(1 ), .O(n55));
  nand2 g44(.a(n53), .b(n12), .O(n56));
  nand2 g45(.a(n56), .b(n55), .O(27));
endmodule


