// Benchmark "lal" written by ABC on Tue Nov  5 15:01:23 2019

module lal ( 
    a, b, c, d, e, f, g, h, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y,
    z, a0,
    b0, c0, d0, e0, f0, g0, h0, i0, j0, k0, l0, m0, n0, o0, p0, q0, r0, s0,
    t0  );
  input  a, b, c, d, e, f, g, h, j, k, l, m, n, o, p, q, r, s, t, u, v,
    w, x, y, z, a0;
  output b0, c0, d0, e0, f0, g0, h0, i0, j0, k0, l0, m0, n0, o0, p0, q0, r0,
    s0, t0;
  wire n46, n48, n49, n50, n51, n52, n53, n54, n55, n56, n57, n58, n59, n60,
    n61, n62, n63, n64, n65, n66, n68, n69, n70, n71, n72, n74, n75, n76,
    n77, n78, n79, n80, n81, n82, n83, n84, n85, n86, n87, n88, n89, n90,
    n91, n92, n93, n94, n95, n96, n99, n103, n104, n106, n107, n109, n110,
    n111, n112, n113, n115, n116, n117, n118, n120, n121, n122, n124, n125,
    n126, n127, n128, n129, n130, n131, n133, n134, n135, n136, n137, n138,
    n140, n141, n142, n143, n144, n145, n147, n148, n149, n150, n151, n152,
    n154, n155, n156, n157;
  inv1  g000(.a(j), .O(n46));
  nor2  g001(.a(r), .b(n46), .O(b0));
  inv1  g002(.a(v), .O(n48));
  inv1  g003(.a(u), .O(n49));
  nor2  g004(.a(t), .b(s), .O(n50));
  nand2 g005(.a(n50), .b(n49), .O(n51));
  nand2 g006(.a(n51), .b(n48), .O(n52));
  inv1  g007(.a(w), .O(n53));
  inv1  g008(.a(x), .O(n54));
  nor2  g009(.a(n54), .b(n53), .O(n55));
  nand2 g010(.a(n55), .b(n52), .O(n56));
  nor2  g011(.a(a0), .b(y), .O(n57));
  nand2 g012(.a(n57), .b(n56), .O(n58));
  nand2 g013(.a(f), .b(e), .O(n59));
  inv1  g014(.a(n59), .O(n60));
  inv1  g015(.a(h), .O(n61));
  inv1  g016(.a(z), .O(n62));
  inv1  g017(.a(a0), .O(n63));
  nand2 g018(.a(n63), .b(n62), .O(n64));
  nand2 g019(.a(n64), .b(n61), .O(n65));
  nor2  g020(.a(n65), .b(n60), .O(n66));
  nand2 g021(.a(n66), .b(n58), .O(c0));
  inv1  g022(.a(n52), .O(n68));
  nand2 g023(.a(n55), .b(z), .O(n69));
  nor2  g024(.a(n69), .b(n68), .O(n70));
  nand2 g025(.a(z), .b(y), .O(n71));
  nand2 g026(.a(n71), .b(n63), .O(n72));
  nor2  g027(.a(n72), .b(n70), .O(e0));
  inv1  g028(.a(c), .O(n74));
  nor2  g029(.a(m), .b(n74), .O(n75));
  inv1  g030(.a(d), .O(n76));
  nor2  g031(.a(n), .b(n76), .O(n77));
  nor2  g032(.a(n77), .b(n75), .O(n78));
  inv1  g033(.a(a), .O(n79));
  nor2  g034(.a(k), .b(n79), .O(n80));
  inv1  g035(.a(b), .O(n81));
  nor2  g036(.a(l), .b(n81), .O(n82));
  nor2  g037(.a(n82), .b(n80), .O(n83));
  nand2 g038(.a(n83), .b(n78), .O(n84));
  inv1  g039(.a(l), .O(n85));
  nor2  g040(.a(n85), .b(b), .O(n86));
  inv1  g041(.a(m), .O(n87));
  nor2  g042(.a(n87), .b(c), .O(n88));
  nor2  g043(.a(n88), .b(n86), .O(n89));
  inv1  g044(.a(n), .O(n90));
  nor2  g045(.a(n90), .b(d), .O(n91));
  inv1  g046(.a(k), .O(n92));
  nor2  g047(.a(n92), .b(a), .O(n93));
  nor2  g048(.a(n93), .b(n91), .O(n94));
  nand2 g049(.a(n94), .b(n89), .O(n95));
  nor2  g050(.a(n95), .b(n84), .O(n96));
  nor2  g051(.a(n96), .b(j), .O(f0));
  nor2  g052(.a(o), .b(j), .O(g0));
  inv1  g053(.a(p), .O(n99));
  nor2  g054(.a(n99), .b(j), .O(h0));
  nand2 g055(.a(n46), .b(g), .O(i0));
  inv1  g056(.a(e0), .O(j0));
  nor2  g057(.a(q), .b(h), .O(n103));
  inv1  g058(.a(n103), .O(n104));
  nor2  g059(.a(n104), .b(n59), .O(k0));
  nor2  g060(.a(n104), .b(n60), .O(n106));
  inv1  g061(.a(n106), .O(n107));
  nor2  g062(.a(n107), .b(s), .O(l0));
  inv1  g063(.a(t), .O(n109));
  nor2  g064(.a(n109), .b(s), .O(n110));
  inv1  g065(.a(s), .O(n111));
  nor2  g066(.a(t), .b(n111), .O(n112));
  nor2  g067(.a(n112), .b(n110), .O(n113));
  nor2  g068(.a(n113), .b(n107), .O(m0));
  nor2  g069(.a(n109), .b(n111), .O(n115));
  nor2  g070(.a(n115), .b(u), .O(n116));
  nand2 g071(.a(n115), .b(u), .O(n117));
  nand2 g072(.a(n117), .b(n106), .O(n118));
  nor2  g073(.a(n118), .b(n116), .O(n0));
  nand2 g074(.a(n117), .b(v), .O(n120));
  nor2  g075(.a(n117), .b(v), .O(n121));
  nor2  g076(.a(n121), .b(n107), .O(n122));
  nand2 g077(.a(n122), .b(n120), .O(o0));
  nand2 g078(.a(t), .b(s), .O(n124));
  nor2  g079(.a(n124), .b(n49), .O(n125));
  nand2 g080(.a(n125), .b(n48), .O(n126));
  nand2 g081(.a(n126), .b(w), .O(n127));
  nor2  g082(.a(w), .b(v), .O(n128));
  inv1  g083(.a(n128), .O(n129));
  nor2  g084(.a(n129), .b(n117), .O(n130));
  nor2  g085(.a(n130), .b(n107), .O(n131));
  nand2 g086(.a(n131), .b(n127), .O(p0));
  nand2 g087(.a(n128), .b(n125), .O(n133));
  nand2 g088(.a(n133), .b(x), .O(n134));
  nor2  g089(.a(x), .b(w), .O(n135));
  inv1  g090(.a(n135), .O(n136));
  nor2  g091(.a(n136), .b(n126), .O(n137));
  nor2  g092(.a(n137), .b(n107), .O(n138));
  nand2 g093(.a(n138), .b(n134), .O(q0));
  nand2 g094(.a(n135), .b(n121), .O(n140));
  nand2 g095(.a(n140), .b(y), .O(n141));
  nor2  g096(.a(y), .b(x), .O(n142));
  inv1  g097(.a(n142), .O(n143));
  nor2  g098(.a(n143), .b(n133), .O(n144));
  nor2  g099(.a(n144), .b(n107), .O(n145));
  nand2 g100(.a(n145), .b(n141), .O(r0));
  nand2 g101(.a(n142), .b(n130), .O(n147));
  nand2 g102(.a(n147), .b(z), .O(n148));
  nor2  g103(.a(z), .b(y), .O(n149));
  inv1  g104(.a(n149), .O(n150));
  nor2  g105(.a(n150), .b(n140), .O(n151));
  nor2  g106(.a(n151), .b(n107), .O(n152));
  nand2 g107(.a(n152), .b(n148), .O(s0));
  nand2 g108(.a(n149), .b(n137), .O(n154));
  nand2 g109(.a(n154), .b(a0), .O(n155));
  nor2  g110(.a(n147), .b(n64), .O(n156));
  nor2  g111(.a(n156), .b(n107), .O(n157));
  nand2 g112(.a(n157), .b(n155), .O(t0));
  buf   g113(.a(r), .O(d0));
endmodule


