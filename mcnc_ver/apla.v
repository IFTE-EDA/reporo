// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:16 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7, v8, v9,
    v10.0 , v10.1 , v10.2 , v10.3 , v10.4 , v10.5 , v10.6 , v10.7 ,
    v10.8 , v10.9 , v10.10 , v10.11   );
  input  v0, v1, v2, v3, v4, v5, v6, v7, v8, v9;
  output v10.0 , v10.1 , v10.2 , v10.3 , v10.4 , v10.5 , v10.6 ,
    v10.7 , v10.8 , v10.9 , v10.10 , v10.11 ;
  wire n23, n24, n25, n26, n27, n28, n29, n30, n31, n32, n33, n34, n35, n36,
    n37, n38, n39, n40, n41, n42, n43, n44, n45, n46, n47, n48, n49, n50,
    n51, n52, n53, n54, n55, n56, n57, n58, n59, n60, n61, n62, n63, n64,
    n65, n66, n67, n69, n70, n71, n72, n73, n74, n75, n76, n77, n78, n79,
    n80, n81, n82, n83, n84, n86, n87, n88, n89, n90, n91, n92, n93, n94,
    n95, n96, n97, n98, n99, n100, n101, n102, n103, n104, n105, n106,
    n107, n108, n109, n110, n111, n112, n113, n114, n115, n116, n117, n118,
    n119, n120, n121, n122, n123, n124, n125, n127, n128, n129, n130, n131,
    n132, n133, n134, n135, n136, n137, n138, n139, n140, n141, n143, n144,
    n145, n146, n147, n148, n149, n150, n151, n152, n153, n154, n155, n156,
    n157, n158, n159, n160, n161, n162, n163, n164, n166, n167, n168, n169,
    n170, n171, n173, n174, n175, n177, n178, n179, n180, n181, n182, n183,
    n184, n185, n186, n187, n188, n189, n190, n191, n192, n193, n194, n195,
    n196, n197, n198, n199, n200, n201, n202, n203, n204, n205, n206, n207,
    n208, n209, n210, n211, n213, n214, n215, n216, n217, n218, n219, n220,
    n221, n222, n223, n224, n225, n226, n227, n228, n229, n230, n231, n233,
    n234, n235, n236, n237, n238, n239, n240, n241, n242, n243, n244, n245,
    n246, n247, n248, n249, n251, n252, n253, n254, n255, n256, n257, n258,
    n259, n260, n261, n262, n263, n264, n266, n267, n268, n269, n270, n271,
    n272, n273, n274, n275, n276, n277, n278, n279, n280, n281, n282, n283,
    n284, n285;
  inv1  g000(.a(v8), .O(n23));
  inv1  g001(.a(v6), .O(n24));
  nor2  g002(.a(n24), .b(v5), .O(n25));
  inv1  g003(.a(v5), .O(n26));
  nor2  g004(.a(v6), .b(n26), .O(n27));
  nor2  g005(.a(n27), .b(n25), .O(n28));
  nor2  g006(.a(n28), .b(v4), .O(n29));
  inv1  g007(.a(v4), .O(n30));
  nor2  g008(.a(v6), .b(v5), .O(n31));
  inv1  g009(.a(n31), .O(n32));
  nor2  g010(.a(n32), .b(n30), .O(n33));
  nor2  g011(.a(n33), .b(n29), .O(n34));
  nor2  g012(.a(n34), .b(n23), .O(n35));
  nor2  g013(.a(n26), .b(v4), .O(n36));
  nor2  g014(.a(v5), .b(n30), .O(n37));
  nor2  g015(.a(n37), .b(n36), .O(n38));
  inv1  g016(.a(v9), .O(n39));
  nor2  g017(.a(v8), .b(v6), .O(n40));
  nand2 g018(.a(n40), .b(n39), .O(n41));
  nor2  g019(.a(n41), .b(n38), .O(n42));
  nor2  g020(.a(n42), .b(n35), .O(n43));
  nor2  g021(.a(n43), .b(v7), .O(n44));
  inv1  g022(.a(n33), .O(n45));
  nand2 g023(.a(v9), .b(n23), .O(n46));
  inv1  g024(.a(n46), .O(n47));
  nand2 g025(.a(n47), .b(v7), .O(n48));
  nor2  g026(.a(n48), .b(n45), .O(n49));
  nor2  g027(.a(n49), .b(n44), .O(n50));
  nor2  g028(.a(n50), .b(v1), .O(n51));
  nand2 g029(.a(n39), .b(v8), .O(n52));
  nand2 g030(.a(n52), .b(n46), .O(n53));
  nand2 g031(.a(n53), .b(v7), .O(n54));
  inv1  g032(.a(n54), .O(n55));
  nor2  g033(.a(v9), .b(v8), .O(n56));
  inv1  g034(.a(n56), .O(n57));
  nor2  g035(.a(n57), .b(v7), .O(n58));
  nor2  g036(.a(n58), .b(n55), .O(n59));
  inv1  g037(.a(v1), .O(n60));
  nor2  g038(.a(v4), .b(n60), .O(n61));
  nand2 g039(.a(n61), .b(n31), .O(n62));
  nor2  g040(.a(n62), .b(n59), .O(n63));
  nor2  g041(.a(n63), .b(n51), .O(n64));
  inv1  g042(.a(v3), .O(n65));
  nor2  g043(.a(v2), .b(v0), .O(n66));
  nand2 g044(.a(n66), .b(n65), .O(n67));
  nor2  g045(.a(n67), .b(n64), .O(v10.0 ));
  inv1  g046(.a(v7), .O(n69));
  nor2  g047(.a(n57), .b(n45), .O(n70));
  nor2  g048(.a(n70), .b(n35), .O(n71));
  nor2  g049(.a(n71), .b(n69), .O(n72));
  inv1  g050(.a(n38), .O(n73));
  nand2 g051(.a(n47), .b(n73), .O(n74));
  nor2  g052(.a(v7), .b(v6), .O(n75));
  inv1  g053(.a(n75), .O(n76));
  nor2  g054(.a(n76), .b(n74), .O(n77));
  nor2  g055(.a(n77), .b(n72), .O(n78));
  nor2  g056(.a(n78), .b(v1), .O(n79));
  nor2  g057(.a(n53), .b(n69), .O(n80));
  nor2  g058(.a(n46), .b(v7), .O(n81));
  nor2  g059(.a(n81), .b(n80), .O(n82));
  nor2  g060(.a(n82), .b(n62), .O(n83));
  nor2  g061(.a(n83), .b(n79), .O(n84));
  nor2  g062(.a(n84), .b(n67), .O(v10.1 ));
  nor2  g063(.a(n24), .b(v3), .O(n86));
  nor2  g064(.a(v6), .b(n65), .O(n87));
  nor2  g065(.a(n87), .b(n86), .O(n88));
  nor2  g066(.a(n88), .b(v2), .O(n89));
  inv1  g067(.a(v2), .O(n90));
  nand2 g068(.a(n24), .b(n65), .O(n91));
  nor2  g069(.a(n91), .b(n90), .O(n92));
  nor2  g070(.a(n92), .b(n89), .O(n93));
  nor2  g071(.a(n93), .b(v0), .O(n94));
  inv1  g072(.a(v0), .O(n95));
  nor2  g073(.a(v2), .b(n95), .O(n96));
  inv1  g074(.a(n96), .O(n97));
  nor2  g075(.a(n97), .b(n91), .O(n98));
  nor2  g076(.a(n98), .b(n94), .O(n99));
  nor2  g077(.a(n99), .b(v8), .O(n100));
  nor2  g078(.a(n65), .b(v2), .O(n101));
  nor2  g079(.a(v3), .b(n90), .O(n102));
  nor2  g080(.a(n102), .b(n101), .O(n103));
  nor2  g081(.a(n103), .b(v0), .O(n104));
  nand2 g082(.a(n65), .b(n90), .O(n105));
  nor2  g083(.a(n105), .b(n95), .O(n106));
  nor2  g084(.a(n106), .b(n104), .O(n107));
  nand2 g085(.a(n75), .b(v8), .O(n108));
  nor2  g086(.a(n108), .b(n107), .O(n109));
  nor2  g087(.a(n109), .b(n100), .O(n110));
  nor2  g088(.a(n110), .b(v5), .O(n111));
  nor2  g089(.a(v8), .b(n69), .O(n112));
  nand2 g090(.a(n112), .b(n27), .O(n113));
  nor2  g091(.a(n113), .b(n67), .O(n114));
  nor2  g092(.a(n114), .b(n111), .O(n115));
  nor2  g093(.a(n115), .b(v1), .O(n116));
  nor2  g094(.a(n60), .b(v0), .O(n117));
  inv1  g095(.a(n117), .O(n118));
  nor2  g096(.a(n118), .b(n105), .O(n119));
  inv1  g097(.a(n119), .O(n120));
  nor2  g098(.a(n23), .b(v7), .O(n121));
  nand2 g099(.a(n121), .b(n31), .O(n122));
  nor2  g100(.a(n122), .b(n120), .O(n123));
  nor2  g101(.a(n123), .b(n116), .O(n124));
  nand2 g102(.a(n39), .b(n30), .O(n125));
  nor2  g103(.a(n125), .b(n124), .O(v10.2 ));
  nor2  g104(.a(n90), .b(v0), .O(n127));
  nor2  g105(.a(n127), .b(n96), .O(n128));
  nor2  g106(.a(n128), .b(v6), .O(n129));
  inv1  g107(.a(n66), .O(n130));
  nand2 g108(.a(n23), .b(v6), .O(n131));
  nor2  g109(.a(n131), .b(n130), .O(n132));
  nor2  g110(.a(n132), .b(n129), .O(n133));
  nor2  g111(.a(n133), .b(v3), .O(n134));
  nand2 g112(.a(n40), .b(v3), .O(n135));
  nor2  g113(.a(n135), .b(n130), .O(n136));
  nor2  g114(.a(n136), .b(n134), .O(n137));
  nor2  g115(.a(v4), .b(v1), .O(n138));
  nand2 g116(.a(n69), .b(n26), .O(n139));
  nor2  g117(.a(n139), .b(n39), .O(n140));
  nand2 g118(.a(n140), .b(n138), .O(n141));
  nor2  g119(.a(n141), .b(n137), .O(v10.3 ));
  nor2  g120(.a(n107), .b(n54), .O(n143));
  nand2 g121(.a(n101), .b(n95), .O(n144));
  nor2  g122(.a(n39), .b(n23), .O(n145));
  nand2 g123(.a(n145), .b(n69), .O(n146));
  nor2  g124(.a(n146), .b(n144), .O(n147));
  nor2  g125(.a(n147), .b(n143), .O(n148));
  nor2  g126(.a(n148), .b(v6), .O(n149));
  nor2  g127(.a(n69), .b(n24), .O(n150));
  nand2 g128(.a(n150), .b(n47), .O(n151));
  nor2  g129(.a(n151), .b(n67), .O(n152));
  nor2  g130(.a(n152), .b(n149), .O(n153));
  nor2  g131(.a(n153), .b(v5), .O(n154));
  inv1  g132(.a(n91), .O(n155));
  nor2  g133(.a(n69), .b(n26), .O(n156));
  nand2 g134(.a(n156), .b(n155), .O(n157));
  nand2 g135(.a(n66), .b(n47), .O(n158));
  nor2  g136(.a(n158), .b(n157), .O(n159));
  nor2  g137(.a(n159), .b(n154), .O(n160));
  nor2  g138(.a(n160), .b(v1), .O(n161));
  nand2 g139(.a(n119), .b(n31), .O(n162));
  nor2  g140(.a(n162), .b(n146), .O(n163));
  nor2  g141(.a(n163), .b(n161), .O(n164));
  nor2  g142(.a(n164), .b(v4), .O(v10.4 ));
  nor2  g143(.a(n69), .b(v5), .O(n166));
  nand2 g144(.a(n166), .b(n145), .O(n167));
  inv1  g145(.a(n167), .O(n168));
  inv1  g146(.a(n138), .O(n169));
  nor2  g147(.a(n169), .b(n91), .O(n170));
  nand2 g148(.a(n170), .b(n168), .O(n171));
  nor2  g149(.a(n171), .b(n128), .O(v10.5 ));
  inv1  g150(.a(n87), .O(n173));
  nor2  g151(.a(n169), .b(n173), .O(n174));
  nand2 g152(.a(n174), .b(n66), .O(n175));
  nor2  g153(.a(n175), .b(n167), .O(v10.6 ));
  nor2  g154(.a(n23), .b(v6), .O(n177));
  inv1  g155(.a(n177), .O(n178));
  nor2  g156(.a(n178), .b(n26), .O(n179));
  nor2  g157(.a(n179), .b(n25), .O(n180));
  nor2  g158(.a(n180), .b(v4), .O(n181));
  nand2 g159(.a(n177), .b(n37), .O(n182));
  inv1  g160(.a(n36), .O(n183));
  nor2  g161(.a(n183), .b(v6), .O(n184));
  nand2 g162(.a(n184), .b(n112), .O(n185));
  nand2 g163(.a(n185), .b(n182), .O(n186));
  nor2  g164(.a(n186), .b(n181), .O(n187));
  nor2  g165(.a(n187), .b(v3), .O(n188));
  nor2  g166(.a(n23), .b(n69), .O(n189));
  inv1  g167(.a(n189), .O(n190));
  nor2  g168(.a(v5), .b(v4), .O(n191));
  nand2 g169(.a(n191), .b(n87), .O(n192));
  nor2  g170(.a(n192), .b(n190), .O(n193));
  nor2  g171(.a(n193), .b(n188), .O(n194));
  nor2  g172(.a(n194), .b(v2), .O(n195));
  inv1  g173(.a(n102), .O(n196));
  nor2  g174(.a(n196), .b(v4), .O(n197));
  nor2  g175(.a(v8), .b(v7), .O(n198));
  inv1  g176(.a(n198), .O(n199));
  nor2  g177(.a(n199), .b(n32), .O(n200));
  nand2 g178(.a(n200), .b(n197), .O(n201));
  nand2 g179(.a(n102), .b(v9), .O(n202));
  nand2 g180(.a(n101), .b(n39), .O(n203));
  nand2 g181(.a(n203), .b(n202), .O(n204));
  inv1  g182(.a(n191), .O(n205));
  nand2 g183(.a(n40), .b(v7), .O(n206));
  nor2  g184(.a(n206), .b(n205), .O(n207));
  nand2 g185(.a(n207), .b(n204), .O(n208));
  nand2 g186(.a(n208), .b(n201), .O(n209));
  nor2  g187(.a(n209), .b(n195), .O(n210));
  nand2 g188(.a(n60), .b(n95), .O(n211));
  nor2  g189(.a(n211), .b(n210), .O(v10.7 ));
  nand2 g190(.a(v8), .b(n26), .O(n213));
  nor2  g191(.a(n213), .b(n128), .O(n214));
  nand2 g192(.a(n66), .b(v5), .O(n215));
  nor2  g193(.a(n215), .b(n199), .O(n216));
  nor2  g194(.a(n216), .b(n214), .O(n217));
  nor2  g195(.a(n217), .b(v4), .O(n218));
  nand2 g196(.a(n66), .b(n37), .O(n219));
  nor2  g197(.a(n219), .b(n199), .O(n220));
  nor2  g198(.a(n220), .b(n218), .O(n221));
  nor2  g199(.a(n221), .b(v1), .O(n222));
  nor2  g200(.a(n118), .b(v2), .O(n223));
  nor2  g201(.a(n199), .b(n205), .O(n224));
  nand2 g202(.a(n224), .b(n223), .O(n225));
  inv1  g203(.a(n128), .O(n226));
  nand2 g204(.a(n166), .b(n56), .O(n227));
  nor2  g205(.a(n227), .b(n169), .O(n228));
  nand2 g206(.a(n228), .b(n226), .O(n229));
  nand2 g207(.a(n229), .b(n225), .O(n230));
  nor2  g208(.a(n230), .b(n222), .O(n231));
  nor2  g209(.a(n231), .b(n91), .O(v10.8 ));
  inv1  g210(.a(n146), .O(n233));
  nor2  g211(.a(n233), .b(n80), .O(n234));
  nor2  g212(.a(n234), .b(n34), .O(n235));
  nand2 g213(.a(v7), .b(n24), .O(n236));
  nor2  g214(.a(n236), .b(n74), .O(n237));
  nor2  g215(.a(n237), .b(n235), .O(n238));
  nor2  g216(.a(n238), .b(v3), .O(n239));
  nor2  g217(.a(n198), .b(v9), .O(n240));
  nor2  g218(.a(n240), .b(n233), .O(n241));
  nor2  g219(.a(n241), .b(n192), .O(n242));
  nor2  g220(.a(n242), .b(n239), .O(n243));
  nor2  g221(.a(n243), .b(v1), .O(n244));
  nor2  g222(.a(n121), .b(n112), .O(n245));
  nor2  g223(.a(n91), .b(v5), .O(n246));
  nand2 g224(.a(n246), .b(n61), .O(n247));
  nor2  g225(.a(n247), .b(n245), .O(n248));
  nor2  g226(.a(n248), .b(n244), .O(n249));
  nor2  g227(.a(n249), .b(n130), .O(v10.9 ));
  nor2  g228(.a(n137), .b(v7), .O(n251));
  inv1  g229(.a(n53), .O(n252));
  inv1  g230(.a(n129), .O(n253));
  nor2  g231(.a(n253), .b(n252), .O(n254));
  nand2 g232(.a(n47), .b(v6), .O(n255));
  nor2  g233(.a(n255), .b(n130), .O(n256));
  nor2  g234(.a(n256), .b(n254), .O(n257));
  nor2  g235(.a(n257), .b(v3), .O(n258));
  nand2 g236(.a(n47), .b(n24), .O(n259));
  nor2  g237(.a(n259), .b(n144), .O(n260));
  nor2  g238(.a(n260), .b(n258), .O(n261));
  nor2  g239(.a(n261), .b(n69), .O(n262));
  nor2  g240(.a(n262), .b(n251), .O(n263));
  nand2 g241(.a(n138), .b(n26), .O(n264));
  nor2  g242(.a(n264), .b(n263), .O(v10.10 ));
  nor2  g243(.a(v6), .b(n30), .O(n266));
  nand2 g244(.a(v6), .b(n30), .O(n267));
  nor2  g245(.a(n267), .b(n23), .O(n268));
  nor2  g246(.a(n268), .b(n266), .O(n269));
  nor2  g247(.a(n269), .b(v5), .O(n270));
  nor2  g248(.a(n178), .b(n183), .O(n271));
  nor2  g249(.a(n271), .b(n270), .O(n272));
  nor2  g250(.a(n272), .b(v1), .O(n273));
  inv1  g251(.a(n61), .O(n274));
  nor2  g252(.a(n274), .b(v5), .O(n275));
  nand2 g253(.a(n275), .b(n40), .O(n276));
  nor2  g254(.a(v5), .b(n60), .O(n277));
  nand2 g255(.a(n277), .b(n189), .O(n278));
  nor2  g256(.a(n26), .b(v1), .O(n279));
  nand2 g257(.a(n279), .b(n198), .O(n280));
  nand2 g258(.a(n280), .b(n278), .O(n281));
  nor2  g259(.a(v6), .b(v4), .O(n282));
  nand2 g260(.a(n282), .b(n281), .O(n283));
  nand2 g261(.a(n283), .b(n276), .O(n284));
  nor2  g262(.a(n284), .b(n273), .O(n285));
  nor2  g263(.a(n285), .b(n67), .O(v10.11 ));
endmodule


