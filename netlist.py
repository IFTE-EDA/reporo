# Netlist class

from collections import defaultdict
import os
import re

from partitioning import FMPartitioning
import technology as tech

class Netlist:
    def __init__(self, directory, netlist_name):
        self.directory = directory
        self.netlist_name = netlist_name
        self.netlist_path = os.path.join(directory, netlist_name)
        with open(self.netlist_path, 'r') as nl:
            self.original_netlist = nl.read()
        self.transformed_netlist = self.original_netlist
        # Pattern that matches gate instance definitions; "(?!module\s)" is a
        # negative lookahead assertion that prohibits matching of the
        # "module xyz (" line that starts a module definition.
        # This pattern does not match the connected nets, because it's used to
        # change the gate type without affecting the connectivity.
        self.gate_pattern = r'^(\s*)(?!module\s)({0})(\s+)({1})(\s*\()'
        self.gate_w_nets_pattern = self.gate_pattern + r'(.*)\);'
        self.module_name = re.search(r'\s*module\s+(\S+)\s*\(', self.original_netlist).group(1)
        # get all inputs and outputs (used for cut cost calculation)
        self.inputs = set([s.strip() for s in re.search(r'^\s*input\s+([^;]+);', self.original_netlist, re.MULTILINE).group(1).split(',')])
        self.outputs = set([s.strip() for s in re.search(r'^\s*output\s+([^;]+);', self.original_netlist, re.MULTILINE).group(1).split(',')])

        self.replaced_gates = []

        self.gates = set()
        # map from gate name to cell name
        self.gate_to_cell = {}
        # map from gate name to connected nets
        self.gate_to_connected_nets = {}
        # map from nets to names of connected gates
        self.net_to_connected_gates = defaultdict(set)

        # get all gate definitions from netlist
        gate_w_nets_pattern = re.compile(self.gate_w_nets_pattern.format(r'\w+', r'\w+'), re.MULTILINE)
        for match in re.finditer(gate_w_nets_pattern, self.original_netlist):
            cell = match.group(2)
            name = match.group(4)
            connectivity = match.group(6)
            # .+? is a non-greedy match; nets is an array of connected nets
            nets = re.findall(r'\(\s*(.+?)\s*\)', connectivity)

            self.gates.add(name)
            self.gate_to_cell[name] = cell
            self.gate_to_connected_nets[name] = frozenset(nets)
            for net in nets:
                self.net_to_connected_gates[net].add(name)

    def count_instances(self, gates=None):
        """ Counts the number of gate instances of a given type (or of
        arbitrary type if gates=None) in the original netlist. The parameter
        gates is an array of gate names to count."""
        num_instances = 0
        if gates is None:
            gates = [r'\w+']
        for gate in gates:
            matches = re.findall(self.gate_pattern.format(gate, r'\w+'), self.original_netlist, re.MULTILINE)
            num_instances += len(matches)
        return num_instances

    def add_input(self, input_name):
        """Add new input signal to the (transformed) netlist."""
        self.transformed_netlist = re.sub(
            r'^(\s*module.+\(\s*)',
            r'\1' + input_name + ', ',
            self.transformed_netlist,
            1,
            re.MULTILINE)
        self.transformed_netlist = re.sub(
            r'^(\s*input\s+)',
            r'\1' + input_name + ', ',
            self.transformed_netlist,
            1,
            re.MULTILINE)

    def add_wire(self, wire_name):
        """Add new wire to the (transformed) netlist."""
        self.transformed_netlist = re.sub(
            r'^(\s*wire\s+)',
            r'\1' + wire_name + ', ',
            self.transformed_netlist,
            1,
            re.MULTILINE)

    def add_gate(self, gate, name, pins):
        """Add new gate to the (transformed) netlist."""
        # this pattern also matches the preceeding line to get the correct indentation
        endmodule_pattern = re.compile(r'^(\s*)(.*)\n(\s*)endmodule', re.MULTILINE)
        # \1 = identation of last gate line, \2 = last gate line,
        # \3 = indentation before 'endmodule'
        replacement = r'\1\2\n\1' + gate + ' ' + name + '(' + pins + r');\n\3endmodule'
        self.transformed_netlist = re.sub(
            endmodule_pattern,
            replacement,
            self.transformed_netlist)

    def get_all_instances(self, gates):
        """Find all instances of given gates."""
        pattern = re.compile(self.gate_pattern.format('|'.join(gates), r'\w+'), re.MULTILINE)
        # get all matching gate instances
        gate_instances = re.findall(pattern, self.transformed_netlist)
        # get instance names
        gate_instances = [gi[3] for gi in gate_instances]
        return gate_instances

    def get_some_instances(self, replacement_ratio, gates):
        """Find some instances of given gates, honoring the given replacement ratio."""
        matches = self.get_all_instances(gates)
        num_relevant_gates = len(matches)
        num_gates_to_replace = self.get_num_gates_to_replace(replacement_ratio, num_relevant_gates)
        if num_gates_to_replace == 0:
            return []
        else:
            gate_instances = []
            for i,m in enumerate(matches):
                if len(gate_instances)/(i+1) < replacement_ratio:
                    gate_instances.append(m)

            #gate_instances = [m for i,m in enumerate(matches) if i%10<(replacement_ratio * 10)
            #stride = round(num_relevant_gates/num_gates_to_replace)
            # For the first num_gates_to_replace number of matches, extract the
            # instance names and add them to the list of replaced gates.
            #gate_instances = matches[::stride]
            return gate_instances

    def get_gates(self):
        return self.gates

    def get_cell(self, gate):
        return self.gate_to_cell[gate]

    def get_connected_nets(self, gate):
        return self.gate_to_connected_nets[gate]

    def get_nets(self):
        return self.net_to_connected_gates.keys()

    def get_connected_gates(self, net):
        return self.net_to_connected_gates[net]

    def partition(self, replacement_ratio, gates):
        """Partition the netlist in two partitions depending on the given ration and gates."""
        print("**************")
        print("* Partitioning")
        print("**************")
        p = FMPartitioning(self, replacement_ratio, gates)
        p.optimize()
        # check that all gates in the partition are instances of the given cells
        assert(set([self.get_cell(gate) in gates for gate in p.best_partition]) == {True})
        return p.best_partition

    def get_num_gates_to_replace(self, replacement_ratio, num_relevant_gates):
        return round(replacement_ratio * num_relevant_gates)

    def replace_gates(self, gate_instances, new_gate, new_pin = ''):
        num_gates_to_replace = len(gate_instances)

        if new_pin:
            new_pin += ', '
        if num_gates_to_replace > 0:
            # Replace the first num_gates_to_replace gates.
            #self.transformed_netlist = re.sub(
            #    pattern,
            #    r'\1' + new_gate + r'\3\4\5' + new_pin,
            #    self.transformed_netlist,
            #    num_gates_to_replace)

            # Replace every nth gate
            for instance_name in gate_instances:
                pattern = re.compile(self.gate_pattern.format(r'\w+', instance_name), re.MULTILINE)
                self.transformed_netlist = re.sub(
                    pattern,
                    r'\1' + new_gate + r'\3\4\5' + new_pin,
                    self.transformed_netlist,
                    1)

        self.replaced_gates += gate_instances


    def replace_gate_ratio(self, replacement_ratio, gates_to_replace, new_gate, new_pin = ''):
        if replacement_ratio > 0.0:
            num_relevant_gates = self.count_instances(gates_to_replace)
            #gate_instances = self.get_some_instances(replacement_ratio, gates_to_replace)
            gate_instances = self.partition(replacement_ratio, gates_to_replace)
            print('************************************')
            print('* Replacement ratio: ' + str(replacement_ratio))
            print('* Found ' + str(num_relevant_gates) + ' relevant gates')
            print('* Replacing ' + str(len(gate_instances)) + ' of them')
            print('************************************')
            self.replace_gates(gate_instances, new_gate, new_pin)

    def calculate_area(self):
        """Calculate the area of the gates in the transformed netlist."""
        gate_pattern = re.compile(self.gate_pattern.format(r'\w+', r'\w+'), re.MULTILINE)
        total_area = 0.0
        for match in re.finditer(gate_pattern, self.transformed_netlist):
            name = match.group(2)
            total_area += tech.area[name]
        return total_area

    def calculate_static_power(self):
        """Calculate the static power of the transformed netlist."""
        #gate_pattern = re.compile(r'^\s*(\w+)\s+\w+\s*\(', re.MULTILINE)
        gate_pattern = re.compile(self.gate_pattern.format(r'\w+', r'\w+'), re.MULTILINE)
        power_consumption = 0.0
        for match in re.finditer(gate_pattern, self.transformed_netlist):
            name = match.group(2)
            power_consumption += tech.static_power[name]
        return power_consumption

    def write_transformed_netlist(self, new_netlist_path):
        with open(new_netlist_path, 'w') as new_netlist:
            new_netlist.write(self.transformed_netlist)
