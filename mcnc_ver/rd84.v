// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:26 2019

module source_pla  ( 
    i_0_, i_1_, i_2_, i_3_, i_4_, i_5_, i_6_, i_7_,
    o_0_, o_1_, o_2_, o_3_  );
  input  i_0_, i_1_, i_2_, i_3_, i_4_, i_5_, i_6_, i_7_;
  output o_0_, o_1_, o_2_, o_3_;
  wire n13, n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26,
    n27, n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40,
    n41, n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53, n54,
    n55, n56, n57, n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68,
    n69, n70, n71, n72, n73, n74, n75, n76, n77, n78, n79, n80, n81, n82,
    n83, n84, n85, n86, n87, n88, n89, n90, n91, n92, n93, n94, n95, n96,
    n97, n98, n99, n100, n101, n102, n103, n104, n105, n106, n107, n108,
    n109, n110, n111, n112, n113, n114, n115, n116, n117, n118, n119, n120,
    n121, n122, n123, n124, n125, n126, n127, n128, n129, n130, n131, n132,
    n133, n134, n135, n136, n137, n138, n139, n140, n141, n142, n143, n144,
    n146, n147, n148, n149, n150, n151, n152, n153, n154, n155, n156, n157,
    n158, n159, n160, n161, n162, n163, n164, n165, n166, n167, n168, n169,
    n170, n171, n173, n174, n175, n176, n178, n179, n180, n181, n182, n183,
    n184, n185, n186, n187, n188, n189, n190, n191, n192, n193, n194, n195,
    n196, n197, n198, n199, n200, n201, n202, n203, n204, n205, n206, n207,
    n208, n209, n210, n211, n212, n213, n214, n215, n216, n217, n218, n219,
    n220, n221, n222, n223, n224, n225, n226, n227, n228, n229, n230, n231,
    n232, n233, n234, n235, n236, n237, n238, n239, n240, n241, n242, n243,
    n244, n245, n246, n247, n248, n249, n250, n251, n252, n253, n254, n255,
    n256, n257;
  inv1  g000(.a(i_4_), .O(n13));
  nor2  g001(.a(n13), .b(i_3_), .O(n14));
  inv1  g002(.a(i_3_), .O(n15));
  nor2  g003(.a(i_4_), .b(n15), .O(n16));
  nor2  g004(.a(n16), .b(n14), .O(n17));
  inv1  g005(.a(i_5_), .O(n18));
  nand2 g006(.a(i_6_), .b(n18), .O(n19));
  inv1  g007(.a(i_6_), .O(n20));
  nand2 g008(.a(n20), .b(i_5_), .O(n21));
  nand2 g009(.a(n21), .b(n19), .O(n22));
  inv1  g010(.a(n22), .O(n23));
  inv1  g011(.a(i_0_), .O(n24));
  inv1  g012(.a(i_7_), .O(n25));
  inv1  g013(.a(i_1_), .O(n26));
  inv1  g014(.a(i_2_), .O(n27));
  nor2  g015(.a(n27), .b(n26), .O(n28));
  inv1  g016(.a(n28), .O(n29));
  nor2  g017(.a(n29), .b(n25), .O(n30));
  nor2  g018(.a(i_2_), .b(i_1_), .O(n31));
  inv1  g019(.a(n31), .O(n32));
  nor2  g020(.a(n32), .b(i_7_), .O(n33));
  nor2  g021(.a(n33), .b(n30), .O(n34));
  nor2  g022(.a(n34), .b(n24), .O(n35));
  nor2  g023(.a(n27), .b(i_1_), .O(n36));
  nor2  g024(.a(i_2_), .b(n26), .O(n37));
  nor2  g025(.a(n37), .b(n36), .O(n38));
  nand2 g026(.a(n25), .b(n24), .O(n39));
  nor2  g027(.a(n39), .b(n38), .O(n40));
  nor2  g028(.a(n40), .b(n35), .O(n41));
  nor2  g029(.a(n41), .b(n23), .O(n42));
  nor2  g030(.a(i_1_), .b(i_0_), .O(n43));
  inv1  g031(.a(n43), .O(n44));
  nor2  g032(.a(n44), .b(i_2_), .O(n45));
  inv1  g033(.a(n45), .O(n46));
  nor2  g034(.a(n25), .b(i_6_), .O(n47));
  nand2 g035(.a(n47), .b(n18), .O(n48));
  nor2  g036(.a(i_7_), .b(n20), .O(n49));
  nand2 g037(.a(n49), .b(i_5_), .O(n50));
  nand2 g038(.a(n50), .b(n48), .O(n51));
  inv1  g039(.a(n51), .O(n52));
  nor2  g040(.a(n52), .b(n46), .O(n53));
  nor2  g041(.a(n53), .b(n42), .O(n54));
  nor2  g042(.a(n54), .b(n17), .O(n55));
  nor2  g043(.a(n25), .b(n20), .O(n56));
  nand2 g044(.a(n56), .b(i_5_), .O(n57));
  nor2  g045(.a(i_7_), .b(i_6_), .O(n58));
  nand2 g046(.a(n58), .b(n18), .O(n59));
  nand2 g047(.a(n59), .b(n57), .O(n60));
  nor2  g048(.a(n38), .b(n24), .O(n61));
  nor2  g049(.a(n29), .b(i_0_), .O(n62));
  nor2  g050(.a(n62), .b(n61), .O(n63));
  nor2  g051(.a(n63), .b(n17), .O(n64));
  nor2  g052(.a(n13), .b(n15), .O(n65));
  nand2 g053(.a(n65), .b(n31), .O(n66));
  nor2  g054(.a(i_4_), .b(i_3_), .O(n67));
  nand2 g055(.a(n67), .b(n28), .O(n68));
  nand2 g056(.a(n68), .b(n66), .O(n69));
  nand2 g057(.a(n69), .b(i_0_), .O(n70));
  nor2  g058(.a(n15), .b(i_0_), .O(n71));
  nand2 g059(.a(n71), .b(i_4_), .O(n72));
  nor2  g060(.a(n72), .b(n38), .O(n73));
  inv1  g061(.a(n73), .O(n74));
  nand2 g062(.a(n74), .b(n70), .O(n75));
  nor2  g063(.a(n75), .b(n64), .O(n76));
  inv1  g064(.a(n76), .O(n77));
  nand2 g065(.a(n77), .b(n60), .O(n78));
  inv1  g066(.a(n65), .O(n79));
  nor2  g067(.a(n79), .b(n29), .O(n80));
  inv1  g068(.a(n67), .O(n81));
  nor2  g069(.a(n81), .b(n32), .O(n82));
  nor2  g070(.a(n82), .b(n80), .O(n83));
  nor2  g071(.a(n52), .b(n24), .O(n84));
  nand2 g072(.a(i_7_), .b(n24), .O(n85));
  nor2  g073(.a(n85), .b(n23), .O(n86));
  nor2  g074(.a(n86), .b(n84), .O(n87));
  nor2  g075(.a(n87), .b(n83), .O(n88));
  inv1  g076(.a(n17), .O(n89));
  nor2  g077(.a(n20), .b(n18), .O(n90));
  nand2 g078(.a(n90), .b(n28), .O(n91));
  nor2  g079(.a(i_6_), .b(i_5_), .O(n92));
  nand2 g080(.a(n92), .b(n31), .O(n93));
  nand2 g081(.a(n93), .b(n91), .O(n94));
  nand2 g082(.a(n94), .b(i_0_), .O(n95));
  inv1  g083(.a(n38), .O(n96));
  nand2 g084(.a(n18), .b(n24), .O(n97));
  nor2  g085(.a(n97), .b(i_6_), .O(n98));
  nand2 g086(.a(n98), .b(n96), .O(n99));
  nand2 g087(.a(n99), .b(n95), .O(n100));
  nand2 g088(.a(n100), .b(n89), .O(n101));
  inv1  g089(.a(n90), .O(n102));
  nor2  g090(.a(n102), .b(n79), .O(n103));
  inv1  g091(.a(n92), .O(n104));
  nor2  g092(.a(n104), .b(n81), .O(n105));
  nor2  g093(.a(n105), .b(n103), .O(n106));
  nor2  g094(.a(n106), .b(n63), .O(n107));
  nand2 g095(.a(n90), .b(n67), .O(n108));
  nand2 g096(.a(n92), .b(n65), .O(n109));
  nand2 g097(.a(n109), .b(n108), .O(n110));
  nand2 g098(.a(n110), .b(n45), .O(n111));
  nor2  g099(.a(i_3_), .b(i_0_), .O(n112));
  nand2 g100(.a(n112), .b(n13), .O(n113));
  nor2  g101(.a(n113), .b(n38), .O(n114));
  nand2 g102(.a(n114), .b(n51), .O(n115));
  nand2 g103(.a(n115), .b(n111), .O(n116));
  nor2  g104(.a(n116), .b(n107), .O(n117));
  nand2 g105(.a(n117), .b(n101), .O(n118));
  nor2  g106(.a(n118), .b(n88), .O(n119));
  nand2 g107(.a(n119), .b(n78), .O(n120));
  nor2  g108(.a(n120), .b(n55), .O(n121));
  nor2  g109(.a(n83), .b(n24), .O(n122));
  nor2  g110(.a(n17), .b(i_2_), .O(n123));
  nor2  g111(.a(n81), .b(n27), .O(n124));
  nor2  g112(.a(n124), .b(n123), .O(n125));
  nor2  g113(.a(n125), .b(i_1_), .O(n126));
  inv1  g114(.a(n37), .O(n127));
  nor2  g115(.a(n81), .b(n127), .O(n128));
  nor2  g116(.a(n128), .b(n126), .O(n129));
  nor2  g117(.a(n129), .b(i_0_), .O(n130));
  nor2  g118(.a(n130), .b(n122), .O(n131));
  nor2  g119(.a(n131), .b(n23), .O(n132));
  nor2  g120(.a(n81), .b(n63), .O(n133));
  nand2 g121(.a(n65), .b(n27), .O(n134));
  nor2  g122(.a(n134), .b(n44), .O(n135));
  nor2  g123(.a(n135), .b(n133), .O(n136));
  nor2  g124(.a(n136), .b(i_7_), .O(n137));
  nor2  g125(.a(n15), .b(n24), .O(n138));
  nor2  g126(.a(n25), .b(n13), .O(n139));
  nand2 g127(.a(n139), .b(n138), .O(n140));
  nor2  g128(.a(n140), .b(n38), .O(n141));
  nor2  g129(.a(n141), .b(n137), .O(n142));
  nor2  g130(.a(n142), .b(n23), .O(n143));
  nor2  g131(.a(n143), .b(n132), .O(n144));
  nand2 g132(.a(n144), .b(n121), .O(o_0_));
  nor2  g133(.a(n49), .b(n47), .O(n146));
  inv1  g134(.a(n146), .O(n147));
  nor2  g135(.a(n18), .b(n13), .O(n148));
  inv1  g136(.a(n148), .O(n149));
  nor2  g137(.a(i_5_), .b(i_4_), .O(n150));
  inv1  g138(.a(n150), .O(n151));
  nand2 g139(.a(n151), .b(n149), .O(n152));
  inv1  g140(.a(n152), .O(n153));
  nand2 g141(.a(i_3_), .b(i_2_), .O(n154));
  nor2  g142(.a(i_3_), .b(i_2_), .O(n155));
  inv1  g143(.a(n155), .O(n156));
  nand2 g144(.a(n156), .b(n154), .O(n157));
  inv1  g145(.a(n157), .O(n158));
  nand2 g146(.a(i_1_), .b(i_0_), .O(n159));
  nand2 g147(.a(n159), .b(n44), .O(n160));
  inv1  g148(.a(n160), .O(n161));
  nor2  g149(.a(n161), .b(n158), .O(n162));
  nor2  g150(.a(n160), .b(n157), .O(n163));
  nor2  g151(.a(n163), .b(n162), .O(n164));
  nor2  g152(.a(n164), .b(n153), .O(n165));
  nand2 g153(.a(n164), .b(n153), .O(n166));
  inv1  g154(.a(n166), .O(n167));
  nor2  g155(.a(n167), .b(n165), .O(n168));
  inv1  g156(.a(n168), .O(n169));
  nand2 g157(.a(n169), .b(n147), .O(n170));
  nand2 g158(.a(n168), .b(n146), .O(n171));
  nand2 g159(.a(n171), .b(n170), .O(o_1_));
  nand2 g160(.a(n148), .b(n56), .O(n173));
  inv1  g161(.a(n154), .O(n174));
  inv1  g162(.a(n159), .O(n175));
  nand2 g163(.a(n175), .b(n174), .O(n176));
  nor2  g164(.a(n176), .b(n173), .O(o_2_));
  inv1  g165(.a(n48), .O(n178));
  nand2 g166(.a(n67), .b(n178), .O(n179));
  inv1  g167(.a(n50), .O(n180));
  nand2 g168(.a(n65), .b(n180), .O(n181));
  nand2 g169(.a(n181), .b(n179), .O(n182));
  nand2 g170(.a(n182), .b(i_2_), .O(n183));
  nand2 g171(.a(n22), .b(n13), .O(n184));
  nand2 g172(.a(n92), .b(i_4_), .O(n185));
  nand2 g173(.a(n185), .b(n184), .O(n186));
  nand2 g174(.a(n186), .b(n15), .O(n187));
  nand2 g175(.a(n92), .b(n16), .O(n188));
  nand2 g176(.a(n188), .b(n187), .O(n189));
  nor2  g177(.a(n25), .b(i_2_), .O(n190));
  nand2 g178(.a(n190), .b(n189), .O(n191));
  nand2 g179(.a(n191), .b(n183), .O(n192));
  nand2 g180(.a(n192), .b(i_1_), .O(n193));
  nand2 g181(.a(n89), .b(i_2_), .O(n194));
  nand2 g182(.a(n194), .b(n134), .O(n195));
  nand2 g183(.a(n195), .b(n92), .O(n196));
  nor2  g184(.a(n125), .b(n23), .O(n197));
  nand2 g185(.a(n90), .b(n13), .O(n198));
  nor2  g186(.a(n198), .b(n156), .O(n199));
  nor2  g187(.a(n199), .b(n197), .O(n200));
  nand2 g188(.a(n200), .b(n196), .O(n201));
  nor2  g189(.a(n25), .b(i_1_), .O(n202));
  nand2 g190(.a(n202), .b(n201), .O(n203));
  nand2 g191(.a(n203), .b(n193), .O(n204));
  nand2 g192(.a(n204), .b(i_0_), .O(n205));
  nand2 g193(.a(n195), .b(i_1_), .O(n206));
  nand2 g194(.a(n65), .b(n36), .O(n207));
  nand2 g195(.a(n207), .b(n206), .O(n208));
  nand2 g196(.a(n208), .b(i_0_), .O(n209));
  nor2  g197(.a(n29), .b(n24), .O(n210));
  inv1  g198(.a(n210), .O(n211));
  inv1  g199(.a(n21), .O(n212));
  nand2 g200(.a(n65), .b(n212), .O(n213));
  nor2  g201(.a(n213), .b(n211), .O(n214));
  nor2  g202(.a(n26), .b(i_0_), .O(n215));
  nor2  g203(.a(n79), .b(n27), .O(n216));
  nand2 g204(.a(n216), .b(n215), .O(n217));
  nand2 g205(.a(n18), .b(i_4_), .O(n218));
  nor2  g206(.a(n218), .b(n15), .O(n219));
  nand2 g207(.a(n219), .b(n210), .O(n220));
  nand2 g208(.a(n220), .b(n217), .O(n221));
  nor2  g209(.a(n221), .b(n214), .O(n222));
  nand2 g210(.a(n222), .b(n209), .O(n223));
  inv1  g211(.a(n85), .O(n224));
  inv1  g212(.a(n69), .O(n225));
  nand2 g213(.a(n96), .b(n89), .O(n226));
  nand2 g214(.a(n226), .b(n225), .O(n227));
  nand2 g215(.a(n227), .b(n22), .O(n228));
  nor2  g216(.a(n102), .b(n32), .O(n229));
  nor2  g217(.a(n104), .b(n29), .O(n230));
  nor2  g218(.a(n230), .b(n229), .O(n231));
  nor2  g219(.a(n231), .b(n17), .O(n232));
  inv1  g220(.a(n110), .O(n233));
  nor2  g221(.a(n233), .b(n38), .O(n234));
  nor2  g222(.a(n234), .b(n232), .O(n235));
  nand2 g223(.a(n235), .b(n228), .O(n236));
  nand2 g224(.a(n236), .b(n224), .O(n237));
  nor2  g225(.a(n76), .b(n18), .O(n238));
  nand2 g226(.a(n174), .b(n43), .O(n239));
  nand2 g227(.a(n175), .b(n155), .O(n240));
  nand2 g228(.a(n240), .b(n239), .O(n241));
  nor2  g229(.a(n241), .b(n163), .O(n242));
  nor2  g230(.a(n242), .b(n152), .O(n243));
  nand2 g231(.a(n148), .b(n43), .O(n244));
  nand2 g232(.a(n175), .b(n150), .O(n245));
  nand2 g233(.a(n245), .b(n244), .O(n246));
  nand2 g234(.a(n246), .b(n158), .O(n247));
  nand2 g235(.a(n155), .b(n148), .O(n248));
  nand2 g236(.a(n174), .b(n150), .O(n249));
  nand2 g237(.a(n249), .b(n248), .O(n250));
  nand2 g238(.a(n250), .b(n161), .O(n251));
  nand2 g239(.a(n251), .b(n247), .O(n252));
  nor2  g240(.a(n252), .b(n243), .O(n253));
  nor2  g241(.a(n253), .b(n20), .O(n254));
  nor2  g242(.a(n254), .b(n238), .O(n255));
  nand2 g243(.a(n255), .b(n237), .O(n256));
  nor2  g244(.a(n256), .b(n223), .O(n257));
  nand2 g245(.a(n257), .b(n205), .O(o_3_));
endmodule


