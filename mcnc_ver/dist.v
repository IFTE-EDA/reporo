// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:20 2019

module source_pla  ( 
    v0, v1, v2, v3, v4, v5, v6, v7,
    v8.0 , v8.1 , v8.2 , v8.3 , v8.4   );
  input  v0, v1, v2, v3, v4, v5, v6, v7;
  output v8.0 , v8.1 , v8.2 , v8.3 , v8.4 ;
  wire n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27,
    n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41,
    n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55,
    n56, n57, n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n69,
    n70, n71, n72, n73, n74, n75, n76, n77, n78, n79, n80, n81, n83, n84,
    n85, n86, n87, n88, n89, n90, n91, n92, n93, n94, n95, n96, n97, n98,
    n99, n100, n101, n102, n103, n104, n105, n106, n107, n108, n109, n110,
    n111, n112, n113, n114, n115, n116, n117, n118, n119, n120, n121, n122,
    n123, n124, n125, n126, n127, n128, n129, n130, n131, n132, n133, n134,
    n135, n136, n137, n138, n139, n140, n141, n142, n143, n144, n145, n146,
    n147, n148, n149, n150, n151, n152, n153, n154, n155, n156, n157, n158,
    n159, n160, n161, n162, n163, n164, n165, n166, n167, n168, n169, n170,
    n171, n172, n173, n174, n176, n177, n178, n179, n180, n181, n182, n183,
    n184, n185, n186, n187, n188, n189, n190, n191, n192, n193, n194, n195,
    n196, n197, n198, n199, n200, n201, n202, n203, n204, n205, n206, n207,
    n208, n209, n210, n211, n212, n213, n214, n215, n216, n217, n218, n219,
    n220, n221, n222, n223, n224, n225, n226, n227, n228, n229, n230, n231,
    n232, n233, n234, n235, n236, n237, n238, n239, n240, n241, n242, n243,
    n244, n245, n246, n247, n248, n249, n250, n251, n252, n253, n254, n255,
    n256, n257, n258, n259, n260, n261, n262, n263, n264, n265, n266, n267,
    n268, n269, n270, n271, n272, n273, n274, n275, n276, n277, n278, n279,
    n280, n281, n282, n283, n284, n285, n286, n287, n288, n289, n290, n291,
    n292, n293, n294, n295, n296, n297, n298, n299, n300, n301, n302, n303,
    n304, n305, n306, n308, n309, n310, n311, n312, n313, n314, n315, n316,
    n317, n318, n319, n320, n321, n322, n323, n324, n325, n326, n327, n328,
    n329, n330, n331, n332, n333, n334, n335, n336, n337, n338, n339, n340,
    n341, n342, n343, n344, n345, n346, n347, n348, n349, n350, n351, n352,
    n353, n354, n355, n356, n357, n358, n359, n360, n361, n362, n363, n364,
    n365, n366, n367, n368, n369, n370, n371, n372, n373, n374, n375, n376,
    n377, n378, n379, n380, n381, n382, n383, n384, n385, n386, n387, n388,
    n389, n390, n391, n392, n393, n394, n395, n396, n397, n398, n399, n400,
    n401, n402, n403, n404, n405, n406, n407, n408, n409, n410, n411, n412,
    n413, n414, n415, n416, n417, n418, n419, n420, n421, n422, n423, n424,
    n425, n426, n427, n428, n429, n430, n431, n432, n433, n434, n435, n436,
    n437, n438, n439, n440, n441, n442, n443, n444, n446, n447, n448, n449,
    n450, n451, n452, n453, n454, n455, n456, n457, n458, n459, n460, n461,
    n462, n463, n464, n465, n466, n467, n468, n469, n470, n471, n472, n473,
    n474, n475, n476, n477, n478, n479, n480, n481, n482, n483, n484, n485,
    n486, n487, n488, n489, n490, n491, n492, n493, n494, n495, n496, n497,
    n498, n499, n500, n501, n502, n503, n504, n505, n506, n507, n508, n509,
    n510, n511, n512, n513, n514, n515, n516, n517, n518, n519, n520, n521,
    n522, n523, n524, n525, n526, n527, n528, n529, n530, n531, n532, n533,
    n534, n535, n536, n537, n538, n539, n540, n541, n542, n543, n544, n545,
    n546, n547, n548, n549, n550, n551, n552, n553, n554, n555, n556, n557,
    n558, n559, n560, n561, n562, n563, n564, n565, n566, n567, n568, n569,
    n570, n571, n572, n573, n574, n575, n576, n577, n578, n579, n580, n581,
    n582, n583, n584, n585, n586, n587, n588, n589, n590, n591, n592, n593,
    n594, n595, n596, n597, n598, n599, n600, n601, n602, n603;
  inv1  g000(.a(v6), .O(n14));
  inv1  g001(.a(v5), .O(n15));
  nor2  g002(.a(n15), .b(v4), .O(n16));
  nand2 g003(.a(n16), .b(v2), .O(n17));
  inv1  g004(.a(v2), .O(n18));
  inv1  g005(.a(v4), .O(n19));
  nor2  g006(.a(v5), .b(n19), .O(n20));
  nand2 g007(.a(n20), .b(n18), .O(n21));
  nand2 g008(.a(n21), .b(n17), .O(n22));
  nand2 g009(.a(n22), .b(v1), .O(n23));
  nor2  g010(.a(v2), .b(v1), .O(n24));
  nor2  g011(.a(n15), .b(n19), .O(n25));
  nand2 g012(.a(n25), .b(n24), .O(n26));
  nand2 g013(.a(n26), .b(n23), .O(n27));
  nand2 g014(.a(n27), .b(n14), .O(n28));
  inv1  g015(.a(v1), .O(n29));
  nand2 g016(.a(v2), .b(n29), .O(n30));
  inv1  g017(.a(n30), .O(n31));
  nor2  g018(.a(n14), .b(v5), .O(n32));
  inv1  g019(.a(n32), .O(n33));
  nor2  g020(.a(n33), .b(n19), .O(n34));
  nand2 g021(.a(n34), .b(n31), .O(n35));
  nand2 g022(.a(n35), .b(n28), .O(n36));
  nand2 g023(.a(n36), .b(v7), .O(n37));
  nand2 g024(.a(v2), .b(v1), .O(n38));
  inv1  g025(.a(n38), .O(n39));
  nor2  g026(.a(v7), .b(n15), .O(n40));
  inv1  g027(.a(n40), .O(n41));
  nor2  g028(.a(n41), .b(v4), .O(n42));
  nand2 g029(.a(n42), .b(n39), .O(n43));
  nand2 g030(.a(n43), .b(n37), .O(n44));
  nand2 g031(.a(n44), .b(v0), .O(n45));
  nor2  g032(.a(n38), .b(v0), .O(n46));
  inv1  g033(.a(n25), .O(n47));
  nor2  g034(.a(v7), .b(n14), .O(n48));
  inv1  g035(.a(n48), .O(n49));
  nor2  g036(.a(n49), .b(n47), .O(n50));
  nand2 g037(.a(n50), .b(n46), .O(n51));
  nand2 g038(.a(n51), .b(n45), .O(n52));
  nand2 g039(.a(n52), .b(v3), .O(n53));
  inv1  g040(.a(v0), .O(n54));
  nor2  g041(.a(v6), .b(v5), .O(n55));
  nor2  g042(.a(n55), .b(n29), .O(n56));
  nor2  g043(.a(n14), .b(n15), .O(n57));
  inv1  g044(.a(n57), .O(n58));
  nor2  g045(.a(n58), .b(v1), .O(n59));
  nor2  g046(.a(n59), .b(n56), .O(n60));
  nor2  g047(.a(n60), .b(n54), .O(n61));
  inv1  g048(.a(v7), .O(n62));
  nor2  g049(.a(n62), .b(n14), .O(n63));
  nor2  g050(.a(n29), .b(v0), .O(n64));
  nand2 g051(.a(n64), .b(v5), .O(n65));
  inv1  g052(.a(n65), .O(n66));
  nand2 g053(.a(n66), .b(n63), .O(n67));
  nor2  g054(.a(n15), .b(v1), .O(n68));
  nor2  g055(.a(v5), .b(n29), .O(n69));
  nor2  g056(.a(n69), .b(n68), .O(n70));
  nor2  g057(.a(n70), .b(v6), .O(n71));
  nor2  g058(.a(n18), .b(n54), .O(n72));
  nand2 g059(.a(n72), .b(n71), .O(n73));
  nand2 g060(.a(n73), .b(n67), .O(n74));
  nor2  g061(.a(n74), .b(n61), .O(n75));
  nor2  g062(.a(n75), .b(n19), .O(n76));
  nor2  g063(.a(n38), .b(n54), .O(n77));
  inv1  g064(.a(n77), .O(n78));
  nand2 g065(.a(n63), .b(n16), .O(n79));
  nor2  g066(.a(n79), .b(n78), .O(n80));
  nor2  g067(.a(n80), .b(n76), .O(n81));
  nand2 g068(.a(n81), .b(n53), .O(v8.0 ));
  nand2 g069(.a(n71), .b(v0), .O(n83));
  nand2 g070(.a(n64), .b(n57), .O(n84));
  nand2 g071(.a(n84), .b(n83), .O(n85));
  nand2 g072(.a(n85), .b(n18), .O(n86));
  nand2 g073(.a(n32), .b(v2), .O(n87));
  inv1  g074(.a(n87), .O(n88));
  nor2  g075(.a(v1), .b(n54), .O(n89));
  nand2 g076(.a(n89), .b(n88), .O(n90));
  nand2 g077(.a(n90), .b(n86), .O(n91));
  nand2 g078(.a(n91), .b(v3), .O(n92));
  nor2  g079(.a(n58), .b(v3), .O(n93));
  nand2 g080(.a(n93), .b(n64), .O(n94));
  nand2 g081(.a(n94), .b(n92), .O(n95));
  nand2 g082(.a(n95), .b(v4), .O(n96));
  inv1  g083(.a(v3), .O(n97));
  nand2 g084(.a(n19), .b(n97), .O(n98));
  nor2  g085(.a(n98), .b(n58), .O(n99));
  nand2 g086(.a(n99), .b(n77), .O(n100));
  nand2 g087(.a(n100), .b(n96), .O(n101));
  nand2 g088(.a(n101), .b(n62), .O(n102));
  nand2 g089(.a(n32), .b(v3), .O(n103));
  nor2  g090(.a(v6), .b(n15), .O(n104));
  nand2 g091(.a(n104), .b(n97), .O(n105));
  nand2 g092(.a(n105), .b(n103), .O(n106));
  inv1  g093(.a(n106), .O(n107));
  nor2  g094(.a(n107), .b(n29), .O(n108));
  nand2 g095(.a(v3), .b(n29), .O(n109));
  nor2  g096(.a(n109), .b(n58), .O(n110));
  nor2  g097(.a(n110), .b(n108), .O(n111));
  nor2  g098(.a(n111), .b(n18), .O(n112));
  inv1  g099(.a(n93), .O(n113));
  nor2  g100(.a(v2), .b(n29), .O(n114));
  inv1  g101(.a(n114), .O(n115));
  nor2  g102(.a(n115), .b(n113), .O(n116));
  nor2  g103(.a(n116), .b(n112), .O(n117));
  nor2  g104(.a(v4), .b(v0), .O(n118));
  nand2 g105(.a(n118), .b(v7), .O(n119));
  nor2  g106(.a(n119), .b(n117), .O(n120));
  nor2  g107(.a(v3), .b(n54), .O(n121));
  nand2 g108(.a(n121), .b(n22), .O(n122));
  nand2 g109(.a(v2), .b(n54), .O(n123));
  inv1  g110(.a(n123), .O(n124));
  inv1  g111(.a(n16), .O(n125));
  nor2  g112(.a(n125), .b(n97), .O(n126));
  nand2 g113(.a(n126), .b(n124), .O(n127));
  nand2 g114(.a(n127), .b(n122), .O(n128));
  nand2 g115(.a(n128), .b(n14), .O(n129));
  nand2 g116(.a(n57), .b(n19), .O(n130));
  inv1  g117(.a(n130), .O(n131));
  nor2  g118(.a(n97), .b(v2), .O(n132));
  inv1  g119(.a(n132), .O(n133));
  nor2  g120(.a(n133), .b(v0), .O(n134));
  nand2 g121(.a(n134), .b(n131), .O(n135));
  nand2 g122(.a(n135), .b(n129), .O(n136));
  nand2 g123(.a(n136), .b(v1), .O(n137));
  nor2  g124(.a(n14), .b(v4), .O(n138));
  nor2  g125(.a(v6), .b(n19), .O(n139));
  nor2  g126(.a(n139), .b(n138), .O(n140));
  nor2  g127(.a(n140), .b(n18), .O(n141));
  nand2 g128(.a(n139), .b(n18), .O(n142));
  inv1  g129(.a(n142), .O(n143));
  nor2  g130(.a(n143), .b(n141), .O(n144));
  nor2  g131(.a(n144), .b(n65), .O(n145));
  nand2 g132(.a(n104), .b(n18), .O(n146));
  nand2 g133(.a(n146), .b(n87), .O(n147));
  inv1  g134(.a(n121), .O(n148));
  nand2 g135(.a(v4), .b(n29), .O(n149));
  nor2  g136(.a(n149), .b(n148), .O(n150));
  nand2 g137(.a(n150), .b(n147), .O(n151));
  nor2  g138(.a(n125), .b(n29), .O(n152));
  inv1  g139(.a(n20), .O(n153));
  nor2  g140(.a(n153), .b(v1), .O(n154));
  nor2  g141(.a(n154), .b(n152), .O(n155));
  nor2  g142(.a(v2), .b(n54), .O(n156));
  inv1  g143(.a(n156), .O(n157));
  nor2  g144(.a(n157), .b(n155), .O(n158));
  nand2 g145(.a(v4), .b(n54), .O(n159));
  nand2 g146(.a(n19), .b(v0), .O(n160));
  nand2 g147(.a(n160), .b(n159), .O(n161));
  nor2  g148(.a(n15), .b(n29), .O(n162));
  inv1  g149(.a(n162), .O(n163));
  nand2 g150(.a(n163), .b(n161), .O(n164));
  nand2 g151(.a(n55), .b(v4), .O(n165));
  inv1  g152(.a(n165), .O(n166));
  nor2  g153(.a(n30), .b(n54), .O(n167));
  nand2 g154(.a(n167), .b(n166), .O(n168));
  nand2 g155(.a(n168), .b(n164), .O(n169));
  nor2  g156(.a(n169), .b(n158), .O(n170));
  nand2 g157(.a(n170), .b(n151), .O(n171));
  nor2  g158(.a(n171), .b(n145), .O(n172));
  nand2 g159(.a(n172), .b(n137), .O(n173));
  nor2  g160(.a(n173), .b(n120), .O(n174));
  nand2 g161(.a(n174), .b(n102), .O(v8.1 ));
  nor2  g162(.a(n62), .b(n19), .O(n176));
  nand2 g163(.a(n176), .b(n132), .O(n177));
  nor2  g164(.a(v3), .b(n18), .O(n178));
  nor2  g165(.a(v7), .b(v4), .O(n179));
  nand2 g166(.a(n179), .b(n178), .O(n180));
  nand2 g167(.a(n180), .b(n177), .O(n181));
  nand2 g168(.a(n181), .b(v0), .O(n182));
  nand2 g169(.a(n19), .b(v2), .O(n183));
  nand2 g170(.a(n97), .b(n54), .O(n184));
  nor2  g171(.a(n184), .b(v7), .O(n185));
  nand2 g172(.a(n185), .b(n183), .O(n186));
  nand2 g173(.a(n186), .b(n182), .O(n187));
  nand2 g174(.a(n187), .b(v5), .O(n188));
  nand2 g175(.a(n176), .b(n18), .O(n189));
  nand2 g176(.a(n179), .b(v2), .O(n190));
  nand2 g177(.a(n190), .b(n189), .O(n191));
  nand2 g178(.a(v3), .b(n54), .O(n192));
  nor2  g179(.a(n192), .b(v5), .O(n193));
  nand2 g180(.a(n193), .b(n191), .O(n194));
  nand2 g181(.a(n194), .b(n188), .O(n195));
  nand2 g182(.a(n195), .b(v6), .O(n196));
  nand2 g183(.a(v7), .b(v5), .O(n197));
  inv1  g184(.a(n197), .O(n198));
  nand2 g185(.a(n198), .b(v2), .O(n199));
  nand2 g186(.a(n62), .b(n15), .O(n200));
  inv1  g187(.a(n200), .O(n201));
  nand2 g188(.a(n201), .b(n18), .O(n202));
  nand2 g189(.a(n202), .b(n199), .O(n203));
  nand2 g190(.a(v3), .b(v0), .O(n204));
  nor2  g191(.a(n204), .b(n19), .O(n205));
  nand2 g192(.a(n205), .b(n203), .O(n206));
  inv1  g193(.a(n178), .O(n207));
  nor2  g194(.a(n207), .b(v0), .O(n208));
  nand2 g195(.a(n208), .b(n42), .O(n209));
  nand2 g196(.a(n209), .b(n206), .O(n210));
  nand2 g197(.a(n210), .b(n14), .O(n211));
  nand2 g198(.a(n211), .b(n196), .O(n212));
  nand2 g199(.a(n212), .b(v1), .O(n213));
  nor2  g200(.a(v4), .b(n29), .O(n214));
  nor2  g201(.a(n214), .b(v6), .O(n215));
  inv1  g202(.a(n138), .O(n216));
  nor2  g203(.a(n216), .b(v1), .O(n217));
  nor2  g204(.a(n217), .b(n215), .O(n218));
  nor2  g205(.a(n218), .b(n15), .O(n219));
  nand2 g206(.a(n15), .b(n19), .O(n220));
  nor2  g207(.a(n220), .b(n29), .O(n221));
  nor2  g208(.a(n221), .b(n219), .O(n222));
  nor2  g209(.a(n222), .b(v0), .O(n223));
  nand2 g210(.a(v5), .b(n54), .O(n224));
  inv1  g211(.a(n224), .O(n225));
  nor2  g212(.a(v5), .b(n54), .O(n226));
  nor2  g213(.a(n226), .b(n225), .O(n227));
  inv1  g214(.a(n227), .O(n228));
  nor2  g215(.a(n14), .b(n19), .O(n229));
  nand2 g216(.a(n229), .b(n29), .O(n230));
  nor2  g217(.a(v6), .b(v4), .O(n231));
  nand2 g218(.a(n231), .b(v1), .O(n232));
  nand2 g219(.a(n232), .b(n230), .O(n233));
  nand2 g220(.a(n233), .b(n228), .O(n234));
  inv1  g221(.a(n55), .O(n235));
  nor2  g222(.a(n160), .b(n29), .O(n236));
  nand2 g223(.a(n236), .b(n235), .O(n237));
  nand2 g224(.a(n237), .b(n234), .O(n238));
  nor2  g225(.a(n238), .b(n223), .O(n239));
  nor2  g226(.a(n239), .b(v2), .O(n240));
  nand2 g227(.a(n165), .b(n130), .O(n241));
  nand2 g228(.a(n241), .b(n29), .O(n242));
  nor2  g229(.a(n58), .b(n19), .O(n243));
  inv1  g230(.a(n243), .O(n244));
  nand2 g231(.a(n244), .b(n220), .O(n245));
  nand2 g232(.a(n245), .b(v1), .O(n246));
  nand2 g233(.a(n246), .b(n242), .O(n247));
  nand2 g234(.a(n247), .b(v0), .O(n248));
  inv1  g235(.a(n70), .O(n249));
  nand2 g236(.a(n140), .b(n249), .O(n250));
  nand2 g237(.a(n104), .b(v4), .O(n251));
  nand2 g238(.a(n251), .b(n250), .O(n252));
  nand2 g239(.a(n252), .b(n54), .O(n253));
  nand2 g240(.a(n253), .b(n248), .O(n254));
  nand2 g241(.a(n254), .b(v2), .O(n255));
  nor2  g242(.a(n62), .b(v5), .O(n256));
  inv1  g243(.a(n256), .O(n257));
  nor2  g244(.a(n257), .b(v3), .O(n258));
  nor2  g245(.a(n41), .b(n97), .O(n259));
  nor2  g246(.a(n259), .b(n258), .O(n260));
  nor2  g247(.a(n19), .b(v2), .O(n261));
  inv1  g248(.a(n261), .O(n262));
  nor2  g249(.a(n262), .b(n29), .O(n263));
  nor2  g250(.a(n183), .b(v1), .O(n264));
  nor2  g251(.a(n264), .b(n263), .O(n265));
  nor2  g252(.a(n14), .b(v0), .O(n266));
  inv1  g253(.a(n266), .O(n267));
  nor2  g254(.a(n267), .b(n265), .O(n268));
  inv1  g255(.a(n89), .O(n269));
  nor2  g256(.a(n142), .b(n269), .O(n270));
  nor2  g257(.a(n270), .b(n268), .O(n271));
  nor2  g258(.a(n271), .b(n260), .O(n272));
  nor2  g259(.a(n19), .b(v3), .O(n273));
  nand2 g260(.a(n273), .b(n32), .O(n274));
  nor2  g261(.a(v4), .b(n97), .O(n275));
  nand2 g262(.a(n275), .b(n104), .O(n276));
  nand2 g263(.a(n276), .b(n274), .O(n277));
  nand2 g264(.a(n277), .b(n29), .O(n278));
  nand2 g265(.a(n97), .b(v1), .O(n279));
  inv1  g266(.a(n279), .O(n280));
  inv1  g267(.a(n104), .O(n281));
  nor2  g268(.a(n281), .b(v4), .O(n282));
  nand2 g269(.a(n282), .b(n280), .O(n283));
  nand2 g270(.a(n283), .b(n278), .O(n284));
  nand2 g271(.a(n284), .b(n72), .O(n285));
  nor2  g272(.a(n15), .b(v3), .O(n286));
  nor2  g273(.a(v5), .b(n97), .O(n287));
  nor2  g274(.a(n287), .b(n286), .O(n288));
  nor2  g275(.a(n288), .b(v1), .O(n289));
  nand2 g276(.a(n15), .b(n97), .O(n290));
  nor2  g277(.a(n290), .b(n29), .O(n291));
  nor2  g278(.a(n291), .b(n289), .O(n292));
  nor2  g279(.a(n216), .b(n123), .O(n293));
  inv1  g280(.a(n139), .O(n294));
  nor2  g281(.a(n157), .b(n294), .O(n295));
  nor2  g282(.a(n295), .b(n293), .O(n296));
  nor2  g283(.a(n296), .b(n292), .O(n297));
  nand2 g284(.a(n48), .b(n20), .O(n298));
  nor2  g285(.a(n97), .b(n18), .O(n299));
  nand2 g286(.a(n299), .b(n89), .O(n300));
  nor2  g287(.a(n300), .b(n298), .O(n301));
  nor2  g288(.a(n301), .b(n297), .O(n302));
  nand2 g289(.a(n302), .b(n285), .O(n303));
  nor2  g290(.a(n303), .b(n272), .O(n304));
  nand2 g291(.a(n304), .b(n255), .O(n305));
  nor2  g292(.a(n305), .b(n240), .O(n306));
  nand2 g293(.a(n306), .b(n213), .O(v8.2 ));
  nand2 g294(.a(v7), .b(n14), .O(n308));
  nand2 g295(.a(n308), .b(n49), .O(n309));
  inv1  g296(.a(n263), .O(n310));
  nand2 g297(.a(n273), .b(v1), .O(n311));
  nand2 g298(.a(n275), .b(n29), .O(n312));
  nand2 g299(.a(n312), .b(n311), .O(n313));
  nand2 g300(.a(n313), .b(v2), .O(n314));
  nand2 g301(.a(n314), .b(n310), .O(n315));
  nand2 g302(.a(n315), .b(v5), .O(n316));
  inv1  g303(.a(n21), .O(n317));
  nand2 g304(.a(n317), .b(v1), .O(n318));
  nand2 g305(.a(n318), .b(n316), .O(n319));
  nand2 g306(.a(n319), .b(n309), .O(n320));
  nand2 g307(.a(n200), .b(n197), .O(n321));
  nand2 g308(.a(n321), .b(v2), .O(n322));
  nand2 g309(.a(n256), .b(n18), .O(n323));
  nand2 g310(.a(n323), .b(n322), .O(n324));
  nand2 g311(.a(n324), .b(v3), .O(n325));
  nor2  g312(.a(v3), .b(v2), .O(n326));
  nand2 g313(.a(n326), .b(n40), .O(n327));
  nand2 g314(.a(n327), .b(n325), .O(n328));
  nand2 g315(.a(n328), .b(v6), .O(n329));
  nand2 g316(.a(n62), .b(n14), .O(n330));
  nor2  g317(.a(n330), .b(n15), .O(n331));
  nand2 g318(.a(n331), .b(n178), .O(n332));
  nand2 g319(.a(n332), .b(n329), .O(n333));
  nand2 g320(.a(n333), .b(v1), .O(n334));
  nand2 g321(.a(n15), .b(n29), .O(n335));
  nor2  g322(.a(n335), .b(n49), .O(n336));
  nand2 g323(.a(n336), .b(n178), .O(n337));
  nand2 g324(.a(n337), .b(n334), .O(n338));
  nand2 g325(.a(n338), .b(n19), .O(n339));
  nand2 g326(.a(n339), .b(n320), .O(n340));
  nand2 g327(.a(n340), .b(n54), .O(n341));
  nand2 g328(.a(v7), .b(n18), .O(n342));
  nand2 g329(.a(n62), .b(v2), .O(n343));
  nand2 g330(.a(n343), .b(n342), .O(n344));
  inv1  g331(.a(n344), .O(n345));
  nor2  g332(.a(n279), .b(n125), .O(n346));
  nor2  g333(.a(n109), .b(n153), .O(n347));
  nor2  g334(.a(n347), .b(n346), .O(n348));
  nor2  g335(.a(n348), .b(n345), .O(n349));
  nor2  g336(.a(n257), .b(n207), .O(n350));
  nor2  g337(.a(n133), .b(n41), .O(n351));
  nor2  g338(.a(n351), .b(n350), .O(n352));
  nor2  g339(.a(n352), .b(n29), .O(n353));
  nor2  g340(.a(n15), .b(n18), .O(n354));
  nor2  g341(.a(v5), .b(v2), .O(n355));
  nor2  g342(.a(n355), .b(n354), .O(n356));
  nor2  g343(.a(n356), .b(n62), .O(n357));
  inv1  g344(.a(n357), .O(n358));
  nand2 g345(.a(n97), .b(n29), .O(n359));
  nor2  g346(.a(n359), .b(n358), .O(n360));
  nor2  g347(.a(n360), .b(n353), .O(n361));
  nor2  g348(.a(n361), .b(n19), .O(n362));
  nor2  g349(.a(n362), .b(n349), .O(n363));
  nor2  g350(.a(n363), .b(n14), .O(n364));
  nor2  g351(.a(n197), .b(v3), .O(n365));
  nor2  g352(.a(n200), .b(n97), .O(n366));
  nor2  g353(.a(n366), .b(n365), .O(n367));
  nor2  g354(.a(n367), .b(n29), .O(n368));
  inv1  g355(.a(n288), .O(n369));
  nand2 g356(.a(n62), .b(n29), .O(n370));
  nor2  g357(.a(n370), .b(n369), .O(n371));
  nor2  g358(.a(n371), .b(n368), .O(n372));
  nor2  g359(.a(n372), .b(v2), .O(n373));
  inv1  g360(.a(n259), .O(n374));
  nor2  g361(.a(n374), .b(n38), .O(n375));
  nor2  g362(.a(n375), .b(n373), .O(n376));
  nor2  g363(.a(n376), .b(n294), .O(n377));
  nor2  g364(.a(n377), .b(n364), .O(n378));
  nor2  g365(.a(n378), .b(n54), .O(n379));
  nand2 g366(.a(n183), .b(n142), .O(n380));
  nand2 g367(.a(n380), .b(n15), .O(n381));
  nand2 g368(.a(n261), .b(n57), .O(n382));
  nand2 g369(.a(n382), .b(n381), .O(n383));
  nand2 g370(.a(n383), .b(v1), .O(n384));
  nand2 g371(.a(n14), .b(v2), .O(n385));
  nand2 g372(.a(n57), .b(n18), .O(n386));
  nand2 g373(.a(n386), .b(n385), .O(n387));
  nand2 g374(.a(n387), .b(n19), .O(n388));
  nand2 g375(.a(n261), .b(n104), .O(n389));
  nand2 g376(.a(n389), .b(n388), .O(n390));
  nand2 g377(.a(n390), .b(n29), .O(n391));
  nand2 g378(.a(n391), .b(n384), .O(n392));
  nand2 g379(.a(n392), .b(n97), .O(n393));
  nand2 g380(.a(n55), .b(v2), .O(n394));
  nand2 g381(.a(n394), .b(n386), .O(n395));
  nand2 g382(.a(n395), .b(v1), .O(n396));
  nand2 g383(.a(v5), .b(n18), .O(n397));
  nand2 g384(.a(n397), .b(n87), .O(n398));
  nand2 g385(.a(n398), .b(n29), .O(n399));
  nand2 g386(.a(n399), .b(n396), .O(n400));
  nand2 g387(.a(n400), .b(n19), .O(n401));
  nand2 g388(.a(n243), .b(n31), .O(n402));
  nand2 g389(.a(n402), .b(n401), .O(n403));
  nand2 g390(.a(n403), .b(v3), .O(n404));
  nand2 g391(.a(n404), .b(n393), .O(n405));
  nand2 g392(.a(n405), .b(v0), .O(n406));
  nor2  g393(.a(n98), .b(n33), .O(n407));
  nand2 g394(.a(v4), .b(v3), .O(n408));
  nor2  g395(.a(n408), .b(n281), .O(n409));
  nor2  g396(.a(n409), .b(n407), .O(n410));
  nor2  g397(.a(n410), .b(n29), .O(n411));
  nor2  g398(.a(n244), .b(n109), .O(n412));
  nor2  g399(.a(n412), .b(n411), .O(n413));
  nor2  g400(.a(n413), .b(n18), .O(n414));
  nor2  g401(.a(n30), .b(v5), .O(n415));
  nand2 g402(.a(n415), .b(n140), .O(n416));
  nand2 g403(.a(n282), .b(n114), .O(n417));
  nand2 g404(.a(n417), .b(n416), .O(n418));
  nor2  g405(.a(n418), .b(n414), .O(n419));
  nor2  g406(.a(n419), .b(v0), .O(n420));
  nand2 g407(.a(v6), .b(n97), .O(n421));
  nand2 g408(.a(n14), .b(v3), .O(n422));
  nand2 g409(.a(n422), .b(n421), .O(n423));
  nand2 g410(.a(n423), .b(n72), .O(n424));
  nand2 g411(.a(n266), .b(n18), .O(n425));
  nand2 g412(.a(n425), .b(n424), .O(n426));
  nand2 g413(.a(n426), .b(n15), .O(n427));
  inv1  g414(.a(n299), .O(n428));
  nor2  g415(.a(n224), .b(n14), .O(n429));
  nand2 g416(.a(n429), .b(n428), .O(n430));
  nand2 g417(.a(n430), .b(n427), .O(n431));
  nand2 g418(.a(n431), .b(n29), .O(n432));
  nand2 g419(.a(n106), .b(v0), .O(n433));
  nand2 g420(.a(n55), .b(n54), .O(n434));
  nand2 g421(.a(n434), .b(n433), .O(n435));
  nand2 g422(.a(n435), .b(v2), .O(n436));
  nor2  g423(.a(n157), .b(n97), .O(n437));
  nand2 g424(.a(n437), .b(n104), .O(n438));
  nand2 g425(.a(n438), .b(n436), .O(n439));
  nand2 g426(.a(n439), .b(v1), .O(n440));
  nand2 g427(.a(n440), .b(n432), .O(n441));
  nor2  g428(.a(n441), .b(n420), .O(n442));
  nand2 g429(.a(n442), .b(n406), .O(n443));
  nor2  g430(.a(n443), .b(n379), .O(n444));
  nand2 g431(.a(n444), .b(n341), .O(v8.3 ));
  inv1  g432(.a(n204), .O(n446));
  inv1  g433(.a(n321), .O(n447));
  nand2 g434(.a(n447), .b(v1), .O(n448));
  nand2 g435(.a(n321), .b(n29), .O(n449));
  nand2 g436(.a(n449), .b(n448), .O(n450));
  nand2 g437(.a(n450), .b(v4), .O(n451));
  nor2  g438(.a(v4), .b(v1), .O(n452));
  nand2 g439(.a(n452), .b(n40), .O(n453));
  nand2 g440(.a(n453), .b(n451), .O(n454));
  nand2 g441(.a(n454), .b(n18), .O(n455));
  nand2 g442(.a(n153), .b(n125), .O(n456));
  nor2  g443(.a(n30), .b(v7), .O(n457));
  nand2 g444(.a(n457), .b(n456), .O(n458));
  nand2 g445(.a(n458), .b(n455), .O(n459));
  nand2 g446(.a(n459), .b(v6), .O(n460));
  nor2  g447(.a(n330), .b(v5), .O(n461));
  nand2 g448(.a(n461), .b(n263), .O(n462));
  nand2 g449(.a(n462), .b(n460), .O(n463));
  nand2 g450(.a(n463), .b(n446), .O(n464));
  nand2 g451(.a(n357), .b(v1), .O(n465));
  nand2 g452(.a(n201), .b(n31), .O(n466));
  nand2 g453(.a(n466), .b(n465), .O(n467));
  nand2 g454(.a(n467), .b(n118), .O(n468));
  nor2  g455(.a(n200), .b(n19), .O(n469));
  nand2 g456(.a(n469), .b(n77), .O(n470));
  nand2 g457(.a(n470), .b(n468), .O(n471));
  nand2 g458(.a(n471), .b(n423), .O(n472));
  nor2  g459(.a(n15), .b(n54), .O(n473));
  inv1  g460(.a(n473), .O(n474));
  nor2  g461(.a(n474), .b(n423), .O(n475));
  nor2  g462(.a(n192), .b(n235), .O(n476));
  nor2  g463(.a(n476), .b(n475), .O(n477));
  nand2 g464(.a(v7), .b(n19), .O(n478));
  nor2  g465(.a(n478), .b(n477), .O(n479));
  inv1  g466(.a(n461), .O(n480));
  inv1  g467(.a(n408), .O(n481));
  nand2 g468(.a(n481), .b(n54), .O(n482));
  nor2  g469(.a(n482), .b(n480), .O(n483));
  nor2  g470(.a(n483), .b(n479), .O(n484));
  nor2  g471(.a(n484), .b(n18), .O(n485));
  nand2 g472(.a(v7), .b(n29), .O(n486));
  nand2 g473(.a(n179), .b(v1), .O(n487));
  nand2 g474(.a(n487), .b(n486), .O(n488));
  nand2 g475(.a(n488), .b(n18), .O(n489));
  inv1  g476(.a(n176), .O(n490));
  inv1  g477(.a(n179), .O(n491));
  nand2 g478(.a(n491), .b(n490), .O(n492));
  nand2 g479(.a(n492), .b(n39), .O(n493));
  nand2 g480(.a(n493), .b(n489), .O(n494));
  inv1  g481(.a(n193), .O(n495));
  nand2 g482(.a(n286), .b(v0), .O(n496));
  nand2 g483(.a(n496), .b(n495), .O(n497));
  nand2 g484(.a(n497), .b(n494), .O(n498));
  nor2  g485(.a(n326), .b(n299), .O(n499));
  nand2 g486(.a(n162), .b(v6), .O(n500));
  nor2  g487(.a(n500), .b(n499), .O(n501));
  nand2 g488(.a(n24), .b(v3), .O(n502));
  nor2  g489(.a(n502), .b(n235), .O(n503));
  nor2  g490(.a(n503), .b(n501), .O(n504));
  nor2  g491(.a(n490), .b(n54), .O(n505));
  nor2  g492(.a(n491), .b(v0), .O(n506));
  nor2  g493(.a(n506), .b(n505), .O(n507));
  nor2  g494(.a(n507), .b(n504), .O(n508));
  nand2 g495(.a(n456), .b(n97), .O(n509));
  nand2 g496(.a(n25), .b(v3), .O(n510));
  nand2 g497(.a(n510), .b(n509), .O(n511));
  nand2 g498(.a(n29), .b(n54), .O(n512));
  nor2  g499(.a(n512), .b(n62), .O(n513));
  nand2 g500(.a(n513), .b(n511), .O(n514));
  nor2  g501(.a(n184), .b(n47), .O(n515));
  nor2  g502(.a(n220), .b(n204), .O(n516));
  nor2  g503(.a(n516), .b(n515), .O(n517));
  nand2 g504(.a(n62), .b(v1), .O(n518));
  nor2  g505(.a(n518), .b(n517), .O(n519));
  inv1  g506(.a(n437), .O(n520));
  nor2  g507(.a(n520), .b(n79), .O(n521));
  nor2  g508(.a(n521), .b(n519), .O(n522));
  nand2 g509(.a(n522), .b(n514), .O(n523));
  nor2  g510(.a(n523), .b(n508), .O(n524));
  nand2 g511(.a(n524), .b(n498), .O(n525));
  nor2  g512(.a(n525), .b(n485), .O(n526));
  nand2 g513(.a(n526), .b(n472), .O(n527));
  nor2  g514(.a(n19), .b(n18), .O(n528));
  nor2  g515(.a(v4), .b(v2), .O(n529));
  nor2  g516(.a(n529), .b(n528), .O(n530));
  nor2  g517(.a(n530), .b(n29), .O(n531));
  nand2 g518(.a(n529), .b(n29), .O(n532));
  inv1  g519(.a(n532), .O(n533));
  nor2  g520(.a(n533), .b(n531), .O(n534));
  nor2  g521(.a(n534), .b(n62), .O(n535));
  nor2  g522(.a(n491), .b(n30), .O(n536));
  nor2  g523(.a(n536), .b(n535), .O(n537));
  nor2  g524(.a(n537), .b(n97), .O(n538));
  nand2 g525(.a(n31), .b(n97), .O(n539));
  nor2  g526(.a(n539), .b(n490), .O(n540));
  nor2  g527(.a(n540), .b(n538), .O(n541));
  nor2  g528(.a(n541), .b(n227), .O(n542));
  nand2 g529(.a(n62), .b(n18), .O(n543));
  nor2  g530(.a(n543), .b(n29), .O(n544));
  nor2  g531(.a(n257), .b(n30), .O(n545));
  nor2  g532(.a(n545), .b(n544), .O(n546));
  nor2  g533(.a(n546), .b(v0), .O(n547));
  nor2  g534(.a(n344), .b(v1), .O(n548));
  nor2  g535(.a(n343), .b(n29), .O(n549));
  nor2  g536(.a(n549), .b(n548), .O(n550));
  nor2  g537(.a(n550), .b(n474), .O(n551));
  nor2  g538(.a(n551), .b(n547), .O(n552));
  nor2  g539(.a(n552), .b(n19), .O(n553));
  nand2 g540(.a(v7), .b(v2), .O(n554));
  nor2  g541(.a(n554), .b(n29), .O(n555));
  nor2  g542(.a(n555), .b(n548), .O(n556));
  inv1  g543(.a(n160), .O(n557));
  nand2 g544(.a(n557), .b(n15), .O(n558));
  nor2  g545(.a(n558), .b(n556), .O(n559));
  nor2  g546(.a(n559), .b(n553), .O(n560));
  nor2  g547(.a(n560), .b(n97), .O(n561));
  nor2  g548(.a(n561), .b(n542), .O(n562));
  inv1  g549(.a(n69), .O(n563));
  nor2  g550(.a(n344), .b(n563), .O(n564));
  inv1  g551(.a(n24), .O(n565));
  nor2  g552(.a(n197), .b(n565), .O(n566));
  nor2  g553(.a(n566), .b(n564), .O(n567));
  nor2  g554(.a(n567), .b(n19), .O(n568));
  nor2  g555(.a(n532), .b(n257), .O(n569));
  nor2  g556(.a(n569), .b(n568), .O(n570));
  nor2  g557(.a(n570), .b(v0), .O(n571));
  inv1  g558(.a(n469), .O(n572));
  nor2  g559(.a(n565), .b(n54), .O(n573));
  inv1  g560(.a(n573), .O(n574));
  nor2  g561(.a(n574), .b(n572), .O(n575));
  nor2  g562(.a(n575), .b(n571), .O(n576));
  nor2  g563(.a(n576), .b(v3), .O(n577));
  inv1  g564(.a(n63), .O(n578));
  nor2  g565(.a(n578), .b(v5), .O(n579));
  nor2  g566(.a(n579), .b(n331), .O(n580));
  nor2  g567(.a(n573), .b(n46), .O(n581));
  nor2  g568(.a(n581), .b(n580), .O(n582));
  nor2  g569(.a(n342), .b(n29), .O(n583));
  nor2  g570(.a(n583), .b(n457), .O(n584));
  nand2 g571(.a(n473), .b(n14), .O(n585));
  nor2  g572(.a(n585), .b(n584), .O(n586));
  nor2  g573(.a(n586), .b(n582), .O(n587));
  nor2  g574(.a(n587), .b(v4), .O(n588));
  nor2  g575(.a(n57), .b(n55), .O(n589));
  nor2  g576(.a(n589), .b(n370), .O(n590));
  nor2  g577(.a(n308), .b(n563), .O(n591));
  nor2  g578(.a(n591), .b(n590), .O(n592));
  nor2  g579(.a(n592), .b(n18), .O(n593));
  nand2 g580(.a(n114), .b(n14), .O(n594));
  nor2  g581(.a(n594), .b(n321), .O(n595));
  nor2  g582(.a(n595), .b(n593), .O(n596));
  nand2 g583(.a(v4), .b(v0), .O(n597));
  nor2  g584(.a(n597), .b(n596), .O(n598));
  nor2  g585(.a(n598), .b(n588), .O(n599));
  nor2  g586(.a(n599), .b(v3), .O(n600));
  nor2  g587(.a(n600), .b(n577), .O(n601));
  nand2 g588(.a(n601), .b(n562), .O(n602));
  nor2  g589(.a(n602), .b(n527), .O(n603));
  nand2 g590(.a(n603), .b(n464), .O(v8.4 ));
endmodule


