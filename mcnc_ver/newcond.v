// Benchmark "source.pla" written by ABC on Tue Nov  5 15:01:25 2019

module source_pla  ( 
    cAin0, cselaluSUM, caluZout, cBIprocessedMSB, cAIprocessedMSB, cALUmsb,
    cDST1s<0> , cDST1s<1> , cDST1s<2> , cDST1s<3> , cDST1s<4> ,
    pCONDvalid, aluVout  );
  input  cAin0, cselaluSUM, caluZout, cBIprocessedMSB, cAIprocessedMSB,
    cALUmsb, cDST1s<0> , cDST1s<1> , cDST1s<2> , cDST1s<3> ,
    cDST1s<4> ;
  output pCONDvalid, aluVout;
  wire n14, n15, n16, n17, n18, n19, n20, n21, n22, n23, n24, n25, n26, n27,
    n28, n29, n30, n31, n32, n33, n34, n35, n36, n37, n38, n39, n40, n41,
    n42, n43, n44, n45, n46, n47, n48, n49, n50, n51, n52, n53, n54, n55,
    n56, n57, n58, n59, n60, n61, n62, n63, n64, n65, n66, n67, n68, n69,
    n70, n71, n72, n73, n74, n75, n76, n77, n78, n79, n80, n81, n82, n83,
    n84, n85, n86, n88, n89, n90, n91;
  inv1  g00(.a(cDST1s<4> ), .O(n14));
  inv1  g01(.a(cDST1s<0> ), .O(n15));
  inv1  g02(.a(cAIprocessedMSB), .O(n16));
  nand2 g03(.a(cALUmsb), .b(n16), .O(n17));
  inv1  g04(.a(cDST1s<3> ), .O(n18));
  inv1  g05(.a(cselaluSUM), .O(n19));
  nor2  g06(.a(n16), .b(n19), .O(n20));
  nand2 g07(.a(n20), .b(n18), .O(n21));
  nand2 g08(.a(n21), .b(n17), .O(n22));
  nand2 g09(.a(n22), .b(cBIprocessedMSB), .O(n23));
  inv1  g10(.a(cALUmsb), .O(n24));
  nor2  g11(.a(n24), .b(n16), .O(n25));
  nor2  g12(.a(n18), .b(cAIprocessedMSB), .O(n26));
  nor2  g13(.a(n26), .b(n25), .O(n27));
  nor2  g14(.a(n27), .b(cBIprocessedMSB), .O(n28));
  nand2 g15(.a(n18), .b(n24), .O(n29));
  nand2 g16(.a(n29), .b(n19), .O(n30));
  nand2 g17(.a(cDST1s<2> ), .b(caluZout), .O(n31));
  nand2 g18(.a(n31), .b(n30), .O(n32));
  nor2  g19(.a(n32), .b(n28), .O(n33));
  nand2 g20(.a(n33), .b(n23), .O(n34));
  nand2 g21(.a(n34), .b(cDST1s<1> ), .O(n35));
  inv1  g22(.a(n31), .O(n36));
  nand2 g23(.a(n36), .b(n18), .O(n37));
  nand2 g24(.a(n37), .b(n35), .O(n38));
  nand2 g25(.a(n38), .b(n15), .O(n39));
  nand2 g26(.a(n24), .b(n16), .O(n40));
  nand2 g27(.a(cDST1s<3> ), .b(cAIprocessedMSB), .O(n41));
  nand2 g28(.a(n41), .b(n40), .O(n42));
  nand2 g29(.a(n42), .b(cBIprocessedMSB), .O(n43));
  inv1  g30(.a(cBIprocessedMSB), .O(n44));
  nor2  g31(.a(cALUmsb), .b(n16), .O(n45));
  nand2 g32(.a(n45), .b(n44), .O(n46));
  nand2 g33(.a(n46), .b(n43), .O(n47));
  nand2 g34(.a(n47), .b(cDST1s<1> ), .O(n48));
  nor2  g35(.a(cAIprocessedMSB), .b(cBIprocessedMSB), .O(n49));
  nand2 g36(.a(n49), .b(n18), .O(n50));
  nand2 g37(.a(n50), .b(n48), .O(n51));
  nand2 g38(.a(n51), .b(cselaluSUM), .O(n52));
  nand2 g39(.a(n24), .b(n19), .O(n53));
  nand2 g40(.a(n53), .b(cDST1s<1> ), .O(n54));
  nand2 g41(.a(n54), .b(n18), .O(n55));
  nand2 g42(.a(n55), .b(n52), .O(n56));
  nor2  g43(.a(n36), .b(n15), .O(n57));
  nand2 g44(.a(n57), .b(n56), .O(n58));
  nand2 g45(.a(n58), .b(n39), .O(n59));
  nand2 g46(.a(n59), .b(n14), .O(n60));
  nand2 g47(.a(cAIprocessedMSB), .b(n44), .O(n61));
  nand2 g48(.a(n16), .b(cBIprocessedMSB), .O(n62));
  nand2 g49(.a(n62), .b(n61), .O(n63));
  nor2  g50(.a(caluZout), .b(n19), .O(n64));
  nor2  g51(.a(n15), .b(cALUmsb), .O(n65));
  nand2 g52(.a(n65), .b(n64), .O(n66));
  nor2  g53(.a(n24), .b(cAin0), .O(n67));
  nand2 g54(.a(n67), .b(n15), .O(n68));
  nand2 g55(.a(n68), .b(n66), .O(n69));
  nand2 g56(.a(n69), .b(n63), .O(n70));
  inv1  g57(.a(cAin0), .O(n71));
  nor2  g58(.a(n16), .b(n44), .O(n72));
  nand2 g59(.a(n72), .b(n64), .O(n73));
  nand2 g60(.a(n73), .b(n71), .O(n74));
  nand2 g61(.a(n74), .b(cDST1s<0> ), .O(n75));
  inv1  g62(.a(n49), .O(n76));
  nand2 g63(.a(n64), .b(n76), .O(n77));
  nor2  g64(.a(cDST1s<0> ), .b(cAin0), .O(n78));
  nand2 g65(.a(n78), .b(n77), .O(n79));
  nand2 g66(.a(n79), .b(n75), .O(n80));
  nand2 g67(.a(n80), .b(cDST1s<4> ), .O(n81));
  nand2 g68(.a(n81), .b(n70), .O(n82));
  inv1  g69(.a(cDST1s<2> ), .O(n83));
  nand2 g70(.a(n83), .b(cDST1s<1> ), .O(n84));
  nor2  g71(.a(n84), .b(cDST1s<3> ), .O(n85));
  nand2 g72(.a(n85), .b(n82), .O(n86));
  nand2 g73(.a(n86), .b(n60), .O(pCONDvalid));
  nor2  g74(.a(n17), .b(cBIprocessedMSB), .O(n88));
  inv1  g75(.a(n45), .O(n89));
  nor2  g76(.a(n89), .b(n44), .O(n90));
  nor2  g77(.a(n90), .b(n88), .O(n91));
  nor2  g78(.a(n91), .b(n19), .O(aluVout));
endmodule


